package com.ucspl;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.ucspl.model.TrialLombok;

import lombok.extern.log4j.Log4j;

@SpringBootApplication
@Log4j
public class UcsplProjectApplication{
	
	private static final Logger logger = Logger.getLogger(UcsplProjectApplication.class); 
	
	
	public static void main(String[] args){
		
		SpringApplication.run(UcsplProjectApplication.class, args);
		
		BasicConfigurator.configure();  
		//logger.trace("Trace Message!");
		//logger.debug("Debug Message!");
		log.info("Info Message using Lombok!");
		logger.info("Info Message!");
		logger.warn("Warn Message!");
		logger.error("Error Message!");
		logger.fatal("Fatal Message!");
		   		 
		TrialLombok t=new TrialLombok(1,"rani","pune");
		System.out.println(t);
		
}
	
}



