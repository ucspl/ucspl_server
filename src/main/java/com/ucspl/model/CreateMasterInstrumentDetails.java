package com.ucspl.model;

import javax.persistence.*;

@Entity
@Table(name="create_master_instrument_details_table")
public class CreateMasterInstrumentDetails {

	private long id;
	private long calibrationLabId;
	private String idNo;
	private String personHolding;
	private String masterName;
	private long instrumentTypeId;
	private String calibrationFrequency;
	private String srNo;
	private long rangeTypeId;
	private String purchasedFrom;
	private String make;
	private long masterTypeId;
	private String dateOfPurchase;
	private String model;
	private int readingOverloading;
	private String dateOfInstallation;
	private int lcUomChart;
	private int status;
	private long branchId;
	private String dateCreated;
	private String dateModified;
	private String createdBy;
	private String modifiedBy;
	private int draft;
	private int approved;
	private int archieved;
	private int submitted;
	private int rejected;
	
	public CreateMasterInstrumentDetails() {
		
	}
		
	public CreateMasterInstrumentDetails(long id, long calibrationLabId, String idNo, String personHolding,
			String masterName, long instrumentTypeId, String calibrationFrequency, String srNo, long rangeTypeId,
			String purchasedFrom, String make, long masterTypeId, String dateOfPurchase, String model,
			int readingOverloading, String dateOfInstallation, int lcUomChart, int status, long branchId,
			String dateCreated, String dateModified, String createdBy, String modifiedBy, int draft, int approved,
			int archieved, int submitted, int rejected) {
		super();
		this.id = id;
		this.calibrationLabId = calibrationLabId;
		this.idNo = idNo;
		this.personHolding = personHolding;
		this.masterName = masterName;
		this.instrumentTypeId = instrumentTypeId;
		this.calibrationFrequency = calibrationFrequency;
		this.srNo = srNo;
		this.rangeTypeId = rangeTypeId;
		this.purchasedFrom = purchasedFrom;
		this.make = make;
		this.masterTypeId = masterTypeId;
		this.dateOfPurchase = dateOfPurchase;
		this.model = model;
		this.readingOverloading = readingOverloading;
		this.dateOfInstallation = dateOfInstallation;
		this.lcUomChart = lcUomChart;
		this.status = status;
		this.branchId = branchId;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
		this.createdBy = createdBy;
		this.modifiedBy = modifiedBy;
		this.draft = draft;
		this.approved = approved;
		this.archieved = archieved;
		this.submitted = submitted;
		this.rejected = rejected;
	}



	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "createmasterinstru_id", nullable = false)
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "calibrationlab_id")
	public long getCalibrationLabId() {
		return calibrationLabId;
	}
	public void setCalibrationLabId(long calibrationLabId) {
		this.calibrationLabId = calibrationLabId;
	}

	@Column(name = "idno")
	public String getIdNo() {
		return idNo;
	}
	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}

	@Column(name = "person_holding")
	public String getPersonHolding() {
		return personHolding;
	}
	public void setPersonHolding(String personHolding) {
		this.personHolding = personHolding;
	}

	@Column(name = "master_name", nullable = false)
	public String getMasterName() {
		return masterName;
	}
	public void setMasterName(String masterName) {
		this.masterName = masterName;
	}

	@Column(name = "instrument_type_id")
	public long getInstrumentTypeId() {
		return instrumentTypeId;
	}
	public void setInstrumentTypeId(long instrumentTypeId) {
		this.instrumentTypeId = instrumentTypeId;
	}

	@Column(name = "calibration_frequency")
	public String getCalibrationFrequency() {
		return calibrationFrequency;
	}
	public void setCalibrationFrequency(String calibrationFrequency) {
		this.calibrationFrequency = calibrationFrequency;
	}

	@Column(name = "srno")
	public String getSrNo() {
		return srNo;
	}
	public void setSrNo(String srNo) {
		this.srNo = srNo;
	}

	@Column(name = "range_type_id")
	public long getRangeTypeId() {
		return rangeTypeId;
	}
	public void setRangeTypeId(long rangeTypeId) {
		this.rangeTypeId = rangeTypeId;
	}

	@Column(name = "purchase_from")
	public String getPurchasedFrom() {
		return purchasedFrom;
	}
	public void setPurchasedFrom(String purchasedFrom) {
		this.purchasedFrom = purchasedFrom;
	}

	@Column(name = "make")
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}

	@Column(name = "master_type_id")
	public long getMasterTypeId() {
		return masterTypeId;
	}
	public void setMasterTypeId(long masterTypeId) {
		this.masterTypeId = masterTypeId;
	}

	@Column(name = "date_of_purchase")
	public String getDateOfPurchase() {
		return dateOfPurchase;
	}
	public void setDateOfPurchase(String dateOfPurchase) {
		this.dateOfPurchase = dateOfPurchase;
	}

	@Column(name = "model")
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}

	@Column(name = "reading_overloading")
	public int getReadingOverloading() {
		return readingOverloading;
	}
	public void setReadingOverloading(int readingOverloading) {
		this.readingOverloading = readingOverloading;
	}

	@Column(name = "date_of_installation")
	public String getDateOfInstallation() {
		return dateOfInstallation;
	}
	public void setDateOfInstallation(String dateOfInstallation) {
		this.dateOfInstallation = dateOfInstallation;
	}

	@Column(name = "lcuom_chart")
	public int getLcUomChart() {
		return lcUomChart;
	}
	public void setLcUomChart(int lcUomChart) {
		this.lcUomChart = lcUomChart;
	}

	@Column(name = "status")
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name = "branch_id")
	public long getBranchId() {
		return branchId;
	}
	public void setBranchId(long branchId) {
		this.branchId = branchId;
	}

//	@Column(name = "date_created")
//	public String getDateCreated() {
//		return dateCreated;
//	}
//	public void setDateCreated(String dateCreated) {
//		this.dateCreated = dateCreated;
//	}
//
//	@Column(name = "date_modified")
//	public String getDateModified() {
//		return dateModified;
//	}
//	public void setDateModified(String dateModified) {
//		this.dateModified = dateModified;
//	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "modified_by")
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public int getDraft() {
		return draft;
	}
	public void setDraft(int draft) {
		this.draft = draft;
	}

	public int getApproved() {
		return approved;
	}
	public void setApproved(int approved) {
		this.approved = approved;
	}

	public int getArchieved() {
		return archieved;
	}
	public void setArchieved(int archieved) {
		this.archieved = archieved;
	}

	public int getSubmitted() {
		return submitted;
	}
	public void setSubmitted(int submitted) {
		this.submitted = submitted;
	}

	public int getRejected() {
		return rejected;
	}
	public void setRejected(int rejected) {
		this.rejected = rejected;
	}
	
	
}
