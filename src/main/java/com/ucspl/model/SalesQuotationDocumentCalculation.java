package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sales_quotation_document_calculation_table")
public class SalesQuotationDocumentCalculation {

	private long id;
	private String documentNumber;
	private double subTotal;
	private double discountOnSubtotal;
	private double total;
	private double cgst;
	private double igst;
	private double sgst;
	private double net;

	public SalesQuotationDocumentCalculation() {
		// TODO Auto-generated constructor stub
	}

	public SalesQuotationDocumentCalculation(long id, String documentNumber, double subTotal, double discountOnSubtotal,
			double total, double cgst, double igst, double sgst, double net) {
		super();
		this.id = id;
		this.documentNumber = documentNumber;
		this.subTotal = subTotal;
		this.discountOnSubtotal = discountOnSubtotal;
		this.total = total;
		this.cgst = cgst;
		this.igst = igst;
		this.sgst = sgst;
		this.net = net;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sales_quot_doc_calculation_id", nullable = false)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "document_number", nullable = false)
	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	@Column(name = "subtotal", nullable = false)
	public double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}

	@Column(name = "discount_on_subtotal", nullable = false)
	public double getDiscountOnSubtotal() {
		return discountOnSubtotal;
	}

	public void setDiscountOnSubtotal(double discountOnSubtotal) {
		this.discountOnSubtotal = discountOnSubtotal;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public double getCgst() {
		return cgst;
	}

	public void setCgst(double cgst) {
		this.cgst = cgst;
	}

	public double getIgst() {
		return igst;
	}

	public void setIgst(double igst) {
		this.igst = igst;
	}

	public double getSgst() {
		return sgst;
	}

	public void setSgst(double sgst) {
		this.sgst = sgst;
	}

	public double getNet() {
		return net;
	}

	public void setNet(double net) {
		this.net = net;
	}

}
