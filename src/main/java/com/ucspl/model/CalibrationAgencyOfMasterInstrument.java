package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "calibration_agency_of_master_instrument_table")
public class CalibrationAgencyOfMasterInstrument {

	private long calibrationAgencyId;
	private String calibrationAgencyName;
	private String description;
	private long status;
	private String createdBy;
	private java.util.Date dateCreated;
	
	public CalibrationAgencyOfMasterInstrument() {
		
	}
	
	public CalibrationAgencyOfMasterInstrument(long calibrationAgencyId, String calibrationAgencyName,
			String description, long status, String createdBy, Date dateCreated) {
		super();
		this.calibrationAgencyId = calibrationAgencyId;
		this.calibrationAgencyName = calibrationAgencyName;
		this.description = description;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "calibration_agency_id", nullable = false)
	public long getCalibrationAgencyId() {
		return calibrationAgencyId;
	}

	public void setCalibrationAgencyId(long calibrationAgencyId) {
		this.calibrationAgencyId = calibrationAgencyId;
	}

	@Column(name = "calibration_agency_name")
	public String getCalibrationAgencyName() {
		return calibrationAgencyName;
	}

	public void setCalibrationAgencyName(String calibrationAgencyName) {
		this.calibrationAgencyName = calibrationAgencyName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getStatus() {
		return status;
	}

	public void setStatus(long status) {
		this.status = status;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "date_created")
	public java.util.Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(java.util.Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	

	
}
