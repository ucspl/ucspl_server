package com.ucspl.model;

import javax.persistence.*;

@Entity
@Table(name="create_master_instrument_cal_certificate_details_table")
public class CreateMasterInstruCalibrationCertificateDetails {

	private long mastInstruCalCertificateDetId;
	private long masterInstruId;
	private String calCertificate;
	private long calCertificateAgencyId;
	private String calibrationDate;
	private String calibrationDue;
	private String dateOfOutward;
	private String dateOfInward;
	private String calibrationCharges;
	private String calibrationData;
	private int draft;
	private int approved;
	private int archieved;
	private int submitted;
	private int rejected;
	
	public CreateMasterInstruCalibrationCertificateDetails() {
		
	}
	
	public CreateMasterInstruCalibrationCertificateDetails(long mastInstruCalCertificateDetId,
			long masterInstruId, String calCertificate, long calCertificateAgencyId, String calibrationDate,
			String calibrationDue, String dateOfOutward, String dateOfInward, String calibrationCharges,
			String calibrationData, int draft, int approved, int archieved, int submitted, int rejected) {
		super();
		this.mastInstruCalCertificateDetId = mastInstruCalCertificateDetId;
		this.masterInstruId = masterInstruId;
		this.calCertificate = calCertificate;
		this.calCertificateAgencyId = calCertificateAgencyId;
		this.calibrationDate = calibrationDate;
		this.calibrationDue = calibrationDue;
		this.dateOfOutward = dateOfOutward;
		this.dateOfInward = dateOfInward;
		this.calibrationCharges = calibrationCharges;
		this.calibrationData = calibrationData;
		this.draft = draft;
		this.approved = approved;
		this.archieved = archieved;
		this.submitted = submitted;
		this.rejected = rejected;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "masterinstru_calcertificate_id", nullable = false)
	public long getMastInstruCalCertificateDetId() {
		return mastInstruCalCertificateDetId;
	}

	public void setMastInstruCalCertificateDetId(long mastInstruCalCertificateDetId) {
		this.mastInstruCalCertificateDetId = mastInstruCalCertificateDetId;
	}
	
	@Column(name = "masterinstru_id")
	public long getMasterInstruId() {
		return masterInstruId;
	}

	public void setMasterInstruId(long masterInstruId) {
		this.masterInstruId = masterInstruId;
	}

	@Column(name = "cal_certificate")
	public String getCalCertificate() {
		return calCertificate;
	}

	public void setCalCertificate(String calCertificate) {
		this.calCertificate = calCertificate;
	}

	@Column(name = "cal_certificate_agency_id")
	public long getCalCertificateAgencyId() {
		return calCertificateAgencyId;
	}

	public void setCalCertificateAgencyId(long calCertificateAgencyId) {
		this.calCertificateAgencyId = calCertificateAgencyId;
	}

	@Column(name = "calibration_date")
	public String getCalibrationDate() {
		return calibrationDate;
	}

	public void setCalibrationDate(String calibrationDate) {
		this.calibrationDate = calibrationDate;
	}

	@Column(name = "calibration_due")
	public String getCalibrationDue() {
		return calibrationDue;
	}

	public void setCalibrationDue(String calibrationDue) {
		this.calibrationDue = calibrationDue;
	}

	@Column(name = "date_of_outward")
	public String getDateOfOutward() {
		return dateOfOutward;
	}

	public void setDateOfOutward(String dateOfOutward) {
		this.dateOfOutward = dateOfOutward;
	}

	@Column(name = "date_of_inward")
	public String getDateOfInward() {
		return dateOfInward;
	}

	public void setDateOfInward(String dateOfInward) {
		this.dateOfInward = dateOfInward;
	}

	@Column(name = "calibration_charges")
	public String getCalibrationCharges() {
		return calibrationCharges;
	}

	public void setCalibrationCharges(String calibrationCharges) {
		this.calibrationCharges = calibrationCharges;
	}

	@Column(name = "calibration_data")
	public String getCalibrationData() {
		return calibrationData;
	}

	public void setCalibrationData(String calibrationData) {
		this.calibrationData = calibrationData;
	}

	public int getDraft() {
		return draft;
	}

	public void setDraft(int draft) {
		this.draft = draft;
	}

	public int getApproved() {
		return approved;
	}

	public void setApproved(int approved) {
		this.approved = approved;
	}

	public int getArchieved() {
		return archieved;
	}

	public void setArchieved(int archieved) {
		this.archieved = archieved;
	}

	public int getSubmitted() {
		return submitted;
	}

	public void setSubmitted(int submitted) {
		this.submitted = submitted;
	}

	public int getRejected() {
		return rejected;
	}

	public void setRejected(int rejected) {
		this.rejected = rejected;
	}
	
	
}
