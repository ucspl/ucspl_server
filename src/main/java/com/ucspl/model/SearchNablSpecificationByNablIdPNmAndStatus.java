
package com.ucspl.model;

public class SearchNablSpecificationByNablIdPNmAndStatus {

	private String nablIdParaName;
	private int draft;
	private int archieved;
	private int submitted;
	private int rejected;
	private int approved;
	
	public SearchNablSpecificationByNablIdPNmAndStatus() {
		
	}
	

	public SearchNablSpecificationByNablIdPNmAndStatus(String nablIdParaName, int draft, int archieved, int submitted,
			int rejected, int approved) {
		super();
		this.nablIdParaName = nablIdParaName;
		this.draft = draft;
		this.archieved = archieved;
		this.submitted = submitted;
		this.rejected = rejected;
		this.approved = approved;
	}



	public String getNablIdParaName() {
		return nablIdParaName;
	}


	public void setNablIdParaName(String nablIdParaName) {
		this.nablIdParaName = nablIdParaName;
	}


	public int getDraft() {
		return draft;
	}

	public void setDraft(int draft) {
		this.draft = draft;
	}

	public int getArchieved() {
		return archieved;
	}

	public void setArchieved(int archieved) {
		this.archieved = archieved;
	}

	public int getSubmitted() {
		return submitted;
	}

	public void setSubmitted(int submitted) {
		this.submitted = submitted;
	}

	public int getRejected() {
		return rejected;
	}

	public void setRejected(int rejected) {
		this.rejected = rejected;
	}

	public int getApproved() {
		return approved;
	}

	public void setApproved(int approved) {
		this.approved = approved;
	}
	
	

}
