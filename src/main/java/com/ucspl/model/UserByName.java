package com.ucspl.model;

public class UserByName {

	private String name;
	private String firstName;
	private String lastName;
	private String userName;
	private long userNumber;
	private String role;
	private int status;
	private String department;

	public UserByName() {
		// TODO Auto-generated constructor stub
	}

	public UserByName(String name, int status, String department) {
		super();
		this.name = name;
		this.status = status;
		this.department = department;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	@Override
	public String toString() {
		return "UserbyName [name=" + name + ", firstName=" + firstName + ", lastName=" + lastName + ", userName="
				+ userName + ", userNumber=" + userNumber + ", role=" + role + ", status=" + status + ", department="
				+ department + "]";
	}

}