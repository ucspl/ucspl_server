package com.ucspl.model;

import javax.persistence.Column;

public class DocumentNumberForSalesAndQuotation {

	private String documentNumber;
	private long customerId;
	private long termsId;
	private long box1;
	private long box2;
	private String remark;
	

	public DocumentNumberForSalesAndQuotation() {
		// TODO Auto-generated constructor stub
	}

	
	public DocumentNumberForSalesAndQuotation(String documentNumber, long customerId, long termsId, long box1,
			long box2, String remark) {
		super();
		this.documentNumber = documentNumber;
		this.customerId = customerId;
		this.termsId = termsId;
		this.box1 = box1;
		this.box2 = box2;
		this.remark = remark;
	}


	@Column(name = "document_number", nullable = false)
	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	@Column(name = "customer_id", nullable = false)
	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	@Column(name = "terms_id", nullable = false)
	public long getTermsId() {
		return termsId;
	}

	public void setTermsId(long termsId) {
		this.termsId = termsId;
	}



	public long getBox1() {
		return box1;
	}



	public void setBox1(long box1) {
		this.box1 = box1;
	}



	public long getBox2() {
		return box2;
	}



	public void setBox2(long box2) {
		this.box2 = box2;
	}

	@Column(nullable = true)
	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}
	

}