package com.ucspl.model;

public class SearchMasterInstruByMastIdAndParameterId {
	
	private long createMasterInstruId;
	private long parameterId;
	
	public SearchMasterInstruByMastIdAndParameterId() {
		
	}
	
	public SearchMasterInstruByMastIdAndParameterId(long createMasterInstruId, long parameterId) {
		super();
		this.createMasterInstruId = createMasterInstruId;
		this.parameterId = parameterId;
	}

	public long getCreateMasterInstruId() {
		return createMasterInstruId;
	}

	public void setCreateMasterInstruId(long createMasterInstruId) {
		this.createMasterInstruId = createMasterInstruId;
	}

	public long getParameterId() {
		return parameterId;
	}

	public void setParameterId(long parameterId) {
		this.parameterId = parameterId;
	}
	
	

}
