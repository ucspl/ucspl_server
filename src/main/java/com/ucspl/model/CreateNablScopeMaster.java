package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "create_nabl_scope_master")
public class CreateNablScopeMaster {

	private long createNableScopeMasterId;
	private String  nablCertificateNo;
	private String  labLocation;
	private String  dateOfIssue;
	private String validUpTo;
	private int draft;
	private int approved;
	private int archieved;
	private int submitted;
	private int rejected;
	private String createdBy;
	private String dateCreated;
	private String dateModified;
	private String  systemCertificateNo;
	
	
	public CreateNablScopeMaster() {
		
	}


	public CreateNablScopeMaster(long createNableScopeMasterId, String nablCertificateNo, String labLocation,
			String dateOfIssue, String validUpTo, int draft, int approved, int archieved, int submitted, int rejected,
			String createdBy, String dateCreated, String dateModified, String systemCertificateNo) {
		super();
		this.createNableScopeMasterId = createNableScopeMasterId;
		this.nablCertificateNo = nablCertificateNo;
		this.labLocation = labLocation;
		this.dateOfIssue = dateOfIssue;
		this.validUpTo = validUpTo;
		this.draft = draft;
		this.approved = approved;
		this.archieved = archieved;
		this.submitted = submitted;
		this.rejected = rejected;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
		this.systemCertificateNo = systemCertificateNo;
	}


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "create_nabl_scope_master_id", nullable = false)
	public long getCreateNableScopeMasterId() {
		return createNableScopeMasterId;
	}


	public void setCreateNableScopeMasterId(long createNableScopeMasterId) {
		this.createNableScopeMasterId = createNableScopeMasterId;
	}

	@Column(name = "nabl_certificate_no")
	public String getNablCertificateNo() {
		return nablCertificateNo;
	}


	public void setNablCertificateNo(String nablCertificateNo) {
		this.nablCertificateNo = nablCertificateNo;
	}

	@Column(name = "lab_location")
	public String getLabLocation() {
		return labLocation;
	}


	public void setLabLocation(String labLocation) {
		this.labLocation = labLocation;
	}

	@Column(name = "date_of_issue")
	public String getDateOfIssue() {
		return dateOfIssue;
	}


	public void setDateOfIssue(String dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}


	@Column(name = "valid_up_to")
	public String getValidUpTo() {
		return validUpTo;
	}


	public void setValidUpTo(String validUpTo) {
		this.validUpTo = validUpTo;
	}


	public int getDraft() {
		return draft;
	}


	public void setDraft(int draft) {
		this.draft = draft;
	}


	public int getApproved() {
		return approved;
	}


	public void setApproved(int approved) {
		this.approved = approved;
	}


	public int getArchieved() {
		return archieved;
	}


	public void setArchieved(int archieved) {
		this.archieved = archieved;
	}


	public int getSubmitted() {
		return submitted;
	}


	public void setSubmitted(int submitted) {
		this.submitted = submitted;
	}


	public int getRejected() {
		return rejected;
	}


	public void setRejected(int rejected) {
		this.rejected = rejected;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}

	
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "system_certificate_number")
	public String getSystemCertificateNo() {
		return systemCertificateNo;
	}


	public void setSystemCertificateNo(String systemCertificateNo) {
		this.systemCertificateNo = systemCertificateNo;
	}
	
	

//	@Column(name = "date_created")
//	public String getDateCreated() {
//		return dateCreated;
//	}
//
//
//	public void setDateCreated(String dateCreated) {
//		this.dateCreated = dateCreated;
//	}
//
//
//
//
//	@Column(name = "date_modified")
//	public String getDateModified() {
//		return dateModified;
//	}
//
//
//
//
//	public void setDateModified(String dateModified) {
//		this.dateModified = dateModified;
//	}
//	
		
}

