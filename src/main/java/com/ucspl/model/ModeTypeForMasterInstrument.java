package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mode_type_of_master_instrument_table")
public class ModeTypeForMasterInstrument {
	
	private long modeId;
	private String modeName;
	private String description;
	private long status;
	private String createdBy;
	private java.util.Date dateCreated;
	
	public ModeTypeForMasterInstrument() {
		
	}

	public ModeTypeForMasterInstrument(long modeId, String modeName, String description, long status, String createdBy,
			Date dateCreated) {
		super();
		this.modeId = modeId;
		this.modeName = modeName;
		this.description = description;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "mode_type_id", nullable = false)
	public long getModeId() {
		return modeId;
	}

	public void setModeId(long modeId) {
		this.modeId = modeId;
	}

	@Column(name = "mode_type_name", nullable = false)
	public String getModeName() {
		return modeName;
	}

	public void setModeName(String modeName) {
		this.modeName = modeName;
	}
	
	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public long getStatus() {
		return status;
	}
  

	public void setStatus(long status) {
		this.status = status;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "date_created")
	public java.util.Date getDateCreated() {
		return dateCreated;
	}


	public void setDateCreated(java.util.Date dateCreated) {
		this.dateCreated = dateCreated;
	}

}
