
package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "group_table_for_nabl_scope_master")
public class GroupForNablScopeMaster {

	private long groupId;
	private String groupName;
	private String description;
	private int status;
	private String createdBy;
	private String dateCreated;
	private long calibrationLabId;

	public GroupForNablScopeMaster() {
		// TODO Auto-generated constructor stub

	}
	
	public GroupForNablScopeMaster(long groupId, String groupName, String description, int status, String createdBy,
			String dateCreated, long calibrationLabId) {
		super();
		this.groupId = groupId;
		this.groupName = groupName;
		this.description = description;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
		this.calibrationLabId = calibrationLabId;
	}


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "group_id", nullable = false)
	public long getGroupId() {
		return groupId;
	}

	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}

	@Column(name = "group_name", nullable = false)
	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public int getStatus() {
		return status;
	}



	public void setStatus(int status) {
		this.status = status;
	}


	@Column(name = "created_by", nullable = false)
	public String getCreatedBy() {
		return createdBy;
	}



	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}


	@Column(name = "date_created", nullable = false)
	public String getDateCreated() {
		return dateCreated;
	}



	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}

	@Column(name = "calibration_lab_id", nullable = false)
	public long getCalibrationLabId() {
		return calibrationLabId;
	}

	public void setCalibrationLabId(long calibrationLabId) {
		this.calibrationLabId = calibrationLabId;
	}	
	
	
}
