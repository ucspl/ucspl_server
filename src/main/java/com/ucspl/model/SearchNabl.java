package com.ucspl.model;

public class SearchNabl {
	
	private String  searchNablScope;
	
	public SearchNabl() {
		
	}

	public SearchNabl(String searchNablScope) {
		super();
		this.searchNablScope = searchNablScope;
	}

	public String getSearchNablScope() {
		return searchNablScope;
	}

	public void setSearchNablScope(String searchNablScope) {
		this.searchNablScope = searchNablScope;
	}
	

}
