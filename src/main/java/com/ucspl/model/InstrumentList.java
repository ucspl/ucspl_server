
package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "instrument_list")
public class InstrumentList {

private long instrumentId;
private String instrumentName;
private long parameterId;
private int status;
private String createdBy;
private String dateCreated;

public InstrumentList() {

}






public InstrumentList(long instrumentId, String instrumentName, long parameterId, int status, String createdBy,
		String dateCreated) {
	super();
	this.instrumentId = instrumentId;
	this.instrumentName = instrumentName;
	this.parameterId = parameterId;
	this.status = status;
	this.createdBy = createdBy;
	this.dateCreated = dateCreated;
}






@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name = "instrument_id", nullable = false)
public long getInstrumentId() {
return instrumentId;
}
public void setInstrumentId(long instrumentId) {
this.instrumentId = instrumentId;
}

@Column(name = "instrument_name")
public String getInstrumentName() {
return instrumentName;
}
public void setInstrumentName(String instrumentName) {
this.instrumentName = instrumentName;
}


@Column(name = "parameter_id")
public long getParameterId() {
	return parameterId;
}

public void setParameterId(long parameterId) {
	this.parameterId = parameterId;
}


public int getStatus() {
	return status;
}

public void setStatus(int status) {
	this.status = status;
}


public String getCreatedBy() {
	return createdBy;
}

public void setCreatedBy(String createdBy) {
	this.createdBy = createdBy;
}


public String getDateCreated() {
	return dateCreated;
}

public void setDateCreated(String dateCreated) {
	this.dateCreated = dateCreated;
}



}
