package com.ucspl.model;

public class SearchMasterInstruByMastIdAndStatus {

	private long mastId;
	private int draft;
	private int archieved;
	private int submitted;
	private int rejected;
	private int approved;
	public SearchMasterInstruByMastIdAndStatus() {
		
	}
	public SearchMasterInstruByMastIdAndStatus(long mastId, int draft, int archieved, int submitted, int rejected,
			int approved) {
		super();
		this.mastId = mastId;
		this.draft = draft;
		this.archieved = archieved;
		this.submitted = submitted;
		this.rejected = rejected;
		this.approved = approved;
	}
	public long getMastId() {
		return mastId;
	}
	public void setMastId(long mastId) {
		this.mastId = mastId;
	}
	public int getDraft() {
		return draft;
	}
	public void setDraft(int draft) {
		this.draft = draft;
	}
	public int getArchieved() {
		return archieved;
	}
	public void setArchieved(int archieved) {
		this.archieved = archieved;
	}
	public int getSubmitted() {
		return submitted;
	}
	public void setSubmitted(int submitted) {
		this.submitted = submitted;
	}
	public int getRejected() {
		return rejected;
	}
	public void setRejected(int rejected) {
		this.rejected = rejected;
	}
	public int getApproved() {
		return approved;
	}
	public void setApproved(int approved) {
		this.approved = approved;
	}
	
	
	
}
