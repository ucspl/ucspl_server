package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="create_uuc_parameter_master_specification_table")
public class CreateUucParameterMasterSpecificationDetails {

	
	public long createUucParameterMasterSpecificationId;
	public long uucMasterInstruId;
	public long parameter;
	public String parameterNumber;
	public long instruUom;
	public long calibrationLabParameter;
	public long rAndRTable;
	public long uncMaster;
	public long ipParameter;
	public long ipParameterUom;
	public long calibrationProcedure;
	public long defaultRepetability;
	public String uncReading;                                
	public String typeOfRepetability;
	public String uncPrintUnit;
	public String massCalMethod;
	public long nablScopeName;
	public long remarks;
	public String accrediation;
	
	public CreateUucParameterMasterSpecificationDetails() {
		
	}



	public CreateUucParameterMasterSpecificationDetails(long createUucParameterMasterSpecificationId,
			long uucMasterInstruId, long parameter, String parameterNumber, long instruUom,
			long calibrationLabParameter, long rAndRTable, long uncMaster, long ipParameter, long ipParameterUom,
			long calibrationProcedure, long defaultRepetability, String uncReading, String typeOfRepetability,
			String uncPrintUnit, String massCalMethod, long nablScopeName, long remarks, String accrediation) {
		super();
		this.createUucParameterMasterSpecificationId = createUucParameterMasterSpecificationId;
		this.uucMasterInstruId = uucMasterInstruId;
		this.parameter = parameter;
		this.parameterNumber = parameterNumber;
		this.instruUom = instruUom;
		this.calibrationLabParameter = calibrationLabParameter;
		this.rAndRTable = rAndRTable;
		this.uncMaster = uncMaster;
		this.ipParameter = ipParameter;
		this.ipParameterUom = ipParameterUom;
		this.calibrationProcedure = calibrationProcedure;
		this.defaultRepetability = defaultRepetability;
		this.uncReading = uncReading;
		this.typeOfRepetability = typeOfRepetability;
		this.uncPrintUnit = uncPrintUnit;
		this.massCalMethod = massCalMethod;
		this.nablScopeName = nablScopeName;
		this.remarks = remarks;
		this.accrediation = accrediation;
	}



	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "create_uuc_parameter_master_specification_id", nullable = false)
	public long getCreateUucParameterMasterSpecificationId() {
		return createUucParameterMasterSpecificationId;
	}

	public void setCreateUucParameterMasterSpecificationId(long createUucParameterMasterSpecificationId) {
		this.createUucParameterMasterSpecificationId = createUucParameterMasterSpecificationId;
	}

	
	
	

	@Column(name = "uuc_master_instrument_id")
	public long getUucMasterInstruId() {
		return uucMasterInstruId;
	}

	public void setUucMasterInstruId(long uucMasterInstruId) {
		this.uucMasterInstruId = uucMasterInstruId;
	}




	@Column(name = "parameter_id")
	public long getParameter() {
		return parameter;
	}

	public void setParameter(long parameter) {
		this.parameter = parameter;
	}

	@Column(name = "parameter_number")
	public String getParameterNumber() {
		return parameterNumber;
	}

	public void setParameterNumber(String parameterNumber) {
		this.parameterNumber = parameterNumber;
	}

	@Column(name = "instru_uom")
	public long getInstruUom() {
		return instruUom;
	}

	public void setInstruUom(long instruUom) {
		this.instruUom = instruUom;
	}

	@Column(name = "calibration_lab_parameter")
	public long getCalibrationLabParameter() {
		return calibrationLabParameter;
	}

	public void setCalibrationLabParameter(long calibrationLabParameter) {
		this.calibrationLabParameter = calibrationLabParameter;
	}

	@Column(name = "randrtable")
	public long getrAndRTable() {
		return rAndRTable;
	}

	public void setrAndRTable(long rAndRTable) {
		this.rAndRTable = rAndRTable;
	}

	@Column(name = "unc_master")
	public long getUncMaster() {
		return uncMaster;
	}

	public void setUncMaster(long uncMaster) {
		this.uncMaster = uncMaster;
	}

	@Column(name = "ip_parameter")
	public long getIpParameter() {
		return ipParameter;
	}

	public void setIpParameter(long ipParameter) {
		this.ipParameter = ipParameter;
	}

	@Column(name = "ip_parameter_uom")
	public long getIpParameterUom() {
		return ipParameterUom;
	}

	public void setIpParameterUom(long ipParameterUom) {
		this.ipParameterUom = ipParameterUom;
	}

	@Column(name = "calibration_procedure")
	public long getCalibrationProcedure() {
		return calibrationProcedure;
	}

	public void setCalibrationProcedure(long calibrationProcedure) {
		this.calibrationProcedure = calibrationProcedure;
	}

	@Column(name = "default_repeatability")
	public long getDefaultRepetability() {
		return defaultRepetability;
	}

	public void setDefaultRepetability(long defaultRepetability) {
		this.defaultRepetability = defaultRepetability;
	}

	@Column(name = "unc_reading")
	public String getUncReading() {
		return uncReading;
	}

	public void setUncReading(String uncReading) {
		this.uncReading = uncReading;
	}

	@Column(name = "type_of_repeatability")
	public String getTypeOfRepetability() {
		return typeOfRepetability;
	}

	public void setTypeOfRepetability(String typeOfRepetability) {
		this.typeOfRepetability = typeOfRepetability;
	}

	@Column(name = "unc_print_unit")
	public String getUncPrintUnit() {
		return uncPrintUnit;
	}

	public void setUncPrintUnit(String uncPrintUnit) {
		this.uncPrintUnit = uncPrintUnit;
	}

	@Column(name = "nabl_scope_name")
	public long getNablScopeName() {
		return nablScopeName;
	}

	public void setNablScopeName(long nablScopeName) {
		this.nablScopeName = nablScopeName; 
	}

	@Column(name = "remarks")
	public long getRemarks() {
		return remarks;
	}

	public void setRemarks(long remarks) {
		this.remarks = remarks;
	}

	@Column(name = "accrediation")
	public String getAccrediation() {
		return accrediation;
	}

	public void setAccrediation(String accrediation) { 
		this.accrediation = accrediation;
	}


	@Column(name = "mass_cal_method")
	public String getMassCalMethod() {
		return massCalMethod;
	}

	public void setMassCalMethod(String massCalMethod) {
		this.massCalMethod = massCalMethod;
	}
	
	
}
