package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="unc_probability_distribution_table")
public class UncProbabilityDistribution {

	
	private long probabilityDistributionId;
	private String probabilityDistributionName;
	private String description;
	private int status;
	private String createdBy;
	private String dateCreated;
	
	
	public UncProbabilityDistribution() {
		
	}

	public UncProbabilityDistribution(long probabilityDistributionId, String probabilityDistributionName,
			String description, int status, String createdBy, String dateCreated) {
		super();
		this.probabilityDistributionId = probabilityDistributionId;
		this.probabilityDistributionName = probabilityDistributionName;
		this.description = description;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "probability_distribution_id", nullable = false)
	public long getProbabilityDistributionId() {
		return probabilityDistributionId;
	}

	public void setProbabilityDistributionId(long probabilityDistributionId) {
		this.probabilityDistributionId = probabilityDistributionId;
	}
	
	@Column(name = "probability_distribution_name", nullable = false)
	public String getProbabilityDistributionName() {
		return probabilityDistributionName;
	}

	public void setProbabilityDistributionName(String probabilityDistributionName) {
		this.probabilityDistributionName = probabilityDistributionName;
	}
	

	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "date_created")
	public String getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	
	
}
