package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "create_calibration_result_data_of_master_instrument_details_table")
public class CreateCalibrationResultDataForMasterInstrument {

	private long id;
	private long masterSpecificationId;
	private long createMasterInstruId; 
	private long parameterId;
	private String uniqueSrNumber;
	private String parameterNumber;
	private String masterIdParaName;
	private String stdReading;
	private String uucReading;
	private String error;
	private String unc;
	private String acFrequency;
	private long uncFormula;
	private long uncUom;
	private long acFrequencyUom;
	
	public CreateCalibrationResultDataForMasterInstrument() {
		                                                                                                                        
	}                                                                                                                            
	                                                                                                                          
	




	public CreateCalibrationResultDataForMasterInstrument(long id, long masterSpecificationId,
			long createMasterInstruId, long parameterId, String uniqueSrNumber, String parameterNumber,
			String masterIdParaName, String stdReading, String uucReading, String error, String unc, String acFrequency,
			long uncFormula, long uncUom, long acFrequencyUom) {
		super();
		this.id = id;
		this.masterSpecificationId = masterSpecificationId;
		this.createMasterInstruId = createMasterInstruId;
		this.parameterId = parameterId;
		this.uniqueSrNumber = uniqueSrNumber;
		this.parameterNumber = parameterNumber;
		this.masterIdParaName = masterIdParaName;
		this.stdReading = stdReading;
		this.uucReading = uucReading;
		this.error = error;
		this.unc = unc;
		this.acFrequency = acFrequency;
		this.uncFormula = uncFormula;
		this.uncUom = uncUom;
		this.acFrequencyUom = acFrequencyUom;
	}






	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "create_calibration_result_data_id", nullable = false)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "master_specification_id")
	public long getMasterSpecificationId() {
		return masterSpecificationId;
	}

	public void setMasterSpecificationId(long masterSpecificationId) {
		this.masterSpecificationId = masterSpecificationId;
	}

	@Column(name = "create_master_id")
	public long getCreateMasterInstruId() {
		return createMasterInstruId;
	}

	public void setCreateMasterInstruId(long createMasterInstruId) {
		this.createMasterInstruId = createMasterInstruId;
	}

	@Column(name = "parameter_id")
	public long getParameterId() {
		return parameterId;
	}

	public void setParameterId(long parameterId) {
		this.parameterId = parameterId;
	}

//	@Column(name = "masterid_paraname")
//	public String getMasterIdParaName() {
//		return masterIdParaName;
//	}
//
//	public void setMasterIdParaName(String masterIdParaName) {
//		this.masterIdParaName = masterIdParaName;
//	}
	
	@Column(name = "parameter_number")
	public String getParameterNumber() {
		return parameterNumber;
	}
	public void setParameterNumber(String parameterNumber) {
		this.parameterNumber = parameterNumber; 
	}


	@Column(name = "mastid_paranm")
	public String getMasterIdParaName() {
		return masterIdParaName;
	}
	public void setMasterIdParaName(String masterIdParaName) {
		this.masterIdParaName = masterIdParaName;
	}
	   
	@Column(name = "unique_sr_number")
	public String getUniqueSrNumber() {
		return uniqueSrNumber;
	}

	public void setUniqueSrNumber(String uniqueSrNumber) {
		this.uniqueSrNumber = uniqueSrNumber;
	}


	@Column(name = "std_reading")
	public String getStdReading() {
		return stdReading;
	}

	public void setStdReading(String stdReading) {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
		this.stdReading = stdReading;
	}

	@Column(name = "uuc_reading")
	public String getUucReading() {
		return uucReading;
	}

	public void setUucReading(String uucReading) {
		this.uucReading = uucReading;
	}

	@Column(name = "error")
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	@Column(name = "unc")
	public String getUnc() {
		return unc;
	}

	public void setUnc(String unc) {
		this.unc = unc;
	}

	@Column(name = "ac_frequency")
	public String getAcFrequency() {
		return acFrequency;
	}

	public void setAcFrequency(String acFrequency) {
		this.acFrequency = acFrequency;
	}

	@Column(name = "unc_formula")
	public long getUncFormula() {
		return uncFormula;
	}

	public void setUncFormula(long uncFormula) {
		this.uncFormula = uncFormula;
	}

	@Column(name = "unc_uom")
	public long getUncUom() {
		return uncUom;
	}

	public void setUncUom(long uncUom) {
		this.uncUom = uncUom;
	}

	@Column(name = "ac_frequency_uom")
	public long getAcFrequencyUom() {
		return acFrequencyUom;
	}

	public void setAcFrequencyUom(long acFrequencyUom) {
		this.acFrequencyUom = acFrequencyUom;
	}



   
                                                                                                                    	                         
}
