
package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="unc_source_of_uncertainty_table")
public class UncSourceOfUncertainty {

	
	private long uncSourceOfUncertaintyId;
	private String uncSourceOfUncertaintyName;
	private String description;
	private int status;
	private String createdBy;
	private String dateCreated;
	
	
	public UncSourceOfUncertainty() {
		
	}

	public UncSourceOfUncertainty(long uncSourceOfUncertaintyId, String uncSourceOfUncertaintyName, String description,
			int status, String createdBy, String dateCreated) {
		super();
		this.uncSourceOfUncertaintyId = uncSourceOfUncertaintyId;
		this.uncSourceOfUncertaintyName = uncSourceOfUncertaintyName;
		this.description = description;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "unc_source_of_uncertainty_id", nullable = false)
	public long getUncSourceOfUncertaintyId() {
		return uncSourceOfUncertaintyId;
	}

	public void setUncSourceOfUncertaintyId(long uncSourceOfUncertaintyId) {
		this.uncSourceOfUncertaintyId = uncSourceOfUncertaintyId;
	}

	@Column(name = "unc_source_of_uncertainty_name")
	public String getUncSourceOfUncertaintyName() {
		return uncSourceOfUncertaintyName;
	}

	public void setUncSourceOfUncertaintyName(String uncSourceOfUncertaintyName) {
		this.uncSourceOfUncertaintyName = uncSourceOfUncertaintyName;
	}
	
	public String getDescription() {
		return description;
	}
	

	public void setDescription(String description) {
		this.description = description;
	}

	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "date_created")
	public String getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	
	
}
