package com.ucspl.model;

public class TrialWages extends TrialEmployee{

	private double rate;
	private double time;
	
	public TrialWages() {
		
	}

	public TrialWages(double hra, double pt, double pf, double basicSalary, double gs, double net, String name,
			double medicle,double rate,double time) {
		super(hra, pt, pf, basicSalary, gs, net, name, medicle);
		// TODO Auto-generated constructor stub
		this.rate=rate;
		this.time=time;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public double getTime() {
		return time;
	}

	public void setTime(double time) {
		this.time = time;
	}

	
	public void calculateSalary1() {
		
		double value= rate * time ;
		super.setBasicSalary(value);
		super.calculateSalary();
	}
	
	
	    
}
