package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "create_customer_purchase_order_table")
public class CreateCustomerPurchaseOrder {

	private long id;
	private String documentType;
	private String documentNumber;
	private long customerId;
	private String custPoNo;
	private String poDate;
	private String validToDate;
	private String createdBy;
	private int draft;
	private int approved;
	private int submitted;
	private int archieved;
	private int rejected;
	private String approvedBy;
	private long poAttachedFileId;
	private String updateHistory;
	private String poValue;
	private String remainingPoValue;


	public CreateCustomerPurchaseOrder() {

	}

	public CreateCustomerPurchaseOrder(long id, String documentType, String documentNumber, long customerId,
			String custPoNo, String poDate, String validToDate, String createdBy, int draft, int approved,
			int submitted, int archieved, int rejected, String approvedBy, long poAttachedFileId, String updateHistory,
			String poValue, String remainingPoValue) {
		super();
		this.id = id;
		this.documentType = documentType;
		this.documentNumber = documentNumber;
		this.customerId = customerId;
		this.custPoNo = custPoNo;
		this.poDate = poDate;
		this.validToDate = validToDate;
		this.createdBy = createdBy;
		this.draft = draft;
		this.approved = approved;
		this.submitted = submitted;
		this.archieved = archieved;
		this.rejected = rejected;
		this.approvedBy = approvedBy;
		this.poAttachedFileId = poAttachedFileId;
		this.updateHistory = updateHistory;
		this.poValue = poValue;
		this.remainingPoValue = remainingPoValue;
	}



	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "customer_purchase_order_id", nullable = false)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "document_type")
	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	@Column(name = "document_number")
	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	@Column(name = "customer_id")
	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	@Column(name = "cust_po_no")
	public String getCustPoNo() {
		return custPoNo;
	}

	public void setCustPoNo(String custPoNo) {
		this.custPoNo = custPoNo;
	}

	
	@Column(name = "po_date")
	public String getPoDate() {
		return poDate;
	}

	
	public void setPoDate(String poDate) {
		this.poDate = poDate;
	}

	@Column(name = "valid_to_date")
	public String getValidToDate() {
		return validToDate;
	}

	public void setValidToDate(String validToDate) {
		this.validToDate = validToDate;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	public int getDraft() {
		return draft;
	}


	public void setDraft(int draft) {
		this.draft = draft;
	}


	public int getApproved() {
		return approved;
	}


	public void setApproved(int approved) {
		this.approved = approved;
	}


	public int getSubmitted() {
		return submitted;
	}


	public void setSubmitted(int submitted) {
		this.submitted = submitted;
	}


	public int getArchieved() {
		return archieved;
	}


	public void setArchieved(int archieved) {
		this.archieved = archieved;
	}


	public int getRejected() {
		return rejected;
	}


	public void setRejected(int rejected) {
		this.rejected = rejected;
	}



	public String getApprovedBy() {
		return approvedBy;
	}


	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	@Column(name = "po_attached_file_id")
	public long getPoAttachedFileId() {
		return poAttachedFileId;
	}


	public void setPoAttachedFileId(long poAttachedFileId) {
		this.poAttachedFileId = poAttachedFileId;
	}

	@Column(name = "update_history")
	public String getUpdateHistory() {
		return updateHistory;
	}

	public void setUpdateHistory(String updateHistory) {
		this.updateHistory = updateHistory;
	}

	@Column(name = "po_value")
	public String getPoValue() {
		return poValue;
	}

	public void setPoValue(String poValue) {
		this.poValue = poValue;
	}

	@Column(name = "remaining_po_value")
	public String getRemainingPoValue() {
		return remainingPoValue;
	}

	public void setRemainingPoValue(String remainingPoValue) {
		this.remainingPoValue = remainingPoValue;
	}
	
	


}
