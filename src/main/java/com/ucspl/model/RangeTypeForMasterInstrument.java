package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "range_type_of_master_instrument_table")
public class RangeTypeForMasterInstrument {

	private long rangeTypeId;
	private String rangeTypeName;
	private String description;
	private long status;
	private String createdBy;
	private java.util.Date dateCreated;
	
	public RangeTypeForMasterInstrument() {
		
	}
	
	public RangeTypeForMasterInstrument(long rangeTypeId, String rangeTypeName, String description, long status,
			String createdBy, Date dateCreated) {
		super();
		this.rangeTypeId = rangeTypeId;
		this.rangeTypeName = rangeTypeName;
		this.description = description;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "range_type_id", nullable = false)
	public long getRangeTypeId() {
		return rangeTypeId;
	}
	public void setRangeTypeId(long rangeTypeId) {
		this.rangeTypeId = rangeTypeId;
	}

	@Column(name = "range_type_name", nullable = false)
	public String getRangeTypeName() {
		return rangeTypeName;
	}
	public void setRangeTypeName(String rangeTypeName) {
		this.rangeTypeName = rangeTypeName;
	}
	

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}


	public long getStatus() {
		return status;
	}
	public void setStatus(long status) {
		this.status = status;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "date_created")
	public java.util.Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(java.util.Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	

}
