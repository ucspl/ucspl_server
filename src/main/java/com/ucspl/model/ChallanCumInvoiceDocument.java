package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "invoice_document_table_of_create_challan_cum_invoice_document_table")
public class ChallanCumInvoiceDocument {

	private long id;
	private String invDocumentNumber;
	private String invoiceNumber;
	private int srNo;
	private String poSrNo;
	private String itemOrPartCode;
	private int instruId;
	private String description;
	private String rangeFrom;
	private String rangeTo;
	private String sacCode;
	private String hsnNo;
	private long quantity;
	private double unitPrice;
	private double amount;
	private String uniqueNo;

	public ChallanCumInvoiceDocument() {

	}



	public ChallanCumInvoiceDocument(long id, String invDocumentNumber, String invoiceNumber, int srNo, String poSrNo,
			String itemOrPartCode, int instruId, String description, String rangeFrom, String rangeTo, String sacCode,
			String hsnNo, long quantity, double unitPrice, double amount, String uniqueNo) {
		super();
		this.id = id;
		this.invDocumentNumber = invDocumentNumber;
		this.invoiceNumber = invoiceNumber;
		this.srNo = srNo;
		this.poSrNo = poSrNo;
		this.itemOrPartCode = itemOrPartCode;
		this.instruId = instruId;
		this.description = description;
		this.rangeFrom = rangeFrom;
		this.rangeTo = rangeTo;
		this.sacCode = sacCode;
		this.hsnNo = hsnNo;
		this.quantity = quantity;
		this.unitPrice = unitPrice;
		this.amount = amount;
		this.uniqueNo = uniqueNo;
	}







	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "invoice_document_id", nullable = false)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "inv_document_number")
	public String getInvDocumentNumber() {
		return invDocumentNumber;
	}

	public void setInvDocumentNumber(String invDocumentNumber) {
		this.invDocumentNumber = invDocumentNumber;
	}

	@Column(name = "invoice_number")
	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	@Column(name = "sr_no")
	public int getSrNo() {
		return srNo;
	}

	public void setSrNo(int srNo) {
		this.srNo = srNo;
	}
	
	@Column(name = "po_sr_no")
	public String getPoSrNo() {
		return poSrNo;
	}

	public void setPoSrNo(String poSrNo) {
		this.poSrNo = poSrNo;
	}

	@Column(name = "item_or_part_code")
	public String getItemOrPartCode() {
		return itemOrPartCode;
	}

	public void setItemOrPartCode(String itemOrPartCode) {
		this.itemOrPartCode = itemOrPartCode;
	}

	@Column(name = "instru_id")
	public int getInstruId() {
		return instruId;
	}

	public void setInstruId(int instruId) {
		this.instruId = instruId;
	}


	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	@Column(name = "sac_code")
	public String getSacCode() {
		return sacCode;
	}

	public void setSacCode(String sacCode) {
		this.sacCode = sacCode;
	}

	@Column(name = "quantity", nullable = false)
	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	@Column(name = "unitprice", nullable = false)
	public double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}

	@Column(name = "amount", nullable = false)
	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Column(name = "hsn_number")
	public String getHsnNo() {
		return hsnNo;
	}

	public void setHsnNo(String hsnNo) {
		this.hsnNo = hsnNo;
	}

	@Column(name = "range_from")
	public String getRangeFrom() {
		return rangeFrom;
	}

	public void setRangeFrom(String rangeFrom) {
		this.rangeFrom = rangeFrom;
	}

	@Column(name = "range_to")
	public String getRangeTo() {
		return rangeTo;
	}

	public void setRangeTo(String rangeTo) {
		this.rangeTo = rangeTo;
	}

	@Column(name = "unique_no")
	public String getUniqueNo() {
		return uniqueNo;
	}
	public void setUniqueNo(String uniqueNo) {
		this.uniqueNo = uniqueNo;
	}
	


	
}
