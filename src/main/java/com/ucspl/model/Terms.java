package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "terms_table_for_sales_and_quotation")
public class Terms {
	
		private long TermsId;
		private String TermsName;
		private String description;
		private long status;
		private String createdBy;
		private java.util.Date dateCreated;

		public Terms() {
			// TODO Auto-generated constructor stub

		}
	
		public Terms(long termsId, String termsName, String description, long status, String createdBy,
				Date dateCreated) {
			super();
			TermsId = termsId;
			TermsName = termsName;
			this.description = description;
			this.status = status;
			this.createdBy = createdBy;
			this.dateCreated = dateCreated;
		}

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "terms_id", nullable = false)
		public long getTermsId() {
			return TermsId;
		}

		public void setTermsId(long termsId) {
			TermsId = termsId;
		}
		
		@Column(name = "terms_name", nullable = false)
		public String getTermsName() {
			return TermsName;
		}

		public void setTermsName(String termsName) {
			TermsName = termsName;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public long getStatus() {
			return status;
		}

		public void setStatus(long status) {
			this.status = status;
		}
		
		@Column(name = "created_by")
		public String getCreatedBy() {
			return createdBy;
		}

		public void setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
		}

		@Column(name = "date_created")
		public java.util.Date getDateCreated() {
			return dateCreated;
		}

		public void setDateCreated(java.util.Date dateCreated) {
			this.dateCreated = dateCreated;
		}
		

}
