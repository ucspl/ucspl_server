package com.ucspl.model;

public class TrialEmployee {

	 private double hra;
	 private double pt;
	 private double pf;
	 private double basicSalary;
	 private double gs;
	 private double net;
	 private String name;
	 private double medicle;
	 
	 public TrialEmployee() {
		 
	}
	public TrialEmployee(double hra, double pt, double pf, double basicSalary, double gs, double net, String name,
			double medicle) {
		super();
		this.hra = hra;
		this.pt = pt;
		this.pf = pf;
		this.basicSalary = basicSalary;
		this.gs = gs;
		this.net = net;
		this.name = name;
		this.medicle = medicle;
	}
	
	public double getHra() {
		return hra;
	}
	public void setHra(double hra) {
		this.hra = hra;
	}
	public double getPt() {
		return pt;
	}
	public void setPt(double pt) {
		this.pt = pt;
	}
	public double getPf() {
		return pf;
	}
	public void setPf(double pf) {
		this.pf = pf;
	}
	public double getBasicSalary() {
		return basicSalary;
	}
	public void setBasicSalary(double basicSalary) {
		this.basicSalary = basicSalary;
	}
	public double getGs() {
		return gs;
	}
	public void setGs(double gs) {
		this.gs = gs;
	}
	public double getNet() {
		return net;
	}
	public void setNet(double net) {
		this.net = net;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getMedicle() {
		return medicle;
	}
	public void setMedicle(double medicle) {
		this.medicle = medicle;
	}
	 
	public void calculateSalary() {
		
		System.out.println("basicSalary :" + basicSalary);
		gs=basicSalary*100;
		System.out.println("GrossSalary :" + gs);
	}
	
	    
}
