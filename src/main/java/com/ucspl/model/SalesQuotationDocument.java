package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sr_no_sales_quotation_document_table_of_create_sales_quotation")
public class SalesQuotationDocument {

	private long id;
	private String documentNumber;
	private int srNo;
	private long instruName;
	private String description;
	private String idNo;
	private String accrediation;
	private String rangeAccuracy;
	private String sacCode;
	private long quantity;
	private double rate;
	private double discountOnItem;
	private double discountOnTotal;
	private double amount;
	private double subTotal;
	private double total;
	private double cgst;
	private double igst;
	private double sgst;
	private double net;

	public SalesQuotationDocument() {
		// TODO Auto-generated constructor stub
	}

	
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sales_quotation_document_id", nullable = false)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "document_number", nullable = false)
	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	@Column(name = "sr_no", nullable = false)
	public int getSrNo() {
		return srNo;
	}

	public void setSrNo(int srNo) {
		this.srNo = srNo;
	}


	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}


	@Column(name = "instru_name", nullable = false)
	public long getInstruName() {
		return instruName;
	}

	public void setInstruName(long instruName) {
		this.instruName = instruName;
	}


	@Column(name = "id_no", nullable = false)
	public String getIdNo() {
		return idNo;
	}

	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}

	public String getAccrediation() {
		return accrediation;
	}

	public void setAccrediation(String accrediation) {
		this.accrediation = accrediation;
	}

	@Column(name = "range_accuracy", nullable = false)
	public String getRangeAccuracy() {
		return rangeAccuracy;
	}

	public void setRangeAccuracy(String rangeAccuracy) {
		this.rangeAccuracy = rangeAccuracy;
	}

	@Column(name = "sac_code", nullable = false)
	public String getSacCode() {
		return sacCode;
	}

	public void setSacCode(String sacCode) {
		this.sacCode = sacCode;
	}

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	@Column(name = "discount_on_item")
	public double getDiscountOnItem() {
		return discountOnItem;
	}

	public void setDiscountOnItem(double discountOnItem) {
		this.discountOnItem = discountOnItem;
	}

	@Column(name = "discount_on_total", nullable = false)
	public double getDiscountOnTotal() {
		return discountOnTotal;
	}

	public void setDiscountOnTotal(double discountOnTotal) {
		this.discountOnTotal = discountOnTotal;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Column(name = "subtotal", nullable = false)
	public double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public double getCgst() {
		return cgst;
	}

	public void setCgst(double cgst) {
		this.cgst = cgst;
	}

	public double getIgst() {
		return igst;
	}

	public void setIgst(double igst) {
		this.igst = igst;
	}

	public double getSgst() {
		return sgst;
	}

	public void setSgst(double sgst) {
		this.sgst = sgst;
	}

	public double getNet() {
		return net;
	}

	public void setNet(double net) {
		this.net = net;
	}

}