package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "sr_no_table_of_create_delivery_challan_table")
public class SrNoDeliveryChallan {
	
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name = "deli_challan_document_id", nullable = false)
private long id;

@Column(name = "deli_challan_document_number")
private String deliChallanDocumentNumber;

@Column(name = "deli_challan_number")
private String deliChallanNumber;

@Column(name = "sr_no")
private int srNo;


//private String poSrNo;
@Column(name = "item_or_part_code")
private String itemOrPartCode;

@Column(name = "instru_id")
private int instruId;

private String description;

@Column(name = "range_from")
private String rangeFrom;

@Column(name = "range_to")
private String rangeTo;

@Column(name = "sac_code")
private String sacCode;

@Column(name = "hsn_number")
private String hsnNo;

private long quantity;
//private double unitPrice;
//private double amount;

@Column(name = "unique_no")
private String uniqueNo;



}
