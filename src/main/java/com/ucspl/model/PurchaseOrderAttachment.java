
package com.ucspl.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "purchase_order_attachment_table")
@Table(name = "purchase_order_attachment_table")
public class PurchaseOrderAttachment {

private long purchaseOrderAttachmentId;
private String originalFileName;
private String attachedFileName;
private String type;
private String documentNumber;
private String fileLocation;
private String documentName;

public PurchaseOrderAttachment(){
	
}

public PurchaseOrderAttachment(long purchaseOrderAttachmentId, String originalFileName, String attachedFileName,
		String type, String documentNumber, String fileLocation, String documentName) {
	super();
	this.purchaseOrderAttachmentId = purchaseOrderAttachmentId;
	this.originalFileName = originalFileName;
	this.attachedFileName = attachedFileName;
	this.type = type;
	this.documentNumber = documentNumber;
	this.fileLocation = fileLocation;
	this.documentName = documentName;
}




@Id
@Column(name = "purchase_order_attachment_id")
@GeneratedValue(strategy = GenerationType.IDENTITY)
public long getPurchaseOrderAttachmentId() {
	return purchaseOrderAttachmentId;
}


public void setPurchaseOrderAttachmentId(long purchaseOrderAttachmentId) {
	this.purchaseOrderAttachmentId = purchaseOrderAttachmentId;
}

@Column(name = "original_file_name")
public String getOriginalFileName() {
	return originalFileName;
}


public void setOriginalFileName(String originalFileName) {
	this.originalFileName = originalFileName;
}

@Column(name = "attached_file_name")
public String getAttachedFileName() {
	return attachedFileName;
}


public void setAttachedFileName(String attachedFileName) {
	this.attachedFileName = attachedFileName;
}


public String getType() {
	return type;
}


public void setType(String type) {
	this.type = type;
}

@Column(name = "document_number")
public String getDocumentNumber() {
	return documentNumber;
}


public void setDocumentNumber(String documentNumber) {
	this.documentNumber = documentNumber;
}

@Column(name = "file_location")
public String getFileLocation() {
	return fileLocation;
}


public void setFileLocation(String fileLocation) {
	this.fileLocation = fileLocation;
}

@Column(name = "document_name")
public String getDocumentName() {
	return documentName;
}

public void setDocumentName(String documentName) {
	this.documentName = documentName;
}


}
	
	
	
	
	
	
	