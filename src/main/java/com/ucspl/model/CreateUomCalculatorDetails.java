package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "create_uom_calculator_details_table")
public class CreateUomCalculatorDetails {

	private long createUomCalculatorDetailsId;
	private long uomCalculatorId;
	private String parameterNumber;
	private long uomFrom;
	private long uomTo;
	private String multiplyBy;
	private int draft;
	private int approved;
	private int submitted;

	public CreateUomCalculatorDetails() {

	}

	public CreateUomCalculatorDetails(long createUomCalculatorDetailsId, long uomCalculatorId, String parameterNumber,
			long uomFrom, long uomTo, String multiplyBy, int draft, int approved, int submitted) {
		super();
		this.createUomCalculatorDetailsId = createUomCalculatorDetailsId;
		this.uomCalculatorId = uomCalculatorId;
		this.parameterNumber = parameterNumber;
		this.uomFrom = uomFrom;
		this.uomTo = uomTo;
		this.multiplyBy = multiplyBy;
		this.draft = draft;
		this.approved = approved;
		this.submitted = submitted;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "uom_calculator_details_id", nullable = false)
	public long getCreateUomCalculatorDetailsId() {
		return createUomCalculatorDetailsId;
	}

	public void setCreateUomCalculatorDetailsId(long createUomCalculatorDetailsId) {
		this.createUomCalculatorDetailsId = createUomCalculatorDetailsId;
	}

	@Column(name = "uom_calculator_id")
	public long getUomCalculatorId() {
		return uomCalculatorId;
	}

	public void setUomCalculatorId(long uomCalculatorId) {
		this.uomCalculatorId = uomCalculatorId;
	}

	@Column(name = "parameter_number")
	public String getParameterNumber() {
		return parameterNumber;
	}

	public void setParameterNumber(String parameterNumber) {
		this.parameterNumber = parameterNumber;
	}

	@Column(name = "uom_from")
	public long getUomFrom() {
		return uomFrom;
	}

	public void setUomFrom(long uomFrom) {
		this.uomFrom = uomFrom;
	}

	@Column(name = "uom_to")
	public long getUomTo() {
		return uomTo;
	}

	public void setUomTo(long uomTo) {
		this.uomTo = uomTo;
	}

	@Column(name = "multiply_by")
	public String getMultiplyBy() {
		return multiplyBy;
	}

	public void setMultiplyBy(String multiplyBy) {
		this.multiplyBy = multiplyBy;
	}

	public int getDraft() {
		return draft;
	}

	public void setDraft(int draft) {
		this.draft = draft;
	}

	public int getApproved() {
		return approved;
	}

	public void setApproved(int approved) {
		this.approved = approved;
	}

	public int getSubmitted() {
		return submitted;
	}

	public void setSubmitted(int submitted) {
		this.submitted = submitted;
	}

}
