package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "reqdocno_and_customer_instrument_relationship_table")
public class ReqDocnoAndCustInstruRelationship {

	
	private long reqDocnoAndCustInstruId;
	private long reqDocId;
	private long custInstruIdentificationId;
	private long createReqAndIwdTableId;
	private String reqInwDocumentNo;
	private String inwardReqNo;
	private String inwardInstrumentNo;	
	private String parameterNumber;
	private int singleParameter;
	private int multipleParameter;
	private int calibrationTag;
	private int invoiceTag;
	private String invoiceDocNo;
	private long invoiceDocId;
	private String invoiceUniqueNo;
	private String createdBy;
	private String dcDocumentNumber;
	private int dcTag;
	private String dcUniqueNo;
	
	

	public ReqDocnoAndCustInstruRelationship() {
		
	}


	public ReqDocnoAndCustInstruRelationship(long reqDocnoAndCustInstruId, long reqDocId,
			long custInstruIdentificationId, long createReqAndIwdTableId, String reqInwDocumentNo, String inwardReqNo,
			String inwardInstrumentNo, String parameterNumber, int singleParameter, int multipleParameter,
			int calibrationTag, int invoiceTag, String invoiceDocNo, long invoiceDocId, String invoiceUniqueNo,
			String createdBy, String dcDocumentNumber, int dcTag, String dcUniqueNo) {
		super();
		this.reqDocnoAndCustInstruId = reqDocnoAndCustInstruId;
		this.reqDocId = reqDocId;
		this.custInstruIdentificationId = custInstruIdentificationId;
		this.createReqAndIwdTableId = createReqAndIwdTableId;
		this.reqInwDocumentNo = reqInwDocumentNo;
		this.inwardReqNo = inwardReqNo;
		this.inwardInstrumentNo = inwardInstrumentNo;
		this.parameterNumber = parameterNumber;
		this.singleParameter = singleParameter;
		this.multipleParameter = multipleParameter;
		this.calibrationTag = calibrationTag;
		this.invoiceTag = invoiceTag;
		this.invoiceDocNo = invoiceDocNo;
		this.invoiceDocId = invoiceDocId;
		this.invoiceUniqueNo = invoiceUniqueNo;
		this.createdBy = createdBy;
		this.dcDocumentNumber = dcDocumentNumber;
		this.dcTag = dcTag;
		this.dcUniqueNo = dcUniqueNo;
	}


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "reqno_instru_unique_id", nullable = false)
	public long getReqDocnoAndCustInstruId() {
		return reqDocnoAndCustInstruId;
	}

	public void setReqDocnoAndCustInstruId(long reqDocnoAndCustInstruId) {
		this.reqDocnoAndCustInstruId = reqDocnoAndCustInstruId;
	}

	@Column(name = "requ_doc_id")
	public long getReqDocId() {
		return reqDocId;
	}

	public void setReqDocId(long reqDocId) {
		this.reqDocId = reqDocId;
	}

	@Column(name = "customer_instru_identification_id")
	public long getCustInstruIdentificationId() {
		return custInstruIdentificationId;
	}

	public void setCustInstruIdentificationId(long custInstruIdentificationId) {
		this.custInstruIdentificationId = custInstruIdentificationId;
	}

	@Column(name = "create_requ_iwd_table_id", nullable = false)
	public long getCreateReqAndIwdTableId() {
		return createReqAndIwdTableId;
	}

	public void setCreateReqAndIwdTableId(long createReqAndIwdTableId) {
		this.createReqAndIwdTableId = createReqAndIwdTableId;
	}

	@Column(name = "requ_iwd_document_number")
	public String getReqInwDocumentNo() {
		return reqInwDocumentNo;
	}

	public void setReqInwDocumentNo(String reqInwDocumentNo) {
		this.reqInwDocumentNo = reqInwDocumentNo;
	}

	@Column(name = "iwd_requ_number")
	public String getInwardReqNo() {
		return inwardReqNo;
	}

	public void setInwardReqNo(String inwardReqNo) {
		this.inwardReqNo = inwardReqNo;
	}

	@Column(name = "inward_instruments_no")
	public String getInwardInstrumentNo() {
		return inwardInstrumentNo;
	}

	public void setInwardInstrumentNo(String inwardInstrumentNo) {
		this.inwardInstrumentNo = inwardInstrumentNo;
	}

	@Column(name = "parameters_number")
	public String getParameterNumber() {
		return parameterNumber;
	}

	public void setParameterNumber(String parameterNumber) {
		this.parameterNumber = parameterNumber;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "single_parameter")
	public int getSingleParameter() {
		return singleParameter;
	}

	public void setSingleParameter(int singleParameter) {
		this.singleParameter = singleParameter;
	}

	@Column(name = "multiple_parameter")
	public int getMultipleParameter() {
		return multipleParameter;
	}

	public void setMultipleParameter(int multipleParameter) {
		this.multipleParameter = multipleParameter;
	}
	
	@Column(name = "calibration_tag")
	public int getCalibrationTag() {
		return calibrationTag;
	}

	public void setCalibrationTag(int calibrationTag) {
		this.calibrationTag = calibrationTag;
	}

	@Column(name = "invoice_tag")
	public int getInvoiceTag() {
		return invoiceTag;
	}


	public void setInvoiceTag(int invoiceTag) {
		this.invoiceTag = invoiceTag;
	}


	@Column(name = "invoice_doc_no")
	public String getInvoiceDocNo() {
		return invoiceDocNo;
	}


	public void setInvoiceDocNo(String invoiceDocNo) {
		this.invoiceDocNo = invoiceDocNo;
	}

	@Column(name = "invoice_doc_id")
	public long getInvoiceDocId() {
		return invoiceDocId;
	}


	public void setInvoiceDocId(long invoiceDocId) {
		this.invoiceDocId = invoiceDocId;
	}




	@Column(name = "invoice_unique_number")
	public String getInvoiceUniqueNo() {
		return invoiceUniqueNo;
	}



	public void setInvoiceUniqueNo(String invoiceUniqueNo) {
		this.invoiceUniqueNo = invoiceUniqueNo;
	}

	@Column(name = "dc_document_number")
	public String getDcDocumentNumber() {
		return dcDocumentNumber;
	}


	public void setDcDocumentNumber(String dcDocumentNumber) {
		this.dcDocumentNumber = dcDocumentNumber;
	}

	@Column(name = "dc_tag")
	public int getDcTag() {
		return dcTag;
	}


	public void setDcTag(int dcTag) {
		this.dcTag = dcTag;
	}

	@Column(name = "dc_unique_number")
	public String getDcUniqueNo() {
		return dcUniqueNo;
	}


	public void setDcUniqueNo(String dcUniqueNo) {
		this.dcUniqueNo = dcUniqueNo;
	}
	

}
