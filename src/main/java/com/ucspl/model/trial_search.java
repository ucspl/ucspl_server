package com.ucspl.model;

import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;

@NamedStoredProcedureQuery(name = "search_and_modify_user_by_name", procedureName = "search_and_modify_user_by_name", parameters = {
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "name", type = String.class),
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "status", type = Integer.class), })
public class trial_search {

	private String firstName;
	private String lastName;
	private String username;
	private long usernumber;
	private String role;
	private String department;

	public trial_search(String firstName, String lastName, String username, long usernumber, String role,
			String department) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.username = username;
		this.usernumber = usernumber;
		this.role = role;
		this.department = department;
	}

	public trial_search() {
		// TODO Auto-generated constructor stub
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public long getUsernumber() {
		return usernumber;
	}

	public void setUsernumber(long usernumber) {
		this.usernumber = usernumber;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

}
