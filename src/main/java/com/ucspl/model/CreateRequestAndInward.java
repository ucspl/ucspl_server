package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "create_request_and_inward_table")
public class CreateRequestAndInward {

	
	 private long id;
	 private long inwardType;
	 private String inwardDate;
	 private long reqNoCounter;
	 private String reqInwDocumentNo;
	 private String inwardReqNo;
	 private long customerName;
	 private String endDate;
	 private long reportInNameOf;
	 private long shippedTo;
	 private String customerDCNo;
	 private String dcDate;
	 private String expectedDeliveryDate;
	 private String calibratedAt;
	 private int labLocation;
	 private int noOfInstruments;
	 private String reqContactName;
	 private String countryPrefixForMobile1;
	 private String reqContactPhone;
	 private String reqEmail;
	 private String reqType;
		private String checkedBy;
		private String createdBy;
		private String modifiedBy;
		private java.util.Date dateCreated;
		private java.util.Date dateModified;
		private String assignedTo;
		private String typeOfDocument;
		private int draft;
		private int approved;
		private int archieved;
		private int submitted;
		private int rejected;
	//	private int multiParameterFlag;
		
		public CreateRequestAndInward() {
			
		}
		
		


		public CreateRequestAndInward(long id, long inwardType, String inwardDate, long reqNoCounter,
			String reqInwDocumentNo, String inwardReqNo, long customerName, String endDate, long reportInNameOf,
			long shippedTo, String customerDCNo, String dcDate, String expectedDeliveryDate, String calibratedAt,
			int labLocation, int noOfInstruments, String reqContactName, String countryPrefixForMobile1,
			String reqContactPhone, String reqEmail, String reqType, String checkedBy, String createdBy,
			String modifiedBy, Date dateCreated, Date dateModified, String assignedTo, String typeOfDocument, int draft,
			int approved, int archieved, int submitted, int rejected) {
		super();
		this.id = id;
		this.inwardType = inwardType;
		this.inwardDate = inwardDate;
		this.reqNoCounter = reqNoCounter;
		this.reqInwDocumentNo = reqInwDocumentNo;
		this.inwardReqNo = inwardReqNo;
		this.customerName = customerName;
		this.endDate = endDate;
		this.reportInNameOf = reportInNameOf;
		this.shippedTo = shippedTo;
		this.customerDCNo = customerDCNo;
		this.dcDate = dcDate;
		this.expectedDeliveryDate = expectedDeliveryDate;
		this.calibratedAt = calibratedAt;
		this.labLocation = labLocation;
		this.noOfInstruments = noOfInstruments;
		this.reqContactName = reqContactName;
		this.countryPrefixForMobile1 = countryPrefixForMobile1;
		this.reqContactPhone = reqContactPhone;
		this.reqEmail = reqEmail;
		this.reqType = reqType;
		this.checkedBy = checkedBy;
		this.createdBy = createdBy;
		this.modifiedBy = modifiedBy;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
		this.assignedTo = assignedTo;
		this.typeOfDocument = typeOfDocument;
		this.draft = draft;
		this.approved = approved;
		this.archieved = archieved;
		this.submitted = submitted;
		this.rejected = rejected;
	}




		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "id", nullable = false)
		public long getId() {
			return id;
		}
		public void setId(long id) {
			this.id = id;
		}
		
		@Column(name = "inward_type", nullable = false)
		public long getInwardType() {
			return inwardType;
		}
		public void setInwardType(long inwardType) {
			this.inwardType = inwardType;
		}
		
		@Column(name = "inward_date")
		public String getInwardDate() {
			return inwardDate;
		}
		public void setInwardDate(String inwardDate) {
			this.inwardDate = inwardDate;
		}
		
		@Column(name = "req_no_counter")
		public long getReqNoCounter() {
			return reqNoCounter;
		}
		public void setReqNoCounter(long reqNoCounter) {
			this.reqNoCounter = reqNoCounter;
		}


		@Column(name = "cust_id")
		public long getCustomerName() {
			return customerName;
		}
		public void setCustomerName(long customerName) {
			this.customerName = customerName;
		}
		
		@Column(name = "shipped_to_id")
		public long getShippedTo() {
			return shippedTo;
		}

		public void setShippedTo(long shippedTo) {
			this.shippedTo = shippedTo;
		}
		
		@Column(name = "req_iwd_document_number")
		public String getReqInwDocumentNo() {
			return reqInwDocumentNo;
		}

		public void setReqInwDocumentNo(String reqInwDocumentNo) {
			this.reqInwDocumentNo = reqInwDocumentNo;
		}

		@Column(name = "iwd_req_number")
		public String getInwardReqNo() {
			return inwardReqNo;
		}

		public void setInwardReqNo(String inwardReqNo) {
			this.inwardReqNo = inwardReqNo;
		}


		@Column(name = "end_date")
		public String getEndDate() {
			return endDate;
		}
		public void setEndDate(String endDate) {
			this.endDate = endDate;
		}
		
		@Column(name = "reportin_nameof_cust_id")
		public long getReportInNameOf() {
			return reportInNameOf;
		}
		public void setReportInNameOf(long reportInNameOf) {
			this.reportInNameOf = reportInNameOf;
		}
		
		@Column(name = "cust_dc_no")
		public String getCustomerDCNo() {
			return customerDCNo;
		}
		public void setCustomerDCNo(String customerDCNo) {
			this.customerDCNo = customerDCNo;
		}
		
		@Column(name = "dc_date")
		public String getDcDate() {
			return dcDate;
		}
		public void setDcDate(String dcDate) {
			this.dcDate = dcDate;
		}
		
		@Column(name = "expected_delivery_date")
		public String getExpectedDeliveryDate() {
			return expectedDeliveryDate;
		}
		public void setExpectedDeliveryDate(String expectedDeliveryDate) {
			this.expectedDeliveryDate = expectedDeliveryDate;
		}
		
		@Column(name = "calibrated_at")
		public String getCalibratedAt() {
			return calibratedAt;
		}
		public void setCalibratedAt(String calibratedAt) {
			this.calibratedAt = calibratedAt;
		}
		
		@Column(name = "lab_location")
		public int getLabLocation() {
			return labLocation;
		}
		public void setLabLocation(int labLocation) {
			this.labLocation = labLocation;
		}
		
		@Column(name = "no_of_instruments")
		public int getNoOfInstruments() {
			return noOfInstruments;
		}




		public void setNoOfInstruments(int noOfInstruments) {
			this.noOfInstruments = noOfInstruments;
		}
		
		
		
		
		
		
		
		
		
		@Column(name = "req_contact_name")
		public String getReqContactName() {
			return reqContactName;
		}







		public void setReqContactName(String reqContactName) {
			this.reqContactName = reqContactName;
		}
		
		@Column(name = "country_prefix_for_mobile1")
		public String getCountryPrefixForMobile1() {
			return countryPrefixForMobile1;
		}
		public void setCountryPrefixForMobile1(String countryPrefixForMobile1) {
			this.countryPrefixForMobile1 = countryPrefixForMobile1;
		}
		
		@Column(name = "req_contact_phone")
		public String getReqContactPhone() {
			return reqContactPhone;
		}
		public void setReqContactPhone(String reqContactPhone) {
			this.reqContactPhone = reqContactPhone;
		}
		
		@Column(name = "req_email")
		public String getReqEmail() {
			return reqEmail;
		}
		public void setReqEmail(String reqEmail) {
			this.reqEmail = reqEmail;
		}
		
		@Column(name = "req_type")
		public String getReqType() {
			return reqType;
		}
		public void setReqType(String reqType) {
			this.reqType = reqType;
		}
		
		@Column(name = "created_by")
		public String getCreatedBy() {
			return createdBy;
		}
		public void setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
		}
		
		@Column(name = "date_created")
		public java.util.Date getDateCreated() {
			return dateCreated;
		}
		public void setDateCreated(java.util.Date dateCreated) {
			this.dateCreated = dateCreated;
		}

		@Column(name = "assigned_to")
		public String getAssignedTo() {
			return assignedTo;
		}

		public void setAssignedTo(String assignedTo) {
			this.assignedTo = assignedTo;
		}

//		@Column(name = "multi_parameter_list_flag")
//		public int getMultiParameterFlag() {
//			return multiParameterFlag;
//		}
//
//		public void setMultiParameterFlag(int multiParameterFlag) {
//			this.multiParameterFlag = multiParameterFlag;
//		}


		public int getDraft() {
			return draft;
		}

		public void setDraft(int draft) {
			this.draft = draft;
		}

		public int getApproved() {
			return approved;
		}

		public void setApproved(int approved) {
			this.approved = approved;
		}

		public int getArchieved() {
			return archieved;
		}

		public void setArchieved(int archieved) {
			this.archieved = archieved;
		}

		public int getSubmitted() {
			return submitted;
		}

		public void setSubmitted(int submitted) {
			this.submitted = submitted;
		}

		public int getRejected() {
			return rejected;
		}

		public void setRejected(int rejected) {
			this.rejected = rejected;
		}

		@Column(name = "type_of_document")
		public String getTypeOfDocument() {
			return typeOfDocument;
		}

		public void setTypeOfDocument(String typeOfDocument) {
			this.typeOfDocument = typeOfDocument;
		}

		@Column(name = "date_modified")
		public java.util.Date getDateModified() {
			return dateModified;
		}


		public void setDateModified(java.util.Date dateModified) {
			this.dateModified = dateModified;
		}


		
		
	 
	 
}
