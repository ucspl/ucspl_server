
package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "create_nabl_scope_master_specification_details_table")
public class CreateNablSpecification {

	private long nablSpecificationId;
	private long createNablId; 
	private long calibrationLabId;
	private String groupId;
	private String parameterId;
	private String parameterNumber;
	private String nablIdWithParameterName;
	private long nablScopeId;
	private String rangeFrom;
	private String rangeTo;
	private long rangeUom;
	private String freqFrom;
	private long freqFromUom;
	private String freqTo;
	private long freqToUom;
	private String leastCount;
	private long leastCountUom;
	private String uncValue;
	private long uncFormulaId;
	private long uncUom;
	private long tempChartType;
	private long modeType;
	private String amendmentDate;
	private int inhouse;
	private int onsite;
	private int draft;
	private int approved;
	private int archieved;
	private int submitted;
	private int rejected;
	
	public CreateNablSpecification() {
		
	}

	

	public CreateNablSpecification(long nablSpecificationId, long createNablId, long calibrationLabId, String groupId,
			String parameterId, String parameterNumber, String nablIdWithParameterName, long nablScopeId,
			String rangeFrom, String rangeTo, long rangeUom, String freqFrom, long freqFromUom, String freqTo,
			long freqToUom, String leastCount, long leastCountUom, String uncValue, long uncFormulaId, long uncUom,
			long tempChartType, long modeType, String amendmentDate, int inhouse, int onsite, int draft, int approved,
			int archieved, int submitted, int rejected) {
		super();
		this.nablSpecificationId = nablSpecificationId;
		this.createNablId = createNablId;
		this.calibrationLabId = calibrationLabId;
		this.groupId = groupId;
		this.parameterId = parameterId;
		this.parameterNumber = parameterNumber;
		this.nablIdWithParameterName = nablIdWithParameterName;
		this.nablScopeId = nablScopeId;
		this.rangeFrom = rangeFrom;
		this.rangeTo = rangeTo;
		this.rangeUom = rangeUom;
		this.freqFrom = freqFrom;
		this.freqFromUom = freqFromUom;
		this.freqTo = freqTo;
		this.freqToUom = freqToUom;
		this.leastCount = leastCount;
		this.leastCountUom = leastCountUom;
		this.uncValue = uncValue;
		this.uncFormulaId = uncFormulaId;
		this.uncUom = uncUom;
		this.tempChartType = tempChartType;
		this.modeType = modeType;
		this.amendmentDate = amendmentDate;
		this.inhouse = inhouse;
		this.onsite = onsite;
		this.draft = draft;
		this.approved = approved;
		this.archieved = archieved;
		this.submitted = submitted;
		this.rejected = rejected;
	}



	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "nabl_specification_id", nullable = false)
	public long getNablSpecificationId() {
		return nablSpecificationId;
	}


	public void setNablSpecificationId(long nablSpecificationId) {
		this.nablSpecificationId = nablSpecificationId;
	}

	@Column(name = "create_nabl_id", nullable = false)
	public long getCreateNablId() {
		return createNablId;
	}


	public void setCreateNablId(long createNablId) {
		this.createNablId = createNablId;
	}

	@Column(name = "calibration_lab_id", nullable = false)
	public long getCalibrationLabId() {
		return calibrationLabId;
	}


	public void setCalibrationLabId(long calibrationLabId) {
		this.calibrationLabId = calibrationLabId;
	}
	
	@Column(name = "group_id", nullable = false)
	public String getGroupId() {
		return groupId;
	}


	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	@Column(name = "parameter_id", nullable = false)
	public String getParameterId() {
		return parameterId;
	}


	public void setParameterId(String parameterId) {
		this.parameterId = parameterId;
	}

	@Column(name = "parameter_number")
	public String getParameterNumber() {
		return parameterNumber;
	}

	public void setParameterNumber(String parameterNumber) {
		this.parameterNumber = parameterNumber;
	}
	

	@Column(name = "range_from")
	public String getRangeFrom() {
		return rangeFrom;
	}

	public void setRangeFrom(String rangeFrom) {
		this.rangeFrom = rangeFrom;
	}

	@Column(name = "range_to")
	public String getRangeTo() {
		return rangeTo;
	}

	public void setRangeTo(String rangeTo) {
		this.rangeTo = rangeTo;
	}

	@Column(name = "range_uom")
	public long getRangeUom() {
		return rangeUom;
	}

	public void setRangeUom(long rangeUom) {
		this.rangeUom = rangeUom;
	}

	@Column(name = "freq_from")
	public String getFreqFrom() {
		return freqFrom;
	}

	public void setFreqFrom(String freqFrom) {
		this.freqFrom = freqFrom;
	}

	@Column(name = "freq_from_uom")
	public long getFreqFromUom() {
		return freqFromUom;
	}

	public void setFreqFromUom(long freqFromUom) {
		this.freqFromUom = freqFromUom;
	}

	@Column(name = "freq_to")
	public String getFreqTo() {
		return freqTo;
	}

	public void setFreqTo(String freqTo) {
		this.freqTo = freqTo;
	}

	@Column(name = "freq_to_uom")
	public long getFreqToUom() {
		return freqToUom;
	}

	public void setFreqToUom(long freqToUom) {
		this.freqToUom = freqToUom;
	}

	@Column(name = "least_count")
	public String getLeastCount() {
		return leastCount;
	}

	public void setLeastCount(String leastCount) {
		this.leastCount = leastCount;
	}

	@Column(name = "least_count_uom")
	public long getLeastCountUom() {
		return leastCountUom;
	}

	public void setLeastCountUom(long leastCountUom) {
		this.leastCountUom = leastCountUom;
	}

	@Column(name = "unc_value")
	public String getUncValue() {
		return uncValue;
	}


	public void setUncValue(String uncValue) {
		this.uncValue = uncValue;
	}

	@Column(name = "unc_formula_id")
	public long getUncFormulaId() {
		return uncFormulaId;
	}


	public void setUncFormulaId(long uncFormulaId) {
		this.uncFormulaId = uncFormulaId;
	}

	@Column(name = "unc_uom")
	public long getUncUom() {
		return uncUom;
	}


	public void setUncUom(long uncUom) {
		this.uncUom = uncUom;
	}

	@Column(name = "temp_chart_type")
	public long getTempChartType() {
		return tempChartType;
	}

	public void setTempChartType(long tempChartType) {
		this.tempChartType = tempChartType;
	}

	@Column(name = "mode_type")
	public long getModeType() {
		return modeType;
	}

	public void setModeType(long modeType) {
		this.modeType = modeType;
	}

	@Column(name = "amendment_date")
	public String getAmendmentDate() {
		return amendmentDate;
	}


	public void setAmendmentDate(String amendmentDate) {
		this.amendmentDate = amendmentDate;
	}


	public int getInhouse() {
		return inhouse;
	}


	public void setInhouse(int inhouse) {
		this.inhouse = inhouse;
	}


	public int getOnsite() {
		return onsite;
	}


	public void setOnsite(int onsite) {
		this.onsite = onsite;
	}
	
	public int getDraft() {
		return draft;
	}

	public void setDraft(int draft) {
		this.draft = draft;
	}

	public int getApproved() {
		return approved;
	}

	public void setApproved(int approved) {
		this.approved = approved;
	}

	public int getArchieved() { 
		return archieved;
	}

	public void setArchieved(int archieved) {
		this.archieved = archieved;
	}

	public int getSubmitted() {
		return submitted;
	}

	public void setSubmitted(int submitted) {
		this.submitted = submitted;
	}

	public int getRejected() {
		return rejected;
	}

	public void setRejected(int rejected) {
		this.rejected = rejected;
	}
	
	@Column(name = "nabl_id_with_parameter_name")
	public String getNablIdWithParameterName() {
		return nablIdWithParameterName;
	}

	public void setNablIdWithParameterName(String nablIdWithParameterName) {
		this.nablIdWithParameterName = nablIdWithParameterName;
	}

	@Column(name = "nabl_scope_id")
	public long getNablScopeId() {
		return nablScopeId;
	}

	public void setNablScopeId(long nablScopeId) {
		this.nablScopeId = nablScopeId;
	}
	
}