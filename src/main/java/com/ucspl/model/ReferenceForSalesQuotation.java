
package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "reference_table_for_sales_quotation")
public class ReferenceForSalesQuotation {

	private long referenceId;
	private String referenceName;
	private long status;
	private String createdBy;
	private java.util.Date dateCreated;

	public ReferenceForSalesQuotation() {
		// TODO Auto-generated constructor stub

	}

	

	public ReferenceForSalesQuotation(long referenceId, String referenceName, long status, String createdBy,
			Date dateCreated) {
		super();
		this.referenceId = referenceId;
		this.referenceName = referenceName;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "reference_id", nullable = false)
	public long getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(long referenceId) {
		this.referenceId = referenceId;
	}
	@Column(name = "reference_name", nullable = false)
	public String getReferenceName() {
		return referenceName;
	}

	public void setReferenceName(String referenceName) {
		this.referenceName = referenceName;
	}



	public long getStatus() {
		return status;
	}



	public void setStatus(long status) {
		this.status = status;
	}



	public String getCreatedBy() {
		return createdBy;
	}


	@Column(name = "created_by")
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}


	@Column(name = "date_created")
	public java.util.Date getDateCreated() {
		return dateCreated;
	}



	public void setDateCreated(java.util.Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	


}
