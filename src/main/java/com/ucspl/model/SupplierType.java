package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "supplier_type_table")
public class SupplierType {

	private long supplierTypeId;
	private String supplierTypeName;
	private long status;
	private String createdBy;
	private java.util.Date dateCreated;
	
	
	public SupplierType() {
		
	}
	

	public SupplierType(long supplierTypeId, String supplierTypeName, long status, String createdBy, Date dateCreated) {
		super();
		this.supplierTypeId = supplierTypeId;
		this.supplierTypeName = supplierTypeName;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}



	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "supplier_type_id", nullable = false)
	public long getSupplierTypeId() {
		return supplierTypeId;
	}

	public void setSupplierTypeId(long supplierTypeId) {
		this.supplierTypeId = supplierTypeId;
	}

	@Column(name = "supplier_type_name")
	public String getSupplierTypeName() {
		return supplierTypeName;
	}

	public void setSupplierTypeName(String supplierTypeName) {
		this.supplierTypeName = supplierTypeName;
	}


	public long getStatus() {
		return status;
	}


	public void setStatus(long status) {
		this.status = status;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "date_created")
	public java.util.Date getDateCreated() {
		return dateCreated;
	}


	public void setDateCreated(java.util.Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	
}


