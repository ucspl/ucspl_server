package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "instrument_type_of_master_instrument_table")
public class InstrumentTypeForMasterInstrument {

	private long instruTypeId;
	private String instruTypeName;
	private String description;
	private long status;
	private String createdBy;
	private java.util.Date dateCreated;
	
	public InstrumentTypeForMasterInstrument() {
		
	}
	public InstrumentTypeForMasterInstrument(long instruTypeId, String instruTypeName, String description, long status,
			String createdBy, Date dateCreated) {
		super();
		this.instruTypeId = instruTypeId;
		this.instruTypeName = instruTypeName;
		this.description = description;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "instrument_type_id", nullable = false)
	public long getInstruTypeId() {
		return instruTypeId;
	}
	public void setInstruTypeId(long instruTypeId) {
		this.instruTypeId = instruTypeId;
	}
	
	@Column(name = "instrument_type_name", nullable = false)
	public String getInstruTypeName() {
		return instruTypeName;
	}
	public void setInstruTypeName(String instruTypeName) {
		this.instruTypeName = instruTypeName;
	}
	

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}


	public long getStatus() {
		return status;
	}
	public void setStatus(long status) {
		this.status = status;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "date_created")
	public java.util.Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(java.util.Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	

}
