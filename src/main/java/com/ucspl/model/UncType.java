
package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="unc_type_table")
public class UncType {

	
	private long uncTypeId;
	private String uncTypeName; 
	private String description;
	private int status;
	private String createdBy;
	private String dateCreated;
	
	
	public UncType() {
		
	}

	public UncType(long uncTypeId, String uncTypeName, String description, int status, String createdBy,
			String dateCreated) {
		super();
		this.uncTypeId = uncTypeId;
		this.uncTypeName = uncTypeName;
		this.description = description;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "unc_type_id", nullable = false)
	public long getUncTypeId() {
		return uncTypeId;
	}

	public void setUncTypeId(long uncTypeId) {
		this.uncTypeId = uncTypeId;
	}
	@Column(name = "unc_type_name")
	public String getUncTypeName() {
		return uncTypeName;
	}

	public void setUncTypeName(String uncTypeName) {
		this.uncTypeName = uncTypeName;
	}
	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "date_created")
	public String getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	
	
}
