package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "customer_contact_list_table")
public class CustomerContactList {

	private long customerContactListId;
	private long customerId;
	private String personName;
	private String department;
	private String jobTitle;
	private String mobileNumber1;
	private String mobileNumber2;
	private String phoneNumber;
	private String email;
	private int status;

	public CustomerContactList()
	{
		
	}

	public CustomerContactList(long customerContactListId, long customerId, String personName, String department,
			String jobTitle, String mobileNumber1, String mobileNumber2, String phoneNumber, String email, int status) {
		super();
		this.customerContactListId = customerContactListId;
		this.customerId = customerId;
		this.personName = personName;
		this.department = department;
		this.jobTitle = jobTitle;
		this.mobileNumber1 = mobileNumber1;
		this.mobileNumber2 = mobileNumber2;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.status = status;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "customer_contact_list_id", nullable = false)
	public long getCustomerContactListId() {
		return customerContactListId;
	}

	public void setCustomerContactListId(long customerContactListId) {
		this.customerContactListId = customerContactListId;
	}

	@Column(name = "customer_id", nullable = false)
	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	@Column(name = "person_name")
	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	@Column(name = "department")
	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	@Column(name = "job_title")
	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	@Column(name = "mobilenumber1")
	public String getMobileNumber1() {
		return mobileNumber1;
	}

	public void setMobileNumber1(String mobileNumber1) {
		this.mobileNumber1 = mobileNumber1;
	}

	@Column(name = "mobilenumber2")
	public String getMobileNumber2() {
		return mobileNumber2;
	}

	public void setMobileNumber2(String mobileNumber2) {
		this.mobileNumber2 = mobileNumber2;
	}

	@Column(name = "phone_number")
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Column(name = "email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "status")
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	
	
}
