package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "create_supplier")
public class Supplier {

	private long id;
	private String name;
	private String primaryContact;
	private String secondaryContact;
	private String phoneNo;
	private String countryPrefixForPhoneNo;
	private String countryPrefixForPrimaryMobile;
	private String countryPrefixForSecondaryMobile;
	private String primaryPhoneNo;
	private String secondaryPhoneNo;
	private long supplierType;
	private String email;
	private String panNo;
	private String gstNo;
	private String approvalStatus;
	private int status;
	private String country;
	private String addressLine1;
	private String addressLine2;
	private String state;
	private String city;
	private String code;
	private String createdBy;
	private String modifiedBy;
	private java.util.Date dateCreated;
	private java.util.Date dateModified;
	
	public Supplier()
	{
		
	}
	
	public Supplier(long id, String name, String primaryContact, String secondaryContact, String phoneNo,
			String countryPrefixForPhoneNo, String countryPrefixForPrimaryMobile,
			String countryPrefixForSecondaryMobile, String primaryPhoneNo, String secondaryPhoneNo, long supplierType,
			String email, String panNo, String gstNo, String approvalStatus, int status, String country,
			String addressLine1, String addressLine2, String state, String city, String code, String createdBy,
			String modifiedBy, Date dateCreated, Date dateModified) {
		super();
		this.id = id;
		this.name = name;
		this.primaryContact = primaryContact;
		this.secondaryContact = secondaryContact;
		this.phoneNo = phoneNo;
		this.countryPrefixForPhoneNo = countryPrefixForPhoneNo;
		this.countryPrefixForPrimaryMobile = countryPrefixForPrimaryMobile;
		this.countryPrefixForSecondaryMobile = countryPrefixForSecondaryMobile;
		this.primaryPhoneNo = primaryPhoneNo;
		this.secondaryPhoneNo = secondaryPhoneNo;
		this.supplierType = supplierType;
		this.email = email;
		this.panNo = panNo;
		this.gstNo = gstNo;
		this.approvalStatus = approvalStatus;
		this.status = status;
		this.country = country;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.state = state;
		this.city = city;
		this.code = code;
		this.createdBy = createdBy;
		this.modifiedBy = modifiedBy;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
	}





	@Id
	@Column(name = "supplier_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	@Column(name = "supplier_name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "primary_contact")
	public String getPrimaryContact() {
		return primaryContact;
	}
	public void setPrimaryContact(String primaryContact) {
		this.primaryContact = primaryContact;
	}
	
	@Column(name = "secondary_contact")
	public String getSecondaryContact() {
		return secondaryContact;
	}
	public void setSecondaryContact(String secondaryContact) {
		this.secondaryContact = secondaryContact;
	}
	
	@Column(name = "country_prefix_for_phone_no")
	public String getCountryPrefixForPhoneNo() {
		return countryPrefixForPhoneNo;
	}
	public void setCountryPrefixForPhoneNo(String countryPrefixForPhoneNo) {
		this.countryPrefixForPhoneNo = countryPrefixForPhoneNo;
	}

	
	@Column(name = "phone_number")
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	
	@Column(name = "country_prefix_for_mobile1")
	public String getCountryPrefixForPrimaryMobile() {
		return countryPrefixForPrimaryMobile;
	}

	public void setCountryPrefixForPrimaryMobile(String countryPrefixForPrimaryMobile) {
		this.countryPrefixForPrimaryMobile = countryPrefixForPrimaryMobile;
	}


	@Column(name = "country_prefix_for_mobile2")
	public String getCountryPrefixForSecondaryMobile() {
		return countryPrefixForSecondaryMobile;
	}

	public void setCountryPrefixForSecondaryMobile(String countryPrefixForSecondaryMobile) {
		this.countryPrefixForSecondaryMobile = countryPrefixForSecondaryMobile;
	}

	@Column(name = "primary_mobile_no")
	public String getPrimaryPhoneNo() {
		return primaryPhoneNo;
	}
	public void setPrimaryPhoneNo(String primaryPhoneNo) {
		this.primaryPhoneNo = primaryPhoneNo;
	}
	
	@Column(name = "secondary_mobile_no")
	public String getSecondaryPhoneNo() {
		return secondaryPhoneNo;
	}
	public void setSecondaryPhoneNo(String secondaryPhoneNo) {
		this.secondaryPhoneNo = secondaryPhoneNo;
	}
	
	@Column(name = "supplier_type")
	public long getSupplierType() {
		return supplierType;
	}
	public void setSupplierType(long supplierType) {
		this.supplierType = supplierType;
	}
	
	@Column(name = "email")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name = "panno")
	public String getPanNo() {
		return panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	@Column(name = "gstno")
	public String getGstNo() {
		return gstNo;
	}


	public void setGstNo(String gstNo) {
		this.gstNo = gstNo;
	}

	@Column(name = "approval_status")
	public String getApprovalStatus() {
		return approvalStatus;
	}
	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}
	
	@Column(name = "status")
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	@Column(name = "country")
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	@Column(name = "addressline1")
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	
	@Column(name = "addressline2")
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	
	@Column(name = "state")
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	@Column(name = "city")
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	@Column(name = "pin")
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "date_created")
	public java.util.Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(java.util.Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	@Column(name = "modified_by")
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Column(name = "date_modified")
	public java.util.Date getDateModified() {
		return dateModified;
	}
	public void setDateModified(java.util.Date dateModified) {
		this.dateModified = dateModified;
	}
	
}
