package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="create_env_master_table")
public class CreateEnvMaster {
	
	public long id;
    public String calibrationLab;
    public String calibratedAt;
    public String status;
    public String validFromDate;
    public String  validToDate;
    public String  temperature1;
    public String tempSign;
    public String temperature2;
    public String humidity1; 
    public String humiditySign;
    public String humidity2;
    public String atmpress1; 
    public String atmpressSign;
    public String atmpress2;
    public int draft;
    public int approved;
    public int submitted;
    
    
    public CreateEnvMaster() {}


	public CreateEnvMaster(long id, String calibrationLab, String calibratedAt, String status, String validFromDate,
			String validToDate, String temperature1, String tempSign, String temperature2, String humidity1,
			String humiditySign, String humidity2, String atmpress1, String atmpressSign, String atmpress2, int draft,
			int approved, int submitted) {
		super();
		this.id = id;
		this.calibrationLab = calibrationLab;
		this.calibratedAt = calibratedAt;
		this.status = status;
		this.validFromDate = validFromDate;
		this.validToDate = validToDate;
		this.temperature1 = temperature1;
		this.tempSign = tempSign;
		this.temperature2 = temperature2;
		this.humidity1 = humidity1;
		this.humiditySign = humiditySign;
		this.humidity2 = humidity2;
		this.atmpress1 = atmpress1;
		this.atmpressSign = atmpressSign;
		this.atmpress2 = atmpress2;
		this.draft = draft;
		this.approved = approved;
		this.submitted = submitted;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "create_env_master_id", nullable = false)
	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "calibration_lab_id")
	public String getCalibrationLab() {
		return calibrationLab;
	}


	public void setCalibrationLab(String calibrationLab) {
		this.calibrationLab = calibrationLab;
	}

	@Column(name = "calibrated_at")
	public String getCalibratedAt() {
		return calibratedAt;
	}


	public void setCalibratedAt(String calibratedAt) {
		this.calibratedAt = calibratedAt;
	}

	@Column(name = "status")
	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "valid_from_date")
	public String getValidFromDate() {
		return validFromDate;
	}


	public void setValidFromDate(String validFromDate) {
		this.validFromDate = validFromDate;
	}

	@Column(name = "valid_to_date")
	public String getValidToDate() {
		return validToDate;
	}


	public void setValidToDate(String validToDate) {
		this.validToDate = validToDate;
	}

	@Column(name = "temperature1")
	public String getTemperature1() {
		return temperature1;
	}


	public void setTemperature1(String temperature1) {
		this.temperature1 = temperature1;
	}

	@Column(name = "temp_sign")
	public String getTempSign() {
		return tempSign;
	}


	public void setTempSign(String tempSign) {
		this.tempSign = tempSign;
	}

	@Column(name = "temperature2")
	public String getTemperature2() {
		return temperature2;
	}


	public void setTemperature2(String temperature2) {
		this.temperature2 = temperature2;
	}

	@Column(name = "humidity1")
	public String getHumidity1() {
		return humidity1;
	}

	
	public void setHumidity1(String humidity1) {
		this.humidity1 = humidity1;
	}

	@Column(name = "humidity_sign")
	public String getHumiditySign() {
		return humiditySign;
	}


	public void setHumiditySign(String humiditySign) {
		this.humiditySign = humiditySign;
	}

	@Column(name = "humidity2")
	public String getHumidity2() {
		return humidity2;
	}


	public void setHumidity2(String humidity2) {
		this.humidity2 = humidity2;
	}

	@Column(name = "atmpress1")
	public String getAtmpress1() {
		return atmpress1;
	}


	public void setAtmpress1(String atmpress1) {
		this.atmpress1 = atmpress1;
	}

	@Column(name = "atmpress_sign")
	public String getAtmpressSign() {
		return atmpressSign;
	}


	public void setAtmpressSign(String atmpressSign) {
		this.atmpressSign = atmpressSign;
	}

	@Column(name = "atmpress2")
	public String getAtmpress2() {
		return atmpress2;
	}


	public void setAtmpress2(String atmpress2) {
		this.atmpress2 = atmpress2;
	}


	public int getDraft() {
		return draft;
	}


	public void setDraft(int draft) {
		this.draft = draft;
	}


	public int getApproved() {
		return approved;
	}


	public void setApproved(int approved) {
		this.approved = approved;
	}


	public int getSubmitted() {
		return submitted;
	}


	public void setSubmitted(int submitted) {
		this.submitted = submitted;
	}
    
    


}
