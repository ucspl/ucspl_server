
package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "freq_uom_of_master_instrument_table")
public class FrquencyUomOfMasterInstruments {

	private long freqUomId;
	private String freqUomName;
	private String description;
	private long status;
	private String createdBy;
	private String dateCreated;

	public FrquencyUomOfMasterInstruments() {
		// TODO Auto-generated constructor stub

	}

	public FrquencyUomOfMasterInstruments(long freqUomId, String freqUomName, String description, long status,
			String createdBy, String dateCreated) {
		super();
		this.freqUomId = freqUomId;
		this.freqUomName = freqUomName;
		this.description = description;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}



	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "freq_uom_id", nullable = false)
	public long getFreqUomId() {
		return freqUomId;
	}

	public void setFreqUomId(long freqUomId) {
		this.freqUomId = freqUomId;
	}
	
	@Column(name = "freq_uom_name", nullable = false)
	public String getFreqUomName() {
		return freqUomName;
	}

	public void setFreqUomName(String freqUomName) {
		this.freqUomName = freqUomName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getStatus() {
		return status;
	}

	public void setStatus(long status) {
		this.status = status;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "date_created")
	public String getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}

}
