
package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "create_proforma_document_table")
public class CreateProformaDocument {

	private long id;
	private long documentTypeId;
	private String proformaDocumentNumber;
	private String requestNumber;
	private long branchId;
	private String invoiceTypeId;
	private long custShippedTo;
	private long custBilledTo;
	private String invoiceDate;
	private String poDate;
	private String dcDate;
	private String poNo;
	private String dcNo;
	private String vendorCodeNumber;
	private String archieveDate;
	private String createdBy;
	private int draft;
	private int approved;
	private int submitted;
	private int archieved;
	private int rejected;
	private String remark;

	public CreateProformaDocument() {

	}

	public CreateProformaDocument(long id, long documentTypeId, String proformaDocumentNumber, String requestNumber,
			String customerDcNumber, long branchId, String invoiceTypeId, long custShippedTo,
			long custBilledTo, String invoiceDate, String poDate, String dcDate, String poNo,
			String vendorCodeNumber, String archieveDate, String createdBy, int draft, int approved, int submitted,
			int archieved, int rejected, String remark) {
		super();
		this.id = id;
		this.documentTypeId = documentTypeId;
		this.proformaDocumentNumber = proformaDocumentNumber;
		this.requestNumber = requestNumber;
		this.branchId = branchId;
		this.invoiceTypeId = invoiceTypeId;
		this.custShippedTo = custShippedTo;
		this.custBilledTo = custBilledTo;
		this.invoiceDate = invoiceDate;
		this.poDate = poDate;
		this.dcDate = dcDate;
		this.poNo = poNo;
		this.dcNo = dcNo;
		this.vendorCodeNumber = vendorCodeNumber;
		this.archieveDate = archieveDate;
		this.createdBy = createdBy;
		this.draft = draft;
		this.approved = approved;
		this.submitted = submitted;
		this.archieved = archieved;
		this.rejected = rejected;
		this.remark = remark;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "create_proforma_document_id", nullable = false)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "document_type_id", nullable = false)
	public long getDocumentTypeId() {
		return documentTypeId;
	}

	public void setDocumentTypeId(long documentTypeId) {
		this.documentTypeId = documentTypeId;
	}
	

	@Column(name = "proforma_document_number")
	public String getProformaDocumentNumber() {
		return proformaDocumentNumber;
	}

	public void setProformaDocumentNumber(String proformaDocumentNumber) {
		this.proformaDocumentNumber = proformaDocumentNumber;
	}


	@Column(name = "request_number")
	public String getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	@Column(name = "branch_id")
	public long getBranchId() {
		return branchId;
	}

	public void setBranchId(long branchId) {
		this.branchId = branchId;
	}

	@Column(name = "invoice_type_id")
	public String getInvoiceTypeId() {
		return invoiceTypeId;
	}


	public void setInvoiceTypeId(String invoiceTypeId) {
		this.invoiceTypeId = invoiceTypeId;
	}
	

	@Column(name = "cust_shipped_to")
	public long getCustShippedTo() {
		return custShippedTo;
	}

	public void setCustShippedTo(long custShippedTo) {
		this.custShippedTo = custShippedTo;
	}

	@Column(name = "cust_billed_to")
	public long getCustBilledTo() {
		return custBilledTo;
	}

	public void setCustBilledTo(long custBilledTo) {
		this.custBilledTo = custBilledTo;
	}

	@Column(name = "invoice_date")
    public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	@Column(name = "po_date")
	public String getPoDate() {
		return poDate;
	}


	public void setPoDate(String poDate) {
		this.poDate = poDate;
	}

	@Column(name = "dc_date")
	public String getDcDate() {
		return dcDate;
	}

	public void setDcDate(String dcDate) {
		this.dcDate = dcDate;
	}

	@Column(name = "po_no")
	public String getPoNo() {
		return poNo;
	}

	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}

	@Column(name = "dc_no")
	public String getDcNo() {
		return dcNo;
	}

	public void setDcNo(String dcNo) {
		this.dcNo = dcNo;
	}

	@Column(name = "vendor_code_number")
	public String getVendorCodeNumber() {
		return vendorCodeNumber;
	}

	public void setVendorCodeNumber(String vendorCodeNumber) {
		this.vendorCodeNumber = vendorCodeNumber;
	}

	@Column(name = "archieve_date")
	public String getArchieveDate() {
		return archieveDate;
	}

	public void setArchieveDate(String archieveDate) {
		this.archieveDate = archieveDate;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public int getDraft() {
		return draft;
	}


	public void setDraft(int draft) {
		this.draft = draft;
	}


	public int getApproved() {
		return approved;
	}


	public void setApproved(int approved) {
		this.approved = approved;
	}


	public int getSubmitted() {
		return submitted;
	}


	public void setSubmitted(int submitted) {
		this.submitted = submitted;
	}


	public int getArchieved() {
		return archieved;
	}


	public void setArchieved(int archieved) {
		this.archieved = archieved;
	}


	public int getRejected() {
		return rejected;
	}


	public void setRejected(int rejected) {
		this.rejected = rejected;
	}


	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
