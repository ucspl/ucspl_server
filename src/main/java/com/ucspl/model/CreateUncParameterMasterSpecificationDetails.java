package com.ucspl.model;

import javax.persistence.*;

@Entity
@Table(name="create_unc_parameter_master_specification_table")
public class CreateUncParameterMasterSpecificationDetails {

	
	private long createUncParameterMasterSpecificationId;
	private long uncMasterInstruId;
	private String parameterNumber;
	private long  sourceOfUncertainity;
	private long  probabiltyDistribution;
	private long type;
	private long dividingFactor;
	private String sensitivityCoefficient;
	private String valueForEstimate;
	private String percentageForEstimateAg;
	private String percentageForEstimateDg;
	private long degreeOfFreedom;
	private long masterType;
	private long masterNumber;
	
	
	public CreateUncParameterMasterSpecificationDetails() {
		
	}


	public CreateUncParameterMasterSpecificationDetails(long createUncParameterMasterSpecificationId,
			long uncMasterInstruId, String parameterNumber, long sourceOfUncertainity, long probabiltyDistribution,
			long type, long dividingFactor, String sensitivityCoefficient, String valueForEstimate,
			String percentageForEstimateAg, String percentageForEstimateDg, long degreeOfFreedom, long masterType,
			long masterNumber) {
		super();
		this.createUncParameterMasterSpecificationId = createUncParameterMasterSpecificationId;
		this.uncMasterInstruId = uncMasterInstruId;
		this.parameterNumber = parameterNumber;
		this.sourceOfUncertainity = sourceOfUncertainity;
		this.probabiltyDistribution = probabiltyDistribution;
		this.type = type;
		this.dividingFactor = dividingFactor;
		this.sensitivityCoefficient = sensitivityCoefficient;
		this.valueForEstimate = valueForEstimate;
		this.percentageForEstimateAg = percentageForEstimateAg;
		this.percentageForEstimateDg = percentageForEstimateDg;
		this.degreeOfFreedom = degreeOfFreedom;
		this.masterType = masterType;
		this.masterNumber = masterNumber;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "create_unc_para_master_specification_id", nullable = false)
	public long getCreateUncParameterMasterSpecificationId() {
		return createUncParameterMasterSpecificationId;
	}


	public void setCreateUncParameterMasterSpecificationId(long createUncParameterMasterSpecificationId) {
		this.createUncParameterMasterSpecificationId = createUncParameterMasterSpecificationId;
	}

	@Column(name = "unc_master_id")
	public long getUncMasterInstruId() {
		return uncMasterInstruId;
	}


	public void setUncMasterInstruId(long uncMasterInstruId) {
		this.uncMasterInstruId = uncMasterInstruId;
	}
 
	@Column(name = "parameter_number")
	public String getParameterNumber() {
		return parameterNumber;
	}


	public void setParameterNumber(String parameterNumber) {
		this.parameterNumber = parameterNumber;
	}

	@Column(name = "unc_source_of_uncertainity_id")
	public long getSourceOfUncertainity() {
		return sourceOfUncertainity;
	}


	public void setSourceOfUncertainity(long sourceOfUncertainity) {
		this.sourceOfUncertainity = sourceOfUncertainity;
	}

	@Column(name = "unc_probability_distribution_id")
	public long getProbabiltyDistribution() {
		return probabiltyDistribution;
	}


	public void setProbabiltyDistribution(long probabiltyDistribution) {
		this.probabiltyDistribution = probabiltyDistribution;
	}

	@Column(name = "unc_type_id")
	public long getType() {
		return type;
	}


	public void setType(long type) {
		this.type = type;
	}

	@Column(name = "unc_dividing_factor_id")
	public long getDividingFactor() {
		return dividingFactor;
	}


	public void setDividingFactor(long dividingFactor) {
		this.dividingFactor = dividingFactor;
	}

	@Column(name = "sensitivity_coefficient")
	public String getSensitivityCoefficient() {
		return sensitivityCoefficient;
	}


	public void setSensitivityCoefficient(String sensitivityCoefficient) {
		this.sensitivityCoefficient = sensitivityCoefficient;
	}

	@Column(name = "value_for_estimate")
	public String getValueForEstimate() {
		return valueForEstimate;
	}


	public void setValueForEstimate(String valueForEstimate) {
		this.valueForEstimate = valueForEstimate;
	}

	@Column(name = "percentage_for_estimate_ag")
	public String getPercentageForEstimateAg() {
		return percentageForEstimateAg;
	}


	public void setPercentageForEstimateAg(String percentageForEstimateAg) {
		this.percentageForEstimateAg = percentageForEstimateAg;
	}

	@Column(name = "percentage_for_estimate_dg")
	public String getPercentageForEstimateDg() {
		return percentageForEstimateDg;
	}


	public void setPercentageForEstimateDg(String percentageForEstimateDg) {
		this.percentageForEstimateDg = percentageForEstimateDg;
	}

	@Column(name = "unc_degree_of_freedom_id")
	public long getDegreeOfFreedom() {
		return degreeOfFreedom;
	}


	public void setDegreeOfFreedom(long degreeOfFreedom) {
		this.degreeOfFreedom = degreeOfFreedom;
	}

	@Column(name = "unc_master_type_id")
	public long getMasterType() {
		return masterType;
	}


	public void setMasterType(long masterType) {
		this.masterType = masterType;
	}

	@Column(name = "unc_master_number_id")
	public long getMasterNumber() {
		return masterNumber;
	}


	public void setMasterNumber(long masterNumber) {
		this.masterNumber = masterNumber;
	}
	
	
	
}
