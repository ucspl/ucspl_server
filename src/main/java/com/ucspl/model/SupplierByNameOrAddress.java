package com.ucspl.model;

public class SupplierByNameOrAddress {

	
	private String name;
	private String nameAnd;
	private String address;
	private int status;
	
	public SupplierByNameOrAddress() {
		
	}

	public SupplierByNameOrAddress(String name, String nameAnd, String address, int status) {
		super();
		this.name = name;
		this.nameAnd = nameAnd;
		this.address = address;
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameAnd() {
		return nameAnd;
	}

	public void setNameAnd(String nameAnd) {
		this.nameAnd = nameAnd;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "SupplierByNameOrAddress [name=" + name + ", nameAnd=" + nameAnd + ", address=" + address + ", status="
				+ status + "]";
	}
	
	
	
}
