package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="junction_of_uom_and_createuuc_para_specification_table")
public class JunctionOfUomAndCreateUUCParaSpecification {
	
	private long junctionOfUomAndCreateUUCParaSpecId;
	private long createUucId;
	private long createUucParameterMasterSpecificationId;
	private long uomId;
	
	public JunctionOfUomAndCreateUUCParaSpecification() {
		
	}
	
	
	public JunctionOfUomAndCreateUUCParaSpecification(long junctionOfUomAndCreateUUCParaSpecId, long createUucId,
			long createUucParameterMasterSpecificationId, long uomId) {
		super();
		this.junctionOfUomAndCreateUUCParaSpecId = junctionOfUomAndCreateUUCParaSpecId;
		this.createUucId = createUucId;
		this.createUucParameterMasterSpecificationId = createUucParameterMasterSpecificationId;
		this.uomId = uomId;
	}


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "junction_uom_and_createuucparaspec_id", nullable = false)
	public long getJunctionOfUomAndCreateUUCParaSpecId() {
		return junctionOfUomAndCreateUUCParaSpecId;
	}


	public void setJunctionOfUomAndCreateUUCParaSpecId(long junctionOfUomAndCreateUUCParaSpecId) {
		this.junctionOfUomAndCreateUUCParaSpecId = junctionOfUomAndCreateUUCParaSpecId;
	}

	@Column(name = "create_uuc_id")
	public long getCreateUucId() {
		return createUucId;
	}


	public void setCreateUucId(long createUucId) {
		this.createUucId = createUucId;
	}

	@Column(name = "create_uuc_paraspec_id")
	public long getCreateUucParameterMasterSpecificationId() {
		return createUucParameterMasterSpecificationId;
	}

	public void setCreateUucParameterMasterSpecificationId(long createUucParameterMasterSpecificationId) {          
		this.createUucParameterMasterSpecificationId = createUucParameterMasterSpecificationId;
	}

	@Column(name = "uom_id")
	public long getUomId() {
		return uomId;
	}


	public void setUomId(long uomId) {
		this.uomId = uomId;
	}
	
	

}
