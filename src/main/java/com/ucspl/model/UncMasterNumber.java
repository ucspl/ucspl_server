package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="unc_master_number_table")
public class UncMasterNumber {

	
	private long uncMasterNumberId;
	private String uncMasterNumberName; 
	private String description;
	private int status;
	private String createdBy;
	private String dateCreated;
	
	
	public UncMasterNumber() {
		
	}


	public UncMasterNumber(long uncMasterNumberId, String uncMasterNumberName, String description, int status,
			String createdBy, String dateCreated) {
		super();
		this.uncMasterNumberId = uncMasterNumberId;
		this.uncMasterNumberName = uncMasterNumberName;
		this.description = description;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "master_number_id", nullable = false)
	public long getUncMasterNumberId() {
		return uncMasterNumberId;
	}


	public void setUncMasterNumberId(long uncMasterNumberId) {
		this.uncMasterNumberId = uncMasterNumberId;
	}

	
	
	@Column(name = "master_number_name")
	public String getUncMasterNumberName() {
		return uncMasterNumberName;
	}


	public void setUncMasterNumberName(String uncMasterNumberName) {
		this.uncMasterNumberName = uncMasterNumberName;
	}
	

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "date_created")
	public String getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	
	
}
