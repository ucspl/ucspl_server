
package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "nabl_scope_name_table")
public class NablScopeName {

	private long nablScopeNameId;
	private String nablScopeName;
	private String description;
	private long status;
	private String createdBy;
	private String dateCreated;

	public NablScopeName() {
		// TODO Auto-generated constructor stub

	}

	public NablScopeName(long nablScopeNameId, String nablScopeName, String description, long status, String createdBy,
			String dateCreated) {
		super();
		this.nablScopeNameId = nablScopeNameId;
		this.nablScopeName = nablScopeName;
		this.description = description;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "nabl_scope_name_id", nullable = false)
	public long getNablScopeNameId() {
		return nablScopeNameId;
	}

	public void setNablScopeNameId(long nablScopeNameId) {
		this.nablScopeNameId = nablScopeNameId;
	}

	@Column(name = "nabl_scope_name", nullable = false)
	public String getNablScopeName() {
		return nablScopeName;
	}

	public void setNablScopeName(String nablScopeName) {
		this.nablScopeName = nablScopeName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getStatus() {
		return status;
	}

	public void setStatus(long status) {
		this.status = status;
	}

	@Column(name = "created_by", nullable = false)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column(name = "date_created", nullable = false)
	public String getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}

	
}
