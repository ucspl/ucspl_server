

package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="unc_master_name_table")
public class UncMasterName {

	
	private long uncMasterNameId;
	private String uncMasterName; 
	private String description;
	private int status;
	private String createdBy;
	private String dateCreated;
	
	
	public UncMasterName() {
		
	}


	public UncMasterName(long uncMasterNameId, String uncMasterName, String description, int status,
			String createdBy, String dateCreated) {
		super();
		this.uncMasterNameId = uncMasterNameId;
		this.uncMasterName = uncMasterName;
		this.description = description;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "unc_master_name_id", nullable = false)
	public long getUncMasterNameId() {
		return uncMasterNameId;
	}

	public void setUncMasterNameId(long uncMasterNameId) {
		this.uncMasterNameId = uncMasterNameId;
	}

	
	
	@Column(name = "unc_master_name")
	public String getUncMasterName() {
		return uncMasterName;
	}


	public void setUncMasterName(String uncMasterName) {
		this.uncMasterName = uncMasterName;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "date_created")
	public String getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	
	
	
}
