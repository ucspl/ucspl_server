package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="unc_master_type_table")
public class UncMasterType {

	
	private long uncMasterTypeId;
	private String uncMasterTypeName;
	private String description;
	private int status;
	private String createdBy;
	private String dateCreated;
	
	
	public UncMasterType() {
		
	}


	public UncMasterType(long uncMasterTypeId, String uncMasterTypeName, String description, int status,
			String createdBy, String dateCreated) {
		super();
		this.uncMasterTypeId = uncMasterTypeId;
		this.uncMasterTypeName = uncMasterTypeName;
		this.description = description;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "master_type_id", nullable = false)
	public long getUncMasterTypeId() {
		return uncMasterTypeId;
	}


	public void setUncMasterTypeId(long uncMasterTypeId) {
		this.uncMasterTypeId = uncMasterTypeId;
	}

	@Column(name = "master_type_name")
	public String getUncMasterTypeName() {
		return uncMasterTypeName;
	}


	public void setUncMasterTypeName(String uncMasterTypeName) {
		this.uncMasterTypeName = uncMasterTypeName;
	}


	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "date_created")
	public String getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	
	
}
