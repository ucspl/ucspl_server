package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;

public class SearchSalesAndQuotation {

	private String searchDocument;
	private int draft;
	private int approved;
	private int submitted;
	private int archieved;
	private int rejected;
	private java.util.Date fromDate;
	private java.util.Date toDate;
	
	

	public SearchSalesAndQuotation() {

	}

	
	public SearchSalesAndQuotation(String searchDocument, int draft, int approved, int submitted, int archieved,
			int rejected, Date fromDate, Date toDate) {
		super();
		this.searchDocument = searchDocument;
		this.draft = draft;
		this.approved = approved;
		this.submitted = submitted;
		this.archieved = archieved;
		this.rejected = rejected;
		this.fromDate = fromDate;
		this.toDate = toDate;
	}


	@Column(name = "searchdocument", nullable = false)
	public String getSearchDocument() {
		return searchDocument;
	}

	public void setSearchDocument(String searchDocument) {
		this.searchDocument = searchDocument;
	}



	public int getDraft() {
		return draft;
	}



	public void setDraft(int draft) {
		this.draft = draft;
	}



	public int getApproved() {
		return approved;
	}



	public void setApproved(int approved) {
		this.approved = approved;
	}



	public int getSubmitted() {
		return submitted;
	}



	public void setSubmitted(int submitted) {
		this.submitted = submitted;
	}



	public int getArchieved() {
		return archieved;
	}



	public void setArchieved(int archieved) {
		this.archieved = archieved;
	}



	public int getRejected() {
		return rejected;
	}



	public void setRejected(int rejected) {
		this.rejected = rejected;
	}


	public java.util.Date getFromDate() {
		return fromDate;
	}


	public void setFromDate(java.util.Date fromDate) {
		this.fromDate = fromDate;
	}


	public java.util.Date getToDate() {
		return toDate;
	}


	public void setToDate(java.util.Date toDate) {
		this.toDate = toDate;
	}


	@Override
	public String toString() {
		return "SearchSalesAndQuotation [searchDocument=" + searchDocument + ", draft=" + draft + ", approved="
				+ approved + ", submitted=" + submitted + ", archieved=" + archieved + ", rejected=" + rejected
				+ ", fromDate=" + fromDate + ", toDate=" + toDate + "]";
	}
	
	
	
	
}