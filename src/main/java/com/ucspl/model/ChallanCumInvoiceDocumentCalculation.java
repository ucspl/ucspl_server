package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "challan_cum_invoice_document_calculation_table")
public class ChallanCumInvoiceDocumentCalculation {

	private long id;
	private String invDocumentNumber;
	private double subTotal;
	private double total;
	private double discountOnSubtotal;
	private double cgst;
	private double sgst;
	private double igst;
	private double net;

	public ChallanCumInvoiceDocumentCalculation() {

	}

	public ChallanCumInvoiceDocumentCalculation(long id, String invDocumentNumber, double subTotal, double total,
			double discountOnSubtotal, double cgst, double sgst, double igst, double net) {
		super();
		this.id = id;
		this.invDocumentNumber = invDocumentNumber;
		this.subTotal = subTotal;
		this.total = total;
		this.discountOnSubtotal = discountOnSubtotal;
		this.cgst = cgst;
		this.sgst = sgst;
		this.igst = igst;
		this.net = net;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "inv_document_number", nullable = false)
	public String getInvDocumentNumber() {
		return invDocumentNumber;
	}

	public void setInvDocumentNumber(String invDocumentNumber) {
		this.invDocumentNumber = invDocumentNumber;
	}

	@Column(name = "subtotal", nullable = false)
	public double getSubTotal() {
		return subTotal;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "challan_cum_invoice_calculation_id", nullable = false)
	public long getId() {
		return id;
	}

	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	@Column(name = "discount_on_subtotal", nullable = false)
	public double getDiscountOnSubtotal() {
		return discountOnSubtotal;
	}

	public void setDiscountOnSubtotal(double discountOnSubtotal) {
		this.discountOnSubtotal = discountOnSubtotal;
	}

	public double getCgst() {
		return cgst;
	}

	public void setCgst(double cgst) {
		this.cgst = cgst;
	}

	public double getSgst() {
		return sgst;
	}

	public void setSgst(double sgst) {
		this.sgst = sgst;
	}

	public double getIgst() {
		return igst;
	}

	public void setIgst(double igst) {
		this.igst = igst;
	}

	public double getNet() {
		return net;
	}

	public void setNet(double net) {
		this.net = net;
	}

}
