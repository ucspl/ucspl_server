package com.ucspl.model;

public class CustomerTaxId {

	private long customerId;
	private long taxId;
	
	public CustomerTaxId() {
		
	}

	public CustomerTaxId(long customerId, long taxId) {
		super();
		this.customerId = customerId;
		this.taxId = taxId;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public long getTaxId() {
		return taxId;
	}

	public void setTaxId(long taxId) {
		this.taxId = taxId;
	}
	
	
	
}
