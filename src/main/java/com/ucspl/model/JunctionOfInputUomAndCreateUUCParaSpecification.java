package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "junction_of_input_uom_and_createuuc_para_specification_table")
public class JunctionOfInputUomAndCreateUUCParaSpecification {

	private long junctionOfInputUomAndCreateUUCParaSpecId;
	private long createUucId;
	private long createUucParameterMasterSpecificationId;
	private long uomId;

	public JunctionOfInputUomAndCreateUUCParaSpecification() {

	}

	public JunctionOfInputUomAndCreateUUCParaSpecification(long junctionOfInputUomAndCreateUUCParaSpecId,
			long createUucId, long createUucParameterMasterSpecificationId, long uomId) {
		super();
		this.junctionOfInputUomAndCreateUUCParaSpecId = junctionOfInputUomAndCreateUUCParaSpecId;
		this.createUucId = createUucId;
		this.createUucParameterMasterSpecificationId = createUucParameterMasterSpecificationId;
		this.uomId = uomId;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "junction_input_uom_and_createuucparaspec_id", nullable = false)
	public long getJunctionOfInputUomAndCreateUUCParaSpecId() {
		return junctionOfInputUomAndCreateUUCParaSpecId;
	}

	public void setJunctionOfInputUomAndCreateUUCParaSpecId(long junctionOfInputUomAndCreateUUCParaSpecId) {
		this.junctionOfInputUomAndCreateUUCParaSpecId = junctionOfInputUomAndCreateUUCParaSpecId;
	}

	@Column(name = "create_uuc_id")
	public long getCreateUucId() {
		return createUucId;
	}

	public void setCreateUucId(long createUucId) {
		this.createUucId = createUucId;
	}

	@Column(name = "create_uuc_paraspec_id")
	public long getCreateUucParameterMasterSpecificationId() {
		return createUucParameterMasterSpecificationId;
	}

	public void setCreateUucParameterMasterSpecificationId(long createUucParameterMasterSpecificationId) {
		this.createUucParameterMasterSpecificationId = createUucParameterMasterSpecificationId;
	}

	@Column(name = "ip_uom_id")
	public long getUomId() {
		return uomId;
	}

	public void setUomId(long uomId) {
		this.uomId = uomId;
	}

}
