package com.ucspl.model;

public class SearchAttachmentByMastIdAndCalCertId {
	
	private long mastId;
	private long mastCalCertId;
	
	public SearchAttachmentByMastIdAndCalCertId() {
		
	}
	
	public SearchAttachmentByMastIdAndCalCertId(long mastId, long mastCalCertId) {
		super();
		this.mastId = mastId;
		this.mastCalCertId = mastCalCertId;
	}
	public long getMastId() {
		return mastId;
	}
	public void setMastId(long mastId) {
		this.mastId = mastId;
	}
	public long getMastCalCertId() {
		return mastCalCertId;
	}
	public void setMastCalCertId(long mastCalCertId) {
		this.mastCalCertId = mastCalCertId;
	}
	
	

}
