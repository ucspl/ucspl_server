
package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "srno_proforma_document_table")
public class  SrNoProformaDocument {

	private long id;
	private String proformaDocumentNumber;
	private String invoiceNumber;
	private int srNo;
	private String poSrNo;
	private String itemOrPartCode;
	private int instruId;
	private String description;
	private String range;
	private String sacCode;
	private String hsnNo;
	private long quantity;
	private double unitPrice;
	private double amount;

	public SrNoProformaDocument() {

	}

	public SrNoProformaDocument(long id, String proformaDocumentNumber, String invoiceNumber, int srNo, String poSrNo,
			String itemOrPartCode, int instruId, String description, String range, String sacCode, String hsnNo,
			long quantity, double unitPrice, double amount) {
		super();
		this.id = id;
		this.proformaDocumentNumber = proformaDocumentNumber;
		this.invoiceNumber = invoiceNumber;
		this.srNo = srNo;
		this.poSrNo = poSrNo;
		this.itemOrPartCode = itemOrPartCode;
		this.instruId = instruId;
		this.description = description;
		this.range = range;
		this.sacCode = sacCode;
		this.hsnNo = hsnNo;
		this.quantity = quantity;
		this.unitPrice = unitPrice;
		this.amount = amount;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "srno_proforma_document_id", nullable = false)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "proforma_document_number", nullable = false)
	public String getProformaDocumentNumber() {
		return proformaDocumentNumber;
	}


	public void setProformaDocumentNumber(String proformaDocumentNumber) {
		this.proformaDocumentNumber = proformaDocumentNumber;
	}

	
	@Column(name = "invoice_number", nullable = false)
	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	@Column(name = "sr_no", nullable = false)
	public int getSrNo() {
		return srNo;
	}

	public void setSrNo(int srNo) {
		this.srNo = srNo;
	}
	
	@Column(name = "po_sr_no", nullable = false)
	public String getPoSrNo() {
		return poSrNo;
	}

	public void setPoSrNo(String poSrNo) {
		this.poSrNo = poSrNo;
	}

	@Column(name = "item_or_part_code", nullable = false)
	public String getItemOrPartCode() {
		return itemOrPartCode;
	}

	public void setItemOrPartCode(String itemOrPartCode) {
		this.itemOrPartCode = itemOrPartCode;
	}

	@Column(name = "instru_id", nullable = false)
	public int getInstruId() {
		return instruId;
	}

	public void setInstruId(int instruId) {
		this.instruId = instruId;
	}


	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "range", nullable = false)
	public String getRange() {
		return range;
	}

	public void setRange(String range) {
		this.range = range;
	}

	@Column(name = "sac_code", nullable = false)
	public String getSacCode() {
		return sacCode;
	}

	public void setSacCode(String sacCode) {
		this.sacCode = sacCode;
	}

	@Column(name = "quantity", nullable = false)
	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	@Column(name = "unitprice", nullable = false)
	public double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}

	@Column(name = "amount", nullable = false)
	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Column(name = "hsn_number")
	public String getHsnNo() {
		return hsnNo;
	}

	public void setHsnNo(String hsnNo) {
		this.hsnNo = hsnNo;
	}

	
}
