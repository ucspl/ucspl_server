
package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity(name = "create_customer")
@Table(name = "create_customer")
public class Customer {

	private long id;
	private int isParent;
	private long parentReference;
	private String name;
	private String department;
	private String panNo;
	private String gstNo;
	private String countryPrefixForMobile1;
	private String countryPrefixForMobile2;
	private String mobileNumber1;
	private String mobileNumber2;
	private String landlineNumber;
	private String email1;
	private String email2;
	private String addressLine1;
	private String addressLine2;
	private String country;
	private String state;
	private String city;
	private long pin;
	private long regionAreaCode;
	private long billingCreditPeriod;
	private int status;
	private String vendorCode;
	private int passFailRemark;
	private String taxes;
	private String applicableTaxes;
	private long taxpayerType;
	private int cgst;
	private int igst;
	private int sgst;
	private String sezNote;
	private int calibratedProcedure;
	private int referencedUsed;
	private int expandedUncertainty;
	private int note;
	private int lut;
	private int sez;
	private int masterlhs;
	private int uuclhs;
	private int parentCustomer;
	private int customFields;
	private String checkedBy;
	private String createdBy;
	private String modifiedBy;
	private java.util.Date dateCreated;
	private java.util.Date dateModified;
	
	@ManyToOne
	@JoinColumn(name = "region_area_code_id", nullable = false)
	private RegionAreaCode regionAreaCodes;

	
	@ManyToOne
	@JoinColumn(name = "billing_credit_period_id", nullable = false)
	private BillingCreditPeriod billingCreditPeriods;

	
	@ManyToOne
	@JoinColumn(name = "taxpayer_type_id", nullable = false)
	private TaxpayerTypeForCustomer taxpayerTypeForCustomer;
	
	public Customer() {

	}

	
	
	

	public Customer(long id, int isParent, long parentReference, String name, String department, String panNo,
			String gstNo, String countryPrefixForMobile1, String countryPrefixForMobile2, String mobileNumber1,
			String mobileNumber2, String landlineNumber, String email1, String email2, String addressLine1,
			String addressLine2, String country, String state, String city, long pin, long regionAreaCode,
			long billingCreditPeriod, int status, String vendorCode, int passFailRemark, String taxes,
			String applicableTaxes, long taxpayerType, int cgst, int igst, int sgst, String sezNote,
			int calibratedProcedure, int referencedUsed, int expandedUncertainty, int note, int lut, int sez,
			int masterlhs, int uuclhs, int parentCustomer, int customFields, String checkedBy, String createdBy,
			String modifiedBy, Date dateCreated, Date dateModified, RegionAreaCode regionAreaCodes,
			BillingCreditPeriod billingCreditPeriods, TaxpayerTypeForCustomer taxpayerTypeForCustomer) {
		super();
		this.id = id;
		this.isParent = isParent;
		this.parentReference = parentReference;
		this.name = name;
		this.department = department;
		this.panNo = panNo;
		this.gstNo = gstNo;
		this.countryPrefixForMobile1 = countryPrefixForMobile1;
		this.countryPrefixForMobile2 = countryPrefixForMobile2;
		this.mobileNumber1 = mobileNumber1;
		this.mobileNumber2 = mobileNumber2;
		this.landlineNumber = landlineNumber;
		this.email1 = email1;
		this.email2 = email2;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.country = country;
		this.state = state;
		this.city = city;
		this.pin = pin;
		this.regionAreaCode = regionAreaCode;
		this.billingCreditPeriod = billingCreditPeriod;
		this.status = status;
		this.vendorCode = vendorCode;
		this.passFailRemark = passFailRemark;
		this.taxes = taxes;
		this.applicableTaxes = applicableTaxes;
		this.taxpayerType = taxpayerType;
		this.cgst = cgst;
		this.igst = igst;
		this.sgst = sgst;
		this.sezNote = sezNote;
		this.calibratedProcedure = calibratedProcedure;
		this.referencedUsed = referencedUsed;
		this.expandedUncertainty = expandedUncertainty;
		this.note = note;
		this.lut = lut;
		this.sez = sez;
		this.masterlhs = masterlhs;
		this.uuclhs = uuclhs;
		this.parentCustomer = parentCustomer;
		this.customFields = customFields;
		this.checkedBy = checkedBy;
		this.createdBy = createdBy;
		this.modifiedBy = modifiedBy;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
		this.regionAreaCodes = regionAreaCodes;
		this.billingCreditPeriods = billingCreditPeriods;
		this.taxpayerTypeForCustomer = taxpayerTypeForCustomer;
	}





	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "is_parent", nullable = false)
	public int getIsParent() {
		return isParent;
	}


	public void setIsParent(int isParent) {
		this.isParent = isParent;
	}

	@Column(name = "parent_reference", nullable = false)
	public long getParentReference() {
		return parentReference;
	}


	public void setParentReference(long parentReference) {
		this.parentReference = parentReference;
	}



	@Column(name = "name", nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "location", nullable = false)
	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	@Column(name = "pan_no", nullable = false)
	public String getPanNo() {
		return panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	@Column(name = "gst_no", nullable = false)
	public String getGstNo() {
		return gstNo;
	}

	public void setGstNo(String gstNo) {
		this.gstNo = gstNo;
	}
	
	@Column(name = "country_prefix_for_mobile_no1")
	public String getCountryPrefixForMobile1() {
		return countryPrefixForMobile1;
	}

	public void setCountryPrefixForMobile1(String countryPrefixForMobile1) {
		this.countryPrefixForMobile1 = countryPrefixForMobile1;
	}

	@Column(name = "country_prefix_for_mobile_no2")
	public String getCountryPrefixForMobile2() {
		return countryPrefixForMobile2;
	}

	public void setCountryPrefixForMobile2(String countryPrefixForMobile2) {
		this.countryPrefixForMobile2 = countryPrefixForMobile2;
	}
	
	
	@Column(name = "mobilenumber1")
	public String getMobileNumber1() {
		return mobileNumber1;
	}


	public void setMobileNumber1(String mobileNumber1) {
		this.mobileNumber1 = mobileNumber1;
	}

	@Column(name = "mobilenumber2")
	public String getMobileNumber2() {
		return mobileNumber2;
	}

	public void setMobileNumber2(String mobileNumber2) {
		this.mobileNumber2 = mobileNumber2;
	}

	@Column(name = "landlinenumber")
	public String getLandlineNumber() {
		return landlineNumber;
	}

	public void setLandlineNumber(String landlineNumber) {
		this.landlineNumber = landlineNumber;
	}

	public String getEmail1() {
		return email1;
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	@Column(name = "addressline1", nullable = false)
	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	@Column(name = "addressline2")
	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	@Column(name = "country", nullable = false)
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Column(name = "state", nullable = false)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "city", nullable = false)
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "pin", nullable = false)
	public long getPin() {
		return pin;
	}

	public void setPin(long pin) {
		this.pin = pin;
	}

	@Column(name = "region_area_code_id", nullable = false)
	public long getRegionAreaCode() {
		return regionAreaCode;
	}

	public void setRegionAreaCode(long regionAreaCode) {
		this.regionAreaCode = regionAreaCode;
	}

	@Column(name = "billing_credit_period_id", nullable = false)
	public long getBillingCreditPeriod() {
		return billingCreditPeriod;
	}

	public void setBillingCreditPeriod(long billingCreditPeriod) {
		this.billingCreditPeriod = billingCreditPeriod;
	}

	@Column(name = "status", nullable = false)
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name = "vendor_code")
	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	@Column(name = "pass_fail_remark")
	public int getPassFailRemark() {
		return passFailRemark;
	}

	public void setPassFailRemark(int passFailRemark) {
		this.passFailRemark = passFailRemark;
	}

	@Column(name = "taxes")
	public String getTaxes() {
		return taxes;
	}

	public void setTaxes(String taxes) {
		this.taxes = taxes;
	}

	@Column(name = "applicable_taxes")
	public String getApplicableTaxes() {
		return applicableTaxes;
	}

	public void setApplicableTaxes(String applicableTaxes) {
		this.applicableTaxes = applicableTaxes;
	}

	
	@Column(name = "taxpayer_type_id")
	public long getTaxpayerType() {
		return taxpayerType;
	}



	public void setTaxpayerType(long taxpayerType) {
		this.taxpayerType = taxpayerType;
	}



	public int getCgst() {
		return cgst;
	}

	public void setCgst(int cgst) {
		this.cgst = cgst;
	}

	public int getIgst() {
		return igst;
	}

	public void setIgst(int igst) {
		this.igst = igst;
	}

	public int getSgst() {
		return sgst;
	}

	public void setSgst(int sgst) {
		this.sgst = sgst;
	}

	@Column(name = "sez_note")
	public String getSezNote() {
		return sezNote;
	}

	public void setSezNote(String sezNote) {
		this.sezNote = sezNote;
	}

	@Column(name = "calibrated_procedure")
	public int getCalibratedProcedure() {
		return calibratedProcedure;
	}

	public void setCalibratedProcedure(int calibratedProcedure) {
		this.calibratedProcedure = calibratedProcedure;
	}

	@Column(name = "referenced_used")
	public int getReferencedUsed() {
		return referencedUsed;
	}

	public void setReferencedUsed(int referencedUsed) {
		this.referencedUsed = referencedUsed;
	}

	@Column(name = "expanded_uncertainty")
	public int getExpandedUncertainty() {
		return expandedUncertainty;
	}

	public void setExpandedUncertainty(int expandedUncertainty) {
		this.expandedUncertainty = expandedUncertainty;
	}

	@Column(name = "note")
	public int getNote() {
		return note;
	}

	public void setNote(int note) {
		this.note = note;
	}

	@Column(name = "lut")
	public int getLut() {
		return lut;
	}

	public void setLut(int lut) {
		this.lut = lut;
	}

	@Column(name = "sez")
	public int getSez() {
		return sez;
	}

	public void setSez(int sez) {
		this.sez = sez;
	}

	@Column(name = "master_lhs")
	public int getMasterlhs() {
		return masterlhs;
	}

	public void setMasterlhs(int masterlhs) {
		this.masterlhs = masterlhs;
	}

	@Column(name = "uuc_lhs")
	public int getUuclhs() {
		return uuclhs;
	}

	public void setUuclhs(int uuclhs) {
		this.uuclhs = uuclhs;
	}

	@Column(name = "parent_customer")
	public int getParentCustomer() {
		return parentCustomer;
	}

	public void setParentCustomer(int parentCustomer) {
		this.parentCustomer = parentCustomer;
	}

	@Column(name = "custom_fields")
	public int getCustomFields() {
		return customFields;
	}

	public void setCustomFields(int customFields) {
		this.customFields = customFields;
	}

	@Column(name = "checked_by")
	public String getCheckedBy() {
		return checkedBy;
	}

	public void setCheckedBy(String checkedBy) {
		this.checkedBy = checkedBy;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "modified_by")
	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Column(name = "date_created")
	public java.util.Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(java.util.Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	@Column(name = "date_modified")
	public java.util.Date getDateModified() {
		return dateModified;
	}

	public void setDateModified(java.util.Date dateModified) {
		this.dateModified = dateModified;
	}

	
}









