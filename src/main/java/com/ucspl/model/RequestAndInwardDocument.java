

package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "instrument_details_of_request_and_inward_document_table")
public class RequestAndInwardDocument {
	
	private long id;
	 private long custInstruIdentificationId;
	 private long createReqAndIwdTableId;
	 private String reqInwDocumentNo;
	 private String inwardReqNo;
	 private String srNo;
	 private String inwardInstrumentNo;	
	 private String parameterNumber;
	 private long nomenclature;
	 private long parameter;
	 private String rangeSize;
	 private String make;
	 private String idNo;
	 private String frequencyCondition;
	 private String instrumentType;
	 private String rangeFrom;
	 private String rangeTo;
	 private long ruom;
	 private String leastCount;
	 private long lcuom1;
	 private String accuracy;
	 private long formulaAccuracy;
	 private long uomAccuracy;
	 private String accuracy1;
	 private long formulaAccuracy1;
	 private long uomAccuracy1;
	 private String location;
	 private String calibrationFrequency;
	 private String labType;
	 private String dateOfCalibration;
	 private String dateOfIssueCertificate;
	 private String receivedDate;
	 private String orderNo;
	 private String certificateNo;
	 private String certificatePrintMode;
	 private String remark;
	 private String calibrationDueOn;
	 private String calibratedBy;
	 private String authorizedBy;
	 private String reminderDate;
	 private String reminderDateMonth;
	 private String instruCondition;
	 private String assignedTo;
	 private String statusOfOrder;
	 private int multiPara;
	 private boolean active;
	 private int calibrationTag;
	 private int isMultiPara;
	 
	 public RequestAndInwardDocument() {
		 
	 }
	 

	public RequestAndInwardDocument(long id, long custInstruIdentificationId, long createReqAndIwdTableId,
			String reqInwDocumentNo, String inwardReqNo, String srNo, String inwardInstrumentNo, String parameterNumber,
			long nomenclature, long parameter, String rangeSize, String make, String idNo, String frequencyCondition,
			String instrumentType, String rangeFrom, String rangeTo, long ruom, String leastCount, long lcuom1,
			String accuracy, long formulaAccuracy, long uomAccuracy, String accuracy1, long formulaAccuracy1,
			long uomAccuracy1, String location, String calibrationFrequency, String labType, String dateOfCalibration,
			String dateOfIssueCertificate, String receivedDate, String orderNo, String certificateNo,
			String certificatePrintMode, String remark, String calibrationDueOn, String calibratedBy,
			String authorizedBy, String reminderDate, String reminderDateMonth, String instruCondition,
			String assignedTo, String statusOfOrder, int multiPara, boolean active, int calibrationTag,
			int isMultiPara) {
		super();
		this.id = id;
		this.custInstruIdentificationId = custInstruIdentificationId;
		this.createReqAndIwdTableId = createReqAndIwdTableId;
		this.reqInwDocumentNo = reqInwDocumentNo;
		this.inwardReqNo = inwardReqNo;
		this.srNo = srNo;
		this.inwardInstrumentNo = inwardInstrumentNo;
		this.parameterNumber = parameterNumber;
		this.nomenclature = nomenclature;
		this.parameter = parameter;
		this.rangeSize = rangeSize;
		this.make = make;
		this.idNo = idNo;
		this.frequencyCondition = frequencyCondition;
		this.instrumentType = instrumentType;
		this.rangeFrom = rangeFrom;
		this.rangeTo = rangeTo;
		this.ruom = ruom;
		this.leastCount = leastCount;
		this.lcuom1 = lcuom1;
		this.accuracy = accuracy;
		this.formulaAccuracy = formulaAccuracy;
		this.uomAccuracy = uomAccuracy;
		this.accuracy1 = accuracy1;
		this.formulaAccuracy1 = formulaAccuracy1;
		this.uomAccuracy1 = uomAccuracy1;
		this.location = location;
		this.calibrationFrequency = calibrationFrequency;
		this.labType = labType;
		this.dateOfCalibration = dateOfCalibration;
		this.dateOfIssueCertificate = dateOfIssueCertificate;
		this.receivedDate = receivedDate;
		this.orderNo = orderNo;
		this.certificateNo = certificateNo;
		this.certificatePrintMode = certificatePrintMode;
		this.remark = remark;
		this.calibrationDueOn = calibrationDueOn;
		this.calibratedBy = calibratedBy;
		this.authorizedBy = authorizedBy;
		this.reminderDate = reminderDate;
		this.reminderDateMonth = reminderDateMonth;
		this.instruCondition = instruCondition;
		this.assignedTo = assignedTo;
		this.statusOfOrder = statusOfOrder;
		this.multiPara = multiPara;
		this.active = active;
		this.calibrationTag = calibrationTag;
		this.isMultiPara = isMultiPara;
	}




	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "req_doc_id", nullable = false)
	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}
	
	
	@Column(name = "custinstru_identification_id")
	public long getCustInstruIdentificationId() {
		return custInstruIdentificationId;
	}


	public void setCustInstruIdentificationId(long custInstruIdentificationId) {
		this.custInstruIdentificationId = custInstruIdentificationId;
	}


	@Column(name = "create_req_iwd_table_id")
	public long getCreateReqAndIwdTableId() {
		return createReqAndIwdTableId;
	}

	public void setCreateReqAndIwdTableId(long createReqAndIwdTableId) {
		this.createReqAndIwdTableId = createReqAndIwdTableId;
	}
	
	
	@Column(name = "req_iwd_document_number")
	public String getReqInwDocumentNo() {
		return reqInwDocumentNo;
	}

	public void setReqInwDocumentNo(String reqInwDocumentNo) {
		this.reqInwDocumentNo = reqInwDocumentNo;
	}

	@Column(name = "iwd_req_number")
	public String getInwardReqNo() {
		return inwardReqNo;
	}


	public void setInwardReqNo(String inwardReqNo) {
		this.inwardReqNo = inwardReqNo;
	}

	@Column(name = "sr_no")
	public String getSrNo() {
		return srNo;
	}


	public void setSrNo(String srNo) {
		this.srNo = srNo;
	}

	@Column(name = "inward_instrument_no")
	public String getInwardInstrumentNo() {
		return inwardInstrumentNo;
	}


	public void setInwardInstrumentNo(String inwardInstrumentNo) {
		this.inwardInstrumentNo = inwardInstrumentNo;
	}

	
	@Column(name = "parameter_number")
	public String getParameterNumber() {
		return parameterNumber;
	}

	public void setParameterNumber(String parameterNumber) {
		this.parameterNumber = parameterNumber;
	}

	public long getNomenclature() {
		return nomenclature;
	}

	public void setNomenclature(long nomenclature) {
		this.nomenclature = nomenclature;
	}


	public long getParameter() {
		return parameter;
	}


	public void setParameter(long parameter) {
		this.parameter = parameter;
	}

	@Column(name = "range_size")
	public String getRangeSize() {
		return rangeSize;
	}


	public void setRangeSize(String rangeSize) {
		this.rangeSize = rangeSize;
	}

	@Column(name = "make")
	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	@Column(name = "id_no")
	public String getIdNo() { 
		return idNo;
	}

	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}

	@Column(name = "frequency_condition")
	public String getFrequencyCondition() {
		return frequencyCondition;
	}

	public void setFrequencyCondition(String frequencyCondition) {
		this.frequencyCondition = frequencyCondition;
	}

	@Column(name = "instrument_type")
	public String getInstrumentType() {
		return instrumentType;
	}

	public void setInstrumentType(String instrumentType) {
		this.instrumentType = instrumentType;
	}

	@Column(name = "range_from")
	public String getRangeFrom() {
		return rangeFrom;
	}

	public void setRangeFrom(String rangeFrom) {
		this.rangeFrom = rangeFrom;
	}

	@Column(name = "range_to")
	public String getRangeTo() {
		return rangeTo;
	}

	public void setRangeTo(String rangeTo) {
		this.rangeTo = rangeTo;
	}


	@Column(name = "least_count")
	public String getLeastCount() {
		return leastCount;
	}

	public void setLeastCount(String leastCount) {
		this.leastCount = leastCount;
	}

	
	@Column(name = "uom")
	public long getRuom() {
		return ruom;
	}

	public void setRuom(long ruom) {
		this.ruom = ruom;
	}

	@Column(name = "uom1")
	public long getLcuom1() {
		return lcuom1;
	}


	public void setLcuom1(long lcuom1) {
		this.lcuom1 = lcuom1;
	}


	@Column(name = "accuracy")
	public String getAccuracy() {
		return accuracy;
	}

	public void setAccuracy(String accuracy) {
		this.accuracy = accuracy;
	}

	@Column(name = "formula_accuracy")
	public long getFormulaAccuracy() {
		return formulaAccuracy;
	}

	public void setFormulaAccuracy(long formulaAccuracy) {
		this.formulaAccuracy = formulaAccuracy;
	}

	@Column(name = "uom_accuracy")
	public long getUomAccuracy() {
		return uomAccuracy;
	}

	public void setUomAccuracy(long uomAccuracy) {
		this.uomAccuracy = uomAccuracy;
	}

	@Column(name = "accuracy1")
	public String getAccuracy1() {
		return accuracy1;
	}

	public void setAccuracy1(String accuracy1) {
		this.accuracy1 = accuracy1;
	}

	@Column(name = "formula_accuracy1")
	public long getFormulaAccuracy1() {
		return formulaAccuracy1;
	}

	public void setFormulaAccuracy1(long formulaAccuracy1) {
		this.formulaAccuracy1 = formulaAccuracy1;
	}

	@Column(name = "uom_accuracy1")
	public long getUomAccuracy1() {
		return uomAccuracy1;
	}

	public void setUomAccuracy1(long uomAccuracy1) {
		this.uomAccuracy1 = uomAccuracy1;
	}

	@Column(name = "location")
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Column(name = "calibration_frequency")
	public String getCalibrationFrequency() {
		return calibrationFrequency;
	}

	public void setCalibrationFrequency(String calibrationFrequency) {
		this.calibrationFrequency = calibrationFrequency;
	}

	@Column(name = "lab_type")
	public String getLabType() {
		return labType;
	}

	public void setLabType(String labType) {
		this.labType = labType;
	}
	
	@Column(name = "date_of_calibration")
	public String getDateOfCalibration() {
		return dateOfCalibration;
	}

	public void setDateOfCalibration(String dateOfCalibration) {
		this.dateOfCalibration = dateOfCalibration;
	}

	@Column(name = "date_of_issue_certificate")
	public String getDateOfIssueCertificate() {
		return dateOfIssueCertificate;
	}

	public void setDateOfIssueCertificate(String dateOfIssueCertificate) {
		this.dateOfIssueCertificate = dateOfIssueCertificate;
	}

	@Column(name = "received_date")
	public String getReceivedDate() {
		return receivedDate;
	}

	public void setReceivedDate(String receivedDate) {
		this.receivedDate = receivedDate;
	}
	
	@Column(name="certificate_no")
	public String getCertificateNo() {
		return certificateNo;
	}

	public void setCertificateNo(String certificateNo) {
		this.certificateNo = certificateNo;
	}

	@Column(name = "order_no")
	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	@Column(name = "certificate_print_mode")
	public String getCertificatePrintMode() {
		return certificatePrintMode;
	}

	public void setCertificatePrintMode(String certificatePrintMode) {
		this.certificatePrintMode = certificatePrintMode;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "calibration_due_on")
	public String getCalibrationDueOn() {
		return calibrationDueOn;
	}

	public void setCalibrationDueOn(String calibrationDueOn) {
		this.calibrationDueOn = calibrationDueOn;
	}

	@Column(name = "calibrated_by")
	public String getCalibratedBy() {
		return calibratedBy;
	}

	public void setCalibratedBy(String calibratedBy) {
		this.calibratedBy = calibratedBy;
	}

	@Column(name = "authorized_by")
	public String getAuthorizedBy() {
		return authorizedBy;
	}

	public void setAuthorizedBy(String authorizedBy) {
		this.authorizedBy = authorizedBy;
	}

	@Column(name = "reminder_date")
	public String getReminderDate() {
		return reminderDate;
	}

	public void setReminderDate(String reminderDate) {
		this.reminderDate = reminderDate;
	}

	@Column(name = "reminder_date_month")
	public String getReminderDateMonth() {
		return reminderDateMonth;
	}

	public void setReminderDateMonth(String reminderDateMonth) {
		this.reminderDateMonth = reminderDateMonth;
	}

	
	@Column(name = "instrument_condition")
	public String getInstruCondition() {
		return instruCondition;
	}

	public void setInstruCondition(String instruCondition) {
		this.instruCondition = instruCondition;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Column(name = "assigned_to")
	public String getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}

	@Column(name = "status_of_order")
	public String getStatusOfOrder() {
		return statusOfOrder;
	}

	public void setStatusOfOrder(String statusOfOrder) {
		this.statusOfOrder = statusOfOrder;
	}

	@Column(name = "multi_para")
	public int getMultiPara() {
		return multiPara;
	}

	public void setMultiPara(int multiPara) {
		this.multiPara = multiPara;
	}










	@Column(name = "is_multipara")
	public int getIsMultiPara() {
		return isMultiPara;
	}











	public void setIsMultiPara(int isMultiPara) {
		this.isMultiPara = isMultiPara;
	}
	
	
	
	

//	@Column(name = "calibration_tag")
//	public int getCalibrationTag() {
//		return calibrationTag;
//	}
//
//
//	public void setCalibrationTag(int calibrationTag) {
//		this.calibrationTag = calibrationTag;
//	}

	

//	public boolean isActive() {
//		return active;
//	}
//
//	public void setActive(boolean active) {
//		this.active = active;
//	}
	 

}

