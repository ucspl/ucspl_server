package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "create_sales_quotation_table")
public class CreateSalesQuotation {

	private long id;
	private long typeOfQuotation;
	private String documentNumber;
	private String quotationNumber;
	private String dateOfQuotation;
	private long customerId;
	private long branchId;
	private String kindAttachment;
	private long custRefNo;
	private String refDate;
	private String archieveDate;
	private String countryPrefixForMobile;
	private String contactNo;
	private String email;
	private long quotationItemFile;
	private String terms;
	private long enquiryAttachment;
	private String termsAndCondition;
	private int draft;
	private int approved;
	private int submitted;
	private int archieved;
	private int rejected;
	private String remark;
	private String createdBy;
	private java.util.Date dateCreated;
	
	@ManyToOne
	@JoinColumn(name = "doc_type_id", nullable = false)
	private TypeOfQuotationForSalesAndQuotation typeOfQuotationForSalesAndQuotation;

	public CreateSalesQuotation() {

	}

	public CreateSalesQuotation(long id, long typeOfQuotation, String documentNumber, String quotationNumber,
			String dateOfQuotation, long customerId, long branchId, String kindAttachment, long custRefNo,
			String refDate, String archieveDate, String countryPrefixForMobile, String contactNo, String email,
			long quotationItemFile, String terms, long enquiryAttachment, String termsAndCondition, int draft,
			int approved, int submitted, int archieved, int rejected, String remark, String createdBy, Date dateCreated,
			TypeOfQuotationForSalesAndQuotation typeOfQuotationForSalesAndQuotation) {
		super();
		this.id = id;
		this.typeOfQuotation = typeOfQuotation;
		this.documentNumber = documentNumber;
		this.quotationNumber = quotationNumber;
		this.dateOfQuotation = dateOfQuotation;
		this.customerId = customerId;
		this.branchId = branchId;
		this.kindAttachment = kindAttachment;
		this.custRefNo = custRefNo;
		this.refDate = refDate;
		this.archieveDate = archieveDate;
		this.countryPrefixForMobile = countryPrefixForMobile;
		this.contactNo = contactNo;
		this.email = email;
		this.quotationItemFile = quotationItemFile;
		this.terms = terms;
		this.enquiryAttachment = enquiryAttachment;
		this.termsAndCondition = termsAndCondition;
		this.draft = draft;
		this.approved = approved;
		this.submitted = submitted;
		this.archieved = archieved;
		this.rejected = rejected;
		this.remark = remark;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
		this.typeOfQuotationForSalesAndQuotation = typeOfQuotationForSalesAndQuotation;
	}


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "create_sales_quotation_id", nullable = false)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "doc_type_id", nullable = false)
	public long getTypeOfQuotation() {
		return typeOfQuotation;
	}


	public void setTypeOfQuotation(long typeOfQuotation) {
		this.typeOfQuotation = typeOfQuotation;
	}

	
	
	@Column(name = "document_number", nullable = false)
	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	
	
	@Column(name = "quotation_no")
	public String getQuotationNumber() {
		return quotationNumber;
	}

	public void setQuotationNumber(String quotationNumber) {
		this.quotationNumber = quotationNumber;
	}


	@Column(name = "date_of_quotation", nullable = false)
	public String getDateOfQuotation() {
		return dateOfQuotation;
	}

	public void setDateOfQuotation(String dateOfQuotation) {
		this.dateOfQuotation = dateOfQuotation;
	}
	

	@Column(name = "customer_id", nullable = false)
	public long getCustomerId() {
		return customerId;
	}

	

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	@Column(name = "branch_id", nullable = false)
	public long getBranchId() {
		return branchId;
	}

	public void setBranchId(long branchId) {
		this.branchId = branchId;
	}

	@Column(name = "kind_attachment")
	public String getKindAttachment() {
		return kindAttachment;
	}

	public void setKindAttachment(String kindAttachment) {
		this.kindAttachment = kindAttachment;
	}

	@Column(name = "ref_date", nullable = false)
	public String getRefDate() {
		return refDate;
	}

	public void setRefDate(String refDate) {
		this.refDate = refDate;
	}
	

	@Column(name = "cust_ref_no")
	public long getCustRefNo() {
		return custRefNo;
	}


	

	public void setCustRefNo(long custRefNo) {
		this.custRefNo = custRefNo;
	}

	@Column(name = "archieve_date", nullable = false)
	public String getArchieveDate() {
		return archieveDate;
	}

	public void setArchieveDate(String archieveDate) {
		this.archieveDate = archieveDate;
	}
	


	@Column(name = "countryprefixformobile")
	public String getCountryPrefixForMobile() {
		return countryPrefixForMobile;
	}

	public void setCountryPrefixForMobile(String countryPrefixForMobile) {
		this.countryPrefixForMobile = countryPrefixForMobile;
	}



	@Column(name = "contact_no")
	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "quotation_item_file_id")
	public long getQuotationItemFile() {
		return quotationItemFile;
	}

	public void setQuotationItemFile(long quotationItemFile) {
		this.quotationItemFile = quotationItemFile;
	}

	public String getTerms() {
		return terms;
	}

	public void setTerms(String terms) {
		this.terms = terms;
	}

	@Column(name = "enquiry_attachment_id")
	public long getEnquiryAttachment() {
		return enquiryAttachment;
	}

	public void setEnquiryAttachment(long enquiryAttachment) {
		this.enquiryAttachment = enquiryAttachment;
	}

	@Column(name = "terms_and_condition")
	public String getTermsAndCondition() {
		return termsAndCondition;
	}

	public void setTermsAndCondition(String termsAndCondition) {
		this.termsAndCondition = termsAndCondition;
	}

	
	public int getDraft() {
		return draft;
	}


	public void setDraft(int draft) {
		this.draft = draft;
	}


	public int getApproved() {
		return approved;
	}


	public void setApproved(int approved) {
		this.approved = approved;
	}


	public int getSubmitted() {
		return submitted;
	}


	public void setSubmitted(int submitted) {
		this.submitted = submitted;
	}


	public int getArchieved() {
		return archieved;
	}


	public void setArchieved(int archieved) {
		this.archieved = archieved;
	}


	public int getRejected() {
		return rejected;
	}


	public void setRejected(int rejected) {
		this.rejected = rejected;
	}


	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "created_by", nullable = false)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}


	@Column(name = "date_created", nullable = false)
	public java.util.Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(java.util.Date dateCreated) {
		this.dateCreated = dateCreated;
	}

		

}
