package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "taxpayer_type_for_customer_table")
public class TaxpayerTypeForCustomer {

	private long taxpayerTypeId;
	private String taxpayerTypeName;
	private long status;
	private String createdBy;
	private java.util.Date dateCreated;

	public TaxpayerTypeForCustomer() {

	}

	public TaxpayerTypeForCustomer(long taxpayerTypeId, String taxpayerTypeName, long status, String createdBy,
			Date dateCreated) {
		super();
		this.taxpayerTypeId = taxpayerTypeId;
		this.taxpayerTypeName = taxpayerTypeName;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "taxpayer_type_id", nullable = false)
	public long getTaxpayerTypeId() {
		return taxpayerTypeId;
	}

	public void setTaxpayerTypeId(long taxpayerTypeId) {
		this.taxpayerTypeId = taxpayerTypeId;
	}

	@Column(name = "taxpayer_type_name", nullable = false)
	public String getTaxpayerTypeName() {
		return taxpayerTypeName;
	}

	public void setTaxpayerTypeName(String taxpayerTypeName) {
		this.taxpayerTypeName = taxpayerTypeName;
	}

	public long getStatus() {
		return status;
	}

	public void setStatus(long status) {
		this.status = status;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "date_created")
	public java.util.Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(java.util.Date dateCreated) {
		this.dateCreated = dateCreated;
	}

}
