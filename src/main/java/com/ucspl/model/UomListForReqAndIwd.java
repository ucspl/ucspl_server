package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="uom_list_for_req_and_iwd_table")
public class UomListForReqAndIwd {

	private long uomId;
	private String uomName;
	private long parameterId;
	private String description;
	private int status;
	private String createdBy;
	private String dateCreated;
	
	public UomListForReqAndIwd() {
		
	}
	


	public UomListForReqAndIwd(long uomId, String uomName, long parameterId, String description,
			int status, String createdBy, String dateCreated) {
		super();
		this.uomId = uomId;
		this.uomName = uomName;
		this.parameterId = parameterId;
		this.description = description;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}



	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "uom_id", nullable = false)
	public long getUomId() {
		return uomId;
	}

	public void setUomId(long uomId) {
		this.uomId = uomId;
	}

	@Column(name = "uom_name")
	public String getUomName() {
		return uomName;
	}

	public void setUomName(String uomName) {
		this.uomName = uomName;
	}

	@Column(name = "parameter_id")
	public long getParameterId() {
		return parameterId;
	}
	public void setParameterId(long parameterId) {
		this.parameterId = parameterId;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "date_created")
	public String getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	
}
