package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "supplier_attachments_table")
@Table(name = "supplier_attachments_table")
public class SupplierAttachments {

private long id;
	
	@Column(name = "supp_id")
	private long suppId;
	private String documentType;
	private String documentName;
	private String documentNumber;
	private String originalFileName;
	private String attachedFileName;
	private String type;
	private String fileLocation;
	//private byte[] picbyte;
	
	public SupplierAttachments() {
	
	}

	public SupplierAttachments(long id, long suppId, String documentType, String documentName, String documentNumber,
			String originalFileName, String attachedFileName, String type, String fileLocation) {
		super();
		this.id = id;
		this.suppId = suppId;
		this.documentType = documentType;
		this.documentName = documentName;
		this.documentNumber = documentNumber;
		this.originalFileName = originalFileName;
		this.attachedFileName = attachedFileName;
		this.type = type;
		this.fileLocation = fileLocation;
	}

	@Id
	@Column(name = "supplier_attachments_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getSuppId() {
		return suppId;
	}

	public void setSuppId(long suppId) {
		this.suppId = suppId;
	}

	@Column(name = "document_type")
	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	@Column(name = "document_name")
	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	@Column(name = "document_number")
	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	@Column(name = "original_file_name")
	public String getOriginalFileName() {
		return originalFileName;
	}

	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}

	@Column(name = "attached_file_name")
	public String getAttachedFileName() {
		return attachedFileName;
	}

	public void setAttachedFileName(String attachedFileName) {
		this.attachedFileName = attachedFileName;
	}

	@Column(name = "type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "file_location")
	public String getFileLocation() {
		return fileLocation;
	}

	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}
	
	
	
}
