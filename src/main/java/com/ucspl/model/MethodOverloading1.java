package com.ucspl.model;

public class MethodOverloading1 {

	 public int add(int a,int b)
	 {
		 int result= a+b;
		 System.out.println("Addition of two no. is:"+result);
		 return result;
	      //
	 }
	 
	 public int add(int a,int b,int c)
	 {
		 int result=a+b+c;
		 System.out.println("Addition of three no.s is:"+result);
		 return result;
		 //
	 }
	
}
