
package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sr_no_table_for_create_po_document")
public class SrNoPurchaseOrderDocument {

	private long id;
	private String documentNumber;
	private int srNo;
	private long instruName;
	private String description;
	private String idNo;
	private String range;
	private long quantity;
	private double rate;
	private double discountOnItem;
	private double amount;
	private String customerPartNo;
	private String hsnNo;
	private long remainingQuantity;
	private String rangeFrom;
	private String rangeTo;
	
	

	public SrNoPurchaseOrderDocument() {
		// TODO Auto-generated constructor stub
	}

	public SrNoPurchaseOrderDocument(long id, String documentNumber, int srNo, long instruName, String description,
			String idNo, String range, long quantity, double rate, double discountOnItem, double amount,
			String customerPartNo, String hsnNo, long remainingQuantity, String rangeFrom, String rangeTo) {
		super();
		this.id = id;
		this.documentNumber = documentNumber;
		this.srNo = srNo;
		this.instruName = instruName;
		this.description = description;
		this.idNo = idNo;
		this.range = range;
		this.quantity = quantity;
		this.rate = rate;
		this.discountOnItem = discountOnItem;
		this.amount = amount;
		this.customerPartNo = customerPartNo;
		this.hsnNo = hsnNo;
		this.remainingQuantity = remainingQuantity;
		this.rangeFrom = rangeFrom;
		this.rangeTo = rangeTo;
	}





	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "create_po_doc_id", nullable = false)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "document_number", nullable = false)
	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	@Column(name = "sr_no")
	public int getSrNo() {
		return srNo;
	}

	public void setSrNo(int srNo) {
		this.srNo = srNo;
	}


	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}


	@Column(name = "instru_name")
	public long getInstruName() {
		return instruName;
	}

	public void setInstruName(long instruName) {
		this.instruName = instruName;
	}


	@Column(name = "id_no")
	public String getIdNo() {
		return idNo;
	}

	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}

	

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	@Column(name = "discount_on_item")
	public double getDiscountOnItem() {
		return discountOnItem;
	}

	public void setDiscountOnItem(double discountOnItem) {
		this.discountOnItem = discountOnItem;
	}

	
	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getRange() {
		return range;
	}

	public void setRange(String range) {
		this.range = range;
	}



	@Column(name = "customer_part_number")
	public String getCustomerPartNo() {
		return customerPartNo;
	}


	public void setCustomerPartNo(String customerPartNo) {
		this.customerPartNo = customerPartNo;
	}

	@Column(name = "hsn_number")
	public String getHsnNo() {
		return hsnNo;
	}

	public void setHsnNo(String hsnNo) {
		this.hsnNo = hsnNo;
	}

	@Column(name = "remaining_quantity")
	public long getRemainingQuantity() {
		return remainingQuantity;
	}

	public void setRemainingQuantity(long remainingQuantity) {
		this.remainingQuantity = remainingQuantity;
	}

	@Column(name = "range_from")
	public String getRangeFrom() {
		return rangeFrom;
	}

	public void setRangeFrom(String rangeFrom) {
		this.rangeFrom = rangeFrom;
	}

	@Column(name = "range_to")
	public String getRangeTo() {
		return rangeTo;
	}

	public void setRangeTo(String rangeTo) {
		this.rangeTo = rangeTo;
	}

	
	
	


}
