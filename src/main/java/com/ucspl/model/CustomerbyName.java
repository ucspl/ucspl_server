package com.ucspl.model;

import javax.persistence.Column;

public class CustomerbyName {

	private String name;
	private String nameAnd;
	private String department;
	private String address;
	private int status;

	public CustomerbyName() {
		// TODO Auto-generated constructor stub
	}

	public CustomerbyName(String name, String nameAnd, String department, String address, int status) {
		super();
		this.name = name;
		this.nameAnd = nameAnd;
		this.department = department;
		this.address = address;
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "name_and", nullable = false)
	public String getNameAnd() {
		return nameAnd;
	}

	public void setNameAnd(String nameAnd) {
		this.nameAnd = nameAnd;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "CustomerbyName [name=" + name + ", nameAnd=" + nameAnd + ", department=" + department + ", address="
				+ address + ", status=" + status + "]";
	}
	
	

}
