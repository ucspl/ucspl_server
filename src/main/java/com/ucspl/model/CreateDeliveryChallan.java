package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "create_delivery_challan_table")
public class CreateDeliveryChallan {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "deli_challan_table_id", nullable = false)
	private long id;
	
	@Column(name = "document_type_id")
	private long documentTypeId;
	
	@Column(name = "deli_challan_document_number")
	private String  dcDocumentNumber;
	
	@Column(name = "request_number")
	private String requestNumber;
	
	@Column(name = "branch_id")
	private long branchId;
	
	@Column(name = "invoice_type_id")
	private String invoiceTypeId;
	
	@Column(name = "cust_shipped_to")
	//private String documentDate;
	private long custShippedTo;
	
//	@Column(name = "cust_billed_to")
//	private long custBilledTo;
	
	@Column(name = "invoice_date")
	private String invoiceDate;
	
	@Column(name = "po_date")
	private String poDate;
	
	@Column(name = "dc_date")
	private String dcDate;
	
	@Column(name = "po_no")
	private String poNo;
	
	@Column(name = "dc_no")
	private String dcNo;
	
	@Column(name = "vendor_code_number")
	private String vendorCodeNumber;
	
	@Column(name = "archieve_date")
	private String archieveDate;
	
	@Column(name = "created_by")
	private String createdBy;
	
	private int draft;
	private int approved;
	private int submitted;
	private int archieved;
	private int rejected;
	private String remark;
	
	@Column(name = "dispatch_mode")
	private String dispatchMode;
	
	@Column(name = "mode_of_submission")
	private String modeOfSubmission;
	
	@Column(name = "total_quantity")
    private long totalQuantity;
	
	
}

