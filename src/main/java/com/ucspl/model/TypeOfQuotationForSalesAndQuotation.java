package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "quotation_type_for_sales_and_quotation_table")
public class TypeOfQuotationForSalesAndQuotation {
	
	private long docTypeId;
	private String docTypeName;
	private long status;
	private String createdBy;
	private java.util.Date dateCreated;

	
	
	public TypeOfQuotationForSalesAndQuotation() {
		// TODO Auto-generated constructor stub

	}

	
	public TypeOfQuotationForSalesAndQuotation(long docTypeId, String docTypeName, long status, String createdBy,
			Date dateCreated) {
		super();
		this.docTypeId = docTypeId;
		this.docTypeName = docTypeName;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "doc_type_id", nullable = false)
	public long getDocTypeId() {
		return docTypeId;
	}

	public void setDocTypeId(long docTypeId) {
		this.docTypeId = docTypeId;
	}

	@Column(name = "doc_type_name", nullable = false)
	public String getDocTypeName() {
		return docTypeName;
	}

	public void setDocTypeName(String docTypeName) {
		this.docTypeName = docTypeName;
	}


	public long getStatus() {
		return status;
	}


	public void setStatus(long status) {
		this.status = status;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

   @Column(name = "date_created")
	public java.util.Date getDateCreated() {
		return dateCreated;
	}


	public void setDateCreated(java.util.Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	

}
