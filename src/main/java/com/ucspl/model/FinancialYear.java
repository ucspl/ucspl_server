package com.ucspl.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "financial_year_table")
public class FinancialYear {

	private long Id;
	private String fromDate;
	private long status;
	private String createdBy;
	//private java.util.Date dateCreated;

	public FinancialYear() {
		// TODO Auto-generated constructor stub

	}


	

	public FinancialYear(long id, String fromDate, long status, String createdBy) {
		super();
		Id = id;
		this.fromDate = fromDate;
		this.status = status;
		this.createdBy = createdBy;
	}




	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	public long getId() {
		return Id;
	}


	public void setId(long id) {
		Id = id;
	}

	@Column(name = "from_date")
	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public long getStatus() {
		return status;
	}

	public void setStatus(long status) {
		this.status = status;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

//	@Column(name = "date_created")
//	public java.util.Date getDateCreated() {
//		return dateCreated;
//	}
//
//
//	public void setDateCreated(java.util.Date dateCreated) {
//		this.dateCreated = dateCreated;
//	}

}