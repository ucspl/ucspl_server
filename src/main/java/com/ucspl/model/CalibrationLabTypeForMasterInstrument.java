package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "calibration_lab_type_of_master_instrument_table")
public class CalibrationLabTypeForMasterInstrument {

	private long calibrationLabTypeId;
	private String calibrationLabTypeName;
	private String description;
	private long status;
	private String createdBy;
	private java.util.Date dateCreated;
	
	public CalibrationLabTypeForMasterInstrument() {
		
	}

	public CalibrationLabTypeForMasterInstrument(long calibrationLabTypeId, String calibrationLabTypeName,
			String description, long status, String createdBy, Date dateCreated) {
		super();
		this.calibrationLabTypeId = calibrationLabTypeId;
		this.calibrationLabTypeName = calibrationLabTypeName;
		this.description = description;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "calibrationlab_type_id", nullable = false)
	public long getCalibrationLabTypeId() {
		return calibrationLabTypeId;
	}

	public void setCalibrationLabTypeId(long calibrationLabTypeId) {
		this.calibrationLabTypeId = calibrationLabTypeId;
	}

	@Column(name = "calibrationlab_type_name", nullable = false)
	public String getCalibrationLabTypeName() {
		return calibrationLabTypeName;
	}

	public void setCalibrationLabTypeName(String calibrationLabTypeName) {
		this.calibrationLabTypeName = calibrationLabTypeName;
	}
	
	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public long getStatus() {
		return status;
	}


	public void setStatus(long status) {
		this.status = status;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "date_created")
	public java.util.Date getDateCreated() {
		return dateCreated;
	}


	public void setDateCreated(java.util.Date dateCreated) {
		this.dateCreated = dateCreated;
	}
}
