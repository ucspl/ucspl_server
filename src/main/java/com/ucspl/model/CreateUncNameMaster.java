package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;




@Entity
@Table(name="create_unc_name_master_table")
public class CreateUncNameMaster {
	
	public long createUncNameMasterId;
	public String uncName;
	public String dateOfConfiguration;
	public int draft;
	public int archieved;
	public int submitted;
	public int rejected;
	public int approved;
	
	public CreateUncNameMaster() {
		
	}
	


	public CreateUncNameMaster(long createUncNameMasterId, String uncName, String dateOfConfiguration, int draft,
			int archieved, int submitted, int rejected, int approved) {
		super();
		this.createUncNameMasterId = createUncNameMasterId;
		this.uncName = uncName;
		this.dateOfConfiguration = dateOfConfiguration;
		this.draft = draft;
		this.archieved = archieved;
		this.submitted = submitted;
		this.rejected = rejected;
		this.approved = approved;
	}



	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "create_unc_name_master_id", nullable = false)
	public long getCreateUncNameMasterId() {
		return createUncNameMasterId;
	}

	public void setCreateUncNameMasterId(long createUncNameMasterId) {
		this.createUncNameMasterId = createUncNameMasterId;
	}


	@Column(name = "unc_name")
	public String getUncName() {
		return uncName;
	}

	public void setUncName(String uncName) {
		this.uncName = uncName;
	}

	@Column(name = "date_of_configuration")
	public String getDateOfConfiguration() {
		return dateOfConfiguration;
	}

	public void setDateOfConfiguration(String dateOfConfiguration) {
		this.dateOfConfiguration = dateOfConfiguration;
	}

	@Column(name = "draft")
	public int getDraft() {
		return draft;
	}

	public void setDraft(int draft) {
		this.draft = draft;
	}
 
	@Column(name = "submitted")
	public int getSubmitted() {
		return submitted;
	}

	public void setSubmitted(int submitted) {
		this.submitted = submitted;
	}

	@Column(name = "approved")
	public int getApproved() {
		return approved;
	}

	public void setApproved(int approved) {
		this.approved = approved;
	}
	
	
}
