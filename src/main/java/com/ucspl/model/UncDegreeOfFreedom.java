
package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="unc_degree_of_freedom_table")
public class UncDegreeOfFreedom {

	
	private long degreeOfFreedomId;
	private String degreeOfFreedomName;
	private String description;
	private int status;
	private String createdBy;
	private String dateCreated;
	
	
	public UncDegreeOfFreedom() {
		
	}

	public UncDegreeOfFreedom(long degreeOfFreedomId, String degreeOfFreedomName, String description, int status,
			String createdBy, String dateCreated) {
		super();
		this.degreeOfFreedomId = degreeOfFreedomId;
		this.degreeOfFreedomName = degreeOfFreedomName;
		this.description = description;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "degree_of_freedom_id", nullable = false)
	public long getDegreeOfFreedomId() {
		return degreeOfFreedomId;
	}

	public void setDegreeOfFreedomId(long degreeOfFreedomId) {
		this.degreeOfFreedomId = degreeOfFreedomId;
	}

	@Column(name = "degree_of_freedom_name")
	public String getDegreeOfFreedomName() {
		return degreeOfFreedomName;
	}

	public void setDegreeOfFreedomName(String degreeOfFreedomName) {
		this.degreeOfFreedomName = degreeOfFreedomName;
	}

	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "date_created")
	public String getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	
	
}
