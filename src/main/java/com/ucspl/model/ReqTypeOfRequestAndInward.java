package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "req_type_of_request_and_inward_table")
public class ReqTypeOfRequestAndInward {

	private long reqTypeId;
	private String reqTypeName;
	private String description;
	private long status;
	private String createdBy;
	private java.util.Date dateCreated;
	
	public ReqTypeOfRequestAndInward() {
		
	}

	
	
	public ReqTypeOfRequestAndInward(long reqTypeId, String reqTypeName, String description, long status,
			String createdBy, Date dateCreated) {
		super();
		this.reqTypeId = reqTypeId;
		this.reqTypeName = reqTypeName;
		this.description = description;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}



	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "req_type_id", nullable = false)
	public long getReqTypeId() {
		return reqTypeId;
	}
	public void setReqTypeId(long reqTypeId) {
		this.reqTypeId = reqTypeId;
	}
	
	@Column(name = "req_type_name")
	public String getReqTypeName() {
		return reqTypeName;
	}
	public void setReqTypeName(String reqTypeName) {
		this.reqTypeName = reqTypeName;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getStatus() {
		return status;
	}

	public void setStatus(long status) {
		this.status = status;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "date_created")
	public java.util.Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(java.util.Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	
	
	
}
