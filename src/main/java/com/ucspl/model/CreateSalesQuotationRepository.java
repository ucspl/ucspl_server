package com.ucspl.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CreateSalesQuotationRepository extends JpaRepository<CreateSalesQuotation, Long>{

}

