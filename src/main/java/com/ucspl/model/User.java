package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table(name = "create_user")
public class User {
	private long id;
	private String firstName;
	private String middleName;
	private String lastName;
	private int gender;
	private int status;
	private long customer;
	private long role;
	private long department;
	private long selectBranch;
	private String email;
	private String countryPrefixForMobile;
	private String phoneNo;
	private long userNumber;
	private String password;
	private String userName;
	private long securityQuestionId;
	private String securityAnswer;
	private String createdBy;
	private java.util.Date dateCreated;
	private String modifiedBy;

	@ManyToOne
	@JoinColumn(name = "branch_id", nullable = false)
	private Branch branch;

	@ManyToOne
	@JoinColumn(name = "customer_id", nullable = false)
	private Customer custommer;

	@ManyToOne
	@JoinColumn(name = "department_id", nullable = false)
	private Department departments;

	@ManyToOne
	@JoinColumn(name = "role_id", nullable = false)
	private Role roles;

	public User() {

	}

	
	public User(long id, String firstName, String middleName, String lastName, int gender, int status, long customer,
			long role, long department, long selectBranch, String email, String countryPrefixForMobile, String phoneNo,
			long userNumber, String password, String userName, long securityQuestionId, String securityAnswer,
			String createdBy, Date dateCreated, String modifiedBy, Branch branch, Customer custommer,
			Department departments, Role roles) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.gender = gender;
		this.status = status;
		this.customer = customer;
		this.role = role;
		this.department = department;
		this.selectBranch = selectBranch;
		this.email = email;
		this.countryPrefixForMobile = countryPrefixForMobile;
		this.phoneNo = phoneNo;
		this.userNumber = userNumber;
		this.password = password;
		this.userName = userName;
		this.securityQuestionId = securityQuestionId;
		this.securityAnswer = securityAnswer;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
		this.modifiedBy = modifiedBy;
		this.branch = branch;
		this.custommer = custommer;
		this.departments = departments;
		this.roles = roles;
	}


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "first_name", nullable = false)
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "middle_name", nullable = false)
	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	@Column(name = "last_name", nullable = false)
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "gender", nullable = false)
	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	@Column(name = "status", nullable = false)
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name = "customer_id", nullable = false)
	public long getCustomer() {
		return customer;
	}

	public void setCustomer(long customer) {
		this.customer = customer;
	}

	@Column(name = "role", nullable = false)
	public long getRole() {
		return role;
	}

	public void setRole(long role) {
		this.role = role;
	}

	@Column(name = "department", nullable = false)
	public long getDepartment() {
		return department;
	}

	public void setDepartment(long department) {
		this.department = department;
	}

	@Column(name = "branch_id", nullable = false)
	public long getSelectBranch() {
		return selectBranch;
	}

	public void setSelectBranch(long selectBranch) {
		this.selectBranch = selectBranch;
	}

	@Column(name = "email", nullable = false)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name = "countryprefixformobile")
	public String getCountryPrefixForMobile() {
		return countryPrefixForMobile;
	}

	public void setCountryPrefixForMobile(String countryPrefixForMobile) {
		this.countryPrefixForMobile = countryPrefixForMobile;
	}

	@Column(name = "phone", nullable = false)
	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	@Column(name = "user_number", nullable = false)
	public long getUserNumber() {
		return userNumber;
	}

	public void setUserNumber(long userNumber) {
		this.userNumber = userNumber;
	}

	@Column(name = "password", nullable = false)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "user_name", nullable = false)
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "security_question", nullable = false)
	public long getSecurityQuestionId() {
		return securityQuestionId;
	}


	public void setSecurityQuestionId(long securityQuestionId) {
		this.securityQuestionId = securityQuestionId;
	}



	@Column(name = "security_answer", nullable = false)
	public String getSecurityAnswer() {
		return securityAnswer;
	}
	
	public void setSecurityAnswer(String securityAnswer) {
		this.securityAnswer = securityAnswer;
	}

	@Column(name = "createdby")
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column(name = "datecreated")
	public java.util.Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(java.util.Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	

	@Column(name = "modified_by")
	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

}
