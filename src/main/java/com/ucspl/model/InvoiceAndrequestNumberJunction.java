
package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "invoice_and_request_no_junction_table")
public class InvoiceAndrequestNumberJunction {

	private long invoiceRequestNoJunctionId;
	private String requestNumber;
	private String invoiceDocNo;
	
	
	public InvoiceAndrequestNumberJunction() {
		// TODO Auto-generated constructor stub

	}


	public InvoiceAndrequestNumberJunction(long invoiceRequestNoJunctionId, String requestNumber, String invoiceDocNo) {
		super();
		this.invoiceRequestNoJunctionId = invoiceRequestNoJunctionId;
		this.requestNumber = requestNumber;
		this.invoiceDocNo = invoiceDocNo;
	}


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "invoice_request_no_junction_id", nullable = false)
	public long getInvoiceRequestNoJunctionId() {
		return invoiceRequestNoJunctionId;
	}


	public void setInvoiceRequestNoJunctionId(long invoiceRequestNoJunctionId) {
		this.invoiceRequestNoJunctionId = invoiceRequestNoJunctionId;
	}

	@Column(name = "request_number")
	public String getRequestNumber() {
		return requestNumber;
	}


	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	@Column(name = "invoice_doc_number")
	public String getInvoiceDocNo() {
		return invoiceDocNo;
	}


	public void setInvoiceDocNo(String invoiceDocNo) {
		this.invoiceDocNo = invoiceDocNo;
	}

	
}

