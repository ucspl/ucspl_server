package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "inward_type_of_request_and_inward_table")
public class InwardTypeOfRequestAndInward {
	
	private long inwardTypeId;
	private String inwardTypeName;
	private String description;
	private long status;
	private String createdBy;
	private java.util.Date dateCreated;
	
	public InwardTypeOfRequestAndInward() {
		
	}
	
	public InwardTypeOfRequestAndInward(long inwardTypeId, String inwardTypeName, String description, long status,
			String createdBy, Date dateCreated) {
		super();
		this.inwardTypeId = inwardTypeId;
		this.inwardTypeName = inwardTypeName;
		this.description = description;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "inward_type_id", nullable = false)
	public long getInwardTypeId() {
		return inwardTypeId;
	}

	public void setInwardTypeId(long inwardTypeId) {
		this.inwardTypeId = inwardTypeId;
	}


	@Column(name = "inward_type_name", nullable = false)
	public String getInwardTypeName() {
		return inwardTypeName;
	}


	public void setInwardTypeName(String inwardTypeName) {
		this.inwardTypeName = inwardTypeName;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getStatus() {
		return status;
	}

	public void setStatus(long status) {
		this.status = status;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "date_created")
	public java.util.Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(java.util.Date dateCreated) {
		this.dateCreated = dateCreated;
	}

}
