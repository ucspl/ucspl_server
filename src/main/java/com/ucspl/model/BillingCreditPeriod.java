package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "billing_credit_period_table")
public class BillingCreditPeriod {

	private long billingCreditPeriodId;
	private String billingCreditPeriodName;
	private long status;
	private String createdBy;
	private java.util.Date dateCreated;

	public BillingCreditPeriod() {

	}

	public BillingCreditPeriod(long billingCreditPeriodId, String billingCreditPeriodName, long status,
			String createdBy, Date dateCreated) {
		super();
		this.billingCreditPeriodId = billingCreditPeriodId;
		this.billingCreditPeriodName = billingCreditPeriodName;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "billing_credit_period_id", nullable = false)
	public long getBillingCreditPeriodId() {
		return billingCreditPeriodId;
	}

	public void setBillingCreditPeriodId(long billingCreditPeriodId) {
		this.billingCreditPeriodId = billingCreditPeriodId;
	}

	@Column(name = "billing_credit_period_name", nullable = false)
	public String getBillingCreditPeriodName() {
		return billingCreditPeriodName;
	}

	public void setBillingCreditPeriodName(String billingCreditPeriodName) {
		this.billingCreditPeriodName = billingCreditPeriodName;
	}

	public long getStatus() {
		return status;
	}

	public void setStatus(long status) {
		this.status = status;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "date_created")
	public java.util.Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(java.util.Date dateCreated) {
		this.dateCreated = dateCreated;
	}

}
