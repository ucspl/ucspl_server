package com.ucspl.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "enquiry_attachment_table_for_sales_and_quotation")
@Table(name = "enquiry_attachment_table_for_sales_and_quotation")
public class EnquiryAttachmentForSalesAndQuotation {

private long enquiryAttachmentId;
private String originalFileName;
private String attachedFileName;
private String type;
private String documentNumber;
private String fileLocation;

public EnquiryAttachmentForSalesAndQuotation(){
	
}

public EnquiryAttachmentForSalesAndQuotation(long enquiryAttachmentId, String originalFileName, String attachedFileName,
		String type, String documentNumber, String fileLocation) {
	super();
	this.enquiryAttachmentId = enquiryAttachmentId;
	this.originalFileName = originalFileName;
	this.attachedFileName = attachedFileName;
	this.type = type;
	this.documentNumber = documentNumber;
	this.fileLocation = fileLocation;
}

@Id
@Column(name = "enquiry_attachment_id")
@GeneratedValue(strategy = GenerationType.IDENTITY)
public long getEnquiryAttachmentId() {
	return enquiryAttachmentId;
}

public void setEnquiryAttachmentId(long enquiryAttachmentId) {
	this.enquiryAttachmentId = enquiryAttachmentId;
}

@Column(name = "original_file_name")
public String getOriginalFileName() {
	return originalFileName;
}

public void setOriginalFileName(String originalFileName) {
	this.originalFileName = originalFileName;
}

@Column(name = "attached_file_name")
public String getAttachedFileName() {
	return attachedFileName;
}

public void setAttachedFileName(String attachedFileName) {
	this.attachedFileName = attachedFileName;
}

public String getType() {
	return type;
}

public void setType(String type) {
	this.type = type;
}

@Column(name = "document_number")
public String getDocumentNumber() {
	return documentNumber;
}

public void setDocumentNumber(String documentNumber) {
	this.documentNumber = documentNumber;
}

@Column(name = "file_location")
public String getFileLocation() {
	return fileLocation;
}

public void setFileLocation(String fileLocation) {
	this.fileLocation = fileLocation;
}
	
	
	
}
	
	
	
	
	
	
	