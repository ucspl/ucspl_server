package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name = "customer_instrument_identification_table")
public class CustomerInstrumentIdentification {
	

	private long custInstruIdentificationId;
	private long reqDocId;
	private long custId;
	private long nomenclatureId;
	private String idNo;
	private String srNo;
	private String checkedBy;
	private String createdBy;
	private String modifiedBy;
	private java.util.Date dateCreated;
	private java.util.Date dateModified;
	
	public CustomerInstrumentIdentification() {
		
	}

	

	public CustomerInstrumentIdentification(long custInstruIdentificationId, long reqDocId, long custId,
			long nomenclatureId, String idNo, String srNo, String checkedBy, String createdBy, String modifiedBy,
			Date dateCreated, Date dateModified) {
		super();
		this.custInstruIdentificationId = custInstruIdentificationId;
		this.reqDocId = reqDocId;
		this.custId = custId;
		this.nomenclatureId = nomenclatureId;
		this.idNo = idNo;
		this.srNo = srNo;
		this.checkedBy = checkedBy;
		this.createdBy = createdBy;
		this.modifiedBy = modifiedBy;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
	}


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cust_instru_identification_id", nullable = false)
	public long getCustInstruIdentificationId() {
		return custInstruIdentificationId;
	}

	public void setCustInstruIdentificationId(long custInstruIdentificationId) {
		this.custInstruIdentificationId = custInstruIdentificationId;
	}
	
	@Column(name = "req_docu_id")
	public long getReqDocId() {
		return reqDocId;
	}

	public void setReqDocId(long reqDocId) {
		this.reqDocId = reqDocId;
	}


	@Column(name = "customer_id")
	public long getCustId() {
		return custId;
	}

	public void setCustId(long custId) {
		this.custId = custId;
	}

	@Column(name = "instruments_id")
	public long getNomenclatureId() {
		return nomenclatureId;
	}

	public void setNomenclatureId(long nomenclatureId) {
		this.nomenclatureId = nomenclatureId;
	}

	@Column(name = "id_number")
	public String getIdNo() {
		return idNo;
	}

	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}

	@Column(name = "sr_number")
	public String getSrNo() {
		return srNo;
	}

	public void setSrNo(String srNo) {
		this.srNo = srNo;
	}

	@Column(name = "createdby")
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "modifiedby")
	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Column(name = "datecreated")
	public java.util.Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(java.util.Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	@Column(name = "datemodified")
	public java.util.Date getDateModified() {
		return dateModified;
	}

	public void setDateModified(java.util.Date dateModified) {
		this.dateModified = dateModified;
	}
	
	
	
}