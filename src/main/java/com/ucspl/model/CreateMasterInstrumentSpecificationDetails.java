package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "create_master_instrument_specification_details_table")
public class CreateMasterInstrumentSpecificationDetails {

	private long masterSpecificationId;
	private long createMasterInstruId; 
	private long parameterId;
	private String masterIdParaName;
	private String parameterNumber;
	private String rangeFrom;
	private String rangeTo;
	private long rangeUom;
	private String freqFrom;
	private long freqFromUom;
	private String freqTo;
	private long freqToUom;
	private String leastCount;
	private long leastCountUom;
	private String accuracy;
	private long formulaAccuracy;
	private long uomAccuracy;
	private String accuracy1;
	private long formulaAccuracy1;
	private long uomAccuracy1;
	private long tempChartType;
	private long modeType;
	private int draft;
	private int approved;
	private int archieved;
	private int submitted;
	private int rejected;
	
	public CreateMasterInstrumentSpecificationDetails() {
		
	}

	public CreateMasterInstrumentSpecificationDetails(long masterSpecificationId, long createMasterInstruId,
			long parameterId, String masterIdParaName, String parameterNumber, String rangeFrom, String rangeTo,
			long rangeUom, String freqFrom, long freqFromUom, String freqTo, long freqToUom, String leastCount,
			long leastCountUom, String accuracy, long formulaAccuracy, long uomAccuracy, String accuracy1,
			long formulaAccuracy1, long uomAccuracy1, long tempChartType, long modeType, int draft, int approved,
			int archieved, int submitted, int rejected) {
		super();
		this.masterSpecificationId = masterSpecificationId;
		this.createMasterInstruId = createMasterInstruId;
		this.parameterId = parameterId;
		this.masterIdParaName = masterIdParaName;
		this.parameterNumber = parameterNumber;
		this.rangeFrom = rangeFrom;
		this.rangeTo = rangeTo;
		this.rangeUom = rangeUom;
		this.freqFrom = freqFrom;
		this.freqFromUom = freqFromUom;
		this.freqTo = freqTo;
		this.freqToUom = freqToUom;
		this.leastCount = leastCount;
		this.leastCountUom = leastCountUom;
		this.accuracy = accuracy;
		this.formulaAccuracy = formulaAccuracy;
		this.uomAccuracy = uomAccuracy;
		this.accuracy1 = accuracy1;
		this.formulaAccuracy1 = formulaAccuracy1;
		this.uomAccuracy1 = uomAccuracy1;
		this.tempChartType = tempChartType;
		this.modeType = modeType;
		this.draft = draft;
		this.approved = approved;
		this.archieved = archieved;
		this.submitted = submitted;
		this.rejected = rejected;
	}


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "master_specification_id", nullable = false)
	public long getMasterSpecificationId() {
		return masterSpecificationId;
	}

	public void setMasterSpecificationId(long masterSpecificationId) {
		this.masterSpecificationId = masterSpecificationId;
	}

	@Column(name = "createmasterinstru_id", nullable = false)
	public long getCreateMasterInstruId() {
		return createMasterInstruId;
	}

	public void setCreateMasterInstruId(long createMasterInstruId) {
		this.createMasterInstruId = createMasterInstruId;
	}

	@Column(name = "parameter_id")
	public long getParameterId() {
		return parameterId;
	}

	public void setParameterId(long parameterId) {
		this.parameterId = parameterId;
	}

	@Column(name = "masterid_paraname")
	public String getMasterIdParaName() {
		return masterIdParaName;
	}

	public void setMasterIdParaName(String masterIdParaName) {
		this.masterIdParaName = masterIdParaName;
	}

	@Column(name = "parameter_number")
	public String getParameterNumber() {
		return parameterNumber;
	}

	public void setParameterNumber(String parameterNumber) {
		this.parameterNumber = parameterNumber;
	}

	@Column(name = "range_from")
	public String getRangeFrom() {
		return rangeFrom;
	}

	public void setRangeFrom(String rangeFrom) {
		this.rangeFrom = rangeFrom;
	}

	@Column(name = "range_to")
	public String getRangeTo() {
		return rangeTo;
	}

	public void setRangeTo(String rangeTo) {
		this.rangeTo = rangeTo;
	}

	@Column(name = "range_uom")
	public long getRangeUom() {
		return rangeUom;
	}

	public void setRangeUom(long rangeUom) {
		this.rangeUom = rangeUom;
	}

	@Column(name = "freq_from")
	public String getFreqFrom() {
		return freqFrom;
	}

	public void setFreqFrom(String freqFrom) {
		this.freqFrom = freqFrom;
	}

	@Column(name = "freq_from_uom")
	public long getFreqFromUom() {
		return freqFromUom;
	}

	public void setFreqFromUom(long freqFromUom) {
		this.freqFromUom = freqFromUom;
	}

	@Column(name = "freq_to")
	public String getFreqTo() {
		return freqTo;
	}

	public void setFreqTo(String freqTo) {
		this.freqTo = freqTo;
	}

	@Column(name = "freq_to_uom")
	public long getFreqToUom() {
		return freqToUom;
	}

	public void setFreqToUom(long freqToUom) {
		this.freqToUom = freqToUom;
	}

	@Column(name = "least_count")
	public String getLeastCount() {
		return leastCount;
	}

	public void setLeastCount(String leastCount) {
		this.leastCount = leastCount;
	}

	@Column(name = "least_count_uom")
	public long getLeastCountUom() {
		return leastCountUom;
	}

	public void setLeastCountUom(long leastCountUom) {
		this.leastCountUom = leastCountUom;
	}

	@Column(name = "accuracy")
	public String getAccuracy() {
		return accuracy;
	}

	public void setAccuracy(String accuracy) {
		this.accuracy = accuracy;
	}

	@Column(name = "formula_accuracy")
	public long getFormulaAccuracy() {
		return formulaAccuracy;
	}

	public void setFormulaAccuracy(long formulaAccuracy) {
		this.formulaAccuracy = formulaAccuracy;
	}

	@Column(name = "uom_accuracy")
	public long getUomAccuracy() {
		return uomAccuracy;
	}

	public void setUomAccuracy(long uomAccuracy) {
		this.uomAccuracy = uomAccuracy;
	}

	@Column(name = "accuracy1")
	public String getAccuracy1() {
		return accuracy1;
	}

	public void setAccuracy1(String accuracy1) {
		this.accuracy1 = accuracy1;
	}

	@Column(name = "formula_accuracy1")
	public long getFormulaAccuracy1() {
		return formulaAccuracy1;
	}

	public void setFormulaAccuracy1(long formulaAccuracy1) {
		this.formulaAccuracy1 = formulaAccuracy1;
	}

	@Column(name = "uom_accuracy1")
	public long getUomAccuracy1() {
		return uomAccuracy1;
	}

	public void setUomAccuracy1(long uomAccuracy1) {
		this.uomAccuracy1 = uomAccuracy1;
	}

	@Column(name = "temp_chart_type")
	public long getTempChartType() {
		return tempChartType;
	}

	public void setTempChartType(long tempChartType) {
		this.tempChartType = tempChartType;
	}

	@Column(name = "mode_type")
	public long getModeType() {
		return modeType;
	}

	public void setModeType(long modeType) {
		this.modeType = modeType;
	}

	public int getDraft() {
		return draft;
	}

	public void setDraft(int draft) {
		this.draft = draft;
	}

	public int getApproved() {
		return approved;
	}

	public void setApproved(int approved) {
		this.approved = approved;
	}

	public int getArchieved() { 
		return archieved;
	}

	public void setArchieved(int archieved) {
		this.archieved = archieved;
	}

	public int getSubmitted() {
		return submitted;
	}

	public void setSubmitted(int submitted) {
		this.submitted = submitted;
	}

	public int getRejected() {
		return rejected;
	}

	public void setRejected(int rejected) {
		this.rejected = rejected;
	}
	

}
