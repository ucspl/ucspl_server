

package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "unc_formula_of_nabl_table")
public class UncFormulaForNabl {

	private long uncFormulaId;
	private String uncFormulaName;
	private String description;
	private long status;
	private String createdBy;
	private String dateCreated;

	public UncFormulaForNabl() {
		// TODO Auto-generated constructor stub

	}

	public UncFormulaForNabl(long uncFormulaId, String uncFormulaName, String description, long status,
			String createdBy, String dateCreated) {
		super();
		this.uncFormulaId = uncFormulaId;
		this.uncFormulaName = uncFormulaName;
		this.description = description;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}



	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "unc_formula_id", nullable = false)
	public long getUncFormulaId() {
		return uncFormulaId;
	}

	public void setUncFormulaId(long uncFormulaId) {
		this.uncFormulaId = uncFormulaId;
	}
	@Column(name = "unc_formula_name", nullable = false)
	public String getUncFormulaName() {
		return uncFormulaName;
	}

	public void setUncFormulaName(String uncFormulaName) {
		this.uncFormulaName = uncFormulaName;
	}
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getStatus() {
		return status;
	}

	public void setStatus(long status) {
		this.status = status;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "date_created")
	public String getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}

}
