package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "states_table")
public class States {

	private long stateId;
	private long countryId;
	private String stateName;
	private long stateCode;
	
	public States()
	{
		
	}
	
	
	public States(long stateId, long countryId, String stateName, long stateCode) {
		super();
		this.stateId = stateId;
		this.countryId = countryId;
		this.stateName = stateName;
		this.stateCode = stateCode;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "state_id", nullable = false)
	public long getStateId() {
		return stateId;
	}


	public void setStateId(long stateId) {
		this.stateId = stateId;
	}

	@Column(name = "country_id", nullable = false)
	public long getCountryId() {
		return countryId;
	}


	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	@Column(name = "state_name")
	public String getStateName() {
		return stateName;
	}


	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	@Column(name = "state_code_for_tax")
	public long getStateCode() {
		return stateCode;
	}


	public void setStateCode(long stateCode) {
		this.stateCode = stateCode;
	}


	@Override
	public String toString() {
		return "States [stateId=" + stateId + ", countryId=" + countryId + ", stateName=" + stateName + ", stateCode="
				+ stateCode + "]";
	}
	
	
	
}
