package com.ucspl.model;

import javax.persistence.Column;

public class SearchDefinition {

	private String searchDefinitionValue;
	private int status;
	
	public SearchDefinition() {
		
	}

	public SearchDefinition(String searchDefinitionValue, int status) {
		super();
		this.searchDefinitionValue = searchDefinitionValue;
		this.status = status;
	}

	@Column(name = "searchdefinition", nullable = false)
	public String getSearchDefinitionValue() {
		return searchDefinitionValue;
	}

	public void setSearchDefinitionValue(String searchDefinitionValue) {
		this.searchDefinitionValue = searchDefinitionValue;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	
}
