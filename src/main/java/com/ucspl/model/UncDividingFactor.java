
package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="unc_dividing_factor_table")
public class UncDividingFactor {

	
	private long uncDividingFactorId;
	private String uncDividingFactorName;
	private String description;
	private int status;
	private String createdBy;
	private String dateCreated;
	
	
	public UncDividingFactor() {
		
	}

	public UncDividingFactor(long uncDividingFactorId, String uncDividingFactorName, String description, int status,
			String createdBy, String dateCreated) {
		super();
		this.uncDividingFactorId = uncDividingFactorId;
		this.uncDividingFactorName = uncDividingFactorName;
		this.description = description;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "dividing_factor_id", nullable = false)
	public long getUncDividingFactorId() {
		return uncDividingFactorId;
	}

	public void setUncDividingFactorId(long uncDividingFactorId) {
		this.uncDividingFactorId = uncDividingFactorId;
	}
	
	@Column(name = "dividing_factor_name")
	public String getUncDividingFactorName() {
		return uncDividingFactorName;
	}

	public void setUncDividingFactorName(String uncDividingFactorName) {
		this.uncDividingFactorName = uncDividingFactorName;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "date_created")
	public String getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	
	
}
