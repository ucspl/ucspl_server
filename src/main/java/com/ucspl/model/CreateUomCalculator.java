package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "create_uom_calculator_table")
public class CreateUomCalculator {

	private long uomCalculatorId;
	private long parameter;
	private int draft;
	private int submitted;
	private int approved;
	// private long archieved;
	// private long rejected;

	public CreateUomCalculator() {

	}

	public CreateUomCalculator(long uomCalculatorId, long parameter, int draft, int submitted, int approved) {
		super();
		this.uomCalculatorId = uomCalculatorId;
		this.parameter = parameter;
		this.draft = draft;
		this.submitted = submitted;
		this.approved = approved;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "uom_calculator_id", nullable = false)
	public long getUomCalculatorId() {
		return uomCalculatorId;
	}

	public void setUomCalculatorId(long uomCalculatorId) {
		this.uomCalculatorId = uomCalculatorId;
	}

	@Column(name = "parameter_id", nullable = false)
	public long getParameter() {
		return parameter;
	}

	public void setParameter(long parameter) {
		this.parameter = parameter;
	}

	public int getDraft() {
		return draft;
	}

	public void setDraft(int draft) {
		this.draft = draft;
	}

	public int getSubmitted() {
		return submitted;
	}

	public void setSubmitted(int submitted) {
		this.submitted = submitted;
	}

	public int getApproved() {
		return approved;
	}

	public void setApproved(int approved) {
		this.approved = approved;
	}

}
