package com.ucspl.model;

import java.awt.*;
public class DemoFrame  extends Frame 
{
	Button b1;
	TextField t1,t2;
	Label l1,l2;
	TextArea addr;
	List lst;
	Choice ch;
	Checkbox cb1,cb2,cb3;
	CheckboxGroup gp1;
	public DemoFrame()
	{
	  setSize(300,300);
	  setTitle("This is AWT Frame");
	  setLocation(100,100);
	  setVisible(true);
	 // setLayout(new FlowLayout());
	  
	  l1=new Label("Name ");
	  l1.setVisible(true);
	  //l1.setBounds(10, 30, 100, 35);
	  this.add(l1);
	  
	  t1=new TextField(20);
	  t1.setVisible(true);
	  //t1.setBounds(110, 30, 100, 35);
	  add(t1);
	 
	  
	  l2=new Label("Mobile ");
	  l2.setVisible(true);
	  //l1.setBounds(10, 30, 100, 35);
	  this.add(l2);
	  
	  t2=new TextField(20);
	  t2.setVisible(true);
	  //t1.setBounds(110, 30, 100, 35);
	  add(t2);
	 
	  addr=new TextArea(10,30);
	  addr.setVisible(true);
	  add(addr);
	  
	  lst=new List(3,true);
	  lst.setVisible(true);
	  lst.add("Pune");
	  lst.add("Mumbai");
	  lst.add("Nasik");
	  lst.add("Nagpur");
	  add(lst);
	  
	  ch=new Choice();
	  ch.add("Java");
	  ch.add("C");
	  ch.add("C++");
	  ch.add("DS");
	  ch.setVisible(true);
	  add(ch);
	  
	  gp1=new CheckboxGroup();
	  
	  cb1=new Checkbox("Reading",true,gp1);
	  cb2=new Checkbox("Running",false,gp1);
	  cb3=new Checkbox("Writting");
	  cb1.setVisible(true);
	  cb2.setVisible(true);
	  cb3.setVisible(true);
	  add(cb1);
	  add(cb2);
	  add(cb3);
	 
			  
	  b1=new Button("Submit");
	  //b1.setBounds(10,100,80,35);
	  b1.setVisible(true);
	  this.add(b1);
	  // Layout-newFlowLayout();
	  
	}
	

}
