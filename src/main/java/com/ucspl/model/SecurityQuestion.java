package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "security_question_table")
public class SecurityQuestion {

	private long securityQuestionId;
	private String SecurityQuestionName;
	private long status;
	private String createdBy;
	private java.util.Date dateCreated;

	public SecurityQuestion() {
		// TODO Auto-generated constructor stub

	}

	public SecurityQuestion(long securityQuestionId, String securityQuestionName, long status, String createdBy,
			Date dateCreated) {
		super();
		this.securityQuestionId = securityQuestionId;
		SecurityQuestionName = securityQuestionName;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "security_question_id", nullable = false)
	public long getSecurityQuestionId() {
		return securityQuestionId;
	}

	public void setSecurityQuestionId(long securityQuestionId) {
		this.securityQuestionId = securityQuestionId;
	}

	@Column(name = "security_question_name", nullable = false)
	public String getSecurityQuestionName() {
		return SecurityQuestionName;
	}

	public void setSecurityQuestionName(String securityQuestionName) {
		SecurityQuestionName = securityQuestionName;
	}

	public long getStatus() {
		return status;
	}

	public void setStatus(long status) {
		this.status = status;
	}

	@Column(name = "created_by", nullable = false)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "date_created", nullable = false)
	public java.util.Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(java.util.Date dateCreated) {
		this.dateCreated = dateCreated;
	}

}
