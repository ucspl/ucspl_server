package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tax_table")
public class Taxes {

	private long taxId;
	private String taxName;
	
	public Taxes() {
		
	}


	public Taxes(long taxId, String taxName) {
		super();
		this.taxId = taxId;
		this.taxName = taxName;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tax_id", nullable = false)
	public long getTaxId() {
		return taxId;
	}


	public void setTaxId(long taxId) {
		this.taxId = taxId;
	}


	@Column(name = "tax_name", nullable = false)
	public String getTaxName() {
		return taxName;
	}


	public void setTaxName(String taxName) {
		this.taxName = taxName;
	}


	
	
}
