package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "temp_chart_type_of_master_instrument_table")
public class TempChartTypeForMasterInstrument {

	private long tempChartId;
	private String tempChartName;
	private String description;
	private long status;
	private String createdBy;
	private java.util.Date dateCreated;
	
	public TempChartTypeForMasterInstrument() {
		
	}

	public TempChartTypeForMasterInstrument(long tempChartId, String tempChartName, String description, long status,
			String createdBy, Date dateCreated) {
		super();
		this.tempChartId = tempChartId;
		this.tempChartName = tempChartName;
		this.description = description;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "temp_chart_type_id", nullable = false)
	public long getTempChartId() {
		return tempChartId;
	}

	public void setTempChartId(long tempChartId) {
		this.tempChartId = tempChartId;
	}

	@Column(name = "temp_chart_type_name")
	public String getTempChartName() {
		return tempChartName;
	}

	public void setTempChartName(String tempChartName) {
		this.tempChartName = tempChartName;
	}
	

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}


	public long getStatus() {
		return status;
	}
	public void setStatus(long status) {
		this.status = status;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "date_created")
	public java.util.Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(java.util.Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	

}
