

package com.ucspl.model;

public class SearchMasterByMasterName {

	private long envMasterId;
	private String searchDocument;
	private String calibratedAt;

	public SearchMasterByMasterName() {
		
	}
	
	public SearchMasterByMasterName(long envMasterId,String searchDocument,String calibratedAt) {
		super();
		this.envMasterId=envMasterId;
		this.searchDocument = searchDocument;
		this.calibratedAt = calibratedAt;
	}
	
	

	public long getEnvMasterId() {
		return envMasterId;
	}

	public void setEnvMasterId(long envMasterId) {
		this.envMasterId = envMasterId;
	}

	public String getSearchDocument() {
		return searchDocument;
	}

	public void setSearchDocument(String searchDocument) {
		this.searchDocument = searchDocument;
	}

	public String getCalibratedAt() {
		return calibratedAt;
	}

	public void setCalibratedAt(String calibratedAt) {
		this.calibratedAt = calibratedAt;
	}

	
	
	
}
