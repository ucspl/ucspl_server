package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "invoice_type_for_invoice_table")
public class InvoiceTypeForInvoice {
	
	private long invoiceTypeId;
	private String invoiceTypeName;
	private String description;
	private long status;
	private String createdBy;
	private java.util.Date dateCreated;

	public InvoiceTypeForInvoice() {
		
	}

	public InvoiceTypeForInvoice(long invoiceTypeId, String invoiceTypeName, String description, long status,
			String createdBy, Date dateCreated) {
		super();
		this.invoiceTypeId = invoiceTypeId;
		this.invoiceTypeName = invoiceTypeName;
		this.description = description;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "invoice_type_id", nullable = false)
	public long getInvoiceTypeId() {
		return invoiceTypeId;
	}

	public void setInvoiceTypeId(long invoiceTypeId) {
		this.invoiceTypeId = invoiceTypeId;
	}

	@Column(name = "invoice_type_name", nullable = false)
	public String getInvoiceTypeName() {
		return invoiceTypeName;
	}

	public void setInvoiceTypeName(String invoiceTypeName) {
		this.invoiceTypeName = invoiceTypeName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getStatus() {
		return status;
	}

	public void setStatus(long status) {
		this.status = status;
	}
	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "date_created")
	public java.util.Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(java.util.Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	
}
