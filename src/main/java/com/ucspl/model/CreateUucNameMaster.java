package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="create_uuc_name_master_table")
public class CreateUucNameMaster {
	
	public long createUucNameMasterId;
	public long instrument;
	public int draft;
	public int archieved;
	public int submitted;
	public int rejected;
	public int approved;
	
	public CreateUucNameMaster() {
		
	}
	
	public CreateUucNameMaster(long createUucNameMasterId, long instrument, int draft, int archieved, int submitted,
			int rejected, int approved) {
		super();
		this.createUucNameMasterId = createUucNameMasterId;
		this.instrument = instrument;
		this.draft = draft;
		this.archieved = archieved;
		this.submitted = submitted;
		this.rejected = rejected;
		this.approved = approved;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "create_uuc_name_master_id", nullable = false)
	public long getCreateUucNameMasterId() {
		return createUucNameMasterId;
	}

	public void setCreateUucNameMasterId(long createUucNameMasterId) {
		this.createUucNameMasterId = createUucNameMasterId;
	}

	@Column(name = "instrument_id")
	public long getInstrument() {
		return instrument;
	}

	public void setInstrument(long instrument) {
		this.instrument = instrument;
	}

	@Column(name = "draft")
	public int getDraft() {
		return draft;
	}

	public void setDraft(int draft) {
		this.draft = draft;
	}


//	public int getArchieved() {
//		return archieved;
//	}
//
//	public void setArchieved(int archieved) {
//		this.archieved = archieved;
//	}

	@Column(name = "submitted")
	public int getSubmitted() {
		return submitted;
	}

	public void setSubmitted(int submitted) {
		this.submitted = submitted;
	}

//	public int getRejected() {
//		return rejected;
//	}
//
//	public void setRejected(int rejected) {
//		this.rejected = rejected;
//	}

	@Column(name = "approved")
	public int getApproved() {
		return approved;
	}

	public void setApproved(int approved) {
		this.approved = approved;
	}
	
	

}
