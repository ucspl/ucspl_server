package com.ucspl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="formula_accuracy_list_for_req_and_iwd_table")
public class FormulaAccuracyReqAndIwd {

	private long formulaAccuracyId;
	private String formulaAccuracyName;
	private String description;
	private int status;
	private String createdBy;
	private String dateCreated;
	
	public FormulaAccuracyReqAndIwd() {
		
	}
	


	public FormulaAccuracyReqAndIwd(long formulaAccuracyId, String formulaAccuracyName, String description, int status,
			String createdBy, String dateCreated) {
		super();
		this.formulaAccuracyId = formulaAccuracyId;
		this.formulaAccuracyName = formulaAccuracyName;
		this.description = description;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}



	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "formula_accuracy_id", nullable = false)
	public long getFormulaAccuracyId() {
		return formulaAccuracyId;
	}

	public void setFormulaAccuracyId(long formulaAccuracyId) {
		this.formulaAccuracyId = formulaAccuracyId;
	}

	@Column(name = "formula_accuracy_name")
	public String getFormulaAccuracyName() {
		return formulaAccuracyName;
	}

	public void setFormulaAccuracyName(String formulaAccuracyName) {
		this.formulaAccuracyName = formulaAccuracyName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}


	@Column(name = "date_created")
	public String getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	
	
}
