package com.ucspl.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "region_area_code_table")
public class RegionAreaCode {

	private long regionAreaCodeId;
	private String regionAreaCodeName;
	private long status;
	private String createdBy;
	private java.util.Date dateCreated;

	public RegionAreaCode() {
		// TODO Auto-generated constructor stub
	}

	public RegionAreaCode(long regionAreaCodeId, String regionAreaCodeName, long status, String createdBy,
			Date dateCreated) {
		super();
		this.regionAreaCodeId = regionAreaCodeId;
		this.regionAreaCodeName = regionAreaCodeName;
		this.status = status;
		this.createdBy = createdBy;
		this.dateCreated = dateCreated;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "region_area_code_id", nullable = false)
	public long getRegionAreaCodeId() {
		return regionAreaCodeId;
	}

	public void setRegionAreaCodeId(long regionAreaCodeId) {
		this.regionAreaCodeId = regionAreaCodeId;
	}

	@Column(name = "region_area_code_name", nullable = false)
	public String getRegionAreaCodeName() {
		return regionAreaCodeName;
	}

	public void setRegionAreaCodeName(String regionAreaCodeName) {
		this.regionAreaCodeName = regionAreaCodeName;
	}

	public long getStatus() {
		return status;
	}

	public void setStatus(long status) {
		this.status = status;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "date_created")
	public java.util.Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(java.util.Date dateCreated) {
		this.dateCreated = dateCreated;
	}

}
