package com.ucspl.util;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

import com.ucspl.controller.UserController;

public class EmailUtil {
	
	private static final Logger logger = Logger.getLogger(EmailUtil.class);
	
	public void sendEmail(String host,String from,String to,String password, String userPassword, String userName) {
		
		 //Get the session object  
	      Properties props = new Properties(); 

	      props.setProperty("mail.transport.protocol", "smtp");    
	      props.put("mail.smtp.starttls.enable", "true");
	      props.put("mail.smtp.host",host);  
	      props.put("mail.smtp.auth", "true");  
	      props.put("mail.smtp.auth", "true"); 
	      props.put("mail.smtp.port", "465"); 
	      props.put("mail.debug", "true"); 
	      props.put("mail.smtp.socketFactory.port", "465");
	      props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory"); 
	      props.put("mail.smtp.socketFactory.fallback", "false");

	      
	        
	      Session session = Session.getDefaultInstance(props,  
	       new javax.mail.Authenticator() {  
	         protected javax.mail.PasswordAuthentication getPasswordAuthentication() {  
	       return new javax.mail.PasswordAuthentication(from,password);  
	         }  
	       });  
	     
	      //Compose the message  
	       try {  
	        MimeMessage message = new MimeMessage(session);  
	        message.setFrom();  
	        message.setRecipients(Message.RecipientType.TO,to);  
	        message.setSubject("Your Password");  
	        message.setText("Your Passord for UCSPL Software is \n User Name: "+userName + " \n Password : "+userPassword);  
	          
	       //send the message  
	        Transport.send(message);  
	     
	        logger.info("/forgot_password - mail sent successfully...");  
	      
	        } catch (MessagingException e) {e.printStackTrace();
	        
	        logger.error("/forgot_password - Error while sending mail !!");  
	        
	        }  
	}

}
