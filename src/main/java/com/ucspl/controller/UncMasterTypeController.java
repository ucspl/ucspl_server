package com.ucspl.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.UncMasterType;
import com.ucspl.model.UomListForReqAndIwd;
import com.ucspl.services.CreateDefinitionService;



@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class UncMasterTypeController {

	private static final Logger logger = Logger.getLogger(UncMasterTypeController.class);

	@Autowired
	private CreateDefinitionService createDefinitionService;

	public UncMasterTypeController() {

	}

//get all uncMasType list	
	@GetMapping("/crtUncMasterType")
	public List<UncMasterType> getAllUncMasterTypeList() {
		logger.info("/uncMasType - Get all uncMasType List!");

		List<UncMasterType> resultValue = createDefinitionService.getAllUncMasterTypeList();

		return resultValue;
	}

//Save create uncMasType Details 
	@PostMapping("/crtUncMasterType")
	public UncMasterType createUncMasterType(@Valid @RequestBody UncMasterType uncMasterType) {

		UncMasterType response = createDefinitionService.createUncMasterType(uncMasterType);
		return response;
	}

}
