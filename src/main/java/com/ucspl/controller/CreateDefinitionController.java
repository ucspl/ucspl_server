package com.ucspl.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.CreateDefinition;
import com.ucspl.model.SearchDefinition;
import com.ucspl.model.SearchSalesAndQuotation;
import com.ucspl.services.CreateDefinitionService;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class CreateDefinitionController {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateDefinitionService createDefinitionServiceObject;

	public CreateDefinitionController() {
		
	}
	
//	// Save create definition Details
//		@PostMapping("/createdefinition")
//		public CreateDefinition createDefinition(@Valid @RequestBody CreateDefinition createDefinitionObject) {
//
//			CreateDefinition response = createDefinitionServiceObject.createDefinition(createDefinitionObject);
//			return response;
//		}
		
		
//		// code for search and modify definition
//
//		@RequestMapping(value = "/searchdefinition", method = RequestMethod.POST, produces = "application/json")
//		public String searchAndModifyDefinition(
//				@Valid @RequestBody SearchDefinition searchDefinitionObject) {
//
//			String response = createDefinitionServiceObject.searchAndModifyDefinition(searchDefinitionObject);
//			return response;
//
//		}
//
//	//Result For Search Definition
//		@GetMapping(value = "/searchdefinition", produces = "application/json")
//		public List<Object[]> getAllDefinition() {
//
//			List<Object[]> resultValue = createDefinitionServiceObject.getAllDefinition();
//
//			return resultValue;
//
//		}
}
