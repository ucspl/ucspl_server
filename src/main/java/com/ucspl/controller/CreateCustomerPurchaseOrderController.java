package com.ucspl.controller;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateCustomerPurchaseOrder;
import com.ucspl.model.CreateSalesQuotation;
import com.ucspl.model.SearchPo;
import com.ucspl.model.SearchSalesAndQuotation;
import com.ucspl.model.SrNoPurchaseOrderDocument;
import com.ucspl.repository.CreateCustomerPurchaseOrderRepository;
import com.ucspl.services.CreateCustomerPurchaseOrderService;
import com.ucspl.services.CreateSalesQuotationService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class CreateCustomerPurchaseOrderController {

	@Autowired
	private CreateCustomerPurchaseOrderService createCustomerPurchaseOrderService;

	@PersistenceContext
	private EntityManager em;


	public CreateCustomerPurchaseOrderController() {
		// TODO Auto-generated constructor stub
	}

	// Get All Created Customer Purchase Orders
	@GetMapping("/createcustpo")
	public List<CreateCustomerPurchaseOrder> getAllCreateCustomerPurchaseOrders() {

		 List<CreateCustomerPurchaseOrder> response=createCustomerPurchaseOrderService.getAllCreateCustomerPurchaseOrders();
		 return response;
	}

	// Get create Po Details for id
	@GetMapping("/createcustpo/{id}")
	public ResponseEntity<CreateCustomerPurchaseOrder> getCustomerPurchaseOrderById(
			@PathVariable(value = "id") Long createCustomerPurchaseOrderId) throws ResourceNotFoundException {
		
		ResponseEntity<CreateCustomerPurchaseOrder> response=createCustomerPurchaseOrderService.getCustomerPurchaseOrderById(createCustomerPurchaseOrderId);
		return response;
	}
	
	// Save the Customer Purchase Order
	@PostMapping("/createcustpo")
	public CreateCustomerPurchaseOrder createCustomerPurchaseOrder(
			@Valid @RequestBody CreateCustomerPurchaseOrder createCustomerPurchaseOrder) {
		CreateCustomerPurchaseOrder response=createCustomerPurchaseOrderService.createCustomerPurchaseOrder(createCustomerPurchaseOrder);
	     return response;
	}
	
	// Update Create Sales Quotation Document Screen Details for id
	@PutMapping("/createcustpo/{id}")
	public ResponseEntity<CreateCustomerPurchaseOrder> updateCustomerPo(@PathVariable(value = "id") Long poId,
			@Valid @RequestBody CreateCustomerPurchaseOrder createCustomerPurchaseOrderDetails) throws ResourceNotFoundException {

		ResponseEntity<CreateCustomerPurchaseOrder> response = createCustomerPurchaseOrderService.updatePo(poId,
				createCustomerPurchaseOrderDetails);
		return response;
	}
	
	//Get last saved document number
	@GetMapping(path = { "/getPODocumentNo" })
	public String getDocumentNumber() throws IOException {

		String response = createCustomerPurchaseOrderService.getDocumentNumber();
		return response;
	}
		
////////////////////////////////////////// code for search po ///////////////////////////////////////////
	@RequestMapping(value = "/searchPo", method = RequestMethod.POST, produces = "application/json")
	public String searchAndModifyPo(
			@Valid @RequestBody SearchPo searchPoObject) {

		String response = createCustomerPurchaseOrderService.searchAndModifyPo(searchPoObject);
		return response;
	}

	//Result For Search Sales quotation Document
	@GetMapping(value = "/searchPo", produces = "application/json")
	public List<Object[]> getAllPo() {

		List<Object[]> resultValue = createCustomerPurchaseOrderService.getAllPo();
		return resultValue;
	}

	// STORE PROCEDURE FOR GETTING DETAILS OF CREATE PO FOR SEARCH PO
	@RequestMapping(value = "/createPoDetails", method = RequestMethod.POST, produces = "application/json")
	public String docNoForGettingCreatePoDetails(@Valid @RequestBody String docNo) {

		String response = createCustomerPurchaseOrderService.docNoForGettingCreatePoDetails(docNo);
		return response;
	}

	@GetMapping(value = "/createPoDetails", produces = "application/json")
	public List<Object[]> getCreatePODetailsForSearchPo() {

		List<Object[]> resultValueSet = createCustomerPurchaseOrderService.getCreatePODetailsForSearchPo();
		return resultValueSet;
	}
	
	
	// Update remaining quantity in sr no po table
		@PutMapping("/updateRemQuantity/{id}")
		public ResponseEntity<SrNoPurchaseOrderDocument> updateRemainingQuantity(@PathVariable(value = "id") Long srNoId,
				@Valid @RequestBody SrNoPurchaseOrderDocument remaningQuantity) throws ResourceNotFoundException {
			System.out.println("data :"+srNoId +"quantity"+remaningQuantity.getRemainingQuantity());

			ResponseEntity<SrNoPurchaseOrderDocument> response = createCustomerPurchaseOrderService.updateRemainingQuantity(srNoId,
					remaningQuantity);
			return response;
		}
		
		
		
		// Update remaining quantity in create po table
				@PutMapping("/updateRemPoValue/{id}")
				public ResponseEntity<CreateCustomerPurchaseOrder> updateRemainingPoValue(@PathVariable(value = "id") Long createTableId,
						@Valid @RequestBody CreateCustomerPurchaseOrder remaningPovalue) throws ResourceNotFoundException {

					ResponseEntity<CreateCustomerPurchaseOrder> response = createCustomerPurchaseOrderService.updateRemainingPoValue(createTableId,
							remaningPovalue);
					return response;
				}
				
}