package com.ucspl.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.User;
import com.ucspl.model.UserByName;
import com.ucspl.services.UserService;
import com.ucspl.util.EmailUtil;

@RestController
@CrossOrigin
@RequestMapping("/api/v1")
public class UserController {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private UserService userService;

	public UserController() {
		// TODO Auto-generated constructor stub.....
	}

	// Get all Users information
	@GetMapping("/users")
	public List<User> getAllUsers() {

		List<User> allUserList = userService.getAllUsers();
		return allUserList;

	}

	// Get User Information By Id
	@GetMapping("/users/{id}")
	public ResponseEntity<User> getUserById(@PathVariable(value = "id") Long userId) throws ResourceNotFoundException {

		ResponseEntity<User> response = userService.getUserById(userId);
		return response;
	}

	// Save User Details
	@PostMapping("/users")
	public User createUser(@Valid @RequestBody User user) {

		User response = userService.createUser(user);
		return response;
	}

	// Update User Details for id
	@PutMapping("/users/{id}")
	public ResponseEntity<User> updateUser(@PathVariable(value = "id") Long userId,
			@Valid @RequestBody User userDetails) throws ResourceNotFoundException {

		ResponseEntity<User> response = userService.updateUser(userId, userDetails);
		return response;
	}

	// Delete User Details for id
	@DeleteMapping("/users/{id}")
	public Map<String, Boolean> deleteUser(@PathVariable(value = "id") Long userId) throws ResourceNotFoundException {

		Map<String, Boolean> response = new HashMap<>();
		response = userService.deleteUser(userId);
		return response;
	}

	// Search User
	@RequestMapping(value = "/search_user", method = RequestMethod.POST, produces = "application/json")
	public String searchAndModifyUser(@Valid @RequestBody UserByName user) {

		String response = userService.searchAndModifyUser(user);
		return response;
	}

	// Get Search result based on Name or Department
	@GetMapping(value = "/search_user", produces = "application/json")
	public List<Object[]> getAllUsersByNameOrDepartment() {
		List<Object[]> resultValue = userService.getAllUsersByNameOrDepartment();
		return resultValue;
	}

	
	
	
	// Search user for id for update user
	@RequestMapping(value = "/CUserDetails", method = RequestMethod.POST, produces = "application/json")
	public User userNoForGettingUserDetails(@Valid @RequestBody long id) throws ResourceNotFoundException {

		User response = userService.userNoForGettingUserDetails(id);
		return response;
	}

// Forgot Password 

	@PostMapping("/forgot_password")
	public void forgotPassword(@Valid @RequestBody User user) {
		userService.forgotPassword(user);
	}

}