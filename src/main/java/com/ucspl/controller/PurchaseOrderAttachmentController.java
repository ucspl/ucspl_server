
package com.ucspl.controller;

import java.io.IOException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.ImageModel;
import com.ucspl.model.PurchaseOrderAttachment;
import com.ucspl.services.PurchaseOrderAttachmentService;
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class PurchaseOrderAttachmentController {

	PurchaseOrderAttachment imgData= new PurchaseOrderAttachment();
	
	@Autowired
	private PurchaseOrderAttachmentService purchaseOrderAttachmentServiceObject;
	
	public PurchaseOrderAttachmentController() {
	                              
	}
	
	@PostMapping("/uploadPoAttach")
	public ResponseEntity<PurchaseOrderAttachment> uplaodImage(@RequestParam("imageFile") MultipartFile file) throws IOException {

		ResponseEntity<PurchaseOrderAttachment>response = purchaseOrderAttachmentServiceObject.uplaodImage(file);
		return response;
	}	

	@PostMapping("/PoAttach")
	public PurchaseOrderAttachment createPoAttachment(
			@Valid @RequestBody PurchaseOrderAttachment poAttachment) {
		
		PurchaseOrderAttachment response=purchaseOrderAttachmentServiceObject.createPoAttachment(poAttachment);	                                                  	
		return response;
	}
	
	@PostMapping("/getSelPOAttach")
	public ImageModel getSelectedPoFileInByteFormat(@Valid @RequestBody PurchaseOrderAttachment attachFile) throws IOException {

		ImageModel img= purchaseOrderAttachmentServiceObject.getSelectedPoFileInByteFormat(attachFile);
		return img;
	}
	
	@GetMapping(path = { "/getPOAttFiles/{supp_id}" })
	public List<PurchaseOrderAttachment> getImage(@PathVariable("supp_id") String suppId) throws IOException {

		List<PurchaseOrderAttachment> retrievedImage = purchaseOrderAttachmentServiceObject.getImage(suppId);
		return retrievedImage;
	}
	
	@GetMapping("/poAttach")
	public List<PurchaseOrderAttachment> getAllPoAttachments() {

		List<PurchaseOrderAttachment> response = purchaseOrderAttachmentServiceObject.getAllPoAttachments();
		return response;
	}
	
	@GetMapping("/poAttach/{id}")
	public ResponseEntity<PurchaseOrderAttachment> getPoAttachmentById(
			@PathVariable(value = "id") Long poAttachmentsId) throws ResourceNotFoundException {
	
		ResponseEntity<PurchaseOrderAttachment> response = purchaseOrderAttachmentServiceObject.getPoAttachmentById(poAttachmentsId);
		return response;
	}

	// code to delete Po File                                                                                                                                                                                                                                    
	@PostMapping("/deleteAttachedPoFile")                                                                                                
	public String removePoFile(@Valid @RequestBody long indexToDelete) throws IOException {

		String response = purchaseOrderAttachmentServiceObject.removePoFile(indexToDelete);
		return response;
	}
				
}
	
	
	





	
	
	
	
		

	
	