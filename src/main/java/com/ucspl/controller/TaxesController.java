package com.ucspl.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.Taxes;
import com.ucspl.repository.TaxesRepository;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class TaxesController {

	
	private static final Logger logger = Logger.getLogger(TaxesController.class);

	@Autowired
	private TaxesRepository taxesRepository;

	public TaxesController() {
	

	}

	@GetMapping("/taxes")
	public List<Taxes> getAllBranches() {
		logger.info("/taxes - Get all taxes!");

		return taxesRepository.findAll();

	}
	
}
