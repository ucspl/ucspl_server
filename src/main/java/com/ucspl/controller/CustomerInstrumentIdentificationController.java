package com.ucspl.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CustomerInstrumentIdentification;
import com.ucspl.services.CustomerInstrumentIdentificationService;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class CustomerInstrumentIdentificationController {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CustomerInstrumentIdentificationService customerInstrumentIdentificationService;

	public CustomerInstrumentIdentificationController() {
		// TODO Auto-generated constructor stub
	}

// Get all customer instrument identification
	@GetMapping("/cusInsruIdenty") 
	public List<CustomerInstrumentIdentification> getAllCustomerInstrumentIdentification() {

		List<CustomerInstrumentIdentification> response = customerInstrumentIdentificationService.getAllCustomerInstrumentIdentification();
		return response;
	}

// Get customer instrument identification by id
	@GetMapping("/cusInsruIdenty/{id}")
	public ResponseEntity<CustomerInstrumentIdentification> getCustomerInstrumentIdentificationByCustInstruId(
			@PathVariable(value = "id") Long CustomerInstrumentIdentificationId) throws ResourceNotFoundException {
		
		ResponseEntity<CustomerInstrumentIdentification> response = customerInstrumentIdentificationService.getCustomerInstrumentIdentificationByCustInstruId(CustomerInstrumentIdentificationId);
		return response;	
	}

// Create customer instrument identification
	@PostMapping("/cusInsruIdenty")
	public CustomerInstrumentIdentification createCustomerInstrumentIdentification(
			@Valid @RequestBody CustomerInstrumentIdentification customerInstrumentIdentification) {

		System.out.println(customerInstrumentIdentification.toString());
		CustomerInstrumentIdentification response=  customerInstrumentIdentificationService.createCustomerInstrumentIdentification(customerInstrumentIdentification);
		return response;
	}
	
// Update customer instrument identification by id
	@PutMapping("/cusInsruIdenty/{id}")
	public ResponseEntity<CustomerInstrumentIdentification> updateCustomerInstrumentIdentification(
			@PathVariable(value = "id") Long customerInstrumentIdentificationId,
			@Valid @RequestBody CustomerInstrumentIdentification customerInstrumentIdentificationDetails)
			throws ResourceNotFoundException {
		
		ResponseEntity<CustomerInstrumentIdentification> response = customerInstrumentIdentificationService.updateCustomerInstrumentIdentification(customerInstrumentIdentificationId, customerInstrumentIdentificationDetails);
		return response;
	}

	
}
