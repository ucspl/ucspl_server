
package com.ucspl.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.States;
import com.ucspl.repository.StatesRepository;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class StatesController {

	private static final Logger logger = Logger.getLogger(StatesController.class);
	States state = new States();

	@Autowired
	private StatesRepository statesRepository;

	public StatesController() {
	

	}

	@GetMapping("/states")
	public List<States> getAllStates() {
		logger.info("/states - Get all states!");

		
		return statesRepository.findAll();

	}

}
