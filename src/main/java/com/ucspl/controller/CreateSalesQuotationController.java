package com.ucspl.controller;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateSalesQuotation;
import com.ucspl.model.DocumentNumberForSalesAndQuotation;
import com.ucspl.model.SearchSalesAndQuotation;
import com.ucspl.services.CreateSalesQuotationService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class CreateSalesQuotationController {

	@Autowired
	private CreateSalesQuotationService createSalesQuotationService;

	@PersistenceContext
	private EntityManager em;

	public CreateSalesQuotationController() {
	}

	// Get all sales quotation information
	@GetMapping("/createSales")
	public List<CreateSalesQuotation> getAllCreatedSalesQuotation() {

		List<CreateSalesQuotation> allSalesQuotationList = createSalesQuotationService.getAllCreatedSalesQuotation();
		return allSalesQuotationList;
	}

	// Get create Sales Detail for id
	@GetMapping("/createSales/{id}")
	public ResponseEntity<CreateSalesQuotation> getSalesQuotationById(
			@PathVariable(value = "id") Long createSalesQuotationId) throws ResourceNotFoundException {

		ResponseEntity<CreateSalesQuotation> response = createSalesQuotationService
				.getSalesQuotationById(createSalesQuotationId);
		return response;
	}

	// save create sales details
	@PostMapping("/createSales")
	public CreateSalesQuotation createSalesQuotation(@Valid @RequestBody CreateSalesQuotation createSalesQuotation) {

		CreateSalesQuotation response = createSalesQuotationService.createSalesQuotation(createSalesQuotation);
		return response;
	}

	// Update Create Sales Quotation Document Screen Details for id
	@PutMapping("/createSales/{id}")
	public ResponseEntity<CreateSalesQuotation> updateSalesQuotation(@PathVariable(value = "id") Long salesId,
			@Valid @RequestBody CreateSalesQuotation createSalesQuotationDetails) throws ResourceNotFoundException {

		ResponseEntity<CreateSalesQuotation> response = createSalesQuotationService.updateSalesQuotation(salesId,
				createSalesQuotationDetails);
		return response;
	}

    //code for customer_and_terms_junction 
	@PostMapping("/AddCustTermsId")
	public String AddCustomerAndTerm(@Valid @RequestBody DocumentNumberForSalesAndQuotation documentCustomerAndTermsId)
			throws IOException {

		String response = createSalesQuotationService.addCustomerAndTerm(documentCustomerAndTermsId);
		return response;
	}

    // code to delete single term
	@PostMapping("/DelCustTermsId")
	public String removeCustomerAndTerm(
			@Valid @RequestBody DocumentNumberForSalesAndQuotation documentCustomerAndTermsId) throws IOException {

		String response = createSalesQuotationService.removeCustomerAndTerm(documentCustomerAndTermsId);
		return response;
	}

    // remove all terms 
	@PostMapping("/DelAllCustTermsId")
	public String removeAllCustomerAndTerm(@Valid @RequestBody String documentId) throws IOException {

		String response = createSalesQuotationService.removeAllCustomerAndTerm(documentId);
		return response;
	}

    //Get last saved document number
	@GetMapping(path = { "/DocNoForSales" })
	public String getDocumentNumber() throws IOException {

		String response = createSalesQuotationService.getDocumentNumber();
		return response;
	}
	
    //code for search and modify sales and quotation
	@RequestMapping(value = "/searchSales", method = RequestMethod.POST, produces = "application/json")
	public String searchAndModifySalesAndQuotation(
			@Valid @RequestBody SearchSalesAndQuotation searchSalesAndQuotation1) {

		String response = createSalesQuotationService.searchAndModifySalesAndQuotation(searchSalesAndQuotation1);
		return response;
	}

    //Result For Search Sales quotation Document
	@GetMapping(value = "/searchSales", produces = "application/json")
	public List<Object[]> getAllSalesAndQuotation() {

		List<Object[]> resultValue = createSalesQuotationService.getAllSalesAndQuotation();
		return resultValue;
	}

    // STORE PROCEDURE FOR GETTING DETAILS OF CREATE SALES FOR SEARCH SALES
	@RequestMapping(value = "/CSalesDet", method = RequestMethod.POST, produces = "application/json")
	public String docNoForGettingCreateSalesDetails(@Valid @RequestBody String docNo) {

		String response = createSalesQuotationService.docNoForGettingCreateSalesDetails(docNo);
		return response;
	}

	@GetMapping(value = "/CSalesDet", produces = "application/json")
	public List<Object[]> getCreateSalesDetailsForSearchSales() {

		List<Object[]> resultValueSet = createSalesQuotationService.getCreateSalesDetailsForSearchSales();
		return resultValueSet;
	}

}

