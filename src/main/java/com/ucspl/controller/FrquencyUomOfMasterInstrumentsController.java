
package com.ucspl.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.FrquencyUomOfMasterInstruments;
import com.ucspl.services.CreateDefinitionService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class FrquencyUomOfMasterInstrumentsController {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateDefinitionService createDefinitionServiceObject;

	public FrquencyUomOfMasterInstrumentsController() {

	}

// get frequency uom list
	@GetMapping("/frequencyUom")
	public List<FrquencyUomOfMasterInstruments> getAllFrequencyUoms() {

		List<FrquencyUomOfMasterInstruments> response = createDefinitionServiceObject.getAllFrequencyUoms();
		return response;

	}

// Save create frequency uom Details
	@PostMapping("/createFrequencyUom")
	public FrquencyUomOfMasterInstruments createFrequencyUom(
			@Valid @RequestBody FrquencyUomOfMasterInstruments frquencyUomOfMasterInstrumentsObject) {

		FrquencyUomOfMasterInstruments response = createDefinitionServiceObject
				.createFrequencyUom(frquencyUomOfMasterInstrumentsObject);
		return response;
	}

}
