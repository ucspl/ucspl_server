
package com.ucspl.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.Branch;
import com.ucspl.model.TypeOfQuotationForSalesAndQuotation;
import com.ucspl.repository.TypeOfQuotationForSalesAndQuotationRepository;
import com.ucspl.services.CreateDefinitionService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class TypeOfQuotationForSalesAndQuotationController  {



	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateDefinitionService createDefinitionServiceObject;
	public TypeOfQuotationForSalesAndQuotationController() {
	

	}

	//get quotation type list
	@GetMapping("/quotationtype")
	public List<TypeOfQuotationForSalesAndQuotation> getAllQuotationType() {
		List<TypeOfQuotationForSalesAndQuotation> response=createDefinitionServiceObject.getAllQuotationType();
		return response;

	}
	
	// Save create quotation type Details
		@PostMapping("/createTypeOfQuotation")
		public TypeOfQuotationForSalesAndQuotation createTypeOfQuotationForSalesAndQuotation(@Valid @RequestBody TypeOfQuotationForSalesAndQuotation typeOfQuotationForSalesAndQuotationObject) {

			TypeOfQuotationForSalesAndQuotation response =createDefinitionServiceObject.createTypeOfQuotationForSalesAndQuotation(typeOfQuotationForSalesAndQuotationObject);
			return response;
		}

}
