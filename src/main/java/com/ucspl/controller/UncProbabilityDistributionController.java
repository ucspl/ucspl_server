
package com.ucspl.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.UncMasterType;
import com.ucspl.model.UncProbabilityDistribution;
import com.ucspl.model.UomListForReqAndIwd;
import com.ucspl.services.CreateDefinitionService;



@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class UncProbabilityDistributionController {

	private static final Logger logger = Logger.getLogger(UncProbabilityDistributionController.class);

	@Autowired
	private CreateDefinitionService createDefinitionService;

	public UncProbabilityDistributionController() {

	}

//get all uncMasType list	
	@GetMapping("/crtUncProbDistribu")
	public List<UncProbabilityDistribution> getAllUncProbabilityDistribution() {
		logger.info("/crtUncProbDistribu - Get all Unc Probability Distribution List!");

		List<UncProbabilityDistribution> resultValue = createDefinitionService.getAllUncProbabilityDistribution();

		return resultValue;
	}

//Save create uncMasType Details 
	@PostMapping("/crtUncProbDistribu")
	public UncProbabilityDistribution creatUncProbabilityDistribution(@Valid @RequestBody UncProbabilityDistribution UncProbabilityDistribution) {

		UncProbabilityDistribution response = createDefinitionService.creatUncProbabilityDistribution(UncProbabilityDistribution);
		return response;
	}

}
