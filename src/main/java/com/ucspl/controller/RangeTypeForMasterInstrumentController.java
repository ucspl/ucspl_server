package com.ucspl.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.RangeTypeForMasterInstrument;
import com.ucspl.services.CreateDefinitionService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class RangeTypeForMasterInstrumentController {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateDefinitionService createDefinitionServiceObject;

	public RangeTypeForMasterInstrumentController() {

	}

// get Range Type For Master Instrument list
	@GetMapping("/mRangeType")
	public List<RangeTypeForMasterInstrument> getAllRangeTypeForMasterInstrument() {

		List<RangeTypeForMasterInstrument> response = createDefinitionServiceObject.getAllRangeTypeForMasterInstrument();
		return response;
	}

// Save create Range Type For Master Instrument
	@PostMapping("/mRangeType")
	public RangeTypeForMasterInstrument createRangeTypeForMasterInstrument(@Valid @RequestBody RangeTypeForMasterInstrument branchObject) {

		RangeTypeForMasterInstrument response = createDefinitionServiceObject.createRangeTypeForMasterInstrument(branchObject);
		return response;
	}

}
