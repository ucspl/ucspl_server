package com.ucspl.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.Branch;
import com.ucspl.model.TempChartTypeForMasterInstrument;
import com.ucspl.services.CreateDefinitionService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class TempChartTypeForMasterInstrumentController {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateDefinitionService createDefinitionServiceObject;

	public TempChartTypeForMasterInstrumentController() {

	}

// get temp chart list
	@GetMapping("/temChart")
	public List<TempChartTypeForMasterInstrument> getAllTempChartTypeForMasterInstruments() {

		List<TempChartTypeForMasterInstrument> response = createDefinitionServiceObject.getAllTempChartTypeForMasterInstruments();
		return response;
	}

// Save create temp chart Details
	@PostMapping("/temChart")
	public TempChartTypeForMasterInstrument createTempChartTypeForMasterInstrument(@Valid @RequestBody TempChartTypeForMasterInstrument tempChartObject) {

		TempChartTypeForMasterInstrument response = createDefinitionServiceObject.createTempChartTypeForMasterInstrument(tempChartObject);
		return response;
	}

}
