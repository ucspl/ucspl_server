package com.ucspl.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateUomCalculator;

import com.ucspl.repository.CreateUomCalculatorRepository;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class CreateUomCalculatorController {

	private static final Logger logger = LogManager.getLogger(CreateUomCalculatorController.class);
	
	

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateUomCalculatorRepository createUomCalculatorRepository;

	@GetMapping("/crtUomCal")
	public List<CreateUomCalculator> getAllCreateUomCalculator() {

	
		logger.info("/crtUomCal - Get All Created Uom Calculator!");

		return createUomCalculatorRepository.findAll();
	}

	@GetMapping("/crtUomCal/{id}")
	public ResponseEntity<CreateUomCalculator> getCreatedUomCalculatorById(
			@PathVariable(value = "id") Long createUomCalculatorId) throws ResourceNotFoundException {
		CreateUomCalculator createUomCalculator = createUomCalculatorRepository.findById(createUomCalculatorId)
				.orElseThrow(() -> new ResourceNotFoundException(
						" Calibration Result Data For Master Instrument not found for this id :: "
								+ createUomCalculatorId));

		logger.info("/crtUomCal/{id} - Get Uom Calculator By Id!" + createUomCalculatorId);
		return ResponseEntity.ok().body(createUomCalculator);
	}

	@PutMapping("/crtUomCal/{id}")
	public ResponseEntity<CreateUomCalculator> updateUomCalculatorById(
			@PathVariable(value = "id") Long createUomCalculatorId,
			@Valid @RequestBody CreateUomCalculator createUomCalculatorDetails) throws ResourceNotFoundException {
		CreateUomCalculator createUomCalculator = createUomCalculatorRepository.findById(createUomCalculatorId)
				.orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + createUomCalculatorId));

		createUomCalculator.setParameter(createUomCalculatorDetails.getParameter());
		createUomCalculator.setDraft(createUomCalculatorDetails.getDraft());
		createUomCalculator.setSubmitted(createUomCalculatorDetails.getSubmitted());
		createUomCalculator.setApproved(createUomCalculatorDetails.getApproved());

		final CreateUomCalculator updatedUomCalculator = createUomCalculatorRepository.save(createUomCalculator);
		logger.info("/crtUomCal/{id} - Update Uom Calculator for id " + createUomCalculatorId);

		return ResponseEntity.ok(updatedUomCalculator);
	}

	@PostMapping("/crtUomCal")
	public CreateUomCalculator createUomCalculatorFunc(@Valid @RequestBody CreateUomCalculator createUomCalculator) {

		logger.info("/crtUomCal - Save the Uom Calculator !");
		return createUomCalculatorRepository.save(createUomCalculator);
	}

	// Delete User Details for id
	@DeleteMapping("/crtUomCal/{id}")
	public Map<String, Boolean> deleteCreatedUomCalculatorById(@PathVariable(value = "id") Long createUomCalculatorId)
			throws ResourceNotFoundException {

		Map<String, Boolean> response = new HashMap<>();
		createUomCalculatorRepository.deleteById(createUomCalculatorId);
		response.put("deleted", Boolean.TRUE);

		logger.info("/crtUomCal/{id} - Delete Create Uom Calculator Detail of id - " + createUomCalculatorId);

		return response;
	}

	
	// search uom master                                             
		@PostMapping("/searchUomCal")
		public List<Object[]> searchUncMasterByUncNmOrDateOfConfig(@Valid @RequestBody String data) {

			String response = " ";
			String nameOrDate = data;
			List<Object[]> resultValue = null;

			System.out.println("nameOrDate : " + nameOrDate);

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_uom_calculator_by_parameter");

			procedureQuery.registerStoredProcedureParameter("name", String.class, ParameterMode.IN);
			procedureQuery.setParameter("name", (String) nameOrDate);
			procedureQuery.execute();
			
			resultValue = procedureQuery.getResultList();
			
			System.out.print("");                    
			logger.info("/searchUomCal - search unc master by unc master name or date of configuration " + nameOrDate);

			return resultValue;

		}
	
	
}
