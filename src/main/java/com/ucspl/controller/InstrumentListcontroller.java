
package com.ucspl.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.Branch;
import com.ucspl.model.InstrumentList;
import com.ucspl.repository.InstrumentListRepository;
import com.ucspl.services.CreateDefinitionService;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class InstrumentListcontroller {


	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateDefinitionService createDefinitionServiceObject;


public InstrumentListcontroller() {

}

// get instrument list
@GetMapping("/instruList")
public List<InstrumentList> getAllInstruments() {

	List<InstrumentList> response=createDefinitionServiceObject.getAllInstruments();
	return response;


}

// save instrument list
@PostMapping("/createInstrument")
public InstrumentList createInstrument(@Valid @RequestBody InstrumentList createInstrumentObject) {

InstrumentList response=createDefinitionServiceObject.createInstrument(createInstrumentObject);

return response;

}
}
