package com.ucspl.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.ChallanCumInvoiceDocument;
import com.ucspl.model.SrNoDeliveryChallan;
import com.ucspl.services.ChallanCumInvoiceDocumentService;
import com.ucspl.services.SrNoDeliveryChallanService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class SrNoDeliveryChallanController {

	@Autowired
	SrNoDeliveryChallanService srNoDeliveryChallanServiceObject;

	@PersistenceContext
	private EntityManager em;

	public SrNoDeliveryChallanController() {
		// TODO Auto-generated constructor stub
	}

	// Get All Sr No Information for Delivery Challan
	@GetMapping("/createSrNoDeliChallan")
	public List<SrNoDeliveryChallan> getAllSrNoInformationForDeliveryChallan() {

		List<SrNoDeliveryChallan> response = srNoDeliveryChallanServiceObject.getAllSrNoInformationForDeliveryChallan();
		return response;
	}

	// Get Sr No Information for Delivery Challan By Id
	@GetMapping("/createSrNoDeliChallan/{id}")
	public ResponseEntity<SrNoDeliveryChallan> getAllSrNoInformationForDeliveryChallanById(
			@PathVariable(value = "id") Long srNoDeliveryChallanId) throws ResourceNotFoundException {

		ResponseEntity<SrNoDeliveryChallan> response = srNoDeliveryChallanServiceObject
				.getAllSrNoInformationForDeliveryChallanById(srNoDeliveryChallanId);

		return response;
	}

	// Save Sr No Information for Delivery Challan
	@PostMapping("/createSrNoDeliChallan")
	public SrNoDeliveryChallan saveSrNoInformationForDeliveryChallan(@Valid @RequestBody SrNoDeliveryChallan srNoData) {

		SrNoDeliveryChallan response = srNoDeliveryChallanServiceObject.saveSrNoInformationForDeliveryChallan(srNoData);
		return response;

	}

	// Update Sr No Information for Delivery Challan for id
	@PutMapping("/createSrNoDeliChallan/{id}")
	public ResponseEntity<SrNoDeliveryChallan> updateSrNoInformationForDeliveryChallanById(
			@PathVariable(value = "id") Long srNoTableId, @Valid @RequestBody SrNoDeliveryChallan srNoDetails)
			throws ResourceNotFoundException {

		ResponseEntity<SrNoDeliveryChallan> response = srNoDeliveryChallanServiceObject
				.updateSrNoInformationForDeliveryChallanById(srNoTableId, srNoDetails);
		return response;
	}

	// Update unique Doc no in Sr No Delivery Challan table
	@PutMapping("/updateUniqueNoForsrNoDeliChallan/{id}")
	public ResponseEntity<SrNoDeliveryChallan> updateSrNoInformationForDeliveryChallanByUniqueDocNo(
			@PathVariable(value = "id") Long srNoTableId, @Valid @RequestBody SrNoDeliveryChallan uniqueNo)
			throws ResourceNotFoundException {

		ResponseEntity<SrNoDeliveryChallan> response = srNoDeliveryChallanServiceObject
				.updateSrNoInformationForDeliveryChallanByUniqueDocNo(srNoTableId, uniqueNo);
		return response;
	}
	

////////////////////store procedure to get request Info For Calibration ,Delivery  and Invoice Tag for Request document Number/////////

	@PostMapping("/trialreqInfoForCaliDeliAndInvoiceTagForReqDocNo")
	public List<Object[]> trialgetRequestDetailByCalibrationDcAndInvoiceTagForReqDocNo(
			@Valid @RequestBody String[] docNumber) throws IOException {

		List<Object[]> resultValue1 = new ArrayList<Object[]>();

		if (docNumber != null) {
			for (int a = 0; a < docNumber.length; a++) {
				System.out.println("array value :" + docNumber[a]);
				resultValue1.addAll(srNoDeliveryChallanServiceObject
						.trialgetRequestDetailByCalibrationDcAndInvoiceTagForReqDocNo(docNumber[a]));
			}
			return resultValue1;
		}

		else {
			return null;
		}


	}
	
////////////////////store procedure to get request Info For Calibration Tag,delivery tag and Invoice Tag for cust Id/////////

@PostMapping("/reqInfoForCaliDeliAndInvoiceTagForCustId")
public List<Object[]> getRequestDetailByCalibrationDcAndInvoiceTagForCustId(@Valid @RequestBody Integer number)
throws IOException {

List<Object[]> resultValue = srNoDeliveryChallanServiceObject
.getRequestDetailByCalibrationDcAndInvoiceTagForCustId(number);
return resultValue;
}

//store procedure to get delivery challan data with dc tag for dc document document number
	@PostMapping("/getAllDcDataWithDcTagForDcDocNo")
	public List<Object> getAllDeliveryChallanDataForDcDocNo(@Valid @RequestBody String number) throws IOException {

		List<Object> resultValue = srNoDeliveryChallanServiceObject.getAllDeliveryChallanDataForDcDocNo(number);
		return resultValue;
	}
	
	// store procedure to get sr no deli challan data deli challan document document no
	
	@PostMapping("/srNoDataForDeliChallanDoc")
	public List<Object> getSrNoDataForDeliChallanDocumentNo(@Valid @RequestBody String number) throws IOException {

		List<Object> resultValue = srNoDeliveryChallanServiceObject.getSrNoDataForDeliChallanDocumentNo(number);
		return resultValue;
	}
	
	
	
	////////////////////////////////////////////set delivery challan tag///////////////////////////////////////////////////
	//set invoice tag to one for instru
		@PostMapping("/trialsetDcTagToOneForInstru")
		public String trialsetDcTagToOneForInstru(@Valid @RequestBody String data) throws IOException {
			String response = "";

			JSONArray arr = new JSONArray(data); // loop through each object
			for (int i = 0; i < arr.length(); i++) {
				JSONObject jsonProductObject = arr.getJSONObject(i);
				String requestNo = jsonProductObject.getString("requestNo");
				String instruName = jsonProductObject.getString("instruName");
				String reqDocId = jsonProductObject.getString("reqDocId");
				String invoiceDocNo = jsonProductObject.getString("invoiceDocNo");
				String invoiceId = jsonProductObject.getString("invoiceId");
				String invUniqueNo = jsonProductObject.getString("invUniqueNo");
				System.out.println("requestno" + requestNo + " " + "instruName" + instruName + " " + "reqDocId" + reqDocId
						+ " " + "invoiceDocNo" + invoiceDocNo + " " + "invoiceId" + invoiceId);

				response = srNoDeliveryChallanServiceObject.trialsetDcTagToOneForInstru(requestNo, instruName,
						reqDocId, invoiceDocNo, invoiceId, invUniqueNo);

			}
			return response;
		}

	//set invoice tag to one for instru
		@PostMapping("/trialsetDcTagToOneForInstruThroughInvId")
		public String trialSetInvoiceTagToOneForInstruThroughInvId(@Valid @RequestBody String data) throws IOException {
			String response = "";

			JSONArray arr = new JSONArray(data); // loop through each object
			for (int i = 0; i < arr.length(); i++) {
				JSONObject jsonProductObject = arr.getJSONObject(i);
				String requestNo = jsonProductObject.getString("requestNo");
				String instruName = jsonProductObject.getString("instruName");
				String reqDocId = jsonProductObject.getString("reqDocId");
				String invoiceDocNo = jsonProductObject.getString("invoiceDocNo");
				String invoiceId = jsonProductObject.getString("invoiceId");
				System.out.println("requestno" + requestNo + " " + "instruName" + instruName + " " + "reqDocId" + reqDocId
						+ " " + "invoiceDocNo" + invoiceDocNo + " " + "invoiceId" + invoiceId);

				response = srNoDeliveryChallanServiceObject.trialSetInvoiceTagToOneForInstruThroughInvId(requestNo,
						instruName, reqDocId, invoiceDocNo, invoiceId);

			}
			return response;
		}

	//set invoice tag to zero for instru     // dont use array ,remove reqDocNo
		@PostMapping("/trialsetDcTagToZeroForInstru")
		public String trialsetDcTagToZeroForInstru(@Valid @RequestBody String data) throws IOException {
			String response = "";

			JSONArray arr = new JSONArray(data); // loop through each object
	//for (int i=0; i<arr.length(); i++)
//		{ 
			JSONObject jsonProductObject = arr.getJSONObject(0);
			// String requestNo = jsonProductObject.getString("requestNo");
			String instruName = jsonProductObject.getString("instruName");
			String reqDocId = jsonProductObject.getString("reqDocId");
			String invoiceDocNo = jsonProductObject.getString("invoiceDocNo");
			String invoiceId = jsonProductObject.getString("invoiceId");
			String invUniqueNo = jsonProductObject.getString("invUniqueNo");
			System.out.println(
					"instruName" + instruName + " " + "invoiceDocNo" + invoiceDocNo + " " + "invoiceId" + invoiceId);

			response = srNoDeliveryChallanServiceObject.trialsetDcTagToZeroForInstru(instruName, reqDocId,
					invoiceDocNo, invoiceId, invUniqueNo);

			// }
			return response;
		}

	//////////////////set invoice tag to one for range
		@PostMapping("/trialsetDcTagToOneForRange")
		public String trialsetDcTagToOneForRange(@Valid @RequestBody String data) throws IOException {
			String response = "";

			JSONArray arr = new JSONArray(data); // loop through each object
			for (int i = 0; i < arr.length(); i++) {
				JSONObject jsonProductObject = arr.getJSONObject(i);
				String requestNo = jsonProductObject.getString("requestNo");
				String instruName = jsonProductObject.getString("instruName");
				String rangeFrom = jsonProductObject.getString("rangeFrom");
				String rangeTo = jsonProductObject.getString("rangeTo");
				String reqDocId = jsonProductObject.getString("reqDocId");
				String invoiceDocNo = jsonProductObject.getString("invoiceDocNo");
				String invoiceId = jsonProductObject.getString("invoiceId");
				String invUniqueNo = jsonProductObject.getString("invUniqueNo");
				System.out.println("requestno" + requestNo + " " + "instruName" + instruName + " " + "rangeForm" + rangeFrom
						+ " " + "rangeTo" + rangeTo + " " + "reqDocId" + reqDocId + " " + "invoiceDocNo" + invoiceDocNo
						+ " " + "invoiceId" + invoiceId + " " + "invUniqueNo" + invUniqueNo);

				response = srNoDeliveryChallanServiceObject.trialsetDcTagToOneForRange(requestNo, instruName,
						reqDocId, invoiceDocNo, invoiceId, rangeFrom, rangeTo, invUniqueNo);

			}
			return response;
		}

	//set invoice tag to zero for range 
		@PostMapping("/trialsetDcTagToZeroForRange")
		public String trialsetDcTagToZeroForRange(@Valid @RequestBody String data) throws IOException {
			String response = "";

			JSONArray arr = new JSONArray(data); // loop through each object
			for (int i = 0; i < arr.length(); i++) {
				JSONObject jsonProductObject = arr.getJSONObject(i);
				// String requestNo = jsonProductObject.getString("requestNo");
				String instruName = jsonProductObject.getString("instruName");
				String rangeFrom = jsonProductObject.getString("rangeFrom");
				String rangeTo = jsonProductObject.getString("rangeTo");
				String reqDocId = jsonProductObject.getString("reqDocId");
				String invoiceDocNo = jsonProductObject.getString("invoiceDocNo");
				String invoiceId = jsonProductObject.getString("invoiceId");
				String invUniqueNo = jsonProductObject.getString("invUniqueNo");

				System.out.println("instruName" + instruName + " " + "rangeForm" + rangeFrom + " " + "rangeTo" + rangeTo
						+ "invoiceDocNo" + invoiceDocNo + " " + "invoiceId" + invoiceId + " " + "invUniqueNo"
						+ invUniqueNo);

				response = srNoDeliveryChallanServiceObject.trialsetDcTagToZeroForRange(instruName, invoiceDocNo,
						reqDocId, rangeFrom, rangeTo, invUniqueNo);

			}
			return response;
		}

}
