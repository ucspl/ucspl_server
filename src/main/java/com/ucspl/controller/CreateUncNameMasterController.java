
package com.ucspl.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateUncNameMaster;
import com.ucspl.repository.CreateUncNameMasterRepository;

@RestController
@CrossOrigin
@RequestMapping("/api/v1")
public class CreateUncNameMasterController {

	private static final Logger logger = LogManager.getLogger(CreateUncNameMasterController.class);

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateUncNameMasterRepository createUncNameMasterRepository;

	@GetMapping("/crtUncNmMaster")
	public List<CreateUncNameMaster> getAllCreateUncNameMaster() {

		logger.info("/crtUncNmMaster - Get All Created Unc Name Master!");
		
		return createUncNameMasterRepository.findAll();
	}

	@GetMapping("/crtUncNmMaster/{id}")
	public ResponseEntity<CreateUncNameMaster> getCreatedUncNameMasterById(
			@PathVariable(value = "id") Long createUncNameMasterId) throws ResourceNotFoundException {
		CreateUncNameMaster createUncNameMaster = createUncNameMasterRepository.findById(createUncNameMasterId)
				.orElseThrow(() -> new ResourceNotFoundException(
						" Calibration Result Data For Master Instrument not found for this id :: "
								+ createUncNameMasterId));

		logger.info("/crtUncNmMaster/{id} - Get Create Unc Name Master By Id!" + createUncNameMasterId);
		return ResponseEntity.ok().body(createUncNameMaster);
	}

	@PutMapping("/crtUncNmMaster/{id}")
	public ResponseEntity<CreateUncNameMaster> updateUncNameMasterById(
			@PathVariable(value = "id") Long createUncNameMasterId,
			@Valid @RequestBody CreateUncNameMaster createUncNameMasterDetails) throws ResourceNotFoundException {
		CreateUncNameMaster createUncNameMaster = createUncNameMasterRepository.findById(createUncNameMasterId)
				.orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + createUncNameMasterId));

		createUncNameMaster.setDateOfConfiguration(createUncNameMasterDetails.getDateOfConfiguration());

		createUncNameMaster.setDraft(createUncNameMasterDetails.getDraft());
		createUncNameMaster.setSubmitted(createUncNameMasterDetails.getSubmitted());
		createUncNameMaster.setApproved(createUncNameMasterDetails.getApproved());

		final CreateUncNameMaster updatedCreateUncNameMaster = createUncNameMasterRepository.save(createUncNameMaster);
		logger.info("/crtUncNmMaster/{id} - Update Create Unc Name Master for id " + createUncNameMasterId);

		return ResponseEntity.ok(updatedCreateUncNameMaster);
	}

	@PostMapping("/crtUncNmMaster")
	public CreateUncNameMaster createUncNameMasterFunc(@Valid @RequestBody CreateUncNameMaster createUncNameMaster) {

		logger.info("/crtUncNmMaster - Save the Create Unc Name Master !");
		return createUncNameMasterRepository.save(createUncNameMaster);
	}

	// Delete User Details for id
	@DeleteMapping("/crtUncNmMaster/{id}")
	public Map<String, Boolean> deleteCreatedUncNameMaster(@PathVariable(value = "id") Long createUncNameMasterId)
			throws ResourceNotFoundException {

		Map<String, Boolean> response = new HashMap<>();
		createUncNameMasterRepository.deleteById(createUncNameMasterId);
		response.put("deleted", Boolean.TRUE);

		logger.info("/crtUucNmMaster/{id} - Delete Create Unc Name Master Detail of id - " + createUncNameMasterId);

		return response;
	}

	// search unc master
	@PostMapping("/searchUncMaster")
	public List<Object[]> searchUncMasterByUncNmOrDateOfConfig(@Valid @RequestBody String data) {

		String response = " ";
		String nameOrDate = data;
		List<Object[]> resultValue = null;

//		JSONArray arr = new JSONArray(data); // loop through each object
//		JSONObject jsonProductObject = arr.getJSONObject(0);
//
//		String nameOrDate = jsonProductObject.getString("nameOrDate");
		
//  the customer  

		System.out.println("nameOrDate : " + nameOrDate);

		StoredProcedureQuery procedureQuery = em
				.createStoredProcedureQuery("search_unc_master_by_uncmastername_or_dateofconfiguration");

		procedureQuery.registerStoredProcedureParameter("name_or_date", String.class, ParameterMode.IN);
		procedureQuery.setParameter("name_or_date", (String) nameOrDate);
		procedureQuery.execute();
		
		//resultValue. 

		resultValue = procedureQuery.getResultList();
		
		System.out.print("");                    
		logger.info("/searchUncMaster - search unc master by unc master name or date of configuration " + nameOrDate);

//		if (nameOrDate == "") {
//
//			StoredProcedureQuery procedureQuery = em
//					.createStoredProcedureQuery("search_unc_master");
//			procedureQuery.execute();
//
//			resultValue = procedureQuery.getResultList();    
//
//			logger.info("/searchUncMaster - search unc master by just clicking on search button ");                                       
//			
//		}
		

		return resultValue;

	}

}
