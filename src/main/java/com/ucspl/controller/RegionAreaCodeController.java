package com.ucspl.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.Branch;
import com.ucspl.model.RegionAreaCode;
import com.ucspl.repository.RegionAreaCodeRepository;
import com.ucspl.services.CreateDefinitionService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class RegionAreaCodeController {

	private static final Logger logger = Logger.getLogger(BranchController.class);


	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateDefinitionService createDefinitionServiceObject;

	public RegionAreaCodeController() {
		// TODO Auto-generated constructor stub
	}

	// get region area code list
	@GetMapping("/regionareacode")
	public List<RegionAreaCode> getAllRegionAreaCode() {

		List<RegionAreaCode> response = createDefinitionServiceObject.getAllRegionAreaCode();
		return response;

	}

	// Save create region area code
	@PostMapping("/createRegionAreaCode")
	public RegionAreaCode createRegionAreaCode(@Valid @RequestBody RegionAreaCode regionAreaCodeObject) {

		RegionAreaCode response = createDefinitionServiceObject.createRegionAreaCode(regionAreaCodeObject);
		return response;
	}

}