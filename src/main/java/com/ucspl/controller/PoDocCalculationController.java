
package com.ucspl.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.PoDocCalculation;
import com.ucspl.model.SalesQuotationDocumentCalculation;
import com.ucspl.repository.SalesQuotationDocumentCalculationRepository;

import com.ucspl.services.PoDocCalculationService;
import com.ucspl.services.SalesQuotationDocumentCalculationService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class PoDocCalculationController {

	@Autowired
	private PoDocCalculationService poDocCalculationServiceObject;

	@PersistenceContext
	private EntityManager em;

	public PoDocCalculationController() {
		// TODO Auto-generated constructor stub
	}

	// get All Create Po Document
	@GetMapping("/PoDocCal")
	public List<PoDocCalculation> getAllCreatePoDocument() {

		List<PoDocCalculation> allCreatePoDocument = poDocCalculationServiceObject
				.getAllCreatePoDocument();

		return allCreatePoDocument;

	}

	//  Get Po Document Calculations By Id
	@GetMapping("/PoDocCal/{id}")
	public ResponseEntity<PoDocCalculation> getPoDocumentCalculationById(
			@PathVariable(value = "id") Long createPoDocumentCalculationId)
			throws ResourceNotFoundException {

		ResponseEntity<PoDocCalculation> response = poDocCalculationServiceObject
				.getPoDocumentCalculationById(createPoDocumentCalculationId);
		return response;

	}

	// Save Calculation of Po Document
	@PostMapping("/PoDocCal")
	public PoDocCalculation createPoDocCalculation(
			@Valid @RequestBody PoDocCalculation poDocCalculation) {

		PoDocCalculation response = poDocCalculationServiceObject
				.createPoDocCalculation(poDocCalculation);
		return response;

	}

	// Update Po Document calculation for id
	@PutMapping("/PoDocCal/{id}")
	public ResponseEntity<PoDocCalculation> updatePoDocCalculationById(
			@PathVariable(value = "id") Long createPoDocCalculationId,
			@Valid @RequestBody PoDocCalculation poDocumentCalculationDetails)
			throws ResourceNotFoundException {

		ResponseEntity<PoDocCalculation> response = poDocCalculationServiceObject
				.updatePoDocCalculationById(createPoDocCalculationId,
						poDocumentCalculationDetails);
		return response;

	}

	// store procedure for doc calculation table detail

	@RequestMapping(value = "/PoDocCalDet", method = RequestMethod.POST, produces = "application/json")
	public String DocNoForGettingDocDetails(@Valid @RequestBody String docNo) {

		String response = poDocCalculationServiceObject.DocNoForGettingDocDetails(docNo);
		return response;

	}

	// Get details of po Document calculation for Document No
	@SuppressWarnings("unchecked")
	@GetMapping(value = "/PoDocCalDet", produces = "application/json")
	public List<Object[]> getDocDetailsForDocNo() {

		List<Object[]> resultValue = poDocCalculationServiceObject.getDocDetailsForDocNo();
		return resultValue;

	}

}
