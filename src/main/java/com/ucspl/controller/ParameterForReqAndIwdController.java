
package com.ucspl.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.ParameterForReqAndIwd;
import com.ucspl.services.CreateDefinitionService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class ParameterForReqAndIwdController {

	private static final Logger logger = Logger.getLogger(ParameterForReqAndIwdController.class);

	@Autowired
	private CreateDefinitionService createDefinitionService;

	public ParameterForReqAndIwdController() {

	}

//get all parameter list
	@GetMapping("/parameterList")
	public List<ParameterForReqAndIwd> getAllParameterList() {
		logger.info("/parameterList - Get all Parameter List!");

		List<ParameterForReqAndIwd> resultValue = createDefinitionService.getAllParameterList();

		return resultValue;
	}

//Save create parameter Details
	@PostMapping("/parameterList")
	public ParameterForReqAndIwd createParameter(@Valid @RequestBody ParameterForReqAndIwd paraObject) {

		ParameterForReqAndIwd response = createDefinitionService.createParameter(paraObject);
		return response;
	}

}
