package com.ucspl.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateCalibrationResultDataForMasterInstrument;
import com.ucspl.model.CreateMasterInstrumentSpecificationDetails;
import com.ucspl.model.SearchMasterByMasterName;
import com.ucspl.model.SearchMasterInstruByMIdPNmAndStatus;
import com.ucspl.model.SearchMasterInstruByMastIdAndStatus;
import com.ucspl.repository.CreateCalibrationResultDataForMasterInstrumentRepository;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class CreateCalibrationResultDataForMasterInstrumentController {

	private static final Logger logger = LogManager.getLogger(CreateMasterInstrumentSpecificationDetailsController.class);
	
	SearchMasterByMasterName searchMasterByMasterName =new SearchMasterByMasterName();
	
	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateCalibrationResultDataForMasterInstrumentRepository createCalibrationResultDataForMasterInstrumentRepository;

	public CreateCalibrationResultDataForMasterInstrumentController() {

	}
	
	@GetMapping("/crtCRDForPara")
	public List<CreateCalibrationResultDataForMasterInstrument> getAllCalibrationResultDataForMasterInstrument() {

		logger.info("/crtCRDForPara - Get All Created Calibration Result Data For Master Instrument!");
		return createCalibrationResultDataForMasterInstrumentRepository.findAll();
	}

	@GetMapping("/crtCRDForPara/{id}")
	public ResponseEntity<CreateCalibrationResultDataForMasterInstrument> getCalibrationResultDataForMasterInstrumentById(
			@PathVariable(value = "id") Long calibrationResultDataForMasterInstrumentId) throws ResourceNotFoundException {
		CreateCalibrationResultDataForMasterInstrument calibrationResultDataForMasterInstrument = createCalibrationResultDataForMasterInstrumentRepository
				.findById(calibrationResultDataForMasterInstrumentId).orElseThrow(() -> new ResourceNotFoundException(
						" Calibration Result Data For Master Instrument not found for this id :: " + calibrationResultDataForMasterInstrumentId));

		logger.info("/crtCRDForPara/{id} - Get Calibration Result Data For Master Instrument By Id!" + calibrationResultDataForMasterInstrumentId);
		return ResponseEntity.ok().body(calibrationResultDataForMasterInstrument);
	}

	@PutMapping("/crtCRDForPara/{id}")
	public ResponseEntity<CreateCalibrationResultDataForMasterInstrument> updateCalibrationResultDataForMasterInstrumentById(
			@PathVariable(value = "id") Long calibrationResultDataForMasterInstrumentId,
			@Valid @RequestBody CreateCalibrationResultDataForMasterInstrument calibrationResultDataForMasterInstrumentDetails) throws ResourceNotFoundException {
		CreateCalibrationResultDataForMasterInstrument calibrationResultDataForMasterInstrument = createCalibrationResultDataForMasterInstrumentRepository
				.findById(calibrationResultDataForMasterInstrumentId).orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + calibrationResultDataForMasterInstrumentId));

		calibrationResultDataForMasterInstrument.setCreateMasterInstruId(calibrationResultDataForMasterInstrumentDetails.getCreateMasterInstruId());
	//	calibrationResultDataForMasterInstrument.setParameterId(calibrationResultDataForMasterInstrumentDetails.getParameterId());
		calibrationResultDataForMasterInstrument.setStdReading(calibrationResultDataForMasterInstrumentDetails.getStdReading());
		calibrationResultDataForMasterInstrument.setUucReading(calibrationResultDataForMasterInstrumentDetails.getUucReading());
		calibrationResultDataForMasterInstrument.setError(calibrationResultDataForMasterInstrumentDetails.getError());
		calibrationResultDataForMasterInstrument.setUnc(calibrationResultDataForMasterInstrumentDetails.getUnc());
		calibrationResultDataForMasterInstrument.setAcFrequency(calibrationResultDataForMasterInstrumentDetails.getAcFrequency());
		calibrationResultDataForMasterInstrument.setAcFrequencyUom(calibrationResultDataForMasterInstrumentDetails.getAcFrequencyUom());
		calibrationResultDataForMasterInstrument.setUncFormula(calibrationResultDataForMasterInstrumentDetails.getUncFormula());
		calibrationResultDataForMasterInstrument.setUncUom(calibrationResultDataForMasterInstrumentDetails.getUncUom());
		final CreateCalibrationResultDataForMasterInstrument updatedcalibrationResultDataForMasterInstrumentDetail = createCalibrationResultDataForMasterInstrumentRepository
				.save(calibrationResultDataForMasterInstrument);
		logger.info("/crtCRDForPara/{id} - Update Calibration Result Data For Master Instrument for id " + calibrationResultDataForMasterInstrumentId);

		return ResponseEntity.ok(updatedcalibrationResultDataForMasterInstrumentDetail);
	}

	@PostMapping("/crtCRDForPara") 
	public CreateCalibrationResultDataForMasterInstrument createMasterInstrumentSpecificationDetail(
			@Valid @RequestBody CreateCalibrationResultDataForMasterInstrument calibrationResultDataForMasterInstrument) {
			
		logger.info("/crtCRDForPara - Save the Calibration Result Data For Master Instrument!");	
		return createCalibrationResultDataForMasterInstrumentRepository.save(calibrationResultDataForMasterInstrument);
	}
	
	// Delete User Details for id
	@DeleteMapping("/crtCRDForPara/{id}")
	public Map<String, Boolean> deleteCalibrationResultDataForMasterInstrument(@PathVariable(value = "id") Long masterInstrumentDetailId) throws ResourceNotFoundException {

		Map<String, Boolean> response = new HashMap<>();
		createCalibrationResultDataForMasterInstrumentRepository.deleteById(masterInstrumentDetailId);
		response.put("deleted", Boolean.TRUE);
		logger.info("/crtCRDForPara/{id} - Delete Calibration Result Data For Master Instrument Detail of id - " + masterInstrumentDetailId);
			
		return response;
	}

	
	
	//update query for unique sr number 
	@PutMapping("/crtCRDForUniqueNo/{id}")
	public ResponseEntity<CreateCalibrationResultDataForMasterInstrument> updateMasterInstrumentSpecificationDetailsByIdForUniqueSrNumber(
			@PathVariable(value = "id") Long createCalibrationResultDataForMasterInstrumentId,
			@Valid @RequestBody CreateCalibrationResultDataForMasterInstrument createCalibrationResultDataForMasterInstrumentDetails)
			throws ResourceNotFoundException {
		CreateCalibrationResultDataForMasterInstrument createCalibrationResultDataForMasterInstrument = createCalibrationResultDataForMasterInstrumentRepository
				.findById(createCalibrationResultDataForMasterInstrumentId).orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + createCalibrationResultDataForMasterInstrumentId));
	

		createCalibrationResultDataForMasterInstrument.setUniqueSrNumber(createCalibrationResultDataForMasterInstrumentDetails.getUniqueSrNumber());

		final CreateCalibrationResultDataForMasterInstrument updatedCalibrationResultDataForMasterInstrumentDetail = createCalibrationResultDataForMasterInstrumentRepository
				.save(createCalibrationResultDataForMasterInstrument);
		logger.info("/crtCRDForPara/{id} - Update MasterInstrumentSpecificationDetail unique sr number of specification"
				+ createCalibrationResultDataForMasterInstrumentId);

		return ResponseEntity.ok(updatedCalibrationResultDataForMasterInstrumentDetail);
	}
	
	
	
////////////////////////////sp for getting sr no for master instruments by masteridparaname ////////////////////////////////  

	@RequestMapping(value = "/srnoMSDetByMipnForCRD", method = RequestMethod.POST, produces = "application/json")
	public List<Object[]> srNoDetailsOfMasterSpecificationByMasterIdParaNameForCaliResultData(
			@Valid @RequestBody SearchMasterInstruByMastIdAndStatus masterIdParaNameObj) {

		System.out.println("masterid paraname" + masterIdParaNameObj.getMastId());
		logger.info("/srnoMSDetByMipn -" + " Search master specification Details For masterIdParaName"
				+ masterIdParaNameObj.getMastId());

		List<Object[]> resultValue = null;

		if (masterIdParaNameObj.getMastId() != 0 ) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("srno_master_instru_specification_detail_list_by_masteridparaname_for_calibration_result_data");

			procedureQuery.registerStoredProcedureParameter("master_specifi_id", Long.class, ParameterMode.IN);
//			procedureQuery.registerStoredProcedureParameter("draft", Integer.class, ParameterMode.IN);
//			procedureQuery.registerStoredProcedureParameter("approved", Integer.class, ParameterMode.IN);
//			procedureQuery.registerStoredProcedureParameter("submitted", Integer.class, ParameterMode.IN);
//			procedureQuery.registerStoredProcedureParameter("rejected", Integer.class, ParameterMode.IN);
//			procedureQuery.registerStoredProcedureParameter("archieved", Integer.class, ParameterMode.IN);

			procedureQuery.setParameter("master_specifi_id", (long) masterIdParaNameObj.getMastId());                                                                           
//			procedureQuery.setParameter("draft", (int) masterIdParaNameObj.getDraft());
//			procedureQuery.setParameter("approved", (int) masterIdParaNameObj.getApproved());
//			procedureQuery.setParameter("submitted", (int) masterIdParaNameObj.getSubmitted());
//			procedureQuery.setParameter("rejected", (int) masterIdParaNameObj.getRejected());
//			procedureQuery.setParameter("archieved", (int) masterIdParaNameObj.getArchieved());

			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();

		}

		return resultValue;

	}
	
	
////////////////////////////sp for getting sr no for master instruments ////////////////////////////////  

	@RequestMapping(value = "/snMCalSpetByMastIdSt", method = RequestMethod.POST, produces = "application/json")
	public List<Object[]> srNoDetailsOfMasterInstruCalSpecificationByMastIdAndStatus(@Valid @RequestBody  SearchMasterInstruByMastIdAndStatus mastIdAndStatus1) {

		System.out.println("master id for specification :" + mastIdAndStatus1.getMastId());
		logger.info("/srnoMSDet -" + " Search master instru cal certificate Details For Status" + mastIdAndStatus1);

		List<Object[]> resultValue = null;
		

		if (mastIdAndStatus1.getDraft() == 1 || mastIdAndStatus1.getApproved() == 1 || mastIdAndStatus1.getSubmitted() == 1
				|| mastIdAndStatus1.getRejected() == 1 || mastIdAndStatus1.getArchieved() == 1) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("srno_master_instru_cal_specification_detail_list_by_status_and_mastid");

			procedureQuery.registerStoredProcedureParameter("mastid", Long.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("draft", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("approved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("submitted", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("rejected", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("archieved", Integer.class, ParameterMode.IN);

			procedureQuery.setParameter("mastid", (long) mastIdAndStatus1.getMastId());
			procedureQuery.setParameter("draft", (int) mastIdAndStatus1.getDraft());
			procedureQuery.setParameter("approved", (int) mastIdAndStatus1.getApproved());
			procedureQuery.setParameter("submitted", (int) mastIdAndStatus1.getSubmitted());
			procedureQuery.setParameter("rejected", (int) mastIdAndStatus1.getRejected());
			procedureQuery.setParameter("archieved", (int) mastIdAndStatus1.getArchieved());

			procedureQuery.execute();
			resultValue = procedureQuery.getResultList();
		}

		return resultValue;
	}

}
