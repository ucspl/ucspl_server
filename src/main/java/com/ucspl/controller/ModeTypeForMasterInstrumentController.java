package com.ucspl.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.Branch;
import com.ucspl.model.ModeTypeForMasterInstrument;
import com.ucspl.services.CreateDefinitionService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class ModeTypeForMasterInstrumentController {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateDefinitionService createDefinitionServiceObject;

	public ModeTypeForMasterInstrumentController() {

	}

//  get branch list 
	@GetMapping("/mModeType")
	public List<ModeTypeForMasterInstrument> getAllModeTypesOfMasterInstrument() {

		List<ModeTypeForMasterInstrument> response = createDefinitionServiceObject.getAllModeTypesOfMasterInstrument();
		return response;
	}

// Save create branchDetails
	@PostMapping("/mModeType")
	public ModeTypeForMasterInstrument createModeTypesOfMasterInstrument(@Valid @RequestBody ModeTypeForMasterInstrument modeTypeForMasterInstrumentObject) {

		ModeTypeForMasterInstrument response = createDefinitionServiceObject.createModeTypesOfMasterInstrument(modeTypeForMasterInstrumentObject);
		return response;
	}

}
