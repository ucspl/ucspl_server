package com.ucspl.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateMasterInstrumentSpecificationDetails;
import com.ucspl.model.ReqDocnoAndCustInstruRelationship;
import com.ucspl.model.RequestAndInwardDocument;
import com.ucspl.repository.CustomerInstrumentIdentificationRepository;
import com.ucspl.repository.ReqDocnoAndCustInstruRelationshipRepository;
import com.ucspl.repository.RequestAndInwardDocumentRepository;
import com.ucspl.services.ReqDocnoAndCustInstruRelationshipService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class ReqDocnoAndCustInstruRelationshipController {

	
	private static final Logger logger = LogManager.getLogger(RequestAndInwardDocumentController.class);

	private  String docNoVariable;
	
	private  ReqDocnoAndCustInstruRelationship reqDocnoAndCustInstruRelationship;
	
	@PersistenceContext
	private EntityManager em;

	@Autowired
	private RequestAndInwardDocumentRepository requestAndInwardDocumentRepository;
	
	@Autowired
	private ReqDocnoAndCustInstruRelationshipRepository reqDocnoAndCustInstruRelationshipRepository;
	

	@Autowired
	private CustomerInstrumentIdentificationRepository customerInstrumentIdentificationRepository;
	
	@Autowired
	private ReqDocnoAndCustInstruRelationshipService reqDocnoAndCustInstruRelationshipService;
	
	
	public ReqDocnoAndCustInstruRelationshipController() {
		// TODO Auto-generated constructor stub
	}

//get ReqDocnoAndCustInstruRelationship 
	@GetMapping("/reqDocCusInsIden")
	public List<ReqDocnoAndCustInstruRelationship> getAllRequestAndInwardDocument() {

		List<ReqDocnoAndCustInstruRelationship> response = reqDocnoAndCustInstruRelationshipService.getAllRequestAndInwardDocument();
		return response;
	}

//get ReqDocnoAndCustInstruRelationship By Id
	@GetMapping("/reqDocCusInsIden/{id}")
	public ResponseEntity<ReqDocnoAndCustInstruRelationship> getRequestAndInwardDocumentById(
			@PathVariable(value = "id") Long reqDocnoAndCustInstruRelationshipId) throws ResourceNotFoundException {
		
		ResponseEntity<ReqDocnoAndCustInstruRelationship> response = reqDocnoAndCustInstruRelationshipService.getRequestAndInwardDocumentById(reqDocnoAndCustInstruRelationshipId);
		return ResponseEntity.ok().body(reqDocnoAndCustInstruRelationship);
	}

//create ReqDocnoAndCustInstruRelationship 
	@PostMapping("/reqDocCusInsIden")
	public ReqDocnoAndCustInstruRelationship createRequestAndInwardDocument(
			@Valid @RequestBody ReqDocnoAndCustInstruRelationship reqDocnoAndCustInstruRelationship) {

		ReqDocnoAndCustInstruRelationship response = reqDocnoAndCustInstruRelationshipService.createRequestAndInwardDocument(reqDocnoAndCustInstruRelationship);		
		return response;
	}

//update ReqDocnoAndCustInstruRelationship By Id
	@PutMapping("/reqDocCusInsIden/{id}")
	public ResponseEntity<ReqDocnoAndCustInstruRelationship> updateReqDocnoAndCustInstruRelationshipByIdForCalibrationTag(
			@PathVariable(value = "id") Long createReqDocnoAndCustInstruRelationshipId,
			@Valid @RequestBody ReqDocnoAndCustInstruRelationship createReqDocnoAndCustInstruRelationshipDetails) throws ResourceNotFoundException {
		
		ResponseEntity<ReqDocnoAndCustInstruRelationship> response= reqDocnoAndCustInstruRelationshipService.updateReqDocnoAndCustInstruRelationshipByIdForCalibrationTag(createReqDocnoAndCustInstruRelationshipId, createReqDocnoAndCustInstruRelationshipDetails);
		return response;
	}
	
///////////////////////////store procedure to get Req Docno And CustInstru Relationship by custInstruIdentificationId /////////////////////////////
	
	@RequestMapping(value = "/srnoReqDocCusInsIden", method = RequestMethod.POST, produces = "application/json")
	public List<Object[]> srNoDetailsOfReqDocCustIdMasterInstru(@Valid @RequestBody long custInstruIdentificationId) {
	
		List<Object[]> resultValue=reqDocnoAndCustInstruRelationshipService.srNoDetailsOfReqDocCustIdMasterInstru(custInstruIdentificationId);
	    return resultValue;
	}
	
	
///////////////////////////store procedure to get Req Docno And CustInstru Relationship by custInstruIdentificationId /////////////////////////////

	@RequestMapping(value = "/srnoReqDCusInsIdenByCRIId", method = RequestMethod.POST, produces = "application/json")
	public List<Object[]> srNoDetailsOfReqDocCustIdMasterInstruByCreateReqInwId(@Valid @RequestBody long createReqInwId) {

		List<Object[]> resultValue=reqDocnoAndCustInstruRelationshipService.srNoDetailsOfReqDocCustIdMasterInstruByCreateReqInwId(createReqInwId);
		return resultValue;
	}		
	
}
