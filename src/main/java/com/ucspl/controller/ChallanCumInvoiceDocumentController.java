package com.ucspl.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.ChallanCumInvoiceDocument;
import com.ucspl.model.InvoiceAndrequestNumberJunction;
import com.ucspl.services.ChallanCumInvoiceDocumentService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class ChallanCumInvoiceDocumentController {

	@Autowired
	ChallanCumInvoiceDocumentService challanCumInvoiceDocumentServiceObject;

	@PersistenceContext
	private EntityManager em;

	public ChallanCumInvoiceDocumentController() {
		// TODO Auto-generated constructor stub
	}

// Get All Challan Cum Invoice Document Item Information
	@GetMapping("/invDoc")
	public List<ChallanCumInvoiceDocument> getAllChallanCumInvoiceDocument() {

		List<ChallanCumInvoiceDocument> response = challanCumInvoiceDocumentServiceObject
				.getAllChallanCumInvoiceDocument();
		return response;
	}

// Get Challan Cum Invoice Document Item Information By Id
	@GetMapping("/invDoc/{id}")
	public ResponseEntity<ChallanCumInvoiceDocument> getChallanCumInvoiceDocumentById(
			@PathVariable(value = "id") Long challanCumInvoiceDocumentId) throws ResourceNotFoundException {

		ResponseEntity<ChallanCumInvoiceDocument> response = challanCumInvoiceDocumentServiceObject
				.getChallanCumInvoiceDocumentById(challanCumInvoiceDocumentId);

		return response;
	}

// Save Item Details of Challan Cum Invoice Document
	@PostMapping("/invDoc")
	public ChallanCumInvoiceDocument createChallanCumInvoiceDocument(
			@Valid @RequestBody ChallanCumInvoiceDocument challanCumInvoiceDocument) {

		ChallanCumInvoiceDocument response = challanCumInvoiceDocumentServiceObject
				.createChallanCumInvoiceDocument(challanCumInvoiceDocument);
		return response;

	}

	// Update Challan Cum Invoice Document Item Details for id
	@PutMapping("/invDoc/{id}")
	public ResponseEntity<ChallanCumInvoiceDocument> updateChallanCumInvoiceDocumentById(
			@PathVariable(value = "id") Long challanCumInvoiceDocumentId,
			@Valid @RequestBody ChallanCumInvoiceDocument challanCumInvoiceDocumentDetails)
			throws ResourceNotFoundException {

		ResponseEntity<ChallanCumInvoiceDocument> response = challanCumInvoiceDocumentServiceObject
				.updateChallanCumInvoiceDocumentById(challanCumInvoiceDocumentId, challanCumInvoiceDocumentDetails);
		return response;
	}

	// Update Challan Cum Invoice Document Item Details for unique Doc no
	@PutMapping("/updateUniqueInvNo/{id}")
	public ResponseEntity<ChallanCumInvoiceDocument> updateChallanCumInvoiceDocumentByUniqueDocNo(
			@PathVariable(value = "id") Long challanCumInvoiceDocumentId,
			@Valid @RequestBody ChallanCumInvoiceDocument uniqueNo) throws ResourceNotFoundException {

		ResponseEntity<ChallanCumInvoiceDocument> response = challanCumInvoiceDocumentServiceObject
				.updateChallanCumInvoiceDocumentByUniqueDocNo(challanCumInvoiceDocumentId, uniqueNo);
		return response;
	}

	// get sr no store procedure for doc no
	@RequestMapping(value = "/srInvDoc", method = RequestMethod.POST, produces = "application/json")
	public String docNoForGettingSrNo(@Valid @RequestBody String docNo) {

		String response = challanCumInvoiceDocumentServiceObject.docNoForGettingSrNo(docNo);
		return response;
	}

	// sr no detail for doc no 
	@GetMapping(value = "/srInvDoc", produces = "application/json")
	public List<Object[]> getSrNoForDocNo() {

		List<Object[]> response = challanCumInvoiceDocumentServiceObject.getSrNoForDocNo();
		return response;
	}

	// code to delete Invoice Row
	@PostMapping("/deleteInvoiceRow")
	public String removeInvoiceRow(@Valid @RequestBody long indexToDelete) throws IOException {

		System.out.println("Invoice row");
		String response = challanCumInvoiceDocumentServiceObject.removeInvoiceRow(indexToDelete);
		return response;
	}

//////////////////////store procedure to get request Info For Calibration Tag /////////
//
//	@PostMapping("/reqInfoForCaliTag")
//	public List<Object[]> getRequestDetailByCalibrationTag(@Valid @RequestBody String number) throws IOException {
//
//		List<Object[]> resultValue = challanCumInvoiceDocumentServiceObject.getRequestDetailByCalibrationTag(number);
//		return resultValue;
//	}

////////////////////store procedure to get request Info For Calibration Tag and Invoice Tag for cust Id/////////

	@PostMapping("/reqInfoForCaliTagAndInvoiceTagForCustId")
	public List<Object[]> getRequestDetailByCalibrationTagAndInvoiceTagForCustId(@Valid @RequestBody Integer number)
			throws IOException {

		List<Object[]> resultValue = challanCumInvoiceDocumentServiceObject
				.getRequestDetailByCalibrationTagAndInvoiceTagForCustId(number);
		return resultValue;
	}

////////////////////store procedure to get request Info For Calibration Tag and Invoice Tag for Request document Number/////////

	@PostMapping("/reqInfoForCaliTagAndInvoiceTagForReqDocNo")
	public List<Object[]> getRequestDetailByCalibrationTagAndInvoiceTagForReqDocNo(@Valid @RequestBody String docNumber)
			throws IOException {

		List<Object[]> resultValue = challanCumInvoiceDocumentServiceObject
				.getRequestDetailByCalibrationTagAndInvoiceTagForReqDocNo(docNumber);
		return resultValue;
	}

////////////////////store procedure to get request Info For Calibration Tag and Invoice Tag for Request document Number/////////

	@PostMapping("/trialreqInfoForCaliTagAndInvoiceTagForReqDocNo")
	public List<Object[]> trialgetRequestDetailByCalibrationTagAndInvoiceTagForReqDocNo(
			@Valid @RequestBody String[] docNumber) throws IOException {

		List<Object[]> resultValue1 = new ArrayList<Object[]>();

		if (docNumber != null) {
			for (int a = 0; a < docNumber.length; a++) {
				System.out.println("array value :" + docNumber[a]);
				resultValue1.addAll(challanCumInvoiceDocumentServiceObject
						.getRequestDetailByCalibrationTagAndInvoiceTagForReqDocNo(docNumber[a]));
			}
			return resultValue1;
		}

		else {
			return null;
		}

//	String docNumber1="UCS";
//List<Object[]> resultValue = challanCumInvoiceDocumentServiceObject
//.getRequestDetailByCalibrationTagAndInvoiceTagForReqDocNo(docNumber1);

	}

//set invoice tag to one for instru
	@PostMapping("/trialsetInvoiceTagToOneForInstru")
	public String trialSetInvoiceTagToOneForInstru(@Valid @RequestBody String data) throws IOException {
		String response = "";

		JSONArray arr = new JSONArray(data); // loop through each object
		for (int i = 0; i < arr.length(); i++) {
			JSONObject jsonProductObject = arr.getJSONObject(i);
			String requestNo = jsonProductObject.getString("requestNo");
			String instruName = jsonProductObject.getString("instruName");
			String reqDocId = jsonProductObject.getString("reqDocId");
			String invoiceDocNo = jsonProductObject.getString("invoiceDocNo");
			String invoiceId = jsonProductObject.getString("invoiceId");
			String invUniqueNo = jsonProductObject.getString("invUniqueNo");
			System.out.println("requestno" + requestNo + " " + "instruName" + instruName + " " + "reqDocId" + reqDocId
					+ " " + "invoiceDocNo" + invoiceDocNo + " " + "invoiceId" + invoiceId);

			response = challanCumInvoiceDocumentServiceObject.trialSetInvoiceTagToOneForInstru(requestNo, instruName,
					reqDocId, invoiceDocNo, invoiceId, invUniqueNo);

		}
		return response;
	}

//set invoice tag to one for instru
	@PostMapping("/trialsetInvoiceTagToOneForInstruThroughInvId")
	public String trialSetInvoiceTagToOneForInstruThroughInvId(@Valid @RequestBody String data) throws IOException {
		String response = "";

		JSONArray arr = new JSONArray(data); // loop through each object
		for (int i = 0; i < arr.length(); i++) {
			JSONObject jsonProductObject = arr.getJSONObject(i);
			String requestNo = jsonProductObject.getString("requestNo");
			String instruName = jsonProductObject.getString("instruName");
			String reqDocId = jsonProductObject.getString("reqDocId");
			String invoiceDocNo = jsonProductObject.getString("invoiceDocNo");
			String invoiceId = jsonProductObject.getString("invoiceId");
			System.out.println("requestno" + requestNo + " " + "instruName" + instruName + " " + "reqDocId" + reqDocId
					+ " " + "invoiceDocNo" + invoiceDocNo + " " + "invoiceId" + invoiceId);

			response = challanCumInvoiceDocumentServiceObject.trialSetInvoiceTagToOneForInstruThroughInvId(requestNo,
					instruName, reqDocId, invoiceDocNo, invoiceId);

		}
		return response;
	}

//set invoice tag to zero for instru     // dont use array ,remove reqDocNo
	@PostMapping("/trialsetInvoiceTagToZeroForInstru")
	public String trialSetInvoiceTagToZeroForInstru(@Valid @RequestBody String data) throws IOException {
		String response = "";

		JSONArray arr = new JSONArray(data); // loop through each object
//for (int i=0; i<arr.length(); i++)
//	{ 
		JSONObject jsonProductObject = arr.getJSONObject(0);
		// String requestNo = jsonProductObject.getString("requestNo");
		String instruName = jsonProductObject.getString("instruName");
		String reqDocId = jsonProductObject.getString("reqDocId");
		String invoiceDocNo = jsonProductObject.getString("invoiceDocNo");
		String invoiceId = jsonProductObject.getString("invoiceId");
		String invUniqueNo = jsonProductObject.getString("invUniqueNo");
		System.out.println(
				"instruName" + instruName + " " + "invoiceDocNo" + invoiceDocNo + " " + "invoiceId" + invoiceId);

		response = challanCumInvoiceDocumentServiceObject.trialSetInvoiceTagToZeroForInstru(instruName, reqDocId,
				invoiceDocNo, invoiceId, invUniqueNo);

		// }
		return response;
	}

//////////////////set invoice tag to one for range
	@PostMapping("/trialsetInvoiceTagToOneForRange")
	public String trialSetInvoiceTagToOneForRange(@Valid @RequestBody String data) throws IOException {
		String response = "";

		JSONArray arr = new JSONArray(data); // loop through each object
		for (int i = 0; i < arr.length(); i++) {
			JSONObject jsonProductObject = arr.getJSONObject(i);
			String requestNo = jsonProductObject.getString("requestNo");
			String instruName = jsonProductObject.getString("instruName");
			String rangeFrom = jsonProductObject.getString("rangeFrom");
			String rangeTo = jsonProductObject.getString("rangeTo");
			String reqDocId = jsonProductObject.getString("reqDocId");
			String invoiceDocNo = jsonProductObject.getString("invoiceDocNo");
			String invoiceId = jsonProductObject.getString("invoiceId");
			String invUniqueNo = jsonProductObject.getString("invUniqueNo");
			System.out.println("requestno" + requestNo + " " + "instruName" + instruName + " " + "rangeForm" + rangeFrom
					+ " " + "rangeTo" + rangeTo + " " + "reqDocId" + reqDocId + " " + "invoiceDocNo" + invoiceDocNo
					+ " " + "invoiceId" + invoiceId + " " + "invUniqueNo" + invUniqueNo);

			response = challanCumInvoiceDocumentServiceObject.trialSetInvoiceTagToOneForRange(requestNo, instruName,
					reqDocId, invoiceDocNo, invoiceId, rangeFrom, rangeTo, invUniqueNo);

		}
		return response;
	}

//set invoice tag to zero for range 
	@PostMapping("/trialsetInvoiceTagToZeroForRange")
	public String trialSetInvoiceTagToZeroForRange(@Valid @RequestBody String data) throws IOException {
		String response = "";

		JSONArray arr = new JSONArray(data); // loop through each object
		for (int i = 0; i < arr.length(); i++) {
			JSONObject jsonProductObject = arr.getJSONObject(i);
			// String requestNo = jsonProductObject.getString("requestNo");
			String instruName = jsonProductObject.getString("instruName");
			String rangeFrom = jsonProductObject.getString("rangeFrom");
			String rangeTo = jsonProductObject.getString("rangeTo");
			String reqDocId = jsonProductObject.getString("reqDocId");
			String invoiceDocNo = jsonProductObject.getString("invoiceDocNo");
			String invoiceId = jsonProductObject.getString("invoiceId");
			String invUniqueNo = jsonProductObject.getString("invUniqueNo");

			System.out.println("instruName" + instruName + " " + "rangeForm" + rangeFrom + " " + "rangeTo" + rangeTo
					+ "invoiceDocNo" + invoiceDocNo + " " + "invoiceId" + invoiceId + " " + "invUniqueNo"
					+ invUniqueNo);

			response = challanCumInvoiceDocumentServiceObject.trialSetInvoiceTagToZeroForRange(instruName, invoiceDocNo,
					reqDocId, rangeFrom, rangeTo, invUniqueNo);

		}
		return response;
	}

////////////////////store procedure to get Po Data for Po No /////////
	@PostMapping("/poDataForPoNo")
	public List<Object[]> getPoDataForPoNumber(@Valid @RequestBody String number) throws IOException {

		List<Object[]> resultValue = challanCumInvoiceDocumentServiceObject.getPoDataForPoNumber(number);
		return resultValue;
	}

////////////////////store procedure to get all  Po numbers for cust id /////////
	@PostMapping("/allPoNumbersCustId")
	public List<Object> getAllPoNumbersForCustId(@Valid @RequestBody Integer number) throws IOException {

		List<Object> resultValue = challanCumInvoiceDocumentServiceObject.getAllPoNumbersForCustId(number);
		return resultValue;
	}

	// Save request numbers in junction table
	@PostMapping("/saveRequsetNoForInvoice")
	public InvoiceAndrequestNumberJunction saveRequestNumbersInJunctionTable(
			@Valid @RequestBody InvoiceAndrequestNumberJunction reqAndInvoiceDoc) {

		InvoiceAndrequestNumberJunction response = challanCumInvoiceDocumentServiceObject
				.saveRequestNumbersInJunctionTable(reqAndInvoiceDoc);
		return response;

	}

	// store procedure to get all request numbers for invoice document number
	@PostMapping("/getAllRequstNoForInvoiceDocNo")
	public List<Object> getAllRequstNoForInvoiceDocNo(@Valid @RequestBody String number) throws IOException {

		List<Object> resultValue = challanCumInvoiceDocumentServiceObject.getAllRequstNoForInvoiceDocNo(number);
		return resultValue;
	}

	// store procedure to get invoice data with invoice tag for invoice document
	// number
	@PostMapping("/getAllInvoiceDataWithInvoiceTagForInvDocNo")
	public List<Object> getAllInvoiceDataForInvoiceDocNo(@Valid @RequestBody String number) throws IOException {

		List<Object> resultValue = challanCumInvoiceDocumentServiceObject.getAllInvoiceDataForInvoiceDocNo(number);
		return resultValue;
	}

	// remove all request numbers for invoice doc no
	@PostMapping("/removeAllReqNoForInvDocNo")
	public String removeAllRequestNumberForInvDocNo(@Valid @RequestBody String documentNO) throws IOException {

		String response = challanCumInvoiceDocumentServiceObject.removeAllRequestNumberForInvDocNo(documentNO);
		return response;
	}

	// remove all sr numbers for invoice doc no
	@PostMapping("/removeAllSrNoForInvDocNo")  

	public String removeAllSrtNoForInvDocNo(@Valid @RequestBody String documentNO) throws IOException {

		String response = challanCumInvoiceDocumentServiceObject.removeAllSrtNoForInvDocNo(documentNO);
		return response;
	}
	
	
	// set invoice tag and unique no to zero for invoice doc no
	@PostMapping("/setInvAndUniNoToZeroForInvDocNo")  

	public String setInvAndUniNoToZeroForInvDocNo(@Valid @RequestBody String documentNO) throws IOException {

		String response = challanCumInvoiceDocumentServiceObject.setInvAndUniNoToZeroForInvDocNo(documentNO);
		return response;
	}

	// set invoice tag to one for instru
	@PostMapping("/setInvTagToZeroForReqNoAndInvNo")
	public String setInvoiceTagToZeroForRequestNoAndInvNo(@Valid @RequestBody String data) throws IOException {

		String response = "";

		JSONArray arr = new JSONArray(data); // loop through each object
		
			JSONObject jsonProductObject = arr.getJSONObject(0);
			// String requestNo = jsonProductObject.getString("requestNo");
		
			String invoiceDocNo = jsonProductObject.getString("invoiceDocNo");
			String reqDocNo = jsonProductObject.getString("reqDocNo");

			System.out.println("invoiceDocNo" + invoiceDocNo + " " + "reqDocxNo"+reqDocNo);

			response = challanCumInvoiceDocumentServiceObject.setInvoiceTagToZeroForRequestNoAndInvNo(invoiceDocNo, reqDocNo);

		
		return response;
		
	}

}
