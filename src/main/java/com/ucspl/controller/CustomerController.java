
package com.ucspl.controller;

import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.Customer;
import com.ucspl.model.CustomerTaxId;
import com.ucspl.model.CustomerbyName;
import com.ucspl.services.CustomerService;


@RestController
@CrossOrigin
@RequestMapping("/api/v1")
public class CustomerController {


	@Autowired
	private CustomerService customerService;
	
	@PersistenceContext
	private EntityManager em;


	public CustomerController() {
    // TODO Auto-generated constructor stub
	}

	@GetMapping("/getChildCust")
	public List<Customer> getAllChildCustomers() {

		List<Customer> response = customerService.getAllChildCustomers();
		return response;
	}
	
	
	//Get Customers Details 
	@GetMapping("/customers")                      
	public List<Customer> getAllCustomers() {

		List<Customer> allCustomerList = customerService.getAllCustomers();
		return allCustomerList;
	}
	
	// Get Customer Details for id
	@GetMapping("/customers/{id}")
	public ResponseEntity<Customer> getCustomerById(@PathVariable(value = "id") Long customerId)
			throws ResourceNotFoundException {
		
		ResponseEntity<Customer> response = customerService.getCustomerById(customerId);
		return response;
	}

	// Save Customer Details 
	@PostMapping("/customers")
	public Customer createCustomer(@Valid @RequestBody Customer customer) {
		
		
		Customer response = customerService.createCustomer(customer);
		return response;
		
	}
	
	// Update Customer Details for id
	@PutMapping("/customers/{id}")
	public ResponseEntity<Customer> updateCustomer(@PathVariable(value = "id") Long customerId,
			@Valid @RequestBody Customer customerDetails) throws ResourceNotFoundException {

		ResponseEntity<Customer> response = customerService.updateCustomer(customerId, customerDetails);
		return response;
	}
	
	//Search Customer Details
	@RequestMapping(value = "search_cust", method = RequestMethod.POST, produces = "application/json")
	public String searchAndModifyCustomer(@Valid @RequestBody CustomerbyName customer) {
	
		String response = customerService.searchAndModifyCustomer(customer);
		return response;
	}


	//Get Search result based on Name or Department or Address
	@GetMapping(value = "search_cust", produces = "application/json")
	public List<Object[]> getAllCustsByNameOrDeptOrAddress() {

		List<Object[]> resultValue = customerService.getAllCustsByNameOrDeptOrAddress();
		return resultValue;
			
	}

	// Search customer for id
	@RequestMapping(value = "/CCustDetails", method = RequestMethod.POST, produces = "application/json")
	public Customer customerForGettingCustomerDetails(@Valid @RequestBody long id) throws ResourceNotFoundException {

		Customer response = customerService.customerForGettingCustomerDetails(id);	
		return response;
	}

	//Add Customer and Term id
	@PostMapping("/addCustTaxId") 
	public String addCustomerAndTerm(@Valid @RequestBody List<CustomerTaxId> customerTaxId) throws IOException {
				
		String response = customerService.addCustomerAndTerm(customerTaxId);
		return response;
	}
	
	
	//Add Customer and Term id
	@PostMapping("/getCustTaxId") 
	public List<BigInteger> getCustomerAndTerm(@Valid @RequestBody long id) throws IOException {
				
		List<BigInteger> response = customerService.getCustomerAndTerm(id);
		return response;
	}
	
		
}

