
package com.ucspl.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.Branch;
import com.ucspl.model.Terms;
import com.ucspl.repository.TermsRepository;
import com.ucspl.services.CreateDefinitionService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class TermsController {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateDefinitionService createDefinitionServiceObject;

	public TermsController() {
	

	}

	// get all terms list
	@GetMapping("/terms")
	public List<Terms> getAllTerms() {
		
		List<Terms> response=createDefinitionServiceObject.getAllTerms();
		return response;
		
	}

	// save create terms
	@PostMapping("/createTerms")
	public Terms createTerms(@Valid @RequestBody Terms termObject ) {
		
		Terms response=createDefinitionServiceObject.createTerms(termObject);
		return response;
		
	}
	
}





