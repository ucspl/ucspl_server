
package com.ucspl.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.Branch;
import com.ucspl.model.CreateDefinition;
import com.ucspl.model.Department;
import com.ucspl.model.UncFormulaForNabl;
import com.ucspl.repository.BranchRepository;
import com.ucspl.services.CreateDefinitionService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class UncFormulaForNablController {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateDefinitionService createDefinitionServiceObject;

	public UncFormulaForNablController() {

	}

// get unc formula list
	@GetMapping("/uncFormula")
	public List<UncFormulaForNabl> getAllUncFormula() {

		List<UncFormulaForNabl> response = createDefinitionServiceObject.getAllUncFormula();
		return response;

	}

// Save create unc formula Details
	@PostMapping("/createUncFormula")
	public UncFormulaForNabl createUncFormula(@Valid @RequestBody UncFormulaForNabl uncFormulaForNablObject) {

		UncFormulaForNabl response = createDefinitionServiceObject.createUncFormula(uncFormulaForNablObject);
		return response;
	}

}
