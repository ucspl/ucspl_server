
package com.ucspl.controller;

import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.SalesQuotationDocument;
import com.ucspl.model.SrNoPurchaseOrderDocument;
import com.ucspl.repository.SalesQuotationDocumentRepository;
import com.ucspl.services.CustomerService;
import com.ucspl.services.SalesQuotationDocumentService;
import com.ucspl.services.SrNoPurchaseOrderDocumentService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class SrNoPurchaseOrderDocumentController {

	@Autowired
	private SrNoPurchaseOrderDocumentService srNoPurchaseOrderDocumentService;

	@PersistenceContext
	private EntityManager em;

	public SrNoPurchaseOrderDocumentController() {
		// TODO Auto-generated constructor stub
	}

	// Get All purchase Order Document Item Information
	@GetMapping("/poDoc")
	public List<SrNoPurchaseOrderDocument> getAllCreatePoDocument() {

		List<SrNoPurchaseOrderDocument> response = srNoPurchaseOrderDocumentService.getAllCreatePoDocument();
		return response;
	}

	// Get PO Document Item Information By Id
	@GetMapping("/poDoc/{id}")
	public ResponseEntity<SrNoPurchaseOrderDocument> getCreatePoDocumentById(
			@PathVariable(value = "id") Long createPoDocumentId) throws ResourceNotFoundException {

		ResponseEntity<SrNoPurchaseOrderDocument> response = srNoPurchaseOrderDocumentService
				.getCreatePoDocumentById(createPoDocumentId);
		return response;
		
	}

	// Save Item Details of Po Document
	@PostMapping("/poDoc")
	public SrNoPurchaseOrderDocument createPoDocument(
			@Valid @RequestBody SrNoPurchaseOrderDocument srNoPurchaseOrderDocument) {

		SrNoPurchaseOrderDocument response = srNoPurchaseOrderDocumentService
				.createPoDocument(srNoPurchaseOrderDocument);
		return response;
	}

	// Update Po Document Item Details for id
	@PutMapping("/poDoc/{id}")
	public ResponseEntity<SrNoPurchaseOrderDocument> updatePoDocumentById(
			@PathVariable(value = "id") Long createPoDocumentId,
			@Valid @RequestBody SrNoPurchaseOrderDocument poDocumentDetails) throws ResourceNotFoundException {

		ResponseEntity<SrNoPurchaseOrderDocument> response = srNoPurchaseOrderDocumentService
				.updatePoDocumentById(createPoDocumentId, poDocumentDetails);
		return response;

	}
	

	// Store Procedure for getting Sr no for po  document number
	@RequestMapping(value = "/srnopoDoc", method = RequestMethod.POST, produces = "application/json")
	public String docNoForGettingSrNo(@Valid @RequestBody String docNo) {

		String response = srNoPurchaseOrderDocumentService.docNoForGettingSrNo(docNo);
		return response;

	}

	// Get Items Details of Sales quotation Document for Document No
	@GetMapping(value = "/srnopoDoc", produces = "application/json")
	public List<Object[]> getSrNoForDocNo() {

		List<Object[]> response = srNoPurchaseOrderDocumentService.getSrNoForDocNo();
		return response;

	}
	
	  // code to delete Po Row
			@PostMapping("/deletePoRow")
			public String removePoRow(@Valid @RequestBody long indexToDelete) throws IOException {

				System.out.println("po row");
				String response = srNoPurchaseOrderDocumentService.removePoRow(indexToDelete);
				return response;

			}
	
	
	
}
