package com.ucspl.controller;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.CalibrationAgencyOfMasterInstrument;
import com.ucspl.services.CreateDefinitionService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class CalibrationAgencyOfMasterInstrumentController {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateDefinitionService createDefinitionServiceObject;

	public CalibrationAgencyOfMasterInstrumentController() {

	}

// get calibration agency list
	@GetMapping("/calAgency")
	public List<CalibrationAgencyOfMasterInstrument> getAllCalibrationAgency() {

		List<CalibrationAgencyOfMasterInstrument> response = createDefinitionServiceObject.getAllCalibrationAgency();
		return response;
	}

// Save create calibration agency Details
	@PostMapping("/calAgency")
	public CalibrationAgencyOfMasterInstrument createCalibrationAgency(@Valid @RequestBody CalibrationAgencyOfMasterInstrument calibrationAgencyObject) {

		CalibrationAgencyOfMasterInstrument response = createDefinitionServiceObject.createCalibrationAgency(calibrationAgencyObject);
		return response;
	}

}
