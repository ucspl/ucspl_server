package com.ucspl.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateCalibrationResultDataForMasterInstrument;
import com.ucspl.model.CreateUucParameterMasterSpecificationDetails;
import com.ucspl.model.JunctionOfInputUomAndCreateUUCParaSpecification;
import com.ucspl.model.JunctionOfUomAndCreateUUCParaSpecification;
import com.ucspl.repository.CreateUucParameterMasterSpecificationDetailsRepository;
import com.ucspl.repository.JunctionOfInputUomAndCreateUUCParaSpecificationRepository;
import com.ucspl.repository.JunctionOfUomAndCreateUUCParaSpecificationRepository;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class CreateUucParameterMasterSpecificationDetailsController {
	
private static final Logger logger = LogManager.getLogger(CreateUucParameterMasterSpecificationDetailsController.class);
	
	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateUucParameterMasterSpecificationDetailsRepository createUucParameterMasterSpecificationDetailsRepository;
	
	@Autowired
	private JunctionOfInputUomAndCreateUUCParaSpecificationRepository junctionOfInputUomAndCreateUUCParaSpecificationRepository;
	
	@Autowired
	private JunctionOfUomAndCreateUUCParaSpecificationRepository junctionOfUomAndCreateUUCParaSpecificationRepository;
	
	@GetMapping("/crtUucNmMSpe")
	public List<CreateUucParameterMasterSpecificationDetails> getAllCreateUucParameterMasterSpecificationDetails() {

		logger.info("/crtUucNmMSpe - Get All Created Uuc Name  parameter Master Specification!");
		return createUucParameterMasterSpecificationDetailsRepository.findAll();
	}

	@GetMapping("/crtUucNmMSpe/{id}")
	public ResponseEntity<CreateUucParameterMasterSpecificationDetails> getCreateUucParameterMasterSpecificationDetailsById(
			@PathVariable(value = "id") Long createUucParameterMasterSpecificationDetailsId) throws ResourceNotFoundException {
		CreateUucParameterMasterSpecificationDetails createUucParameterMasterSpecificationDetails = createUucParameterMasterSpecificationDetailsRepository
				.findById(createUucParameterMasterSpecificationDetailsId).orElseThrow(() -> new ResourceNotFoundException(
						" Calibration Result Data For Master Instrument not found for this id :: " + createUucParameterMasterSpecificationDetailsId));

		logger.info("/crtUucNmMSpe/{id} - Get Created Uuc Name  parameter Master Specification By Id!" + createUucParameterMasterSpecificationDetailsId);
		return ResponseEntity.ok().body(createUucParameterMasterSpecificationDetails);
	}

	@PutMapping("/crtUucNmMSpe/{id}")
	public ResponseEntity<CreateUucParameterMasterSpecificationDetails> updateUucParameterMasterSpecificationDetailsById(
			@PathVariable(value = "id") Long createUucParameterMasterSpecificationDetailsId,
			@Valid @RequestBody CreateUucParameterMasterSpecificationDetails createUucParameterMasterSpecificationDetails) throws ResourceNotFoundException {
		CreateUucParameterMasterSpecificationDetails createUucParameterMasterSpecifiDetails = createUucParameterMasterSpecificationDetailsRepository
				.findById(createUucParameterMasterSpecificationDetailsId).orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + createUucParameterMasterSpecificationDetailsId));

		createUucParameterMasterSpecifiDetails.setParameter(createUucParameterMasterSpecificationDetails.getParameter());
		createUucParameterMasterSpecifiDetails.setParameterNumber(createUucParameterMasterSpecificationDetails.getParameterNumber());
		createUucParameterMasterSpecifiDetails.setInstruUom(createUucParameterMasterSpecificationDetails.getInstruUom());
		createUucParameterMasterSpecifiDetails.setCalibrationLabParameter(createUucParameterMasterSpecificationDetails.getCalibrationLabParameter());
		createUucParameterMasterSpecifiDetails.setrAndRTable(createUucParameterMasterSpecificationDetails.getrAndRTable());
		createUucParameterMasterSpecifiDetails.setUncMaster(createUucParameterMasterSpecificationDetails.getUncMaster());
		createUucParameterMasterSpecifiDetails.setIpParameter(createUucParameterMasterSpecificationDetails.getIpParameter());
		createUucParameterMasterSpecifiDetails.setIpParameterUom(createUucParameterMasterSpecificationDetails.getIpParameterUom());
		createUucParameterMasterSpecifiDetails.setCalibrationProcedure(createUucParameterMasterSpecificationDetails.getCalibrationProcedure());
		createUucParameterMasterSpecifiDetails.setDefaultRepetability(createUucParameterMasterSpecificationDetails.getDefaultRepetability());
		createUucParameterMasterSpecifiDetails.setUncReading(createUucParameterMasterSpecificationDetails.getUncReading());
		createUucParameterMasterSpecifiDetails.setTypeOfRepetability(createUucParameterMasterSpecificationDetails.getTypeOfRepetability());
		createUucParameterMasterSpecifiDetails.setUncPrintUnit(createUucParameterMasterSpecificationDetails.getUncPrintUnit());
		createUucParameterMasterSpecifiDetails.setNablScopeName(createUucParameterMasterSpecificationDetails.getNablScopeName());
		createUucParameterMasterSpecifiDetails.setRemarks(createUucParameterMasterSpecificationDetails.getRemarks());
		createUucParameterMasterSpecifiDetails.setAccrediation(createUucParameterMasterSpecificationDetails.getAccrediation());

		
		final CreateUucParameterMasterSpecificationDetails updatedUucParameterMasterSpecificationDetail = createUucParameterMasterSpecificationDetailsRepository
				.save(createUucParameterMasterSpecifiDetails);
		logger.info("/crtUucNmMSpe/{id} - Update already Created Uuc Name  parameter Master Specification for id " + createUucParameterMasterSpecificationDetailsId);

		return ResponseEntity.ok(updatedUucParameterMasterSpecificationDetail);
	}

	@PostMapping("/crtUucNmMSpe") 
	public CreateUucParameterMasterSpecificationDetails CreateUucParameterMasterSpecificationDetailsFunction(
			@Valid @RequestBody CreateUucParameterMasterSpecificationDetails createUucParameterMasterSpecificationDetails) {
			
		logger.info("/crtUucNmMSpe - Save the Create Uuc Name parameter Master Specification!");	
		return createUucParameterMasterSpecificationDetailsRepository.save(createUucParameterMasterSpecificationDetails);
	}
	// Delete User Details for id 
	@DeleteMapping("/crtUucNmMSpe/{id}")
	public Map<String, Boolean> deleteCreatedUucParameterMasterSpecificationDetails(@PathVariable(value = "id") Long createUucParameterMasterSpecificationDetailsId) throws ResourceNotFoundException {

		Map<String, Boolean> response = new HashMap<>();
		createUucParameterMasterSpecificationDetailsRepository.deleteById(createUucParameterMasterSpecificationDetailsId);
		response.put("deleted", Boolean.TRUE);
		logger.info("/crtUucNmMSpe/{id} - Delete Create Uuc Name parameter Master Specification Detail of id - " + createUucParameterMasterSpecificationDetailsId);

		return response;
	}
	
	
	

/////////////////////////store procedure to get all created sr no for uuc master instrument //////////////////////////
	@RequestMapping(value = "/srnoUucMSDet", method = RequestMethod.POST, produces = "application/json")
	public List<Object[]> srNoDetailsOfUucMasterSpecification(@Valid @RequestBody long uucMasterId) {

		System.out.println("uuc master id: "+ uucMasterId);   
		
		
		logger.info("/srnoMSDet -" + " Search master specification Details For master id" + uucMasterId);

		List<Object[]> resultValue;
		
	

		StoredProcedureQuery procedureQuery = em
				.createStoredProcedureQuery("srno_uuc_master_instru_specification_detail_list"); 
		procedureQuery.registerStoredProcedureParameter("id_no", Long.class, ParameterMode.IN);

		procedureQuery.setParameter("id_no", (long) uucMasterId);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList(); 

		return resultValue;
	}
	
	
	
	//update query for unique sr number 
	@PutMapping("/crtCUMIForParaNo/{id}")
	public ResponseEntity<CreateUucParameterMasterSpecificationDetails> updateUucMasterInstrumentSpecificationDetailsByIdForParameterNumber(
			@PathVariable(value = "id") Long createUucParaMasterSpecificationDetailsId,
			@Valid @RequestBody CreateUucParameterMasterSpecificationDetails createUucParameterMasterSpecificationDetails)
			throws ResourceNotFoundException {
		CreateUucParameterMasterSpecificationDetails createUucParameterMasterSpecification = createUucParameterMasterSpecificationDetailsRepository
				.findById(createUucParaMasterSpecificationDetailsId).orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + createUucParaMasterSpecificationDetailsId));
	

		createUucParameterMasterSpecification.setParameterNumber(createUucParameterMasterSpecificationDetails.getParameterNumber());

		final CreateUucParameterMasterSpecificationDetails updatedUucParameterMasterSpecificationDetails = createUucParameterMasterSpecificationDetailsRepository
				.save(createUucParameterMasterSpecification);
		logger.info("/crtCRDForPara/{id} - Update Uuc parameter master specification parameter number of specification "
				+ createUucParaMasterSpecificationDetailsId);

		return ResponseEntity.ok(updatedUucParameterMasterSpecificationDetails);
	}
	
	

//////////////////// store procedure to get store data in junction table-junction_of_input_uom_and_createuuc_para_specification_table ///////////

	@PostMapping("/saveUomToUucJunTable")
	public List<JunctionOfUomAndCreateUUCParaSpecification> saveUomDataInJunctionTable(
			@Valid @RequestBody JunctionOfUomAndCreateUUCParaSpecification[] dataToSave) throws IOException {
		int i;
		List<Object[]> response=new ArrayList<Object[]>();
		List<JunctionOfUomAndCreateUUCParaSpecification> response1=new ArrayList<JunctionOfUomAndCreateUUCParaSpecification>();
		

		if(dataToSave!=null) {
			for( i=0;i<dataToSave.length;i++) {
				response1.add(junctionOfUomAndCreateUUCParaSpecificationRepository.save(dataToSave[i]));
				;
			}
			
		}
		logger.info("/saveUomToUucJunTable - Save the Uom and Uuc Name parameter Master Specification id to junction table ! ");                                                                    	                               
		return response1;
		
	}

	
//////////////////// store procedure to get store data in junction table-junction_of_input_uom_and_createuuc_para_specification_table ///////////

	@PostMapping("/saveIpUomToUucJunTable")
	public List<JunctionOfInputUomAndCreateUUCParaSpecification> saveUomDataInJunctionTable(
			@Valid @RequestBody JunctionOfInputUomAndCreateUUCParaSpecification[] dataToSave) throws IOException {
		int i;
		List<Object[]> response = new ArrayList<Object[]>();                                                                                                
//		List <JunctionOfInputUom>                                                             
		
		List<JunctionOfInputUomAndCreateUUCParaSpecification> response1 = new ArrayList<JunctionOfInputUomAndCreateUUCParaSpecification>();
		
		int trialVariable;
//		Integer.s
		

		if (dataToSave != null) {
			for (i = 0; i < dataToSave.length; i++) {
				response1.add(junctionOfInputUomAndCreateUUCParaSpecificationRepository.save(dataToSave[i]));                                              
			}

		}
		logger.info(
				"/saveUomToUucJunTable - Save the input Uom and Uuc Name parameter Master Specification id to junction table ...!");
		return response1;

	}
	
	
	
//	@PostMapping("/trialreqInfoForCaliTagAndInvoiceTagForReqDocNo")
//	public List<Object[]> saveUomDataInJunctionTable(
//			@Valid @RequestBody String[] docNumber) throws IOException {
//
//		List<Object[]> resultValue1 = new ArrayList<Object[]>();
//		List<Object[]> resultValue;
//
//		if (docNumber != null) {
//			for (int a = 0; a < docNumber.length; a++) {
//				System.out.println("array value :" + docNumber[a]);
//				
//				String docNo=docNumber[a];
//				
//				StoredProcedureQuery procedureQuery = em
//						.createStoredProcedureQuery("get_req_data_with_calibration_tag_and_invoice_tag_through_reqdocno");
//						procedureQuery.registerStoredProcedureParameter("doc_no", String.class, ParameterMode.IN);
//
//						procedureQuery.setParameter("doc_no", (String) docNo);
//						procedureQuery.execute();
//
//						resultValue = procedureQuery.getResultList();
//
//						logger.info(
//						"/reqInfoForCaliTagAndInvoiceTagForReqDocNo - get request and inward data with calibration tag and invcoice tag through reqDocNo "    
//						+ docNumber);
//				
//				resultValue1.addAll(resultValue);
//			}
//			return resultValue1;
//		}
//
//		else {
//			return null;
//		}
//
//
//	}
	
}
