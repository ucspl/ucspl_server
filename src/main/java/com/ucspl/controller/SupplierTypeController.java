
package com.ucspl.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.Branch;
import com.ucspl.model.SupplierType;
import com.ucspl.repository.SupplierTypeRepository;
import com.ucspl.services.CreateDefinitionService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class SupplierTypeController {

	
	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateDefinitionService createDefinitionServiceObject;
	
	public SupplierTypeController() {
	

	}

	// get all supplier type list
	@GetMapping("/supplierType")
	public List<SupplierType> getAllSupplierTypes() {
		List<SupplierType>  response=createDefinitionServiceObject.getAllSupplierTypes();
		return response;
	
	}
	
	// save create supplier type
	@PostMapping("/createSupplierType")
	public  SupplierType createSupplierType(@Valid @RequestBody SupplierType supplierTypeObject) {
		
		SupplierType response=createDefinitionServiceObject.createSupplierType(supplierTypeObject);
		return response;
		
	}
	
	
	}

