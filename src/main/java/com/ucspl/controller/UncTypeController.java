
package com.ucspl.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.UncMasterType;
import com.ucspl.model.UncType;
import com.ucspl.model.UomListForReqAndIwd;
import com.ucspl.services.CreateDefinitionService;



@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class UncTypeController {

	private static final Logger logger = Logger.getLogger(UncTypeController.class);

	@Autowired
	private CreateDefinitionService createDefinitionService;

	public UncTypeController() {

	}

//get all uncMasType list	
	@GetMapping("/crtUncType")
	public List<UncType> getAllUncType() {
		logger.info("/crtUncType - Get all unc Type List!");

		List<UncType> resultValue = createDefinitionService.getAllUncType();

		return resultValue;
	}

//Save create uncMasType Details 
	@PostMapping("/crtUncType")
	public UncType createUncType(@Valid @RequestBody UncType uncType) {

		UncType response = createDefinitionService.createUncType(uncType);
		return response;
	}

}
