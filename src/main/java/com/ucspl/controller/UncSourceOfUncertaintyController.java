
package com.ucspl.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.UncMasterType;
import com.ucspl.model.UncSourceOfUncertainty;
import com.ucspl.model.UomListForReqAndIwd;
import com.ucspl.services.CreateDefinitionService;



@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class UncSourceOfUncertaintyController {

	private static final Logger logger = Logger.getLogger(UncSourceOfUncertaintyController.class);

	@Autowired
	private CreateDefinitionService createDefinitionService;

	public UncSourceOfUncertaintyController() {

	}

//get all uncMasType list	
	@GetMapping("/crtUncSouOfUncer")
	public List<UncSourceOfUncertainty> getAllUncSourceOfUncertainty() {
		logger.info("/crtUncSouOfUncer - Get all uncMasType List!");

		List<UncSourceOfUncertainty> resultValue = createDefinitionService.getAllUncSourceOfUncertainty();

		return resultValue;
	}

//Save create uncMasType Details 
	@PostMapping("/crtUncSouOfUncer")
	public UncSourceOfUncertainty createUncSourceOfUncertainty(@Valid @RequestBody UncSourceOfUncertainty uncSourceOfUncertainty) {

		UncSourceOfUncertainty response = createDefinitionService.createUncSourceOfUncertainty(uncSourceOfUncertainty);
		return response;
	}

}

