
package com.ucspl.controller;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CustomerContactList;
import com.ucspl.services.CustomerContactListService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class CustomerContactListController {

	private static final Logger logger = Logger.getLogger(CustomerContactListController.class);

	@Autowired
	private CustomerContactListService customerContactListService;

	@PersistenceContext
	private EntityManager em;

	public CustomerContactListController() {

	}

	@GetMapping("/getcustConList")
	public List<CustomerContactList> getAllCustomerContactList() {

		List<CustomerContactList> response = customerContactListService.getAllCustomerContactList();
		return response;
	}

	@GetMapping("/custConList/{id}")
	public ResponseEntity<CustomerContactList> getCustomerContactById(
			@PathVariable(value = "id") Long customerContactId) throws ResourceNotFoundException {
		ResponseEntity<CustomerContactList> response = customerContactListService
				.getCustomerContactById(customerContactId);
		return response;
	}

	@PostMapping("/custConList")
	public CustomerContactList createCustomerContact(@Valid @RequestBody CustomerContactList customerContactList) {

		CustomerContactList response = customerContactListService.createCustomerContact(customerContactList);
		return response;

	}

	@PutMapping("/custConList/{id}")
	public ResponseEntity<CustomerContactList> updateCustomerContactById(
			@PathVariable(value = "id") Long customerContactId,
			@Valid @RequestBody CustomerContactList customerContactDetails) throws ResourceNotFoundException {

		ResponseEntity<CustomerContactList> response = customerContactListService
				.updateCustomerContactById(customerContactId, customerContactDetails);
		return response;
	}

	// Delete contact list Details for id
	@DeleteMapping("/custConList/{id}")
		public Map<String, Boolean> deleteCustomerContactList(@PathVariable(value = "id") Long customerContactId) throws ResourceNotFoundException {
			
			Map<String, Boolean> response = customerContactListService.deleteCustomerContactList(customerContactId);
			return response;	
		}

	// get customer contact data for cust id ///////////////////////////////////////
	@GetMapping(value = "/conListForCustId/{id}", produces = "application/json")
	public List<CustomerContactList> docNoForGettingSrNo(@PathVariable(value = "id") long custId) {

		List<CustomerContactList> resultValue = customerContactListService.docNoForGettingSrNo(custId);
		return resultValue;
	}

}