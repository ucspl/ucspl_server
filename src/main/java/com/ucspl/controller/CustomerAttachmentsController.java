package com.ucspl.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CustomerAttachments;
import com.ucspl.model.ImageModel;
import com.ucspl.repository.CustomerAttachmentsRepository;
import com.ucspl.services.CustomerAttachmentsService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class CustomerAttachmentsController {

	
	private static final Logger logger = Logger.getLogger(CustomerAttachmentsController.class);

	CustomerAttachments imgData= new CustomerAttachments();
	

	@Autowired
	private CustomerAttachmentsService customerAttachmentsService;
	

	public CustomerAttachmentsController() {
	
	}
	
	@PostMapping("/uploadCustAttach")
	public ResponseEntity<CustomerAttachments> uplaodImage(@RequestParam("imageFile") MultipartFile file) throws IOException {

		ResponseEntity<CustomerAttachments>response = customerAttachmentsService.uplaodImage(file);
		
		return response;
	}
	
	@GetMapping(path = { "/getCustAttFiles/{cust_id}" })
	public List<CustomerAttachments> getImage(@PathVariable("cust_id") long custId) throws IOException {

		List<CustomerAttachments> retrievedImage = customerAttachmentsService.getImage(custId);
		return retrievedImage;
	}
	
	
	@GetMapping("/custAttach")
	public List<CustomerAttachments> getAllCustomerAttachments() {

		List<CustomerAttachments> response = customerAttachmentsService.getAllCustomerAttachments();
		return response;
	}

	@GetMapping("/custAttach/{id}")
	public ResponseEntity<CustomerAttachments> getCustomerAttachmentById(
			@PathVariable(value = "id") Long customerAttachmentsId) throws ResourceNotFoundException {
	
		ResponseEntity<CustomerAttachments> response = customerAttachmentsService.getCustomerAttachmentById(customerAttachmentsId);
		return response;
	}

	@PostMapping("/custAttach")
	public CustomerAttachments createCustomerAttachment(
			@Valid @RequestBody CustomerAttachments customerAttachments) {
		
		CustomerAttachments response=customerAttachmentsService.createCustomerAttachment(customerAttachments);
		
		return response;

	}
	
	
	@PostMapping("/getSelCustAttach")
	public ImageModel getSelectedCustFileInByteFormat(@Valid @RequestBody CustomerAttachments attachFile) throws IOException {

		ImageModel img= customerAttachmentsService.getSelectedCustFileInByteFormat(attachFile);
		return img;
	}
	
	
////////////////////////////// image compress decompress ////////////////////////////////
	
	public static byte[] compressBytes(byte[] data) {

		Deflater deflater = new Deflater();
		deflater.setInput(data);
		deflater.finish();

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);

		byte[] buffer = new byte[1024];

		while (!deflater.finished()) {
			int count = deflater.deflate(buffer);
			outputStream.write(buffer, 0, count);
		}
		try {
			outputStream.close();
		} catch (IOException e) {

		}

	
		logger.info("Compressed Image Byte Size - " + outputStream.toByteArray().length);
		return outputStream.toByteArray();

	}

	// uncompress the image bytes before returning it to the angular application
	public static byte[] decompressBytes(byte[] data) {

		Inflater inflater = new Inflater();
		inflater.setInput(data);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);

		byte[] buffer = new byte[1024];

		try {
			while (!inflater.finished()) {
				int count = inflater.inflate(buffer);
				outputStream.write(buffer, 0, count);
			}
			outputStream.close();
		} catch (IOException ioe) {
		} catch (DataFormatException e) {
		}

		logger.info("Uncompress Image");
		return outputStream.toByteArray();
	}
}
