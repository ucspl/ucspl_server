
package com.ucspl.controller;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.SrNoProformaDocument;
import com.ucspl.services.SrNoProformaDocumentService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class SrNoProformaDocumentController {

	private static final Logger logger = Logger.getLogger(ChallanCumInvoiceDocumentController.class); 
	
	@Autowired
	SrNoProformaDocumentService srNoProformaDocumentServiceObject;

	private String docNoVariable;

	@PersistenceContext
	private EntityManager em;

	

	public SrNoProformaDocumentController() {
		// TODO Auto-generated constructor stub
	}

//  Get All Proforma Document Item Information                                                       
	@GetMapping("/proformaDoc")
	public List<SrNoProformaDocument> getAllProformaDocument() {
		
		List<SrNoProformaDocument> response=srNoProformaDocumentServiceObject.getAllProformaDocument();
		return response;
	}
	
	
//  Get Proforma Document Item Information By Id                                                                                                 
	@GetMapping("/proformaDoc/{id}")
	public ResponseEntity<SrNoProformaDocument> getProformaDocumentById(
			@PathVariable(value = "id") Long proformaDocumentId) throws ResourceNotFoundException {
		
		ResponseEntity<SrNoProformaDocument> response=srNoProformaDocumentServiceObject.getProformaDocumentById(proformaDocumentId);
		return response;
	}
	
	
// Save Item Details of Proforma Document
	@PostMapping("/proformaDoc")
	public SrNoProformaDocument createProformaDocument(
			@Valid @RequestBody SrNoProformaDocument proformaDocument) {
		
		
		SrNoProformaDocument response=srNoProformaDocumentServiceObject.createProformaDocument(proformaDocument);
		return response;
	}

//  Update Proforma Document Item Details for id
	@PutMapping("/proformaDoc/{id}")
	public ResponseEntity<SrNoProformaDocument> updateProformaDocumentById(
			@PathVariable(value = "id") Long proformaDocumentId,
			@Valid @RequestBody SrNoProformaDocument proformaDocumentDetails)
					throws ResourceNotFoundException {
		
		ResponseEntity<SrNoProformaDocument> response=srNoProformaDocumentServiceObject.updateProformaDocumentById(proformaDocumentId,proformaDocumentDetails);
		return response;		
	}

	
//  get sr no store procedure for doc no 
	@RequestMapping(value = "/srProformaDoc", method = RequestMethod.POST, produces = "application/json")
	public String docNoForGettingSrNo(@Valid @RequestBody String docNo) {
	
		String response = srNoProformaDocumentServiceObject.docNoForGettingSrNo(docNo);
		return response;
	}
	
// sr no detail for doc no
	@GetMapping(value = "/srProformaDoc", produces = "application/json")
	public List<Object[]> getSrNoForDocNo() {

		List<Object[]> response=srNoProformaDocumentServiceObject.getSrNoForDocNo();
		return response;
		
	}
	
// code to delete proforma  Row
	@PostMapping("/deleteProformaRow")
	public String removeProformaRow(@Valid @RequestBody long indexToDelete) throws IOException {

		System.out.println("Invoice row");
		String response = srNoProformaDocumentServiceObject.removeProformaRow(indexToDelete);
		return response;
	}

}

