
package com.ucspl.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.Branch;
import com.ucspl.model.UomListForReqAndIwd;
import com.ucspl.services.CreateDefinitionService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class UomListForReqAndIwdController {

	private static final Logger logger = Logger.getLogger(UomListForReqAndIwdController.class);

	@Autowired
	private CreateDefinitionService createDefinitionService;

	public UomListForReqAndIwdController() {

	}

//get all uom list	
	@GetMapping("/uomList")
	public List<UomListForReqAndIwd> getAllUomList() {
		logger.info("/parameterList - Get all Uom List!");

		List<UomListForReqAndIwd> resultValue = createDefinitionService.getAllUomList();

		return resultValue;
	}

//Save create uom Details 
	@PostMapping("/uomList")
	public UomListForReqAndIwd createUom(@Valid @RequestBody UomListForReqAndIwd uomObject) {

		UomListForReqAndIwd response = createDefinitionService.createUom(uomObject);
		return response;
	}

}
