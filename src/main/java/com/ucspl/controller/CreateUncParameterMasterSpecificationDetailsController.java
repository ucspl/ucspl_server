package com.ucspl.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateUncParameterMasterSpecificationDetails;
import com.ucspl.model.CreateUucParameterMasterSpecificationDetails;
import com.ucspl.model.JunctionOfInputUomAndCreateUUCParaSpecification;
import com.ucspl.model.JunctionOfUomAndCreateUUCParaSpecification;
import com.ucspl.model.UncMasterType;
import com.ucspl.model.UomListForReqAndIwd;
import com.ucspl.repository.CreateUncParameterMasterSpecificationDetailsRepository;
import com.ucspl.repository.CreateUucParameterMasterSpecificationDetailsRepository;
import com.ucspl.repository.JunctionOfInputUomAndCreateUUCParaSpecificationRepository;
import com.ucspl.repository.JunctionOfUomAndCreateUUCParaSpecificationRepository;
import com.ucspl.services.CreateDefinitionService;



@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class CreateUncParameterMasterSpecificationDetailsController {

	private static final Logger logger = Logger.getLogger(CreateUncParameterMasterSpecificationDetailsController.class);

	@Autowired
	private CreateUncParameterMasterSpecificationDetailsRepository createUncParameterMasterSpecificationDetailsRepository;

	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	private JunctionOfInputUomAndCreateUUCParaSpecificationRepository junctionOfInputUomAndCreateUUCParaSpecificationRepository;
	
	@Autowired
	private JunctionOfUomAndCreateUUCParaSpecificationRepository junctionOfUomAndCreateUUCParaSpecificationRepository;
	
	public CreateUncParameterMasterSpecificationDetailsController() {

	}
	
	@GetMapping("/crtUncNmMSpe")
	public List<CreateUncParameterMasterSpecificationDetails> getAllCreateUncParameterMasterSpecificationDetails() {

		logger.info("/crtUncNmMSpe - Get All Created Unc Name  parameter Master Specification!");
		return createUncParameterMasterSpecificationDetailsRepository.findAll();
	}

	@GetMapping("/crtUncNmMSpe/{id}")
	public ResponseEntity<CreateUncParameterMasterSpecificationDetails> getCreateUncParameterMasterSpecificationDetailsById(
			@PathVariable(value = "id") Long createUncParameterMasterSpecificationDetailsId) throws ResourceNotFoundException {
		CreateUncParameterMasterSpecificationDetails createUncParameterMasterSpecificationDetails = createUncParameterMasterSpecificationDetailsRepository
				.findById(createUncParameterMasterSpecificationDetailsId).orElseThrow(() -> new ResourceNotFoundException(
						" Calibration Result Data For Master Instrument not found for this id :: " + createUncParameterMasterSpecificationDetailsId));

		logger.info("/crtUncNmMSpe/{id} - Get Created Unc Name  parameter Master Specification By Id!" + createUncParameterMasterSpecificationDetailsId);
		return ResponseEntity.ok().body(createUncParameterMasterSpecificationDetails);
	}

	@PutMapping("/crtUncNmMSpe/{id}")
	public ResponseEntity<CreateUncParameterMasterSpecificationDetails> updateUncParameterMasterSpecificationDetailsById(
			@PathVariable(value = "id") Long createUncParameterMasterSpecificationDetailsId,
			@Valid @RequestBody CreateUncParameterMasterSpecificationDetails createUncParameterMasterSpecificationDetails) throws ResourceNotFoundException {
		CreateUncParameterMasterSpecificationDetails createUncParameterMasterSpecifiDetails = createUncParameterMasterSpecificationDetailsRepository
				.findById(createUncParameterMasterSpecificationDetailsId).orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + createUncParameterMasterSpecificationDetailsId));

	
		//createUncParameterMasterSpecifiDetails.setParameterNumber(createUncParameterMasterSpecificationDetails.getParameterNumber());
		createUncParameterMasterSpecifiDetails.setSourceOfUncertainity(createUncParameterMasterSpecificationDetails.getSourceOfUncertainity());
		createUncParameterMasterSpecifiDetails.setProbabiltyDistribution(createUncParameterMasterSpecificationDetails.getProbabiltyDistribution());
		createUncParameterMasterSpecifiDetails.setType(createUncParameterMasterSpecificationDetails.getType());
		createUncParameterMasterSpecifiDetails.setDividingFactor(createUncParameterMasterSpecificationDetails.getDividingFactor());
		createUncParameterMasterSpecifiDetails.setSensitivityCoefficient(createUncParameterMasterSpecificationDetails.getSensitivityCoefficient());
		createUncParameterMasterSpecifiDetails.setValueForEstimate(createUncParameterMasterSpecificationDetails.getValueForEstimate());
		createUncParameterMasterSpecifiDetails.setPercentageForEstimateAg(createUncParameterMasterSpecificationDetails.getPercentageForEstimateAg());
		createUncParameterMasterSpecifiDetails.setPercentageForEstimateDg(createUncParameterMasterSpecificationDetails.getPercentageForEstimateDg());
		createUncParameterMasterSpecifiDetails.setDegreeOfFreedom(createUncParameterMasterSpecificationDetails.getDegreeOfFreedom());
		createUncParameterMasterSpecifiDetails.setMasterType(createUncParameterMasterSpecificationDetails.getMasterType());
		createUncParameterMasterSpecifiDetails.setMasterNumber(createUncParameterMasterSpecificationDetails.getMasterNumber());


		final CreateUncParameterMasterSpecificationDetails updatedUncParameterMasterSpecificationDetail = createUncParameterMasterSpecificationDetailsRepository
				.save(createUncParameterMasterSpecifiDetails);
		logger.info("/crtUncNmMSpe/{id} - Update already Created Unc Name  parameter Master Specification for id " + createUncParameterMasterSpecificationDetailsId);

		return ResponseEntity.ok(updatedUncParameterMasterSpecificationDetail);
	}

	@PostMapping("/crtUncNmMSpe") 
	public CreateUncParameterMasterSpecificationDetails CreateUncParameterMasterSpecificationDetailsFunction(
			@Valid @RequestBody CreateUncParameterMasterSpecificationDetails createUncParameterMasterSpecificationDetails) {
			
		logger.info("/crtUncNmMSpe - Save the Create Unc Name parameter Master Specification!");	
		return createUncParameterMasterSpecificationDetailsRepository.save(createUncParameterMasterSpecificationDetails);
	}
	// Delete User Details for id 
	@DeleteMapping("/crtUncNmMSpe/{id}")
	public Map<String, Boolean> deleteCreatedUncParameterMasterSpecificationDetails(@PathVariable(value = "id") Long createUncParameterMasterSpecificationDetailsId) throws ResourceNotFoundException {

		Map<String, Boolean> response = new HashMap<>();
		createUncParameterMasterSpecificationDetailsRepository.deleteById(createUncParameterMasterSpecificationDetailsId);
		response.put("deleted", Boolean.TRUE);
		logger.info("/crtUncNmMSpe/{id} - Delete Create Unc Name parameter Master Specification Detail of id - " + createUncParameterMasterSpecificationDetailsId);

		return response;
	}
	
		

/////////////////////////store procedure to get all created sr no for uuc master instrument /////////////////////////////
	@RequestMapping(value = "/srnoUncMSDet", method = RequestMethod.POST, produces = "application/json")
	public List<Object[]> srNoDetailsOfUncMasterSpecification(@Valid @RequestBody long uncMasterId) {

		System.out.println("unc master id: "+ uncMasterId);   
		
		
		logger.info("/srnoMSDet -" + " Search master specification Details For master id" + uncMasterId);

		List<Object[]> resultValue;
		
	
		StoredProcedureQuery procedureQuery = em
				.createStoredProcedureQuery("srno_unc_master_instru_specification_detail_list"); 
		procedureQuery.registerStoredProcedureParameter("id_no", Long.class, ParameterMode.IN);

		procedureQuery.setParameter("id_no", (long) uncMasterId);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList(); 
		System.out.println("unc master id resultvalue: "+ uncMasterId);   

		return resultValue;
	}
	
	
	
	//update query for unique sr number 
	@PutMapping("/crtCUncMIForParaNo/{id}")
	public ResponseEntity<CreateUncParameterMasterSpecificationDetails> updateUncMasterInstrumentSpecificationDetailsByIdForParameterNumber(
			@PathVariable(value = "id") Long createUncParaMasterSpecificationDetailsId,
			@Valid @RequestBody CreateUncParameterMasterSpecificationDetails createUncParameterMasterSpecificationDetails)
			throws ResourceNotFoundException {
		CreateUncParameterMasterSpecificationDetails createUncParameterMasterSpecification = createUncParameterMasterSpecificationDetailsRepository
				.findById(createUncParaMasterSpecificationDetailsId).orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + createUncParaMasterSpecificationDetailsId));
	

		createUncParameterMasterSpecification.setParameterNumber(createUncParameterMasterSpecificationDetails.getParameterNumber());

		final CreateUncParameterMasterSpecificationDetails updatedUncParameterMasterSpecificationDetails = createUncParameterMasterSpecificationDetailsRepository
				.save(createUncParameterMasterSpecification);
		logger.info("/crtCUncMIForParaNo/{id} - Update Unc parameter master specification parameter number of specification"
				+ createUncParaMasterSpecificationDetailsId);

		return ResponseEntity.ok(updatedUncParameterMasterSpecificationDetails);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
          
////////////////////store procedure to get store data in junction table-junction_of_input_uom_and_createuuc_para_specification_table///////////

//	@PostMapping("/saveUomToUncJunTable")
//	public List<JunctionOfUomAndCreateUNCParaSpecification> saveUomDataInJunctionTable(
//			@Valid @RequestBody JunctionOfUomAndCreateUNCParaSpecification[] dataToSave) throws IOException {
//		int i;
//		List<Object[]> response=new ArrayList<Object[]>();
//		List<JunctionOfUomAndCreateUUCParaSpecification> response1=new ArrayList<JunctionOfUomAndCreateUUCParaSpecification>();
//		
//
//		if(dataToSave!=null) {
//			for( i=0;i<dataToSave.length;i++) {
//				response1.add(junctionOfUomAndCreateUUCParaSpecificationRepository.save(dataToSave[i]));
//				;
//			}
//			
//		}
//		logger.info("/saveUomToUucJunTable - Save the Uom and Uuc Name parameter Master Specification id to junction table ! ");	
//		return response1;
//		
//	}
	
	
////////////////////store procedure to get store data in junction table-junction_of_input_uom_and_createuuc_para_specification_table///////////

//	@PostMapping("/saveIpUomToUucJunTable")
//	public List<JunctionOfInputUomAndCreateUUCParaSpecification> saveUomDataInJunctionTable(
//			@Valid @RequestBody JunctionOfInputUomAndCreateUUCParaSpecification[] dataToSave) throws IOException {
//		int i;
//		List<Object[]> response = new ArrayList<Object[]>();
////		List <JunctionOfInputUom 
//		
//		List<JunctionOfInputUomAndCreateUUCParaSpecification> response1 = new ArrayList<JunctionOfInputUomAndCreateUUCParaSpecification>();
//
//		if (dataToSave != null) {
//			for (i = 0; i < dataToSave.length; i++) {
//				response1.add(junctionOfInputUomAndCreateUUCParaSpecificationRepository.save(dataToSave[i]));
//			}
//
//		}
//		logger.info(
//				"/saveUomToUucJunTable - Save the input Uom and Uuc Name parameter Master Specification id to junction table ! ");
//		return response1;
//
//	}
	
	
	
	
	
	
	

}
