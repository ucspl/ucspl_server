package com.ucspl.controller;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateChallanCumInvoiceDocument;
import com.ucspl.model.SearchChallanCumInvoiceDocument;
import com.ucspl.services.CreateChallanCumInvoiceDocumentService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class CreateChallanCumInvoiceDocumentController {

	Object lastId;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	CreateChallanCumInvoiceDocumentService createChallanCumInvoiceDocumentServiceObject;

	public CreateChallanCumInvoiceDocumentController() {
		// TODO Auto-generated constructor stub
	}

	// Get Challan Cum Invoice Document Header Information By Id
	@GetMapping("/createInv")
	public List<CreateChallanCumInvoiceDocument> getAllCreateChallanCumInvoiceDocument() {

		List<CreateChallanCumInvoiceDocument> response = createChallanCumInvoiceDocumentServiceObject
				.getAllCreateChallanCumInvoiceDocument();
		return response;
	}

    //Get Challan Cum Invoice Document Header Information By Id
	@GetMapping("/createInv/{id}")
	public ResponseEntity<CreateChallanCumInvoiceDocument> getCreateChallanCumInvoiceDocumentById(
			@PathVariable(value = "id") Long createChallanCumInvoiceDocumentId) throws ResourceNotFoundException {

		ResponseEntity<CreateChallanCumInvoiceDocument> response = createChallanCumInvoiceDocumentServiceObject
				.getCreateChallanCumInvoiceDocumentById(createChallanCumInvoiceDocumentId);
		return response;

	}

	// Save Header Details of Challan Cum Invoice Document
	@PostMapping("/createInv")
	public CreateChallanCumInvoiceDocument createChallanCumInvoiceDocument(
			@Valid @RequestBody CreateChallanCumInvoiceDocument createChallanCumInvoiceDocument) {

		CreateChallanCumInvoiceDocument response = createChallanCumInvoiceDocumentServiceObject
				.createChallanCumInvoiceDocument(createChallanCumInvoiceDocument);
		return response;

	}

	// Update Challan Cum Invoice Document Header Details for id
	@PutMapping("/createInv/{id}")
	public ResponseEntity<CreateChallanCumInvoiceDocument> updateCreateChallanCumInvoiceDocumentById(
			@PathVariable(value = "id") Long createChallanCumInvoiceDocumentId,
			@Valid @RequestBody CreateChallanCumInvoiceDocument createChallanCumInvoiceDocumentDetails)
			throws ResourceNotFoundException {

		ResponseEntity<CreateChallanCumInvoiceDocument> response = createChallanCumInvoiceDocumentServiceObject
				.updateCreateChallanCumInvoiceDocumentById(createChallanCumInvoiceDocumentId,
						createChallanCumInvoiceDocumentDetails);

		return response;

	}

	// doc no for invoice ///////////////////////////

	@GetMapping(path = { "/DocNoForInv" })
	public String getDocumentNumber() throws IOException {

		String response = createChallanCumInvoiceDocumentServiceObject.getDocumentNumber();
		return response;

	}

	//////////////////////////////// code for search and modify Invoice////////////////////

	@RequestMapping(value = "/searchInvs", method = RequestMethod.POST, produces = "application/json")
	public String searchAndModifyChallanCumInvoiceDocument(
			@Valid @RequestBody SearchChallanCumInvoiceDocument searchChallanCumInvoiceDoc) {

		String response = createChallanCumInvoiceDocumentServiceObject
				.searchAndModifyChallanCumInvoiceDocument(searchChallanCumInvoiceDoc);
		return response;
	}

	// Result For Search Invoice Document  //
	@GetMapping(value = "/searchInvs", produces = "application/json")
	public List<Object[]> getAllChallanCumInvoiceDocument() {

		List<Object[]> resultValue = createChallanCumInvoiceDocumentServiceObject.getAllChallanCumInvoiceDocument();
		return resultValue;

	}

	////////////////////////////// STORE PROCEDURE FOR GETTING DETAILS OF CREATE Invoice FOR SEARCH Invoice //////////////////////////////
	 
	@RequestMapping(value = "/CInvsDet", method = RequestMethod.POST, produces = "application/json")
	public String docNoForGettingCreateChallanCumInvoiceDocumentDetails(@Valid @RequestBody String docNo) {

		String response = createChallanCumInvoiceDocumentServiceObject
				.docNoForGettingCreateChallanCumInvoiceDocumentDetails(docNo);
		return response;
	}

	@GetMapping(value = "/CInvsDet", produces = "application/json")
	public List<Object[]> gettingCreateChallanCumInvoiceDocumentDetails() {

		List<Object[]> resultValue = createChallanCumInvoiceDocumentServiceObject
				.gettingCreateChallanCumInvoiceDocumentDetails();
		return resultValue;
	}

}
