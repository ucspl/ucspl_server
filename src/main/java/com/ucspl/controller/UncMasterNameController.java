
package com.ucspl.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.UncMasterName;
import com.ucspl.services.CreateDefinitionService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class UncMasterNameController {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateDefinitionService createDefinitionServiceObject;

	public UncMasterNameController() {

	}

// get Unc Mater Name list
	@GetMapping("/crtUncMastName")
	public List<UncMasterName> getAllUncMasterNames() {
		List<UncMasterName> response = createDefinitionServiceObject.getAllUncMasterNames();
		return response;
	}

// Save create Unc Master Name Details
	@PostMapping("/crtUncMastName")
	public UncMasterName createUncMasterName(@Valid @RequestBody UncMasterName uncMasterNameObject) {
		UncMasterName response = createDefinitionServiceObject.createUncMasterName(uncMasterNameObject);
		return response;
	}

}
