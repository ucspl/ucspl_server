package com.ucspl.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.InstrumentTypeForMasterInstrument;
import com.ucspl.services.CreateDefinitionService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class InstrumentTypeForMasterInstrumentController {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateDefinitionService createDefinitionServiceObject;

	public InstrumentTypeForMasterInstrumentController() {

	}

// get branch list
	@GetMapping("/mInsType")
	public List<InstrumentTypeForMasterInstrument> getAllInstrumentTypeForMasterInstrument() {

		List<InstrumentTypeForMasterInstrument> response = createDefinitionServiceObject.getAllInstrumentTypeForMasterInstrument();
		return response;
	}

// Save create branchDetails
	@PostMapping("/mInsType")
	public InstrumentTypeForMasterInstrument createInstrumentTypeForMasterInstrument(@Valid @RequestBody InstrumentTypeForMasterInstrument instrumentTypeForMasterInstrumentObject) {

		InstrumentTypeForMasterInstrument response = createDefinitionServiceObject.createInstrumentTypeForMasterInstrument(instrumentTypeForMasterInstrumentObject);
		return response;
	}

}
