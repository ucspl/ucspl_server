package com.ucspl.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.BillingCreditPeriod;
import com.ucspl.repository.BillingCreditPeriodRepository;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class BillingCreditPeriodController {

private static final Logger logger = Logger.getLogger(BillingCreditPeriodController.class);

@Autowired
private BillingCreditPeriodRepository billingCreditPeriodRepository;

public BillingCreditPeriodController() {

}

@GetMapping("/billcredperiod")
public List<BillingCreditPeriod> getAllBillingCreditPeriod() {
logger.info("/billcredperiod - Get all Billing Credit Period!");

return billingCreditPeriodRepository.findAll();

}

}