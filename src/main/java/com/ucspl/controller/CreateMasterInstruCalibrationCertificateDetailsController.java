package com.ucspl.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateMasterInstruCalibrationCertificateDetails;
import com.ucspl.model.SearchMasterInstruByMIdPNmAndStatus;
import com.ucspl.model.SearchMasterInstruByMastIdAndParameterId;
import com.ucspl.model.SearchMasterInstruByMastIdAndStatus;
import com.ucspl.repository.CreateMasterInstruCalibrationCertificateDetailsRepository;
import com.ucspl.services.CreateMasterInstruCalibrationCertificateDetailsService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class CreateMasterInstruCalibrationCertificateDetailsController {

	@Autowired
	private CreateMasterInstruCalibrationCertificateDetailsService createMasterInstruCalibrationCertificateDetailsService;

	@PersistenceContext
	private EntityManager em;

	public CreateMasterInstruCalibrationCertificateDetailsController() {

	}

// Get all master instruments calibration certificate details	
	@GetMapping("/crtMCCerD")
	public List<CreateMasterInstruCalibrationCertificateDetails> getAllCreateMasterInstrumentCalCertificateDetails() {

		List<CreateMasterInstruCalibrationCertificateDetails> response = createMasterInstruCalibrationCertificateDetailsService
				.getAllCreateMasterInstrumentCalCertificateDetails();
		return response;
	}

// Get all master instruments calibration certificate details by id	
	@GetMapping("/crtMCCerD/{id}")
	public ResponseEntity<CreateMasterInstruCalibrationCertificateDetails> getCreateMasterInstrumentCalCertificateDetailsById(
			@PathVariable(value = "id") Long createMasterInstrumentDetailsId) throws ResourceNotFoundException {

		ResponseEntity<CreateMasterInstruCalibrationCertificateDetails> response = createMasterInstruCalibrationCertificateDetailsService
				.getCreateMasterInstrumentCalCertificateDetailsById(createMasterInstrumentDetailsId);
		return response;
	}

// Update master instruments calibration certificate details	
	@PutMapping("/crtMCCerD/{id}")
	public ResponseEntity<CreateMasterInstruCalibrationCertificateDetails> updateCreateMasterInstrumentCalCertificateDetailsById(
			@PathVariable(value = "id") Long createMasterInstruCalCertificateDetailsId,
			@Valid @RequestBody CreateMasterInstruCalibrationCertificateDetails createMasterInstruCalCertificateDetails)
			throws ResourceNotFoundException {

		ResponseEntity<CreateMasterInstruCalibrationCertificateDetails> response = createMasterInstruCalibrationCertificateDetailsService
				.updateCreateMasterInstrumentCalCertificateDetailsById(createMasterInstruCalCertificateDetailsId,
						createMasterInstruCalCertificateDetails);
		return response;
	}

// Create master instruments calibration certificate details	
	@PostMapping("/crtMCCerD")
	public CreateMasterInstruCalibrationCertificateDetails createCreateMasterInstrumentCalCertificateDetails(
			@Valid @RequestBody CreateMasterInstruCalibrationCertificateDetails createMasterInstruCalCertificateDetails) {

		CreateMasterInstruCalibrationCertificateDetails response = createMasterInstruCalibrationCertificateDetailsService
				.createCreateMasterInstrumentCalCertificateDetails(createMasterInstruCalCertificateDetails);
		return response;
	}

// Delete User Details for id
	@DeleteMapping("/crtMCCerD/{id}")
	public Map<String, Boolean> deleteMasterInstrumentCalCertificateDetails(
			@PathVariable(value = "id") Long masterInstrumentDetailId) throws ResourceNotFoundException {

		Map<String, Boolean> response = new HashMap<>();
		response = createMasterInstruCalibrationCertificateDetailsService
				.deleteMasterInstrumentCalCertificateDetails(masterInstrumentDetailId);
		return response;
	}

////////////////////////////sp for getting sr no for master instruments ////////////////////////////////  

	@RequestMapping(value = "/snMCalCerDet", method = RequestMethod.POST, produces = "application/json")
	public List<Object[]> srNoDetailsOfMasterInstruCalCertificate(@Valid @RequestBody long masterId) {

		List<Object[]> resultValue = createMasterInstruCalibrationCertificateDetailsService
				.srNoDetailsOfMasterInstruCalCertificate(masterId);
		return resultValue;
	}

//////////////////////////// sp for getting sr no for master instruments ////////////////////////////////  

	@RequestMapping(value = "/snMCalCerDetBySt", method = RequestMethod.POST, produces = "application/json")
	public List<Object[]> srNoDetailsOfMasterInstruCalCertificateByStatus(
			@Valid @RequestBody SearchMasterInstruByMastIdAndStatus status) {

		List<Object[]> resultValue = createMasterInstruCalibrationCertificateDetailsService
				.srNoDetailsOfMasterInstruCalCertificateByStatus(status);
		return resultValue;
	}

////////////////////////////sp for getting sr no for master instruments ////////////////////////////////  

	@RequestMapping(value = "/srnoCRDForPara", method = RequestMethod.POST, produces = "application/json")
	public List<Object[]> srNoDetailsOfCalibraSpecificationResultForSelectedParameter(@Valid @RequestBody long paraId) {

		List<Object[]> resultValue = createMasterInstruCalibrationCertificateDetailsService
				.srNoDetailsOfCalibraSpecificationResultForSelectedParameter(paraId);
		return resultValue;
	}

////////////////////////////sp for getting sr no for master instruments ////////////////////////////////  

	@RequestMapping(value = "/srCRDForCreMIIdParaId", method = RequestMethod.POST, produces = "application/json")
	public List<Object[]> srNoDetailsOfCalibraSpecificationResultForCreateMastInstruId(
			@Valid @RequestBody SearchMasterInstruByMastIdAndParameterId createMastInstruIdParaId) {

		List<Object[]> resultValue = createMasterInstruCalibrationCertificateDetailsService
				.srNoDetailsOfCalibraSpecificationResultForCreateMastInstruId(createMastInstruIdParaId);
		return resultValue;
	}

}