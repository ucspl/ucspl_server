package com.ucspl.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateUucNameMaster;
import com.ucspl.repository.CreateUucNameMasterRepository;


@RestController
@CrossOrigin
@RequestMapping("/api/v1")
public class CreateUucNameMasterController {

	
	private static final Logger logger = LogManager.getLogger(CreateUucNameMasterController.class);
	
	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateUucNameMasterRepository createUucNameMasterRepository;
	
	
	@GetMapping("/crtUucNmMaster")
	public List<CreateUucNameMaster> getAllCreateUucNameMaster() {

		logger.info("/crtUucNmMaster - Get All Created Uuc Name Master!");
		return createUucNameMasterRepository.findAll();
	}

	@GetMapping("/crtUucNmMaster/{id}")
	public ResponseEntity<CreateUucNameMaster> getCreatedUucNameMasterById(
			@PathVariable(value = "id") Long createUucNameMasterId) throws ResourceNotFoundException {
		CreateUucNameMaster createUucNameMaster = createUucNameMasterRepository
				.findById(createUucNameMasterId).orElseThrow(() -> new ResourceNotFoundException(
						" Calibration Result Data For Master Instrument not found for this id :: " + createUucNameMasterId));

		logger.info("/crtUucNmMaster/{id} - Get Create Uuc Name Master By Id!" + createUucNameMasterId);
		return ResponseEntity.ok().body(createUucNameMaster);
	}

	@PutMapping("/crtUucNmMaster/{id}")
	public ResponseEntity<CreateUucNameMaster> updateUucNameMasterById(
			@PathVariable(value = "id") Long createUucNameMasterId,
			@Valid @RequestBody CreateUucNameMaster createUucNameMasterDetails) throws ResourceNotFoundException {
		CreateUucNameMaster createUucNameMaster = createUucNameMasterRepository
				.findById(createUucNameMasterId).orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + createUucNameMasterId));

		createUucNameMaster.setInstrument(createUucNameMasterDetails.getInstrument());
		createUucNameMaster.setDraft(createUucNameMasterDetails.getDraft());
		createUucNameMaster.setSubmitted(createUucNameMasterDetails.getSubmitted());
		createUucNameMaster.setApproved(createUucNameMasterDetails.getApproved());
	
		final CreateUucNameMaster updatedCreateUucNameMaster = createUucNameMasterRepository
				.save(createUucNameMaster);
		logger.info("/crtUucNmMaster/{id} - Update Create Uuc Name Master for id " + createUucNameMasterId);

		return ResponseEntity.ok(updatedCreateUucNameMaster);
	}

	@PostMapping("/crtUucNmMaster") 
	public CreateUucNameMaster createUucNameMasterFunc(
			@Valid @RequestBody CreateUucNameMaster createUucNameMaster) {
			
		logger.info("/crtUucNmMaster - Save the Create Uuc Name Master !");	
		return createUucNameMasterRepository.save(createUucNameMaster);
	}
	
	// Delete User Details for id
	@DeleteMapping("/crtUucNmMaster/{id}")
	public Map<String, Boolean> deleteCreatedUucNameMaster(@PathVariable(value = "id") Long createUucNameMasterId) throws ResourceNotFoundException {

		Map<String, Boolean> response = new HashMap<>();
		createUucNameMasterRepository.deleteById(createUucNameMasterId);
		response.put("deleted", Boolean.TRUE);
		logger.info("/crtUucNmMaster/{id} - Delete Create Uuc Name Master Detail of id - " + createUucNameMasterId);
			
		return response;
	}

}
