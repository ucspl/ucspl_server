package com.ucspl.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateCalibrationResultDataForMasterInstrument;
import com.ucspl.model.CreateEnvMaster;
import com.ucspl.model.SearchMasterByMasterName;
import com.ucspl.repository.CreateEnvMasterRepository;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class CreateEnvMasterController {

private static final Logger logger = LogManager.getLogger(CreateEnvMasterController.class);
	
	SearchMasterByMasterName searchMasterByMasterName =new SearchMasterByMasterName();
	
	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateEnvMasterRepository createEnvMasterRepository;
	
	@GetMapping("/crtEnvMaster")
	public List<CreateEnvMaster> getAllCreateEnvMaster() {

		logger.info("/crtEnvMaster - Get All Created Calibration Result Data For Master Instrument!");
		return createEnvMasterRepository.findAll();
	}

	@GetMapping("/crtEnvMaster/{id}")
	public ResponseEntity<CreateEnvMaster> getCreateEnvMasterById(
			@PathVariable(value = "id") Long createEnvMasterId) throws ResourceNotFoundException {
		CreateEnvMaster createEnvMaster = createEnvMasterRepository
				.findById(createEnvMasterId).orElseThrow(() -> new ResourceNotFoundException(
						" Environmental master not found for this id :: " + createEnvMasterId));

		logger.info("/crtEnvMaster/{id} - Get Calibration Result Data For Master Instrument By Id!" + createEnvMasterId);
		return ResponseEntity.ok().body(createEnvMaster);
	}

	@PutMapping("/crtEnvMaster/{id}")
	public ResponseEntity<CreateEnvMaster> updateEnvMasterById(
			@PathVariable(value = "id") Long createEnvMasterId,
			@Valid @RequestBody CreateEnvMaster createEnvMasterDetails) throws ResourceNotFoundException {
		CreateEnvMaster createEnvMaster = createEnvMasterRepository
				.findById(createEnvMasterId).orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + createEnvMasterId));

		createEnvMaster.setCalibrationLab(createEnvMasterDetails.getCalibrationLab());
	//	calibrationResultDataForMasterInstrument.setParameterId(calibrationResultDataForMasterInstrumentDetails.getParameterId());                                         
		createEnvMaster.setCalibratedAt(createEnvMasterDetails.getCalibratedAt());
		createEnvMaster.setValidFromDate(createEnvMasterDetails.getValidFromDate());
		createEnvMaster.setValidToDate(createEnvMasterDetails.getValidToDate());
		createEnvMaster.setTemperature1(createEnvMasterDetails.getTemperature1());
		createEnvMaster.setTemperature2(createEnvMasterDetails.getTemperature2());
		createEnvMaster.setHumidity1(createEnvMasterDetails.getHumidity1());
		createEnvMaster.setHumidity2(createEnvMasterDetails.getHumidity2());
		createEnvMaster.setAtmpress1(createEnvMasterDetails.getAtmpress1());                   //comment this part if error does not go                              
		createEnvMaster.setAtmpress2(createEnvMasterDetails.getAtmpress2());                   //partial running ...error while using                                             
		createEnvMaster.setDraft(createEnvMasterDetails.getDraft());    
		createEnvMaster.setApproved(createEnvMasterDetails.getApproved());                    
		createEnvMaster.setSubmitted(createEnvMasterDetails.getSubmitted());
		
		
		final CreateEnvMaster updatedEnvMasterDetail = createEnvMasterRepository
				.save(createEnvMaster);
		logger.info("/crtEnvMaster/{id} - Update Environmental master for id " + createEnvMasterId);

		return ResponseEntity.ok(updatedEnvMasterDetail);
	}
	
	
	@PutMapping("/crtEnvMToDate/{id}")
	public ResponseEntity<CreateEnvMaster> updateEnvMasterToDateById(
			@PathVariable(value = "id") Long createEnvMasterId,
			@Valid @RequestBody CreateEnvMaster createEnvMasterDetails) throws ResourceNotFoundException {
		CreateEnvMaster createEnvMaster = createEnvMasterRepository
				.findById(createEnvMasterId).orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + createEnvMasterId));

		createEnvMaster.setValidToDate(createEnvMasterDetails.getValidToDate());
	
		final CreateEnvMaster updatedEnvMasterDetail = createEnvMasterRepository
				.save(createEnvMaster);
		logger.info("/crtEnvMaster/{id} - Update Environmental master To Date for id " + createEnvMasterId);

		return ResponseEntity.ok(updatedEnvMasterDetail);
	}

	
	@PostMapping("/crtEnvMaster") 
	public CreateEnvMaster createEnvMasterFun(
			@Valid @RequestBody CreateEnvMaster createEnvMaster) {
			
		logger.info("/crtEnvMaster - Save the Environmental master!");	
		return createEnvMasterRepository.save(createEnvMaster);
	}
	
	// Delete User Details for id
	@DeleteMapping("/crtEnvMaster/{id}")
	public Map<String, Boolean> deleteEnvMaster(@PathVariable(value = "id") Long createEnvMasterId) throws ResourceNotFoundException {

		Map<String, Boolean> response = new HashMap<>();
		createEnvMasterRepository.deleteById(createEnvMasterId);
		response.put("deleted", Boolean.TRUE);
		logger.info("/crtEnvMaster/{id} - Delete Environmental master of id - " + createEnvMasterId);
			
		return response;
	}

	
	// Search and modify master instrument
	@RequestMapping(value = "/searchEnvMI", method = RequestMethod.POST, produces = "application/json")
	public List<Object[]> searchAndModifyEnvMasterInstrument(
			@Valid @RequestBody SearchMasterByMasterName searchMasterInstruObject) {

		logger.info("/searchEnvMI - Search environment master instrument For "
				+ searchMasterInstruObject.getSearchDocument());
		System.out.println(searchMasterInstruObject);

		List<Object[]> resultValue;

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("search_and_modify_env_master_instrument");
		procedureQuery.registerStoredProcedureParameter("search_document", String.class, ParameterMode.IN);
		procedureQuery.setParameter("search_document", (String) searchMasterInstruObject.getSearchDocument());

		System.out.println("object is " + searchMasterInstruObject.getSearchDocument());
		procedureQuery.execute();
		resultValue = procedureQuery.getResultList();
		logger.info("/searchEnvMI -" + " Get Result For Search Env master instrument scope- "
				+ searchMasterInstruObject.getSearchDocument());

		return resultValue;

	}

	// Search and modify master instrument
	@RequestMapping(value = "/lastCEnvMInstru", method = RequestMethod.POST, produces = "application/json")
	public List<Object[]> lastCreatedEnvMasterInstrument(
			@Valid @RequestBody SearchMasterByMasterName searchMasterInstruObject) {

		logger.info("/lastCEnvMInstru - get last created environment master instrument "
				+ searchMasterInstruObject.getSearchDocument());

		List<Object[]> resultValue;

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("get_last_created_env_master_instrument");
		procedureQuery.registerStoredProcedureParameter("calilab", String.class, ParameterMode.IN);
		procedureQuery.registerStoredProcedureParameter("caliat", String.class, ParameterMode.IN);
		
		procedureQuery.setParameter("calilab", (String) searchMasterInstruObject.getSearchDocument());
		procedureQuery.setParameter("caliat", (String) searchMasterInstruObject.getCalibratedAt());

		procedureQuery.execute();
		resultValue = procedureQuery.getResultList();

		return resultValue;

	}
		
	// Search and modify master instrument
	@RequestMapping(value = "/secLastCEnvMInstru", method = RequestMethod.POST, produces = "application/json")
	public List<Object[]> secdLastCreatedEnvMasterInstrument(
			@Valid @RequestBody SearchMasterByMasterName searchMasterInstruObject) {

		logger.info("/lastCEnvMInstru - get second last created environmental master instrument"
				+ searchMasterInstruObject.getSearchDocument());

		List<Object[]> resultValue;

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("get_second_last_created_env_master_instrument");
		procedureQuery.registerStoredProcedureParameter("calilab", String.class, ParameterMode.IN);
		procedureQuery.registerStoredProcedureParameter("caliat", String.class, ParameterMode.IN);
		procedureQuery.registerStoredProcedureParameter("id", long.class, ParameterMode.IN);

		procedureQuery.setParameter("calilab", (String) searchMasterInstruObject.getSearchDocument());
		procedureQuery.setParameter("caliat",  (String) searchMasterInstruObject.getCalibratedAt());
		procedureQuery.setParameter("id",      (long)   searchMasterInstruObject.getEnvMasterId());
 
		procedureQuery.execute();
		resultValue = procedureQuery.getResultList();

		return resultValue;

	}
		
}
