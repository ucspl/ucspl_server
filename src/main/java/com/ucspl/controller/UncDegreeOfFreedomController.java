
package com.ucspl.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.UncDegreeOfFreedom;
import com.ucspl.model.UncMasterType;
import com.ucspl.model.UomListForReqAndIwd;
import com.ucspl.services.CreateDefinitionService;



@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class UncDegreeOfFreedomController {

	private static final Logger logger = Logger.getLogger(UncDegreeOfFreedomController.class);

	@Autowired
	private CreateDefinitionService createDefinitionService;

	public UncDegreeOfFreedomController() {

	}

//get all uncMasType list	
	@GetMapping("/crtUncDeFredom")
	public List<UncDegreeOfFreedom> getAllUncDegreeOfFreedom() {
		logger.info("/crtUncDeFredom - Get all unc degree of table List!");

		List<UncDegreeOfFreedom> resultValue = createDefinitionService.getAllUncDegreeOfFreedom();

		return resultValue;
	}

//Save create uncMasType Details 
	@PostMapping("/crtUncDeFredom")
	public UncDegreeOfFreedom createUncDegreeOfFreedom(@Valid @RequestBody UncDegreeOfFreedom uncDegreeOfFreedom) {

		UncDegreeOfFreedom response = createDefinitionService.createUncDegreeOfFreedom(uncDegreeOfFreedom);
		return response;
	}

}
