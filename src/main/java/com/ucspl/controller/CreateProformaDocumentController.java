
package com.ucspl.controller;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateProformaDocument;
import com.ucspl.model.SearchChallanCumInvoiceDocument;
import com.ucspl.model.SearchProformaInvoice;
import com.ucspl.model.SearchSalesAndQuotation;
import com.ucspl.repository.CreateChallanCumInvoiceDocumentRepository;
import com.ucspl.services.CreateChallanCumInvoiceDocumentService;
import com.ucspl.services.CreateProformaDocumentService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class CreateProformaDocumentController {

	Object lastId;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	CreateProformaDocumentService createProformaDocumentServiceObject;

	public CreateProformaDocumentController() {
		// TODO Auto-generated constructor stub
	}

	
	//Get Challan Cum Invoice Document Header Information By Id
	@GetMapping("/createproforma")
	public List<CreateProformaDocument> getAllCreateProformaDocument() {

		List<CreateProformaDocument> response=createProformaDocumentServiceObject.getAllCreateProformaDocument();
		return response;
	}
	
    //Get Challan Cum Invoice Document Header Information By Id
	@GetMapping("/createproforma/{id}")
	public ResponseEntity<CreateProformaDocument> getCreateProformaDocumentById(
			@PathVariable(value = "id") Long createProformaDocumentId) throws ResourceNotFoundException {
		
		ResponseEntity<CreateProformaDocument> response=createProformaDocumentServiceObject. 
				getCreateProformaDocumentById(createProformaDocumentId);
		return response;	
	}

	//Save Header Details of Challan Cum Invoice Document
	@PostMapping("/createproforma")
	public CreateProformaDocument createProformaDocument(
			@Valid @RequestBody CreateProformaDocument createProformaDocumentObject) {

		CreateProformaDocument response=createProformaDocumentServiceObject.createProformaDocument(createProformaDocumentObject);
		return response;
	}

	/////////////////// Update Challan Cum Invoice Document Header Details for id  ////////////////////////
	@PutMapping("/createproforma/{id}")
	public ResponseEntity<CreateProformaDocument> updateCreateProformaDocumentById(
			@PathVariable(value = "id") Long createProformaDocumentId,
			@Valid @RequestBody CreateProformaDocument createProformaDocumentDetails)
					throws ResourceNotFoundException {
		
		ResponseEntity<CreateProformaDocument> response=createProformaDocumentServiceObject.
				updateCreateProformaDocumentById(createProformaDocumentId,createProformaDocumentDetails);
		return response;	
	}

   ////////////////////////////////// doc no for invoice  ///////////////////////////////////

	@GetMapping(path = { "/DocNoForProforma" })
	public String getDocumentNumber() throws IOException {

		String response=createProformaDocumentServiceObject.getDocumentNumber();
		return response;	
	}

	/////////////////////////////  code for search and modify proforma Invoice////////////////////
	@RequestMapping(value = "/searchProforma", method = RequestMethod.POST, produces = "application/json")
	public String searchAndModifyProformaDocument(
			@Valid @RequestBody SearchProformaInvoice searchProformaDocObject) {

		String response = createProformaDocumentServiceObject.searchAndModifyProformaDocument(searchProformaDocObject);
		return response;
	}

	/////////////////////// Result For Search Invoice Document ////////////////////////////////
	@GetMapping(value = "/searchProforma", produces = "application/json")
	public List<Object[]> getAllProformaDocument() {

		List<Object[]> resultValue= createProformaDocumentServiceObject.getAllProformaDocument();
		return resultValue;
	}

	////////////////////////////// STORE PROCEDURE FOR GETTING DETAILS OF CREATE Invoice  FOR SEARCH Invoice /////////////////////////////////////////////  
	@RequestMapping(value = "/CProformaInvsDet", method = RequestMethod.POST, produces = "application/json")
	public String docNoForGettingCreateProformaDocumentDetails(@Valid @RequestBody String docNo) {
		
		String response = createProformaDocumentServiceObject.docNoForGettingCreateProformaDocumentDetails(docNo);
		return response;
	}

	@GetMapping(value = "/CProformaInvsDet", produces = "application/json")
	public List<Object[]> gettingCreateProformaDocumentDetails() {
		
		List<Object[]> resultValue= createProformaDocumentServiceObject.gettingCreateProformaDocumentDetails();
		return resultValue;
	}
}
