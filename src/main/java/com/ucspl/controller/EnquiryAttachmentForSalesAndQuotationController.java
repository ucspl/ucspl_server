
package com.ucspl.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CustomerAttachments;
import com.ucspl.model.EnquiryAttachmentForSalesAndQuotation;
import com.ucspl.model.ImageModel;
import com.ucspl.repository.CustomerAttachmentsRepository;
import com.ucspl.services.CustomerAttachmentsService;
import com.ucspl.services.EnquiryAttachmentForSalesAndQuotationService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class EnquiryAttachmentForSalesAndQuotationController {

	
	EnquiryAttachmentForSalesAndQuotation imgData= new EnquiryAttachmentForSalesAndQuotation();
	

	@Autowired
	private EnquiryAttachmentForSalesAndQuotationService enquiryAttachmentForSalesAndQuotationService;
	

	public EnquiryAttachmentForSalesAndQuotationController() {
	
	}
	
	@PostMapping("/uploadEnqAttach")
	public ResponseEntity<EnquiryAttachmentForSalesAndQuotation> uplaodImage(@RequestParam("imageFile") MultipartFile file) throws IOException {

		ResponseEntity<EnquiryAttachmentForSalesAndQuotation>response = enquiryAttachmentForSalesAndQuotationService.uplaodImage(file);
		
		return response;
	}
	

	@PostMapping("/EnqAttach")
	public EnquiryAttachmentForSalesAndQuotation createEnquiryAttachment(
			@Valid @RequestBody EnquiryAttachmentForSalesAndQuotation enquiryAttachmentForSalesAndQuotation) {
		
		EnquiryAttachmentForSalesAndQuotation response=enquiryAttachmentForSalesAndQuotationService.createEnquiryAttachment(enquiryAttachmentForSalesAndQuotation);
		
		return response;

	}
		
}
	
	