package com.ucspl.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.DefinitionCategory;
import com.ucspl.repository.DefinitionCategoryRepository;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class DefinitionCategoryController {
	private static final Logger logger = Logger.getLogger(DefinitionCategoryController.class);

	@Autowired
	private DefinitionCategoryRepository definitionCategoryRepository;

	public DefinitionCategoryController() {

	}

	@GetMapping("/definitioncategory")
	public List<DefinitionCategory> getAllDEfinitionCategory() {
		logger.info("/definitioncategory - Get all Definition Category!");

		return definitionCategoryRepository.findAll();

	}

}
