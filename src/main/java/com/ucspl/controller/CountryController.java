
package com.ucspl.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.Country;
import com.ucspl.repository.CountryRepository;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class CountryController {

	private static final Logger logger = Logger.getLogger(CountryController.class);

	@Autowired
	private CountryRepository countryRepository;

	public CountryController() {
	
	}

	@GetMapping("/country")
	public List<Country> getAllCountries() {
		logger.info("/country - Get all Countries!");
		return countryRepository.findAll();
	}

}
