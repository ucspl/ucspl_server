

package com.ucspl.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.InwardTypeOfRequestAndInward;

import com.ucspl.repository.InwardTypeOfRequestAndInwardRepository;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class InwardTypeOfRequestAndInwardController {

	private static final Logger logger = Logger.getLogger(InwardTypeOfRequestAndInwardController.class); 
	
	@Autowired
	private InwardTypeOfRequestAndInwardRepository inwardTypeOfRequestAndInwardRepository;

	public InwardTypeOfRequestAndInwardController() {
		// TODO Auto-generated constructor stub
	}

	@GetMapping("/InwType")
	public List<InwardTypeOfRequestAndInward> getAllInwardTypes() {

		logger.info("/InwType - Get all Request And Inward Type!");
		return inwardTypeOfRequestAndInwardRepository.findAll();
	}

}
