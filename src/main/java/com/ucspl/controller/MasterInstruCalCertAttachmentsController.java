package com.ucspl.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.ImageModel;
import com.ucspl.model.MasterInstruCalCertAttachments;
import com.ucspl.model.SearchAttachmentByMastIdAndCalCertId;
import com.ucspl.services.MasterInstruCalCertAttachmentsService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class MasterInstruCalCertAttachmentsController {

	private static final Logger logger = Logger.getLogger(MasterInstruCalCertAttachmentsController.class);
	MasterInstruCalCertAttachments imgData = new MasterInstruCalCertAttachments();

	@Autowired
	private MasterInstruCalCertAttachmentsService masterInstruCalCertAttachmentsService;
	
	@PersistenceContext
	private EntityManager em;

	public MasterInstruCalCertAttachmentsController() {

	}

// Upload file 
	@PostMapping("/uploadMICalCertAttach")
	public ResponseEntity<MasterInstruCalCertAttachments> uplaodImage(@RequestParam("imageFile") MultipartFile file)
			throws IOException {

		ResponseEntity<MasterInstruCalCertAttachments> response = masterInstruCalCertAttachmentsService.uplaodImage(file);
		return response;
	}

//get file by mastid
	@GetMapping(path = { "/getMICalCertAttach/{mast_id}" })
	public List<MasterInstruCalCertAttachments> getImage(@PathVariable("mast_id") long mastId) throws IOException {

		List<MasterInstruCalCertAttachments> retrievedImage = masterInstruCalCertAttachmentsService.getImage(mastId);
		return retrievedImage;
	}

//get all files	
	@GetMapping("/mICalCertAttach")
	public List<MasterInstruCalCertAttachments> getAllMasterInstruCalCertAttachments() {

		List<MasterInstruCalCertAttachments> response = masterInstruCalCertAttachmentsService.getAllMasterInstruCalCertAttachments();
		return response;
	}

//get master atta by id
	@GetMapping("/mICalCertAttach/{id}")
	public ResponseEntity<MasterInstruCalCertAttachments> getMasterInstruCalCertAttachmentById(
			@PathVariable(value = "id") Long masterInstruCalCertAttachmentsId) throws ResourceNotFoundException {

		ResponseEntity<MasterInstruCalCertAttachments> response = masterInstruCalCertAttachmentsService
				.getMasterInstruCalCertAttachmentById(masterInstruCalCertAttachmentsId);
		return response;
	}

//store master attachment
	@PostMapping("/mICalCertAttach")
	public MasterInstruCalCertAttachments createMasterInstruCalCertAttachment(@Valid @RequestBody MasterInstruCalCertAttachments masterInstruCalCertAttachments) {

		MasterInstruCalCertAttachments response = masterInstruCalCertAttachmentsService.createMasterInstruCalCertAttachment(masterInstruCalCertAttachments);
		return response;

	}

//get file in byte format
	@PostMapping("/getSelMICalCertAttach")
	public ImageModel getSelectedMasterInstruCalCertFileInByteFormat(@Valid @RequestBody MasterInstruCalCertAttachments attachFile)
			throws IOException {

		ImageModel img = masterInstruCalCertAttachmentsService.getSelectedMasterInstruCalCertFileInByteFormat(attachFile);
		return img;
	}

// Delete User Details for id
	@DeleteMapping("/delSelMICalCertAttach/{id}")
	public Map<String, Boolean> deleteMasterInstrumentSpecificationDetails(@PathVariable(value = "id") Long masterInstrumentDetailId) throws ResourceNotFoundException {

		Map<String, Boolean> response= masterInstruCalCertAttachmentsService.deleteMasterInstrumentSpecificationDetails(masterInstrumentDetailId);		
		return response;
	}
	
// get attachment by master id and cal id	
	@PostMapping("/mICalCAttach")
	public List<Object[]> getMasterInstruCalCertAttachmentByMastIdAndCalId(@Valid @RequestBody SearchAttachmentByMastIdAndCalCertId masterInstruCalCert) {

		List<Object[]> resultValue = masterInstruCalCertAttachmentsService.getMasterInstruCalCertAttachmentByMastIdAndCalId(masterInstruCalCert);
		return resultValue;
	}

// delete attachment by cal id
	@PostMapping("/delMICalCAttByCalId")
	public String deleteMasterInstruCalCertAttachmentByCalId(@Valid @RequestBody long calId) {
		
		String resultValue=masterInstruCalCertAttachmentsService.deleteMasterInstruCalCertAttachmentByCalId(calId);
		return resultValue;
	}
	
}
