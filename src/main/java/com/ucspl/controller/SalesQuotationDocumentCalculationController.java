package com.ucspl.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.SalesQuotationDocumentCalculation;
import com.ucspl.repository.SalesQuotationDocumentCalculationRepository;
import com.ucspl.services.CreateSalesQuotationService;
import com.ucspl.services.SalesQuotationDocumentCalculationService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class SalesQuotationDocumentCalculationController {

	@Autowired
	private SalesQuotationDocumentCalculationService salesQuotationDocumentCalculationService;

	@PersistenceContext
	private EntityManager em;

	public SalesQuotationDocumentCalculationController() {
		// TODO Auto-generated constructor stub
	}

	// get All Create Sales Quotation Document
	@GetMapping("/SalesQuotDocCal")
	public List<SalesQuotationDocumentCalculation> getAllCreateSalesQuotationDocument() {

		List<SalesQuotationDocumentCalculation> allCreateSalesQuotationDocument = salesQuotationDocumentCalculationService
				.getAllCreateSalesQuotationDocument();

		return allCreateSalesQuotationDocument;

	}

	// Get Sales Quotation Document Calculations By Id
	@GetMapping("/SalesQuotDocCal/{id}")
	public ResponseEntity<SalesQuotationDocumentCalculation> getSalesQuotationDocumentCalculationById(
			@PathVariable(value = "id") Long createSalesQuotationDocumentCalculationId)
			throws ResourceNotFoundException {

		ResponseEntity<SalesQuotationDocumentCalculation> response = salesQuotationDocumentCalculationService
				.getSalesQuotationDocumentCalculationById(createSalesQuotationDocumentCalculationId);
		return response;

	}

	// Save Calculation of Sales Quotation Document
	@PostMapping("/SalesQuotDocCal")
	public SalesQuotationDocumentCalculation createSalesQuotationDocumentCalculation(
			@Valid @RequestBody SalesQuotationDocumentCalculation salesQuotationDocumentCalculation) {

		SalesQuotationDocumentCalculation response = salesQuotationDocumentCalculationService
				.createSalesQuotationDocumentCalculation(salesQuotationDocumentCalculation);
		return response;

	}

	// Update Sales Quotation Document calculation for id
	@PutMapping("/SalesQuotDocCal/{id}")
	public ResponseEntity<SalesQuotationDocumentCalculation> updateSalesQuotationDocumentCalculationById(
			@PathVariable(value = "id") Long createSalesQuotationDocumentCalculationId,
			@Valid @RequestBody SalesQuotationDocumentCalculation salesQuotationDocumentCalculationDetails)
			throws ResourceNotFoundException {

		ResponseEntity<SalesQuotationDocumentCalculation> response = salesQuotationDocumentCalculationService
				.updateSalesQuotationDocumentCalculationById(createSalesQuotationDocumentCalculationId,
						salesQuotationDocumentCalculationDetails);
		return response;

	}

	// store procedure for doc calculation table detail

	@RequestMapping(value = "/SQDocCalDet", method = RequestMethod.POST, produces = "application/json")
	public String DocNoForGettingDocDetails(@Valid @RequestBody String docNo) {

		String response = salesQuotationDocumentCalculationService.DocNoForGettingDocDetails(docNo);
		return response;

	}

	// Get details of Sales Quotation Document calculation for Document No
	@SuppressWarnings("unchecked")
	@GetMapping(value = "/SQDocCalDet", produces = "application/json")
	public List<Object[]> getDocDetailsForDocNo() {

		List<Object[]> resultValue = salesQuotationDocumentCalculationService.getDocDetailsForDocNo();
		return resultValue;

	}

}
