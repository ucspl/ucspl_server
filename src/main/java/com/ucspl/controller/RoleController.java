package com.ucspl.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.Branch;
import com.ucspl.model.Role;
import com.ucspl.repository.RoleRepository;
import com.ucspl.services.CreateDefinitionService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class RoleController {

	private static final Logger logger = Logger.getLogger(BranchController.class);

	@Autowired
	private CreateDefinitionService createDefinitionServiceObject;
	@Autowired
	private RoleRepository roleRepository;

	public RoleController() {
		// TODO Auto-generated constructor stub
	}

	// get role details
	@GetMapping("/role")
	public List<Role> getAllRoles() {

		logger.info("/role - Get all roles!");
		return roleRepository.findAll();
	}

	// Save roleDetails
	@PostMapping("/createrole")
	public Role createRole(@Valid @RequestBody Role roleObject) {

		Role response = createDefinitionServiceObject.createRole(roleObject);
		return response;
	}

}
