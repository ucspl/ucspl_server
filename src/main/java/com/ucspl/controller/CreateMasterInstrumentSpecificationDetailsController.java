package com.ucspl.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateMasterInstrumentSpecificationDetails;
import com.ucspl.model.SearchMasterByMasterName;
import com.ucspl.model.SearchMasterInstruByMIdPNmAndStatus;
import com.ucspl.services.CreateMasterInstrumentSpecificationDetailsService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class CreateMasterInstrumentSpecificationDetailsController {

	SearchMasterByMasterName searchMasterByMasterName = new SearchMasterByMasterName();

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateMasterInstrumentSpecificationDetailsService createMasterInstrumentSpecificationDetailsService;

	public CreateMasterInstrumentSpecificationDetailsController() {

	}

//  Get all master specification details
	@GetMapping("/crtMSpeD")
	public List<CreateMasterInstrumentSpecificationDetails> getAllCreatedMasterSpecificationDetails() {

		List<CreateMasterInstrumentSpecificationDetails> response = createMasterInstrumentSpecificationDetailsService
				.getAllCreatedMasterSpecificationDetails();
		return response;
	}

// Get  master specification details by id 
	@GetMapping("/crtMSpeD/{id}")
	public ResponseEntity<CreateMasterInstrumentSpecificationDetails> getMasterSpecificationDetailsById(
			@PathVariable(value = "id") Long createMasterInstrumentSpecificationDetailsId)
			throws ResourceNotFoundException {

		ResponseEntity<CreateMasterInstrumentSpecificationDetails> response = createMasterInstrumentSpecificationDetailsService
				.getMasterSpecificationDetailsById(createMasterInstrumentSpecificationDetailsId);
		return response;
	}

// Update master specification details by id
	@PutMapping("/crtMSpeD/{id}")
	public ResponseEntity<CreateMasterInstrumentSpecificationDetails> updateMasterInstrumentSpecificationDetailsById(
			@PathVariable(value = "id") Long createMasterInstrumentSpecificationDetailId,
			@Valid @RequestBody CreateMasterInstrumentSpecificationDetails createMasterInstrumentSpecificationDetails)
			throws ResourceNotFoundException {

		ResponseEntity<CreateMasterInstrumentSpecificationDetails> response = createMasterInstrumentSpecificationDetailsService
				.updateMasterInstrumentSpecificationDetailsById(createMasterInstrumentSpecificationDetailId,
						createMasterInstrumentSpecificationDetails);
		return response;
	}
	
	
// Update master specification details by id    crtMSpeDFrParaNm
	@PutMapping("/crtMSpeDFrAppro/{id}")
	public ResponseEntity<CreateMasterInstrumentSpecificationDetails> updateMasterInstrumentSpecificationDetailsByIdForApprove(
			@PathVariable(value = "id") Long createMasterInstrumentSpecificationDetailId,
			@Valid @RequestBody CreateMasterInstrumentSpecificationDetails createMasterInstrumentSpecificationDetails)
			throws ResourceNotFoundException {

		ResponseEntity<CreateMasterInstrumentSpecificationDetails> response = createMasterInstrumentSpecificationDetailsService
				.updateMasterInstrumentSpecificationDetailsByIdForApprove(createMasterInstrumentSpecificationDetailId,
						createMasterInstrumentSpecificationDetails);
		return response;
	}
	
	
// Update master specification details by id 
	@PutMapping("/crtMSpeDFrParaNo/{id}")
	public ResponseEntity<CreateMasterInstrumentSpecificationDetails> updateMasterInstrumentSpecificationDetailsByIdForParaNumber(
			@PathVariable(value = "id") Long createMasterInstrumentSpecificationDetailId,
			@Valid @RequestBody CreateMasterInstrumentSpecificationDetails createMasterInstrumentSpecificationDetails)
			throws ResourceNotFoundException {

		ResponseEntity<CreateMasterInstrumentSpecificationDetails> response = createMasterInstrumentSpecificationDetailsService
				.updateMasterInstrumentSpecificationDetailsByIdForParaNumber(createMasterInstrumentSpecificationDetailId,
						createMasterInstrumentSpecificationDetails);
		return response;
	}
	
	

// Create master specification details
	@PostMapping("/crtMSpeD")
	public CreateMasterInstrumentSpecificationDetails createMasterInstrumentSpecificationDetail(
			@Valid @RequestBody CreateMasterInstrumentSpecificationDetails createMasterInstrumentSpecification) {

		CreateMasterInstrumentSpecificationDetails response = createMasterInstrumentSpecificationDetailsService
				.createMasterInstrumentSpecificationDetail(createMasterInstrumentSpecification);
		return response;
	}

// Delete User Details for id
	@DeleteMapping("/crtMSpeD/{id}")
	public Map<String, Boolean> deleteMasterInstrumentSpecificationDetails(
			@PathVariable(value = "id") Long masterInstrumentDetailId) throws ResourceNotFoundException {

		Map<String, Boolean> response = new HashMap<>();
		response = createMasterInstrumentSpecificationDetailsService
				.deleteMasterInstrumentSpecificationDetails(masterInstrumentDetailId);
		return response;
	}

//////////////////////////// sp for getting sr no for master instruments ////////////////////////////////  

	@RequestMapping(value = "/srnoMSDet", method = RequestMethod.POST, produces = "application/json")
	public List<Object[]> srNoDetailsOfMasterSpecification(@Valid @RequestBody long masterId) {

		List<Object[]> resultValue = createMasterInstrumentSpecificationDetailsService
				.srNoDetailsOfMasterSpecification(masterId);
		return resultValue;
	}

//////////////////////////// sp for getting sr no for master instruments by masteridparaname ////////////////////////////////  

	@RequestMapping(value = "/srnoMSDetByMipn", method = RequestMethod.POST, produces = "application/json")
	public List<Object[]> srNoDetailsOfMasterSpecificationByMasterIdParaName(
			@Valid @RequestBody SearchMasterInstruByMIdPNmAndStatus masterIdParaNameObj) {

		List<Object[]> resultValue = createMasterInstrumentSpecificationDetailsService
				.srNoDetailsOfMasterSpecificationByMasterIdParaName(masterIdParaNameObj);
		return resultValue;
	}

}

