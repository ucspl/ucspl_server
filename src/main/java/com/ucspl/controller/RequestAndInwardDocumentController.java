
package com.ucspl.controller;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CustomerInstrumentIdentification;
import com.ucspl.model.ReqDocnoAndCustInstruRelationship;
import com.ucspl.model.RequestAndInwardDocument;
import com.ucspl.model.RequestAndInwardDocument;
import com.ucspl.repository.CustomerInstrumentIdentificationRepository;
import com.ucspl.repository.ReqDocnoAndCustInstruRelationshipRepository;
import com.ucspl.repository.RequestAndInwardDocumentRepository;
import com.ucspl.services.RequestAndInwardDocumentService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class RequestAndInwardDocumentController {

	private static final Logger logger = LogManager.getLogger(RequestAndInwardDocumentController.class);

	private String docNoVariable;
	private long docNoVariable1;

	private CustomerInstrumentIdentification customerInstrumentIdentification;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private RequestAndInwardDocumentRepository requestAndInwardDocumentRepository;

	@Autowired
	private CustomerInstrumentIdentificationRepository customerInstrumentIdentificationRepository;

	private ReqDocnoAndCustInstruRelationship reqDocnoAndCustInstruRelationship;

	@Autowired
	private ReqDocnoAndCustInstruRelationshipRepository reqDocnoAndCustInstruRelationshipRepository;

	@Autowired
	private RequestAndInwardDocumentService requestAndInwardDocumentService;

	public RequestAndInwardDocumentController() {
		// TODO Auto-generated constructor stub
	}

//get all req and inward documents
	@GetMapping("/reqIwdDoc")
	public List<RequestAndInwardDocument> getAllRequestAndInwardDocument() {

		List<RequestAndInwardDocument> response = requestAndInwardDocumentService.getAllRequestAndInwardDocument();
		return response;
	}

//get all req and inward documents by id	
	@GetMapping("/reqIwdDoc/{id}")
	public ResponseEntity<RequestAndInwardDocument> getRequestAndInwardDocumentById(
			@PathVariable(value = "id") Long requestAndInwardDocumentId) throws ResourceNotFoundException {

		ResponseEntity<RequestAndInwardDocument> response = requestAndInwardDocumentService
				.getRequestAndInwardDocumentById(requestAndInwardDocumentId);
		return response;
	}

//create req and inward documents
	@PostMapping("/reqIwdDoc")
	public RequestAndInwardDocument createRequestAndInwardDocument(
			@Valid @RequestBody RequestAndInwardDocument requestAndInwardDocument) {

		RequestAndInwardDocument response = requestAndInwardDocumentService.createRequestAndInwardDocument(requestAndInwardDocument);

		return response;
	}

//update req and inward documents
	@PutMapping("/reqIwdDoc/{id}")
	public ResponseEntity<RequestAndInwardDocument> updateRequestAndInwardDocument(
			@PathVariable(value = "id") Long requestAndInwardDocumentId,
			@Valid @RequestBody RequestAndInwardDocument requestAndInwardDocumentDetails)
			throws ResourceNotFoundException {
		
		ResponseEntity<RequestAndInwardDocument> response = requestAndInwardDocumentService.updateRequestAndInwardDocument(requestAndInwardDocumentId,requestAndInwardDocumentDetails);
		return response;
	}

//create req and inward documentsfor create certificate
	@PutMapping("/uReqIwdDocCer/{id}")
	public ResponseEntity<RequestAndInwardDocument> updateReqInwardDocumentForCreateCertificate(
			@PathVariable(value = "id") Long requestAndInwardDocumentId,
			@Valid @RequestBody RequestAndInwardDocument requestAndInwardDocumentDetails)
			throws ResourceNotFoundException {
		
		ResponseEntity<RequestAndInwardDocument> response = requestAndInwardDocumentService.updateReqInwardDocumentForCreateCertificate(requestAndInwardDocumentId, requestAndInwardDocumentDetails);
		return response;
	}

//create req and inward documents for calibration tag
	@PutMapping("/reqIwdDocForCaliTag/{id}")
	public ResponseEntity<RequestAndInwardDocument> updateRequestAndInwardDocumentForCalibrationTag(
			@PathVariable(value = "id") Long requestAndInwardDocumentId,
			@Valid @RequestBody RequestAndInwardDocument requestAndInwardDocumentDetails)
			throws ResourceNotFoundException {
	
		ResponseEntity<RequestAndInwardDocument> response = requestAndInwardDocumentService.updateRequestAndInwardDocumentForCalibrationTag(requestAndInwardDocumentId, requestAndInwardDocumentDetails);
		return response;
	}

////////////////////////////////////Store Procedure for getting Sr no//////////////////////////////////

	@RequestMapping(value = "/srnoRIDoc", method = RequestMethod.POST, produces = "application/json")
	public String docNoForGettingSrNo(@Valid @RequestBody long docNo) {

		String response = requestAndInwardDocumentService.docNoForGettingSrNo(docNo);
		return response;
	}

	@GetMapping(value = "/srnoRIDoc", produces = "application/json")
	public List<Object[]> getSrNoForDocNoReq() {

		List<Object[]> resultValue = requestAndInwardDocumentService.getSrNoForDocNoReq() ;		
		return resultValue;

	}

	@RequestMapping(value = "/srnoIdentiRIDoc", method = RequestMethod.POST, produces = "application/json")
	public String objForGettingUniqueSrNo(@Valid @RequestBody CustomerInstrumentIdentification customerInstrumentIden) {

		String response = requestAndInwardDocumentService.objForGettingUniqueSrNo(customerInstrumentIden);
	    return response;
	}

	@GetMapping(value = "/srnoIdentiRIDoc", produces = "application/json")
	public List<Object[]> getIdentySrNoForDocNoReq() {

		List<Object[]> resultValue=requestAndInwardDocumentService.getIdentySrNoForDocNoReq();		
		return resultValue;

	}

	@GetMapping(value = "/srnonmRIDoc", produces = "application/json")
	public List<Object[]> getSrNoForDocNoWithNamesReq() {

		List<Object[]> resultValue = requestAndInwardDocumentService.getSrNoForDocNoWithNamesReq();		
		return resultValue;

	}

	@GetMapping(value = "/srnmRIDocInstrNo", produces = "application/json")
	public List<Object[]> getSrNoForDocNoWithNamesReqByInstruNo() {

		List<Object[]> resultValue= requestAndInwardDocumentService.getSrNoForDocNoWithNamesReqByInstruNo();
		return resultValue;
	}

//////////////////////////////////  store procedure to get cust instru id through reqDocNo ////////////////////////

	@PostMapping("/ciIdForReqNo")
	public List<Object[]> getCustInstruIdForReqDocNo(@Valid @RequestBody String number) throws IOException {

		List<Object[]> resultValue = requestAndInwardDocumentService.getCustInstruIdForReqDocNo(number) ;
		return resultValue;
	}

///////////////////////////////////////////// Multi para list //////////////////////////////////	

	@PostMapping("/mulParaSN")
	public List<Object[]> getDocumentNumber(@Valid @RequestBody String number) throws IOException {

		List<Object[]> resultValue= requestAndInwardDocumentService.getDocumentNumber(number);		
		return resultValue;
	}

}