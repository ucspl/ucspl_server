package com.ucspl.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateMasterInstrumentDetails;
import com.ucspl.model.SearchMasterByMasterName;
import com.ucspl.services.CreateMasterInstrumentDetailsService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class CreateMasterInstrumentDetailsController {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateMasterInstrumentDetailsService createMasterInstrumentDetailsService;

	public CreateMasterInstrumentDetailsController() {

	}

// Get all master instruments details                                                                                     	
	@GetMapping("/crtMInstruDet")
	public List<CreateMasterInstrumentDetails> getAllCreateMasterInstrumentDetails() {

		List<CreateMasterInstrumentDetails> response = createMasterInstrumentDetailsService
				.getAllCreateMasterInstrumentDetails();
		return response;
	}

// Get all master instruments details by id                                                                                                   
	@GetMapping("/crtMInstruDet/{id}")
	public ResponseEntity<CreateMasterInstrumentDetails> getCreateMasterInstrumentDetailsById(
			@PathVariable(value = "id") Long createMasterInstrumentDetailsId) throws ResourceNotFoundException {

		ResponseEntity<CreateMasterInstrumentDetails> response = createMasterInstrumentDetailsService
				.getCreateMasterInstrumentDetailsById(createMasterInstrumentDetailsId);
		return response;
	}

// Update master instruments details by id
	@PutMapping("/crtMInstruDet/{id}")
	public ResponseEntity<CreateMasterInstrumentDetails> updateMasterInstrumentDetailsById(
			@PathVariable(value = "id") Long createMasterInstrumentDetailsId,
			@Valid @RequestBody CreateMasterInstrumentDetails createMasterInstrumentDetails)
			throws ResourceNotFoundException {

		ResponseEntity<CreateMasterInstrumentDetails> response = createMasterInstrumentDetailsService
				.updateMasterInstrumentDetailsById(createMasterInstrumentDetailsId, createMasterInstrumentDetails);
		return response;
	}

// Create master instruments details  
	@PostMapping("/crtMInstruDet")
	public CreateMasterInstrumentDetails createMasterInstrumentDetail(
			@Valid @RequestBody CreateMasterInstrumentDetails createMasterInstrumentDetails) {

		CreateMasterInstrumentDetails response = createMasterInstrumentDetailsService
				.createMasterInstrumentDetail(createMasterInstrumentDetails);
		return response;
	}

// Search and modify master instrument
	@RequestMapping(value = "/searchMI", method = RequestMethod.POST, produces = "application/json")
	public List<Object[]> searchAndModifyMasterInstrument(
			@Valid @RequestBody SearchMasterByMasterName searchMasterInstruObject) {

		List<Object[]> resultValue = createMasterInstrumentDetailsService
				.searchAndModifyMasterInstrument(searchMasterInstruObject);
		return resultValue;
	}

}