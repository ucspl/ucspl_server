
package com.ucspl.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.Branch;
import com.ucspl.model.TaxpayerTypeForCustomer;
import com.ucspl.repository.TaxpayerTypeForCustomerRepository;
import com.ucspl.services.CreateDefinitionService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class TaxpayerTypeForCustomerController {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateDefinitionService createDefinitionServiceObject;

	public TaxpayerTypeForCustomerController() {

	}

	// get taxpayer type list
	@GetMapping("/taxptype")
	public List<TaxpayerTypeForCustomer> getAllTaxpayerType() {

		List<TaxpayerTypeForCustomer> response = createDefinitionServiceObject.getAllTaxpayerType();
		return response;

	}

	// Save create Taxpayer Type
	@PostMapping("/createTaxpayerType")
	public TaxpayerTypeForCustomer createTaxpayerTypeForCustomer(
			@Valid @RequestBody TaxpayerTypeForCustomer taxpayerTypeForCustomerObject) {

		TaxpayerTypeForCustomer response = createDefinitionServiceObject
				.createTaxpayerTypeForCustomer(taxpayerTypeForCustomerObject);
		return response;
	}

}
