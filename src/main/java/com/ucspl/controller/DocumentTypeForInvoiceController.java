package com.ucspl.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.Branch;
import com.ucspl.model.DocumentTypeForInvoice;
import com.ucspl.services.CreateDefinitionService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class DocumentTypeForInvoiceController {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateDefinitionService createDefinitionServiceObject;

	public DocumentTypeForInvoiceController() {

	}

	// get document type list for invoice
	@GetMapping("/DocTypeOfInv")
	public List<DocumentTypeForInvoice> getAllDocumentTypes() {

		List<DocumentTypeForInvoice> response = createDefinitionServiceObject.getAllDocumentTypes();
		return response;

	}

}
