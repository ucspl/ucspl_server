package com.ucspl.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.Branch;
import com.ucspl.model.SecurityQuestion;
import com.ucspl.repository.SecurityQuestionRepository;
import com.ucspl.services.CreateDefinitionService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class SecurityQuestionController {
	private static final Logger logger = Logger.getLogger(SecurityQuestionController.class);

	@Autowired
	private SecurityQuestionRepository securityQuestionRepository;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateDefinitionService createDefinitionServiceObject;

	public SecurityQuestionController() {

	}

	// get security question list
	@GetMapping("/securityQuestion")
	public List<SecurityQuestion> SecurityQuestion() {

		List<SecurityQuestion> response = createDefinitionServiceObject.SecurityQuestion();
		return response;

	}

	// Save Security Question Details
	@PostMapping("/createSecurityQuestion")
	public SecurityQuestion createSecurityQuestion(@Valid @RequestBody SecurityQuestion securityQuestionObject) {

		SecurityQuestion response = createDefinitionServiceObject.createSecurityQuestion(securityQuestionObject);
		return response;
	}
}
