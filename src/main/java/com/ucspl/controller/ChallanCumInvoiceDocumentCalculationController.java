package com.ucspl.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.ChallanCumInvoiceDocumentCalculation;
import com.ucspl.model.SalesQuotationDocumentCalculation;
import com.ucspl.repository.ChallanCumInvoiceDocumentCalculationRepository;
import com.ucspl.services.ChallanCumInvoiceDocumentCalculationService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class ChallanCumInvoiceDocumentCalculationController {

	@Autowired
	ChallanCumInvoiceDocumentCalculationService challanCumInvoiceDocumentCalculationServiceObject;
	private String docNoVariable;

	@PersistenceContext
	private EntityManager em;

	public ChallanCumInvoiceDocumentCalculationController() {

	}

	// Get All Challan Cum Invoice Document Calculations
	@GetMapping("/invDocCal")
	public List<ChallanCumInvoiceDocumentCalculation> getChallanCumInvoiceDocumentCalculation() {

		List<ChallanCumInvoiceDocumentCalculation> allCreateInvoiceDocument = challanCumInvoiceDocumentCalculationServiceObject
				.getChallanCumInvoiceDocumentCalculation();

		return allCreateInvoiceDocument;

	}

	// Get All Challan Cum Invoice Document by id
	@GetMapping("/invDocCal/{id}")
	public ResponseEntity<ChallanCumInvoiceDocumentCalculation> getChallanCumInvoiceDocumentCalculationById(
			@PathVariable(value = "id") Long challanCumInvoiceDocumentCalculationId) throws ResourceNotFoundException {

		ResponseEntity<ChallanCumInvoiceDocumentCalculation> response = challanCumInvoiceDocumentCalculationServiceObject
				.getChallanCumInvoiceDocumentCalculationById(challanCumInvoiceDocumentCalculationId);

		return response;
	}

	// Save Calculation of Challan Cum Invoice Document
	@PostMapping("/invDocCal")
	public ChallanCumInvoiceDocumentCalculation createChallanCumInvoiceDocumentCalculation(
			@Valid @RequestBody ChallanCumInvoiceDocumentCalculation challanCumInvoiceDocumentCalculation) {

		ChallanCumInvoiceDocumentCalculation response = challanCumInvoiceDocumentCalculationServiceObject
				.createChallanCumInvoiceDocumentCalculation(challanCumInvoiceDocumentCalculation);
		return response;
	}

	// Update Challan Cum Invoice Document Calculation for id
	@PutMapping("/invDocCal/{id}")
	public ResponseEntity<ChallanCumInvoiceDocumentCalculation> updateChallanCumInvoiceDocumentCalculationById(
			@PathVariable(value = "id") Long challanCumInvoiceDocumentCalculationId,
			@Valid @RequestBody ChallanCumInvoiceDocumentCalculation challanCumInvoiceDocumentCalculationDetails)
			throws ResourceNotFoundException {

		ResponseEntity<ChallanCumInvoiceDocumentCalculation> response = challanCumInvoiceDocumentCalculationServiceObject
				.updateChallanCumInvoiceDocumentCalculationById(challanCumInvoiceDocumentCalculationId,
						challanCumInvoiceDocumentCalculationDetails);

		return response;
	}

///////////////////////// store procedure for doc calculation table////////////////////////////////////////////////////////////////////

	@RequestMapping(value = "/IncDocCal", method = RequestMethod.POST, produces = "application/json")
	public String docNoForGettingDocDetails(@Valid @RequestBody String docNo) {
		docNoVariable = docNo;
		String response = challanCumInvoiceDocumentCalculationServiceObject.docNoForGettingDocDetails(docNo);
		return response;
	}

	// Get details of Sales Quotation Document calculation for Document Number
	@GetMapping(value = "/IncDocCal", produces = "application/json")
	public List<Object[]> getDocDetailsForDocNo() {

		List<Object[]> resultValue = challanCumInvoiceDocumentCalculationServiceObject.getDocDetailsForDocNo();
		return resultValue;
		
	}

}
