
package com.ucspl.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.ProformaInvoiceCalculation;
import com.ucspl.services.ProformaInvoiceCalculationService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class ProformaInvoiceCalculationController {

	@Autowired
	ProformaInvoiceCalculationService proformaInvoiceCalculationServiceObject;

	@PersistenceContext
	private EntityManager em;

	public ProformaInvoiceCalculationController() {

	}

	// Get All Proforma Document Calculations
	@GetMapping("/proformaDocCal")
	public List<ProformaInvoiceCalculation> getProformsDocumentCalculation() {

		List<ProformaInvoiceCalculation> allCreateProformaDocument = proformaInvoiceCalculationServiceObject
				.getProformsDocumentCalculation();
		return allCreateProformaDocument;
	}

	// Get All proforma Document by id
	@GetMapping("/proformaDocCal/{id}")
	public ResponseEntity<ProformaInvoiceCalculation> getProformaDocumentCalculationById(
			@PathVariable(value = "id") Long proformaDocumentCalculationId) throws ResourceNotFoundException {

		ResponseEntity<ProformaInvoiceCalculation> response = proformaInvoiceCalculationServiceObject
				.getProformaDocumentCalculationById(proformaDocumentCalculationId);

		return response;
	}

	// Save Calculation of proforma Document 
	@PostMapping("/proformaDocCal")
	public ProformaInvoiceCalculation createProformaDocumentCalculation(
			@Valid @RequestBody ProformaInvoiceCalculation proformaDocumentCalculation) {

		ProformaInvoiceCalculation response = proformaInvoiceCalculationServiceObject
				.createProformaDocumentCalculation(proformaDocumentCalculation);

		return response;
	}

	// Update Challan Cum Invoice Document Calculation for id
	@PutMapping("/proformaDocCal/{id}")
	public ResponseEntity<ProformaInvoiceCalculation> updateProformaDocumentCalculationById(
			@PathVariable(value = "id") Long proformaDocumentCalculationId,
			@Valid @RequestBody ProformaInvoiceCalculation proformaDocumentCalculationDetails)
			throws ResourceNotFoundException {

		ResponseEntity<ProformaInvoiceCalculation> response = proformaInvoiceCalculationServiceObject
				.updateProformaDocumentCalculationById(proformaDocumentCalculationId,
						proformaDocumentCalculationDetails);

		return response;
	}

//////////////////////////////// store procedure for doc calculation table//////////////////////////////////////

	@RequestMapping(value = "/proDocCal", method = RequestMethod.POST, produces = "application/json")
	public String docNoForGettingDocDetails(@Valid @RequestBody String docNo) {
		
		String response = proformaInvoiceCalculationServiceObject.docNoForGettingDocDetails(docNo);
		return response;
	}

	// Get details of Sales Quotation Document calculation for Document No
	@GetMapping(value = "/proDocCal", produces = "application/json")
	public List<Object[]> getDocDetailsForDocNo() {

		List<Object[]> resultValue = proformaInvoiceCalculationServiceObject.getDocDetailsForDocNo();
		return resultValue;
	}

}
