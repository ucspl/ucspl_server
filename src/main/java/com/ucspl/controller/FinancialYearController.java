
package com.ucspl.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.Branch;
import com.ucspl.model.CreateDefinition;
import com.ucspl.model.Department;
import com.ucspl.model.FinancialYear;
import com.ucspl.model.User;
import com.ucspl.repository.BranchRepository;
import com.ucspl.services.CreateDefinitionService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class FinancialYearController {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateDefinitionService createDefinitionServiceObject;

	public FinancialYearController() {

	}

// get financial year list
	@GetMapping("/financialYear")
	public List<FinancialYear> getAllFinancialYears() {

		List<FinancialYear> response = createDefinitionServiceObject.getAllFinancialYears();
		return response;
	}

// Save create financial year
	@PostMapping("/createFinancialYear")
	public FinancialYear createFinancialYear(@Valid @RequestBody FinancialYear financialYearObject) {

		FinancialYear response = createDefinitionServiceObject.createFinancialYear(financialYearObject);
		return response;
	}

// Update Financial Year for id
	@PutMapping("/createFinancialYear/{id}")
	public ResponseEntity<FinancialYear> updateFinancialYear(@PathVariable(value = "id") Long financialYearId,
			@Valid @RequestBody FinancialYear financialYearDetails) throws ResourceNotFoundException {

		ResponseEntity<FinancialYear> response = createDefinitionServiceObject.updateFinancialYear(financialYearId,
				financialYearDetails);
		return response;
	}

}
