
package com.ucspl.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.FormulaAccuracyReqAndIwd;
import com.ucspl.services.CreateDefinitionService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class FormulaAccuracyReqAndIwdController {

	private static final Logger logger = Logger.getLogger(FormulaAccuracyReqAndIwdController.class);

	@Autowired
	private CreateDefinitionService createDefinitionService;

	public FormulaAccuracyReqAndIwdController() {

	}

	@GetMapping("/formulaAccuList")
	public List<FormulaAccuracyReqAndIwd> getAllFormulaAccuracy() {
		logger.info("/formulaAccuList - Get all Formula Accuracy List!");

		List<FormulaAccuracyReqAndIwd> resultValue = createDefinitionService.getAllFormulaAccuracy();
		return resultValue;
	}

}
