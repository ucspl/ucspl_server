package com.ucspl.controller;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.ImageModel;
import com.ucspl.model.SupplierAttachments;
import com.ucspl.services.SupplierAttachmentsService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class SupplierAttachmentsController {
	private static final Logger logger = Logger.getLogger(SupplierAttachmentsController.class);

	SupplierAttachments imgData= new SupplierAttachments();
	
	
	@Autowired
	private SupplierAttachmentsService supplierAttachmentsService;
	
	@PersistenceContext
	private EntityManager em;

	public SupplierAttachmentsController() {
	
	}
	
	@PostMapping("/uploadSuppAttach")
	public ResponseEntity<SupplierAttachments> uplaodImage(@RequestParam("imageFile") MultipartFile file) throws IOException {

		ResponseEntity<SupplierAttachments>response = supplierAttachmentsService.uplaodImage(file);
		
		return response;
	}
	
	@GetMapping(path = { "/getSuppAttFiles/{supp_id}" })
	public List<SupplierAttachments> getImage(@PathVariable("supp_id") long suppId) throws IOException {

		List<SupplierAttachments> retrievedImage = supplierAttachmentsService.getImage(suppId);
		return retrievedImage;
	}
	
	
	@GetMapping("/suppAttach")
	public List<SupplierAttachments> getAllSupplierAttachments() {

		List<SupplierAttachments> response = supplierAttachmentsService.getAllSupplierAttachments();
		return response;
	}

	@GetMapping("/suppAttach/{id}")
	public ResponseEntity<SupplierAttachments> getSupplierAttachmentById(
			@PathVariable(value = "id") Long supplierAttachmentsId) throws ResourceNotFoundException {
	
		ResponseEntity<SupplierAttachments> response = supplierAttachmentsService.getSupplierAttachmentById(supplierAttachmentsId);
		return response;
	}

	@PostMapping("/suppAttach")
	public SupplierAttachments createCustomerAttachment(
			@Valid @RequestBody SupplierAttachments supplierAttachments) {
		
		SupplierAttachments response=supplierAttachmentsService.createSupplierAttachment(supplierAttachments);
		
		return response;

	}
	

	
	
	@PostMapping("/getSelSuppAttach")
	public ImageModel getSelectedSuppFileInByteFormat(@Valid @RequestBody SupplierAttachments attachFile) throws IOException {

		ImageModel img= supplierAttachmentsService.getSelectedSuppFileInByteFormat(attachFile);
		return img;
	}
	
	

}
