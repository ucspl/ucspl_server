package com.ucspl.controller;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateChallanCumInvoiceDocument;
import com.ucspl.model.CreateDeliveryChallan;
import com.ucspl.model.SearchChallanCumInvoiceDocument;
import com.ucspl.repository.CreateDeliveryChallanRepository;
import com.ucspl.services.CreateChallanCumInvoiceDocumentService;
import com.ucspl.services.CreateDeliveryChallanService;

import org.springframework.web.bind.annotation.GetMapping;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class CreateDeliveryChallanController {
	@Autowired
	private CreateDeliveryChallanService createDeliveryChallanServiceObject;

	// get last saved doc no for delivery challan ///////////////////////////

	@GetMapping(path = { "/DocNoForDeliChallan" })
	public String getDocumentNumber() throws IOException {

		String response = createDeliveryChallanServiceObject.getDocumentNumber();
		return response;

	}
	
	// Get Delivery Challan Header Information 
		@GetMapping("/createDeliChallan")
		public List< CreateDeliveryChallan> getAllCreatedDeliveryChallan() {

			List< CreateDeliveryChallan> response = createDeliveryChallanServiceObject
					.getAllCreatedDeliveryChallan();
			return response;
		}

	    //Get Delivery Challan Header Information By Id
		@GetMapping("/createDeliChallan/{id}")
		public ResponseEntity<CreateDeliveryChallan> getAllCreatedDeliveryChallanById(
				@PathVariable(value = "id") Long createDeliveryChallanId) throws ResourceNotFoundException {

			ResponseEntity< CreateDeliveryChallan> response = createDeliveryChallanServiceObject
					.getAllCreatedDeliveryChallanById(createDeliveryChallanId);
			return response;

		}

		// Save Delivery Challan
		@PostMapping("/createDeliChallan")
		public  CreateDeliveryChallan saveDeliveryChallan(
				@Valid @RequestBody CreateDeliveryChallan createDeliveryChallan) {

			 CreateDeliveryChallan response = createDeliveryChallanServiceObject
					.saveDeliveryChallan(createDeliveryChallan);
			return response;

		}

		// Update Delivery Challan for id
		@PutMapping("/createDeliChallan/{id}")
		public ResponseEntity<CreateDeliveryChallan> updateDeliveryChallanById(
				@PathVariable(value = "id") Long createDeliveryChallanId,
				@Valid @RequestBody  CreateDeliveryChallan createDeliveryChallanDetails)
				throws ResourceNotFoundException {

			ResponseEntity<CreateDeliveryChallan> response = createDeliveryChallanServiceObject
					.updateDeliveryChallanById(createDeliveryChallanId,
							createDeliveryChallanDetails);

			return response;

		}
}
