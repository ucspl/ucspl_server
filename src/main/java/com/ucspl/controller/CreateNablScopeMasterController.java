package com.ucspl.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateNablScopeMaster;
import com.ucspl.model.SearchNabl;
import com.ucspl.services.CreateNablScopeMasterService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class CreateNablScopeMasterController {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateNablScopeMasterService createNablScopeMasterService;

	public CreateNablScopeMasterController() {
		// TODO Auto-generated constructor stub.....
	}

	// Get nabl scope master information
	@GetMapping("/crtNablScopeMaster")
	public List<CreateNablScopeMaster> getAllNablScopeMasters() {

		List<CreateNablScopeMaster> allNablScopeMastersList = createNablScopeMasterService.getAllNablScopeMasters();
		return allNablScopeMastersList;
	}

	// Get nabl scope master information Information By Id
	@GetMapping("/crtNablScopeMaster/{id}")
	public ResponseEntity<CreateNablScopeMaster> getNablScopeMasterById(
			@PathVariable(value = "id") Long nableScopeMasterId) throws ResourceNotFoundException {

		ResponseEntity<CreateNablScopeMaster> response = createNablScopeMasterService
				.getNablScopeMasterById(nableScopeMasterId);
		return response;
	}

	// Save nabl scope master information Details
	@PostMapping("/crtNablScopeMaster")
	public CreateNablScopeMaster createNablScopeMaster(
			@Valid @RequestBody CreateNablScopeMaster createNablScopeMaster) {

		CreateNablScopeMaster response = createNablScopeMasterService.createNablScopeMaster(createNablScopeMaster);
		return response;
	}

	// Update nabl scope master information Details for id
	@PutMapping("/crtNablScopeMaster/{id}")
	public ResponseEntity<CreateNablScopeMaster> updateNablScopeMaster(
			@PathVariable(value = "id") Long nableScopeMasterId,
			@Valid @RequestBody CreateNablScopeMaster nableScopeMasterDetails) throws ResourceNotFoundException {

		ResponseEntity<CreateNablScopeMaster> response = createNablScopeMasterService
				.updateNablScopeMaster(nableScopeMasterId, nableScopeMasterDetails);
		return response;
	}

	// code for search and modify nabl
	@RequestMapping(value = "/searchNabl", method = RequestMethod.POST, produces = "application/json")
	public String searchAndModifyNablScope(@Valid @RequestBody SearchNabl searchNablObject) {

		String response = createNablScopeMasterService.searchAndModifyNablScope(searchNablObject);
		return response;
	}

	// Result For Search nabl scope
	@GetMapping(value = "/searchNabl", produces = "application/json")
	public List<Object[]> getAllNablScope() {

		List<Object[]> resultValue = createNablScopeMasterService.getAllNablScope();
		return resultValue;
	}


	// STORE PROCEDURE FOR GETTING DETAILS OF CREATE NABL FOR SEARCH NABL 
	@RequestMapping(value = "/createNablDet", method = RequestMethod.POST, produces = "application/json")
	public String certiNoForGettingCreateNablDetails(@Valid @RequestBody String certiNo) {

		String response = createNablScopeMasterService.certiNoForGettingCreateNablDetails(certiNo);
		return response;
	}

	@GetMapping(value = "/createNablDet", produces = "application/json")
	public List<Object[]> getCreateNablDetailsForSearchNabl() {

		List<Object[]> resultValueSet = createNablScopeMasterService.getCreateNablDetailsForSearchNabl();
		return resultValueSet;
	}
	

	///////////// STORE PROCEDURE FOR GETTING DETAILS OF CREATE NABL specification FOR SEARCH NABL////////////////////
	@RequestMapping(value = "/createNablSpecDet", method = RequestMethod.POST, produces = "application/json")
	public String nablScopeMasterIdForGettingSpecificationDetails(@Valid @RequestBody String nablScopeMasterId) {

		String response = createNablScopeMasterService
				.nablScopeMasterIdForGettingSpecificationDetails(nablScopeMasterId);
		return response;
	}

	@GetMapping(value = "/createNablSpecDet", produces = "application/json")
	public List<Object[]> getNablSpecificationDetailsForSearchNabl() {

		List<Object[]> resultValueSet = createNablScopeMasterService.getNablSpecificationDetailsForSearchNabl();
		return resultValueSet;
	}

}
