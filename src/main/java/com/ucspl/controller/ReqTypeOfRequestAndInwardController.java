

package com.ucspl.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.ReqTypeOfRequestAndInward;
import com.ucspl.repository.ReqTypeOfRequestAndInwardRepository;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class ReqTypeOfRequestAndInwardController {

	private static final Logger logger = Logger.getLogger(ReqTypeOfRequestAndInwardController.class); 
	
	@Autowired
	private ReqTypeOfRequestAndInwardRepository reqTypeOfRequestAndInwardRepository;

	public ReqTypeOfRequestAndInwardController() {
		// TODO Auto-generated constructor stub
	}

	@GetMapping("/reqType")
	public List<ReqTypeOfRequestAndInward> getAllreqTypeOfRequestAndInwards() {

		logger.info("/reqType - Get all Request Type!");
		return reqTypeOfRequestAndInwardRepository.findAll();
	}

}
