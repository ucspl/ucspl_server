
package com.ucspl.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.UncDividingFactor;
import com.ucspl.model.UncMasterType;
import com.ucspl.model.UomListForReqAndIwd;
import com.ucspl.services.CreateDefinitionService;



@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class UncDividingFactorController {

	private static final Logger logger = Logger.getLogger(UncDividingFactorController.class);

	@Autowired
	private CreateDefinitionService createDefinitionService;

	public UncDividingFactorController() {

	}

//get all uncMasType list	
	@GetMapping("/crtUncDivFactor")
	public List<UncDividingFactor> getAllUncDividingFactor() {
		logger.info("/crtUncDivFactor - Get all Unc Div Factor List!");

		List<UncDividingFactor> resultValue = createDefinitionService.getAllUncDividingFactor();

		return resultValue;
	}

//Save create uncMasType Details 
	@PostMapping("/crtUncDivFactor")
	public UncDividingFactor createUncDividingFactor(@Valid @RequestBody UncDividingFactor uncDividingFactor) {

		UncDividingFactor response = createDefinitionService.createUncDividingFactor(uncDividingFactor);
		return response;
	}

}
