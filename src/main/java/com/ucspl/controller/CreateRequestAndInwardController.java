package com.ucspl.controller;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateRequestAndInward;
import com.ucspl.model.SearchRequestAndInward;
import com.ucspl.services.CreateRequestAndInwardService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class CreateRequestAndInwardController {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateRequestAndInwardService createRequestAndInwardService;


	public CreateRequestAndInwardController() {
		// TODO Auto-generated constructor stub
	}

//get request and inward 
	@GetMapping("/createreq")
	public List<CreateRequestAndInward> getAllCreatedRequestAndInward() {

		List<CreateRequestAndInward>response= createRequestAndInwardService.getAllCreatedRequestAndInward();
		return response;
	}

//get request and inward by id
	@GetMapping("/createreq/{id}")
	public ResponseEntity<CreateRequestAndInward> getRequestAndInwardById(
			@PathVariable(value = "id") Long createRequestAndInwardId) throws ResourceNotFoundException {
	
		ResponseEntity<CreateRequestAndInward> response = createRequestAndInwardService.getRequestAndInwardById(createRequestAndInwardId);
		return response;
	}

//update request and inward 
	@PutMapping("/createreq/{id}")
	public ResponseEntity<CreateRequestAndInward> updateRequestAndInwardById(
			@PathVariable(value = "id") Long createRequestAndInwardId,
			@Valid @RequestBody CreateRequestAndInward createRequestAndInwardDetails) throws ResourceNotFoundException {
	
		ResponseEntity<CreateRequestAndInward> response=  createRequestAndInwardService.updateRequestAndInwardById(createRequestAndInwardId, createRequestAndInwardDetails);
		return response;
	}

//create request and inward 
	@PostMapping("/createreq")
	public CreateRequestAndInward createRequestAndInward(
			@Valid @RequestBody CreateRequestAndInward createRequestAndInward) {
				
		CreateRequestAndInward response = createRequestAndInwardService.createRequestAndInward(createRequestAndInward);
		return response;

	}

////////////////////// get document number for req and inw ////////////////////////////
	@PostMapping("/docNoForReq")
	public String getDocumentNumber(@Valid @RequestBody int type) throws IOException {

		String response = 	createRequestAndInwardService.getDocumentNumber(type);
		return response;
	}

	@PostMapping("/countDNoForReq")
	public String getCounterDocumentNumber(@Valid @RequestBody CreateRequestAndInward obj) throws IOException {

		String response = createRequestAndInwardService.getCounterDocumentNumber(obj);		
		return response;
	}

////////////////////////////// Search and modify req and iwd  ////////////////////////////////////////
	@RequestMapping(value = "/searchReq", method = RequestMethod.POST, produces = "application/json")
	public String searchAndModifyRequestAndInward(@Valid @RequestBody SearchRequestAndInward searchReqAndIwd1) {

		String response = createRequestAndInwardService.searchAndModifyRequestAndInward(searchReqAndIwd1);
		return response;
	}

///////////////////////////// Result For search and modify req and iwd //////////////////////////////////////
	@GetMapping(value = "/searchReq", produces = "application/json")
	public List<Object[]> getAllRequestAndInward() {

		List<Object[]> resultValue= createRequestAndInwardService.getAllRequestAndInward();
		return resultValue;
	}

///////////////////////// Store procedure for getting search result for doc no //////////////////////////////
	@RequestMapping(value = "/CReqDet", method = RequestMethod.POST, produces = "application/json")
	public String docNoForGettingCreateReqDetails(@Valid @RequestBody String docNo) {

		String response = createRequestAndInwardService.docNoForGettingCreateReqDetails(docNo);
		return response;
	}

	
	@GetMapping(value = "/CReqDet", produces = "application/json")
	public List<Object[]> getCreateReqDetailsForSearchReq() {

		List<Object[]> resultValueSet = createRequestAndInwardService.getCreateReqDetailsForSearchReq();
		return resultValueSet;

	}

}
