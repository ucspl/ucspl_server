

package com.ucspl.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.UncMasterNumber;
import com.ucspl.model.UncMasterType;
import com.ucspl.model.UomListForReqAndIwd;
import com.ucspl.services.CreateDefinitionService;



@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class UncMasterNumberController {

	private static final Logger logger = Logger.getLogger(UncMasterNumberController.class);

	@Autowired
	private CreateDefinitionService createDefinitionService;

	public UncMasterNumberController() {

	}

//get all uncMasType list	
	@GetMapping("/crtUncMastNo")
	public List<UncMasterNumber> getAllUncMasterNumber() {
		logger.info("/crtUncMastNo - Get all uncMasType List!");

		List<UncMasterNumber> resultValue = createDefinitionService.getAllUncMasterNumber();

		return resultValue;
	}

//Save create uncMasType Details 
	@PostMapping("/crtUncMastNo")
	public UncMasterNumber createUncMasterNumber(@Valid @RequestBody UncMasterNumber uncMasterNumber) {

		UncMasterNumber response = createDefinitionService.createUncMasterNumber(uncMasterNumber);
		return response;
	}

}
