
package com.ucspl.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.Branch;
import com.ucspl.model.ReferenceForSalesQuotation;
import com.ucspl.repository.ReferenceForSalesQuotationRepository;
import com.ucspl.services.CreateDefinitionService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class ReferenceForSalesQuotationController {

	
	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateDefinitionService createDefinitionServiceObject;
	
	public ReferenceForSalesQuotationController() {
	

	}

	// get reference list for sales and quotation
	@GetMapping("/referenceforsales")
	public List<ReferenceForSalesQuotation> getAllreferences() {
		
		List<ReferenceForSalesQuotation> response=createDefinitionServiceObject.getAllreferences();
		return response;
	}
	
	// Save create reference for sales and quotation
		@PostMapping("/createRefNoForQuotation")
		public ReferenceForSalesQuotation createReferenceForSalesQuotation(@Valid @RequestBody ReferenceForSalesQuotation referenceForSalesQuotationObject) {

			ReferenceForSalesQuotation response = createDefinitionServiceObject.createReferenceForSalesQuotation(referenceForSalesQuotationObject);
			return response;
		}

}
