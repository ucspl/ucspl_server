package com.ucspl.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.Branch;
import com.ucspl.model.MasterTypeForMasterInstrument;
import com.ucspl.services.CreateDefinitionService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class MasterTypeForMasterInstrumentController {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateDefinitionService createDefinitionServiceObject;

	public MasterTypeForMasterInstrumentController() {

	}

// get branch list
	@GetMapping("/mMasType")
	public List<MasterTypeForMasterInstrument> getAllMasterTypeForMasterInstrument() {

		List<MasterTypeForMasterInstrument> response = createDefinitionServiceObject.getAllMasterTypeForMasterInstrument();
		return response;
	}

// Save create branchDetails
	@PostMapping("/mMasType")
	public MasterTypeForMasterInstrument createMasterTypeForMasterInstrument(@Valid @RequestBody MasterTypeForMasterInstrument masterTypeForMasterInstrumentObject) {

		MasterTypeForMasterInstrument response = createDefinitionServiceObject.createMasterTypeForMasterInstrument(masterTypeForMasterInstrumentObject);
		return response;
	}

}
