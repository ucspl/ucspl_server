package com.ucspl.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateUomCalculatorDetails;
import com.ucspl.repository.CreateUomCalculatorDetailsRepository;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class CreateUomCalculatorDetailsController {

	private static final Logger logger = Logger.getLogger(CreateUomCalculatorDetailsController.class);

	@Autowired
	private CreateUomCalculatorDetailsRepository createUomCalculatorDetailsRepository;

	@PersistenceContext
	private EntityManager em;

	public CreateUomCalculatorDetailsController() {

	}

	@GetMapping("/crtUomCalSpe")
	public List<CreateUomCalculatorDetails> getAllCreateUomCalculatorDetails() {

		logger.info("/crtUomCalSpe - Get All Created Uom calculator details!");
		return createUomCalculatorDetailsRepository.findAll();
	}

	@GetMapping("/crtUomCalSpe/{id}")
	public ResponseEntity<CreateUomCalculatorDetails> getCreateUncParameterMasterSpecificationDetailsById(
			@PathVariable(value = "id") Long createUomCalculatorDetailsId) throws ResourceNotFoundException {
		CreateUomCalculatorDetails createUomCalculatorDetails = createUomCalculatorDetailsRepository
				.findById(createUomCalculatorDetailsId)
				.orElseThrow(() -> new ResourceNotFoundException(
						" Calibration Result Data For Master Instrument not found for this id :: "
								+ createUomCalculatorDetailsId));

		logger.info("/crtUomCalSpe/{id} - Get Created Uom calculator details By Id!" + createUomCalculatorDetailsId);
		return ResponseEntity.ok().body(createUomCalculatorDetails);
	}

	@PutMapping("/crtUomCalSpe/{id}")
	public ResponseEntity<CreateUomCalculatorDetails> updateUomCalculatorDetailsById(
			@PathVariable(value = "id") Long createUomCalculatorDetailsId,
			@Valid @RequestBody CreateUomCalculatorDetails createUomCalculatorDetails)
			throws ResourceNotFoundException {
		CreateUomCalculatorDetails createUomCalculatorDetailsToUpdate = createUomCalculatorDetailsRepository
				.findById(createUomCalculatorDetailsId).orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + createUomCalculatorDetailsId));

//      createUncParameterMasterSpecifiDetails.setParameterNumber(createUncParameterMasterSpecificationDetails.getParameterNumber());
		createUomCalculatorDetailsToUpdate.setUomFrom(createUomCalculatorDetails.getUomFrom());
		createUomCalculatorDetailsToUpdate.setUomTo(createUomCalculatorDetails.getUomTo());
		createUomCalculatorDetailsToUpdate.setMultiplyBy(createUomCalculatorDetails.getMultiplyBy());
//		createUomCalculatorDetailsToUpdate.setDraft(createUomCalculatorDetails.getDraft());
//		createUomCalculatorDetailsToUpdate.setApproved(createUomCalculatorDetails.getApproved());
//		createUomCalculatorDetailsToUpdate.setSubmitted(createUomCalculatorDetails.getSubmitted());

		final CreateUomCalculatorDetails updatedUomCalculatorDetail = createUomCalculatorDetailsRepository
				.save(createUomCalculatorDetailsToUpdate);
		logger.info("/crtUomCalSpe/{id} - Update already Created Uom calculator details for id "
				+ createUomCalculatorDetailsId);

		return ResponseEntity.ok(updatedUomCalculatorDetail);
	}

	@PostMapping("/crtUomCalSpe")
	public CreateUomCalculatorDetails createUomCalculatorDetailsFunction(
			@Valid @RequestBody CreateUomCalculatorDetails createUomCalculatorDetails) {

		logger.info("/crtUomCalSpe - Save the Create Uom Calculator details!");
		return createUomCalculatorDetailsRepository.save(createUomCalculatorDetails);
	}

	// Delete User Details for id
	@DeleteMapping("/crtUomCalSpe/{id}")
	public Map<String, Boolean> deleteCreateUomCalculatorDetails(
			@PathVariable(value = "id") Long createUomCalculatorDetailsId) throws ResourceNotFoundException {

		Map<String, Boolean> response = new HashMap<>();
		createUomCalculatorDetailsRepository.deleteById(createUomCalculatorDetailsId);
		response.put("deleted", Boolean.TRUE);
		logger.info("/crtUomCalSpe/{id} - Delete Create Uom calculator Detail of id - " + createUomCalculatorDetailsId);

		return response;
	}

///////////////////////// store procedure to get all created sr no for uuc master instrument /////////////////////////////  
	@RequestMapping(value = "/srnoUomCalSDet", method = RequestMethod.POST, produces = "application/json")
	public List<Object[]> srNoDetailsOfUomCalculatorDetails(@Valid @RequestBody long uomCalculatorId) {

		System.out.println("uom Calculator id: " + uomCalculatorId);

		logger.info("/srnoUomCalSDet -" + " Search uom Calculator Details For master id" + uomCalculatorId);

		List<Object[]> resultValue;

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("srno_uom_calculator_detail_list");    // srno_unc_master_instru_specification_detail_list
		procedureQuery.registerStoredProcedureParameter("id_no", Long.class, ParameterMode.IN);

		procedureQuery.setParameter("id_no", (long) uomCalculatorId);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();
		System.out.println("uom calculator id : " + uomCalculatorId);

		return resultValue;
	}
	
 


	
	// update query for unique sr number
	@PutMapping("/crtCUomCalForParaNo/{id}")
	public ResponseEntity<CreateUomCalculatorDetails> updateCreateUomCalculatorDetailsByIdForParameterNumber(
			@PathVariable(value = "id") Long createUomCalculatorDetailsId,
			@Valid @RequestBody CreateUomCalculatorDetails createUomCalculatorDetails)
			throws ResourceNotFoundException {
		CreateUomCalculatorDetails createUomCalcuDetails = createUomCalculatorDetailsRepository
				.findById(createUomCalculatorDetailsId).orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id:: " + createUomCalculatorDetailsId));

		createUomCalcuDetails.setParameterNumber(createUomCalculatorDetails.getParameterNumber());

		
		final CreateUomCalculatorDetails updatedUomCalculatorDetails = createUomCalculatorDetailsRepository
				.save(createUomCalcuDetails);
		logger.info("/crtCUomCalForParaNo/{id} - Update Uom calculator details parameter number"
				+ createUomCalculatorDetailsId);

		return ResponseEntity.ok(updatedUomCalculatorDetails);
	}                                                                                                           

}
