
package com.ucspl.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.Supplier;
import com.ucspl.model.SupplierByNameOrAddress;
import com.ucspl.services.SupplierService;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class SupplierController {


	@Autowired
	private SupplierService supplierService;
	
	SupplierByNameOrAddress searchSupplier = new SupplierByNameOrAddress();


	public SupplierController() {
	
	}

	@GetMapping("/supplier")
	public List<Supplier> getAllSuppliers() {

		List<Supplier> returnValue=supplierService.getAllSuppliers();
		return returnValue;
	}
	
	@GetMapping("/supplier/{id}")
	public ResponseEntity<Supplier> getSupplierById(
			@PathVariable(value = "id") Long supplierId) throws ResourceNotFoundException {

		ResponseEntity<Supplier> response = supplierService.getSupplierById(supplierId);
		return response;
	}

	@PostMapping("/supplier")
	public Supplier createSupplier(@Valid @RequestBody Supplier supplier) {
						
		Supplier response = supplierService.createSupplier(supplier);
		return response;
		
	}
	
	@PutMapping("/supplier/{id}")
	public ResponseEntity<Supplier> updateSupplierById(
			@PathVariable(value = "id") Long supplierId,
			@Valid @RequestBody Supplier supplierDetails) throws ResourceNotFoundException {
		ResponseEntity<Supplier> response = supplierService.updateSupplierById(supplierId, supplierDetails);
		return response;
	}
	
	
	// Search customer for id
	@RequestMapping(value = "/CSuppDetails", method = RequestMethod.POST, produces = "application/json")
	public Supplier getSupplierDetailsById(@Valid @RequestBody long id) throws ResourceNotFoundException {
        System.out.println("id is"+id);
		Supplier response = supplierService.getSupplierDetailsById(id);
		return response;
	
	}	
	
	
	
///////////////////////search suppliers///////////////////////////////////////////

	//Search Customer Details
	@RequestMapping(value = "searchSupplier", method = RequestMethod.POST, produces = "application/json")
	public String searchAndModifySupplier(@Valid @RequestBody SupplierByNameOrAddress supplier) {
		
		String response = supplierService.searchAndModifySupplier(supplier);
		return response;
	}
	

	//Get Search result based on Name or Address
	@GetMapping(value = "searchSupplier", produces = "application/json")
	public List<Object[]> getAllSuppByNameOrDeptOrAddress() {

		List<Object[]> resultValue = supplierService.getAllSuppByNameOrDeptOrAddress();
		return resultValue;

	}

	
	
}
