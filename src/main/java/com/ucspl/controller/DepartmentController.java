package com.ucspl.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.Branch;
import com.ucspl.model.Department;
import com.ucspl.repository.DepartmentRepository;
import com.ucspl.services.CreateDefinitionService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class DepartmentController {

	private static final Logger logger = Logger.getLogger(DepartmentController.class);

	@Autowired
	private DepartmentRepository departmentRepository;

	@Autowired
	private CreateDefinitionService createDefinitionServiceObject;

	public DepartmentController() {

	}

// get department list
	@GetMapping("/department")
	public List<Department> getAllDepartment() {

		List<Department> response = createDefinitionServiceObject.getAllDepartment();
		return response;
	}

// Save create branchDetails
	@PostMapping("/createdepartment")
	public Department createDepartment(@Valid @RequestBody Department departmentObject) {

		Department response = createDefinitionServiceObject.createDepartment(departmentObject);
		return response;
	}
}
