package com.ucspl.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.model.DocumentType;
import com.ucspl.repository.DocumentTypeRepository;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class DocumentTypeController {

	private static final Logger logger = Logger.getLogger(BranchController.class); 
	
	@Autowired
	private DocumentTypeRepository documentTypeRepository;

	List<Object[]> resultValue;

	@PersistenceContext
	private EntityManager em;

	

	private static int searchDocument = 0;

	public DocumentTypeController() {
		// TODO Auto-generated constructor stub
	}

	@GetMapping("/document")
	public List<DocumentType> getAllDocuments() {

		logger.info("/document - Get all Document names for manage approvers screen!");
		return documentTypeRepository.findAll();
	}

	@RequestMapping(value = "/srhusr_by_docty", method = RequestMethod.POST, produces = "application/json")
	public String searchUserForDocumentType(@Valid @RequestBody int documentType) {
		

		searchDocument = documentType;
		String response = "{\"message\":\"Post With ngResource: The user firstname: " + documentType + "\"}";

		logger.info("/srhusr_by_docty - Search users for document type id "+ documentType);
		return response;
	}

	@SuppressWarnings("unchecked")
	@GetMapping(value = "/srhusr_by_docty", produces = "application/json")
	public List<Object[]> getAllUsersForDocumentType() {

		

		int documentid = searchDocument;

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("search_and_modify_user_document_type");
		procedureQuery.registerStoredProcedureParameter("documentid", Integer.class, ParameterMode.IN);

		procedureQuery.setParameter("documentid", (int) documentid);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();
	
		searchDocument = 0;

		logger.info("/srhusr_by_docty -"  + " Get result for Search users for Document type id- " + documentid);

		return resultValue;

	}

	@SuppressWarnings("unchecked")
	@GetMapping(value = "/srhusr_by_docty_adduserT", produces = "application/json")
	public List<Object[]> getAllUsers() {

		


		int documentid = searchDocument;

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("search_and_modify_user_document_type_adduser_table");
		procedureQuery.registerStoredProcedureParameter("documentid", Integer.class, ParameterMode.IN);

		procedureQuery.setParameter("documentid", (int) documentid);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

	

		logger.info("/srhusr_by_docty_adduserT -"  + " Show details of all users");  

		return resultValue;

	}

	@RequestMapping(value = "/adduserT_in_DB", method = RequestMethod.POST, produces = "application/json")
	public String addUserInExistingTable(@Valid @RequestBody Integer[] userNumberAndDocId) {
		
		int userNumber = userNumberAndDocId[0];
		int docId = userNumberAndDocId[1];

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("adduser_table_in_user_document_junction");
		procedureQuery.registerStoredProcedureParameter("unumber", Integer.class, ParameterMode.IN);
		procedureQuery.registerStoredProcedureParameter("documentid", Integer.class, ParameterMode.IN);

		procedureQuery.setParameter("unumber", (int) userNumber);
		procedureQuery.setParameter("documentid", (int) docId);

		procedureQuery.execute();

		String response = "{\"message\":\"Post With ngResource: The user is added sucessfully: " + userNumber + "\"}";
		logger.info("/adduserT_in_DB -"  + " Add new user having userNumber "+userNumber+" for Document type id- " + docId);
		
		return response;
	}

	@RequestMapping(value = "/extuserT_in_DB", method = RequestMethod.POST, produces = "application/json")
	public String removeUserFromExistingTable(@Valid @RequestBody Integer[] userNumberAndDocId) {
		
		int userNumber = userNumberAndDocId[0];
		int docId = userNumberAndDocId[1];

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("existinguser_table_in_user_document_junction");
		procedureQuery.registerStoredProcedureParameter("unumber", Integer.class, ParameterMode.IN);
		procedureQuery.registerStoredProcedureParameter("documentid", Integer.class, ParameterMode.IN);

		procedureQuery.setParameter("unumber", (int) userNumber);
		procedureQuery.setParameter("documentid", (int) docId);

		procedureQuery.execute();

		String response = "{\"message\":\"Post With ngResource: The user is deleted sucessfully: " + userNumber + "\"}";
		logger.info("/adduserT_in_DB -"  + " Remove user having userNumber "+userNumber+" for Document type id- " + docId);

		return response;
	}

}