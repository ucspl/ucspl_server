package com.ucspl.controller;

import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.SalesQuotationDocument;
import com.ucspl.repository.SalesQuotationDocumentRepository;
import com.ucspl.services.CustomerService;
import com.ucspl.services.SalesQuotationDocumentService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class SalesQuotationDocumentController {

	@Autowired
	private SalesQuotationDocumentService salesQuotationDocumentService;

	@PersistenceContext
	private EntityManager em;

	public SalesQuotationDocumentController() {
		// TODO Auto-generated constructor stub
	}

	// Get All Sales Quotation Document Item Information
	@GetMapping("/SalesQuotDoc")
	public List<SalesQuotationDocument> getAllCreateSalesQuotationDocument() {

		List<SalesQuotationDocument> response = salesQuotationDocumentService.getAllCreateSalesQuotationDocument();
		return response;
	}

	// Get Sales quotation Document Item Information By Id
	@GetMapping("/SalesQuotDoc/{id}")
	public ResponseEntity<SalesQuotationDocument> getCreateSalesQuotationDocumentById(
			@PathVariable(value = "id") Long createSalesQuotationDocumentId) throws ResourceNotFoundException {

		ResponseEntity<SalesQuotationDocument> response = salesQuotationDocumentService
				.getCreateSalesQuotationDocumentById(createSalesQuotationDocumentId);
		return response;
		
	}

	// Save Item Details of Sales Quotation Document
	@PostMapping("/SalesQuotDoc")
	public SalesQuotationDocument createSalesQuotationDocument(
			@Valid @RequestBody SalesQuotationDocument salesQuotationDocument) {

		SalesQuotationDocument response = salesQuotationDocumentService
				.createSalesQuotationDocument(salesQuotationDocument);
		return response;
	}

	// Update Sales Quotation Document Item Details for id
	@PutMapping("/SalesQuotDoc/{id}")
	public ResponseEntity<SalesQuotationDocument> updateSalesQuotationDocumentById(
			@PathVariable(value = "id") Long createSalesQuotationDocumentId,
			@Valid @RequestBody SalesQuotationDocument salesQuotationDocumentDetails) throws ResourceNotFoundException {

		ResponseEntity<SalesQuotationDocument> response = salesQuotationDocumentService
				.updateSalesQuotationDocumentById(createSalesQuotationDocumentId, salesQuotationDocumentDetails);
		return response;

	}

	// Store Procedure for getting Sr no for document number

	@RequestMapping(value = "/srnoSQDoc", method = RequestMethod.POST, produces = "application/json")
	public String docNoForGettingSrNo(@Valid @RequestBody String docNo) {

		String response = salesQuotationDocumentService.docNoForGettingSrNo(docNo);
		return response;

	}

	// Get Items Details of Sales quotation Document for Document No
	@GetMapping(value = "/srnoSQDoc", produces = "application/json")
	public List<Object[]> getSrNoForDocNo() {

		List<Object[]> response = salesQuotationDocumentService.getSrNoForDocNo();
		return response;

	}

	// Store Procedure for getting terms id

	@RequestMapping(value = "/CSalesTerms", method = RequestMethod.POST, produces = "application/json")
	public String docNoForGettingTermsId(@Valid @RequestBody String docNo) {

		String response = salesQuotationDocumentService.docNoForGettingTermsId(docNo);
		return response;

	}

	// Get Terms Details of Sales quotation Document for Document No
	@GetMapping(value = "/CSalesTerms", produces = "application/json")
	public List<BigInteger> getTermsIdForDocNo() {

		List<BigInteger> response = salesQuotationDocumentService.getTermsIdForDocNo();
		return response;

	}
	 // code to delete sales Row
	@PostMapping("/deleteSalesRow")
	public String removeSalesRow(@Valid @RequestBody long indexToDelete) throws IOException {

		System.out.println("sales row");
		String response = salesQuotationDocumentService.removeSalesRow(indexToDelete);
		return response;

	}

	
}
