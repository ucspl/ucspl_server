
package com.ucspl.controller;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateNablSpecification;
import com.ucspl.model.SearchNablSpecificationByNablIdPNmAndStatus;
import com.ucspl.services.CreateNablSpecificationService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class CreateNablSpecificationController {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateNablSpecificationService createNablSpecificationService;

	public CreateNablSpecificationController() {

	}

	// Get All Created Nabl Specification
	@GetMapping("/crtNablSpeDet")
	public List<CreateNablSpecification> getAllCreatedNablSpecificationDetails() {

		List<CreateNablSpecification> response = createNablSpecificationService.getAllCreatedNablSpecificationDetails();
		return response;
	}

	// Get nabl specification By Id
	@GetMapping("/crtNablSpeDet/{id}")
	public ResponseEntity<CreateNablSpecification> getNablSpecificationDetailsById(
			@PathVariable(value = "id") Long createNablSpecificationDetailsId) throws ResourceNotFoundException {

		ResponseEntity<CreateNablSpecification> response = createNablSpecificationService
				.getNablSpecificationDetailsById(createNablSpecificationDetailsId);
		return response;

	}

	// Save the Nabl specification
	@PostMapping("/crtNablSpeDet")
	public CreateNablSpecification createNablSpecification(
			@Valid @RequestBody CreateNablSpecification createNablSpecification) {

		CreateNablSpecification response = createNablSpecificationService
				.createNablSpecification(createNablSpecification);
		return response;
	}

	// Update Nabl Specification Details for id
	@PutMapping("/crtNablSpeDet/{id}")
	public ResponseEntity<CreateNablSpecification> updateNablSpecificationDetailsById(
			@PathVariable(value = "id") Long createNablSpecificationDetailsId,
			@Valid @RequestBody CreateNablSpecification createNablSpecificationDetails)
			throws ResourceNotFoundException {

		ResponseEntity<CreateNablSpecification> response = createNablSpecificationService
				.updateNablSpecificationDetailsById(createNablSpecificationDetailsId, createNablSpecificationDetails);
		return response;
	}

	// Delete Nabl Instrument Detail of id
	@DeleteMapping("/crtNablSpeDet/{id}")
	public Map<String, Boolean> deleteNablSpecificationDetails(@PathVariable(value = "id") Long nablDetailId)
			throws ResourceNotFoundException {

		Map<String, Boolean> response = createNablSpecificationService.deleteNablSpecificationDetails(nablDetailId);
		return response;
	}

//////////////////////////////sp for getting sr no for master instruments ////////////////////////////////  
	@RequestMapping(value = "/srNoNablDet", method = RequestMethod.POST, produces = "application/json")
	public List<Object[]> srNoDetailsOfNablSpecification(@Valid @RequestBody long nablId) {

		List<Object[]> response = createNablSpecificationService.srNoDetailsOfNablSpecification(nablId);
		return response;

	}

/////////////sp for getting sr no for nabl specification by nablidparaname /////////////
	@RequestMapping(value = "/srnoNablSpecDetByNablIdPn", method = RequestMethod.POST, produces = "application/json")
	public List<Object[]> srNoDetailsOfNablSpecificationByNablIdParaName(
			@Valid @RequestBody SearchNablSpecificationByNablIdPNmAndStatus nablIdParaNameObj) {

		List<Object[]> response = createNablSpecificationService
				.srNoDetailsOfNablSpecificationByNablIdParaName(nablIdParaNameObj);
		return response;
	}

}
