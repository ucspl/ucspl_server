

package com.ucspl.services;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.ImageModel;
import com.ucspl.model.MasterInstruCalCertAttachments;
import com.ucspl.model.SearchAttachmentByMastIdAndCalCertId;
import com.ucspl.repository.MasterInstruCalCertAttachmentsRepository;

@Service
public class MasterInstruCalCertAttachmentsService {

	private static final Logger logger = Logger.getLogger(MasterInstruCalCertAttachmentsService.class);

	MasterInstruCalCertAttachments imgData= new MasterInstruCalCertAttachments();
	
	@Autowired
	private MasterInstruCalCertAttachmentsRepository masterInstruCalCertAttachmentsRepository;
	
	@PersistenceContext
	private EntityManager em;

	public ResponseEntity<MasterInstruCalCertAttachments> uplaodImage(MultipartFile file) throws IOException {

		String path="C:\\Users\\Admin\\Documents\\Workspace\\upload\\certificate";
		  		      
		String fileName = file.getOriginalFilename();

		String docLocation =path+"\\"+fileName;
		File file1 = new File(docLocation);
		FileCopyUtils.copy(file.getBytes(), file1);
	    
		imgData.setAttachedFileName(fileName);
		imgData.setType(file.getContentType());
		imgData.setFileLocation(docLocation);
		
		return ResponseEntity.ok().body(imgData);
	}
	

	public List<MasterInstruCalCertAttachments> getImage(long mastId) throws IOException {

		final List<MasterInstruCalCertAttachments> retrievedImage = masterInstruCalCertAttachmentsRepository.findMasterInstruCalCertAttachmentsByMastId(mastId);
		List<MasterInstruCalCertAttachments> resultValue;
		return retrievedImage;
	}
	

	public List<MasterInstruCalCertAttachments> getAllMasterInstruCalCertAttachments() {

		logger.info("/getMICalCertAttach - Get All Created master instru calibration certificate Attachments!");
		return masterInstruCalCertAttachmentsRepository.findAll();
	}


	public ResponseEntity<MasterInstruCalCertAttachments> getMasterInstruCalCertAttachmentById(Long masterInstruCalCertAttachmentsId) throws ResourceNotFoundException {
		MasterInstruCalCertAttachments masterInstruCalCertAttachments = masterInstruCalCertAttachmentsRepository
				.findById(masterInstruCalCertAttachmentsId).orElseThrow(() -> new ResourceNotFoundException(
						" customer attachment not found for this id :: " + masterInstruCalCertAttachmentsId));
		
		logger.info("/mICalCertAttach/{id} - Get master instru calibration certificate attachment By Id!"+ masterInstruCalCertAttachmentsId);
		
		return ResponseEntity.ok().body(masterInstruCalCertAttachments);
	}
	
	

	public MasterInstruCalCertAttachments createMasterInstruCalCertAttachment(MasterInstruCalCertAttachments masterInstruCalCertAttachments) {
		
		masterInstruCalCertAttachments.setAttachedFileName(imgData.getAttachedFileName());
		masterInstruCalCertAttachments.setType(imgData.getType());
		masterInstruCalCertAttachments.setFileLocation(imgData.getFileLocation());
		
		logger.info("/mICalCertAttach - Save the master instru calibration certificate Attachments!");
		
		return masterInstruCalCertAttachmentsRepository.save(masterInstruCalCertAttachments);

	}
	
	
	public Map<String, Boolean> deleteMasterInstrumentSpecificationDetails(Long masterInstrumentDetailId) throws ResourceNotFoundException {

		Map<String, Boolean> response = new HashMap<>();
		masterInstruCalCertAttachmentsRepository.deleteById(masterInstrumentDetailId);
		response.put("deleted", Boolean.TRUE);
		logger.info("/delSelMICalCertAttach/{id} - Delete Master Instrument Detail of id - " + masterInstrumentDetailId);
				
		return response;
	}

	public ImageModel getSelectedMasterInstruCalCertFileInByteFormat(MasterInstruCalCertAttachments attachFile) throws IOException {

		File file = new File(attachFile.getFileLocation());
		
		Path path = Paths.get(attachFile.getFileLocation());
		String name = attachFile.getAttachedFileName();
		String originalFileName = attachFile.getOriginalFileName();
		String contentType = attachFile.getType();
		byte[] content = null;
		try {
		    content = Files.readAllBytes(path);
		} catch (final IOException e) {
		}
		MultipartFile result = new MockMultipartFile(name,
		                     originalFileName, contentType, content);
		    
		String fileName = result.getOriginalFilename();

		ImageModel img = new ImageModel(result.getName(), result.getContentType(),
				(result.getBytes()));

		return img;
	}
	
	
	public List<Object[]> getMasterInstruCalCertAttachmentByMastIdAndCalId(SearchAttachmentByMastIdAndCalCertId masterInstruCalCert) {

		System.out.println("master id" + masterInstruCalCert.getMastId());

		logger.info("/srnoMSDet -" + " Search master instru cal certificate Details For master id" +  masterInstruCalCert.getMastId());

		List<Object[]> resultValue;

		StoredProcedureQuery procedureQuery = em
				.createStoredProcedureQuery("master_instru_cal_certificate_detail_attachment_list");
		procedureQuery.registerStoredProcedureParameter("mast_id", Long.class, ParameterMode.IN);
		procedureQuery.registerStoredProcedureParameter("mast_cal_id", Long.class, ParameterMode.IN);

		procedureQuery.setParameter("mast_id", (long)  masterInstruCalCert.getMastId());
		procedureQuery.setParameter("mast_cal_id", (long)  masterInstruCalCert.getMastCalCertId());
		
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

		return resultValue;
		

	}
	
	public String deleteMasterInstruCalCertAttachmentByCalId(long calId) {

		System.out.println("master certificate calibration id" + calId);
		logger.info("/delMICalCAttByCalId -" + " Search master instru cal certificate Details For master  certificate calibration id" +  calId);

		String resultValue;

		StoredProcedureQuery procedureQuery = em
				.createStoredProcedureQuery("delete_master_instru_cal_certificate_detail_attachment_list");

		procedureQuery.registerStoredProcedureParameter("mast_cal_id", Long.class, ParameterMode.IN);
		procedureQuery.setParameter("mast_cal_id", (long)  calId);
		
		procedureQuery.execute();

		resultValue="{\"message\":\"Post With ngResource: Deleted data of  master certificate calibration id sucessfully: "
				+ calId + "\"}";
		return resultValue;
		

	}
	
}

