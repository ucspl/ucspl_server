
package com.ucspl.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.ChallanCumInvoiceDocumentCalculation;
import com.ucspl.model.ProformaInvoiceCalculation;
import com.ucspl.model.SalesQuotationDocumentCalculation;
import com.ucspl.repository.ChallanCumInvoiceDocumentCalculationRepository;
import com.ucspl.repository.ProformaInvoiceCalculationRepository;
import com.ucspl.repository.SalesQuotationDocumentCalculationRepository;

@Service
public class ProformaInvoiceCalculationService {

	
	private static final Logger logger = Logger.getLogger(ProformaInvoiceCalculationService.class);

	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	private ProformaInvoiceCalculationRepository proformaInvoiceCalculationRepositoryObject;
	
	
	
	
	private String docNoVariable;
	
	// Get All Proforma Document Calculations
	public List<ProformaInvoiceCalculation> getProformsDocumentCalculation() {
			
			logger.info("/invDocCal -  Get All Proforma Document Calculations!");

			return proformaInvoiceCalculationRepositoryObject.findAll();
		}
		
		
	// Get All Challan Cum Invoice Document by id
	public ResponseEntity<ProformaInvoiceCalculation> getProformaDocumentCalculationById(
			@PathVariable(value = "id") Long proformaDocumentCalculationId) throws ResourceNotFoundException {

		ProformaInvoiceCalculation proformaInvoiceCalculation = proformaInvoiceCalculationRepositoryObject.
					findById(proformaDocumentCalculationId).orElseThrow(() -> new ResourceNotFoundException(
							"Document not found for this id :: " + proformaDocumentCalculationId));

			logger.info("/proformaDocCal/{id}\" - // Get All Challan Cum Invoice Document by id!" + proformaDocumentCalculationId);

			return ResponseEntity.ok().body(proformaInvoiceCalculation);
		}
		
	
		
		// Save Calculation of proforma Document
	public ProformaInvoiceCalculation createProformaDocumentCalculation(
			@Valid @RequestBody ProformaInvoiceCalculation proformaDocumentCalculation) {
	

			    logger.info("/invDocCal - Save Calculation of proforma Document!");

			return proformaInvoiceCalculationRepositoryObject.save(proformaDocumentCalculation);

		}
		

		// Update proforma Document Calculation for id
	public ResponseEntity<ProformaInvoiceCalculation> updateProformaDocumentCalculationById(
			@PathVariable(value = "id") Long proformaDocumentCalculationId,
			@Valid @RequestBody ProformaInvoiceCalculation proformaDocumentCalculationDetails)
			throws ResourceNotFoundException {
		ProformaInvoiceCalculation proformaInvoiceCalculation = proformaInvoiceCalculationRepositoryObject
					.findById(proformaDocumentCalculationId).orElseThrow(() -> new ResourceNotFoundException(
							"Document not found for this id :: " + proformaDocumentCalculationId));

		proformaInvoiceCalculation.setProformaDocumentNumber(proformaDocumentCalculationDetails.getProformaDocumentNumber());
		proformaInvoiceCalculation.setSubTotal(proformaDocumentCalculationDetails.getSubTotal());
		proformaInvoiceCalculation.setDiscountOnSubtotal(proformaDocumentCalculationDetails.getDiscountOnSubtotal());
		proformaInvoiceCalculation.setTotal(proformaDocumentCalculationDetails.getTotal());
		proformaInvoiceCalculation.setCgst(proformaDocumentCalculationDetails.getCgst());
		proformaInvoiceCalculation.setSgst(proformaDocumentCalculationDetails.getSgst());
		proformaInvoiceCalculation.setIgst(proformaDocumentCalculationDetails.getIgst());
		proformaInvoiceCalculation.setNet(proformaDocumentCalculationDetails.getNet());

			final ProformaInvoiceCalculation updatedProformaInvoiceCalculation = proformaInvoiceCalculationRepositoryObject
					.save(proformaInvoiceCalculation);

			logger.info("/proformaDocCal/{id} - Update proforma Document Calculation for id "
					+ proformaDocumentCalculationId);

			return ResponseEntity.ok(updatedProformaInvoiceCalculation);
		}
		

			
    // store procedure for doc calculation table detail
		public String docNoForGettingDocDetails(@Valid @RequestBody String docNo) {


			docNoVariable = docNo;

			String response = "{\"message\":\" " + docNoVariable + "\"}";

			logger.info("/ProDocCal -" + docNoVariable + " Document No For Getting Proforma Document Calculation Details");

			return response;
		}
		
		
		// Get details of Sales Quotation Document calculation for Document No
		public List<Object[]> getDocDetailsForDocNo() {

			List<Object[]> resultValue;
			String documentNo = docNoVariable;

			StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("doc_details_for_proforma_document_calculation");
			procedureQuery.registerStoredProcedureParameter("doc_no", String.class, ParameterMode.IN);
			procedureQuery.setParameter("doc_no", (String) documentNo);
			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();

		

			logger.info("/IncDocCal - Get Details of proforma Document Calculation for Document No "
					+ docNoVariable);

			return resultValue;

		}	
		
}
