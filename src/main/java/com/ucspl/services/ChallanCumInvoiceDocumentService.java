package com.ucspl.services;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.ChallanCumInvoiceDocument;
import com.ucspl.model.InvoiceAndrequestNumberJunction;
import com.ucspl.model.SalesQuotationDocument;
import com.ucspl.repository.ChallanCumInvoiceDocumentRepository;
import com.ucspl.repository.InvoiceAndrequestNumberJunctionRepository;

@Service
public class ChallanCumInvoiceDocumentService {

	private static final Logger logger = Logger.getLogger(ChallanCumInvoiceDocumentService.class);

	@Autowired
	private ChallanCumInvoiceDocumentRepository challanCumInvoiceDocumentRepository;
	
	@Autowired
	private InvoiceAndrequestNumberJunctionRepository invoiceAndrequestNumberJunctionRepository;


	@PersistenceContext
	private EntityManager em;

	private String docNoVariable;

	// Get All Challan Cum Invoice Document Item Information
	public List<ChallanCumInvoiceDocument> getAllChallanCumInvoiceDocument() {

		logger.info("/invDoc - Get All Challan Cum Invoice Document Item Information!");
		return challanCumInvoiceDocumentRepository.findAll();
	}

	// Get Challan Cum Invoice Document Item Information By Id
	public ResponseEntity<ChallanCumInvoiceDocument> getChallanCumInvoiceDocumentById(
			@PathVariable(value = "id") Long challanCumInvoiceDocumentId) throws ResourceNotFoundException {
		ChallanCumInvoiceDocument challanCumInvoiceDocument = challanCumInvoiceDocumentRepository
				.findById(challanCumInvoiceDocumentId).orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + challanCumInvoiceDocumentId));

		logger.info("/invDoc/{id} - Get Challan Cum Invoice Document Item Information By Id!"
				+ challanCumInvoiceDocumentId);
		return ResponseEntity.ok().body(challanCumInvoiceDocument);
	}

	// Save Item Details of Challan Cum Invoice Document
	public ChallanCumInvoiceDocument createChallanCumInvoiceDocument(
			@Valid @RequestBody ChallanCumInvoiceDocument challanCumInvoiceDocument) {

		logger.info("/invDoc - Save Item Details of Challan Cum Invoice Document !");
		return challanCumInvoiceDocumentRepository.save(challanCumInvoiceDocument);
	}

	// Update Challan Cum Invoice Document Item Details for id
	public ResponseEntity<ChallanCumInvoiceDocument> updateChallanCumInvoiceDocumentById(
			@PathVariable(value = "id") Long challanCumInvoiceDocumentId,
			@Valid @RequestBody ChallanCumInvoiceDocument challanCumInvoiceDocumentDetails)
			throws ResourceNotFoundException {
		ChallanCumInvoiceDocument challanCumInvoiceDocument = challanCumInvoiceDocumentRepository
				.findById(challanCumInvoiceDocumentId).orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + challanCumInvoiceDocumentId));

		challanCumInvoiceDocument.setInvDocumentNumber(challanCumInvoiceDocumentDetails.getInvDocumentNumber());
		challanCumInvoiceDocument.setInvoiceNumber(challanCumInvoiceDocumentDetails.getInvoiceNumber());
		challanCumInvoiceDocument.setSrNo(challanCumInvoiceDocumentDetails.getSrNo());
		challanCumInvoiceDocument.setPoSrNo(challanCumInvoiceDocumentDetails.getPoSrNo());
		challanCumInvoiceDocument.setItemOrPartCode(challanCumInvoiceDocumentDetails.getItemOrPartCode());
		challanCumInvoiceDocument.setInstruId(challanCumInvoiceDocumentDetails.getInstruId());
		challanCumInvoiceDocument.setDescription(challanCumInvoiceDocumentDetails.getDescription());
		challanCumInvoiceDocument.setRangeFrom(challanCumInvoiceDocumentDetails.getRangeFrom());
		challanCumInvoiceDocument.setRangeTo(challanCumInvoiceDocumentDetails.getRangeTo());
		challanCumInvoiceDocument.setSacCode(challanCumInvoiceDocumentDetails.getSacCode());
		challanCumInvoiceDocument.setHsnNo(challanCumInvoiceDocumentDetails.getHsnNo());
		challanCumInvoiceDocument.setQuantity(challanCumInvoiceDocumentDetails.getQuantity());
		challanCumInvoiceDocument.setUnitPrice(challanCumInvoiceDocumentDetails.getUnitPrice());
		challanCumInvoiceDocument.setAmount(challanCumInvoiceDocumentDetails.getAmount());

		final ChallanCumInvoiceDocument updatedChallanCumInvoiceDocument = challanCumInvoiceDocumentRepository
				.save(challanCumInvoiceDocument);
		logger.info("/invDoc/{id} - Update Challan Cum Invoice Document Item Details for id "
				+ challanCumInvoiceDocumentId);

		return ResponseEntity.ok(updatedChallanCumInvoiceDocument);
	}
	
		
			
			// Update Challan Cum Invoice Document Item Details for unique no
			public ResponseEntity<ChallanCumInvoiceDocument> updateChallanCumInvoiceDocumentByUniqueDocNo(
					@PathVariable(value = "id") Long challanCumInvoiceDocumentId,
					@Valid @RequestBody ChallanCumInvoiceDocument uniqueNo)
					throws ResourceNotFoundException {
				ChallanCumInvoiceDocument challanCumInvoiceDocument1 = challanCumInvoiceDocumentRepository
						.findById(challanCumInvoiceDocumentId).orElseThrow(() -> new ResourceNotFoundException(
								"Document not found for this id :: " + challanCumInvoiceDocumentId));

				challanCumInvoiceDocument1.setUniqueNo(uniqueNo.getUniqueNo());
				
				final ChallanCumInvoiceDocument updatedChallanCumInvoiceDocument1 = challanCumInvoiceDocumentRepository
						.save(challanCumInvoiceDocument1);
				logger.info("/updateUniqueInvNo/{id} - Update Challan Cum Invoice Document Item Details for unique no "
						+ challanCumInvoiceDocumentId);

				return ResponseEntity.ok(updatedChallanCumInvoiceDocument1);
			}

	

	// get sr no store procedure for doc no
	public String docNoForGettingSrNo(@Valid @RequestBody String docNo) {

		docNoVariable = docNo;
		String response = "{\"message\":\" " + docNoVariable + "\"}";

		logger.info(
				"/srInvDoc -" + docNoVariable + " Document No For Getting Challan Cum Invoice Document Item Details");
		return response;
	}

	// sr no detail for doc no
	public List<Object[]> getSrNoForDocNo() {

		List<Object[]> resultValue;
		String documentNo = docNoVariable;

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("sr_no_for_challan_cum_invoice_document");
		procedureQuery.registerStoredProcedureParameter("doc_no", String.class, ParameterMode.IN);

		procedureQuery.setParameter("doc_no", (String) documentNo);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

		logger.info("/srInvDoc - Get Items Details of Challan Cum Invoice Document for Document No " + docNoVariable);

		return resultValue;

	}

	// code to delete sales Row
	public String removeInvoiceRow(@Valid @RequestBody long indexToDelete) throws IOException {

		logger.info("/deleteInvoiceRow - delete row Item Details of Invoice Document !");
		challanCumInvoiceDocumentRepository.deleteById(indexToDelete);
		String response = "{\"message\":\" " + indexToDelete + "row deleted successfully" + "\"}";
		return response;
	}

////////////////////store procedure to get request Info For Calibration Tag and Invoice Tag for Request document Number/////////

	public List<Object[]> getRequestDetailByCalibrationTagAndInvoiceTagForReqDocNo(String docNumber)
			throws IOException {

		List<Object[]> resultValue;

		StoredProcedureQuery procedureQuery = em
				.createStoredProcedureQuery("get_req_data_with_calibration_tag_and_invoice_tag_through_reqdocno");
		procedureQuery.registerStoredProcedureParameter("doc_no", String.class, ParameterMode.IN);

		procedureQuery.setParameter("doc_no", (String) docNumber);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

		logger.info(
				"/reqInfoForCaliTagAndInvoiceTagForReqDocNo - get request and inward data with calibration tag and invcoice tag through reqDocNo "
						+ docNumber);

		return resultValue;
	}

////////////////////store procedure to get request Info For Calibration Tag and Invoice Tag for cust Id/////////

	public List<Object[]> getRequestDetailByCalibrationTagAndInvoiceTagForCustId(Integer number) throws IOException {

		List<Object[]> resultValue;

		StoredProcedureQuery procedureQuery = em
				.createStoredProcedureQuery("get_req_data_with_calibration_tag_and_invoice_tag_through_custid");
		procedureQuery.registerStoredProcedureParameter("cust_id", Integer.class, ParameterMode.IN);

		procedureQuery.setParameter("cust_id", (Integer) number);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

		logger.info(
				"/reqInfoForCaliTagAndInvoiceTag - get request and inward data with calibration tag and invoice tag through customer id "
						+ number);

		return resultValue;
	}

	//////////////////// store procedure to get Po Data for Po No /////////
	public List<Object[]> getPoDataForPoNumber(@Valid @RequestBody String number) throws IOException {
		List<Object[]> resultValue;

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("get_po_data_for_po_number");
		procedureQuery.registerStoredProcedureParameter("doc_no", String.class, ParameterMode.IN);

		procedureQuery.setParameter("doc_no", (String) number);
		// procedureQuery.setParameter("cust_id", (String) number);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

		logger.info("/poDataForPoNo - get Po Data for Po No " + number);

		return resultValue;

	}

    ////////////////store procedure to get all  Po numbers for cust id /////////
	public List<Object> getAllPoNumbersForCustId(@Valid @RequestBody Integer number) throws IOException {

		List<Object> resultValue;

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("get_all_po_numbers_for_cust_id");
		procedureQuery.registerStoredProcedureParameter("cust_id", Integer.class, ParameterMode.IN);

		procedureQuery.setParameter("cust_id", (Integer) number);
		// procedureQuery.setParameter("cust_id", (String) number);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

		logger.info("/allPoNumbersCustId - get all  Po numbers for cust id  " + number);

		return resultValue;
	}
	
	   // Save request numbers in junction table
		public InvoiceAndrequestNumberJunction saveRequestNumbersInJunctionTable(
				@Valid @RequestBody InvoiceAndrequestNumberJunction reqAndInvoiceDoc) {

			logger.info("/saveRequsetNoForInvoice - Save request numbers in junction table !");
			return invoiceAndrequestNumberJunctionRepository.save(reqAndInvoiceDoc);
		}
		

		// store procedure to get all request numbers for invoice document number
		public List<Object> getAllRequstNoForInvoiceDocNo(@Valid @RequestBody String number) throws IOException {

			List<Object> resultValue;

			StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("get_all_request_numbers_for_invoice_doc_no");
			procedureQuery.registerStoredProcedureParameter("doc_no", String.class, ParameterMode.IN);

			procedureQuery.setParameter("doc_no", (String) number);
			// procedureQuery.setParameter("cust_id", (String) number);
			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();

			logger.info("/getAllRequstNoForInvoiceDocNo - get all  request numbers for invoice doc no  " + number);

			return resultValue;

           }
		
		// store procedure to get invoice data with invoice tag for invoice document number
		public List<Object> getAllInvoiceDataForInvoiceDocNo(@Valid @RequestBody String number) throws IOException {

			List<Object> resultValue;

			StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("get_all_invoice_data_with_invoice_tag_for_invoice_doc_no");
			procedureQuery.registerStoredProcedureParameter("doc_no", String.class, ParameterMode.IN);

			procedureQuery.setParameter("doc_no", (String) number);
			// procedureQuery.setParameter("cust_id", (String) number);
			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();

			logger.info("/getAllInvoiceDataWithInvoiceTagForInvDocNo - get all  invoice data for invoice doc no  " + number);

			return resultValue;
		}
	
			
				// remove all request numbers for invoice doc no
				public String removeAllRequestNumberForInvDocNo(@Valid @RequestBody String documentNO) throws IOException {

					StoredProcedureQuery procedureQuery = em
							.createStoredProcedureQuery("remove_all_request_numbers_for_invoice_doc_no");
					procedureQuery.registerStoredProcedureParameter("documentNO", String.class, ParameterMode.IN);

					procedureQuery.setParameter("documentNO", (String) documentNO);
					procedureQuery.execute();

					String response = "{\"message\":\"Post With ngResource: The customerid is added sucessfully: " + documentNO
							+ "\"}";

					logger.info(
							"/removeAllReqNoForInvDocNo - remove all request numbers for invoice doc no " + documentNO);

					return response;

				}
				
				// remove all sr numbers for invoice doc no
				public String removeAllSrtNoForInvDocNo(@Valid @RequestBody String documentNO) throws IOException {
					StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("remove_all_sr_no_for_invoice_doc_no");
					procedureQuery.registerStoredProcedureParameter("documentNO", String.class, ParameterMode.IN);

					procedureQuery.setParameter("documentNO", (String) documentNO);
					procedureQuery.execute();

					String response = "{\"message\":\"Post With ngResource: data deleted sucessfully: " + documentNO
							+ "\"}";

					logger.info("/removeAllSrNoForInvDocNo - remove all sr numbers for invoice doc no " + documentNO);

					return response;

					
				}
				
				
				// set invoice tag and unique no to zero for invoice doc no
				public String setInvAndUniNoToZeroForInvDocNo(@Valid @RequestBody String documentNO) throws IOException {
					StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("set_invoicetag_and_uniqueno_to_zero");
					procedureQuery.registerStoredProcedureParameter("invdocno", String.class, ParameterMode.IN);

					procedureQuery.setParameter("invdocno", (String) documentNO);
					procedureQuery.execute();

					String response = "{\"message\":\"Post With ngResource: data deleted sucessfully: " + documentNO
							+ "\"}";

					logger.info("/removeAllSrNoForInvDocNo - set invoice tag and unique no to zero for invoice doc no " + documentNO);

					return response;

					
				}
				
				
				// set invoice tag to one for instru through reqDocId                   requestNo,instruName,reqDocId,invoiceDocNo,invoiceId,invUniqueNo
				public String trialSetInvoiceTagToOneForInstru(@Valid @RequestBody String reqNo,String instruName,String reqDocNo1,String invoiceDocNo,String invoiceId1, String invUniqueNo) throws IOException {
					StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("set_invoice_tag_to_one_for_instru_name");
					long reqDocNo=Long.parseLong(reqDocNo1);
					long invoiceId=Long.parseLong(invoiceId1);
					
					procedureQuery.registerStoredProcedureParameter("reqno", String.class, ParameterMode.IN);
					procedureQuery.registerStoredProcedureParameter("instruname", String.class, ParameterMode.IN);
					procedureQuery.registerStoredProcedureParameter("reqdocid", Long.class, ParameterMode.IN);
					procedureQuery.registerStoredProcedureParameter("invdocno", String.class, ParameterMode.IN);
					procedureQuery.registerStoredProcedureParameter("invoiceid", Long.class, ParameterMode.IN);
					
					procedureQuery.registerStoredProcedureParameter("invuniqueno", String.class, ParameterMode.IN);
					

					procedureQuery.setParameter("reqno", (String) reqNo);
					procedureQuery.setParameter("instruname", (String) instruName);
					procedureQuery.setParameter("reqdocid", (Long) reqDocNo);
					procedureQuery.setParameter("invdocno", (String) invoiceDocNo);
					procedureQuery.setParameter("invoiceid", (Long) invoiceId);
					procedureQuery.setParameter("invuniqueno", (String) invUniqueNo);
					procedureQuery.execute();

					String response = "{\"message\":\"Post With ngResource: tag set successfully: " + reqNo+instruName+reqDocNo+invoiceDocNo+invoiceId
							+ "\"}";

					logger.info("/trialsetInvoiceTagToOneForInstru - set invoice tag to 1 for instrument " + reqNo+" "+instruName+" "+reqDocNo+" "+invoiceDocNo+" "+invoiceId);

					return response;

					
				}
				
				// set invoice tag to one for instru through InvId
				public String trialSetInvoiceTagToOneForInstruThroughInvId(@Valid @RequestBody String reqNo,String instruName,String reqDocNo1,String invoiceDocNo,String invoiceId1) throws IOException {
					StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("set_invoice_tag_to_one_for_instru_name_through_inv_id");
					long reqDocNo=Long.parseLong(reqDocNo1);
					long invoiceId=Long.parseLong(invoiceId1);
					
					procedureQuery.registerStoredProcedureParameter("reqno", String.class, ParameterMode.IN);
					procedureQuery.registerStoredProcedureParameter("instruname", String.class, ParameterMode.IN);
					procedureQuery.registerStoredProcedureParameter("reqdocid", Long.class, ParameterMode.IN);
					procedureQuery.registerStoredProcedureParameter("invdocno", String.class, ParameterMode.IN);
					procedureQuery.registerStoredProcedureParameter("invoiceid", Long.class, ParameterMode.IN);

					procedureQuery.setParameter("reqno", (String) reqNo);
					procedureQuery.setParameter("instruname", (String) instruName);
					procedureQuery.setParameter("reqdocid", (Long) reqDocNo);
					procedureQuery.setParameter("invdocno", (String) invoiceDocNo);
					procedureQuery.setParameter("invoiceid", (Long) invoiceId);
					procedureQuery.execute();

					String response = "{\"message\":\"Post With ngResource: tag set successfully: " + reqNo+instruName+reqDocNo+invoiceDocNo+invoiceId
							+ "\"}";

					logger.info("/trialsetInvoiceTagToOneForInstruThroughInvId - set invoice tag to 1 for instrument " + reqNo+" "+instruName+" "+reqDocNo+" "+invoiceDocNo+" "+invoiceId);

					return response;

					
				}
				
				
				// set invoice tag to Zero for instru   instruName,reqDocId,invoiceDocNo,invoiceId,invUniqueNo
				public String trialSetInvoiceTagToZeroForInstru(@Valid @RequestBody String instruName,String reqDocId1,String invoiceDocNo,String invoiceId1, String invUniqueNo) throws IOException {
					StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("set_invoice_tag_to_zero_for_instru_name");
					long reqDocNo=Long.parseLong(reqDocId1);
					long invoiceId=Long.parseLong(invoiceId1);
					
					//procedureQuery.registerStoredProcedureParameter("reqno", String.class, ParameterMode.IN);
					procedureQuery.registerStoredProcedureParameter("instruname", String.class, ParameterMode.IN);
					procedureQuery.registerStoredProcedureParameter("reqdocid", Long.class, ParameterMode.IN);
			        //procedureQuery.registerStoredProcedureParameter("invdocno", String.class, ParameterMode.IN);
					procedureQuery.registerStoredProcedureParameter("invoiceid", Long.class, ParameterMode.IN);
					procedureQuery.registerStoredProcedureParameter("invuniqueno", String.class, ParameterMode.IN);
					
					//procedureQuery.setParameter("reqno", (String) reqNo);
					procedureQuery.setParameter("instruname", (String) instruName);
			        procedureQuery.setParameter("reqdocid", (Long) reqDocNo);
					//procedureQuery.setParameter("invdocno", (String) invoiceDocNo);
			         procedureQuery.setParameter("invoiceid", (Long) invoiceId);
			         procedureQuery.setParameter("invuniqueno", (String) invUniqueNo);
					procedureQuery.execute();

					String response = "{\"message\":\"Post With ngResource: tag set successfully: " + instruName+invoiceDocNo+invoiceId
							+ "\"}";

					logger.info("/trialsetInvoiceTagToZeroForInstru - set invoice tag to 0 for instrument " + instruName+" "+invoiceDocNo+" "+invoiceId);

					return response;

					
				}
				
				///// set invoice tag to one for range
				public String trialSetInvoiceTagToOneForRange(@Valid @RequestBody String reqNo,String instruName,String reqDocNo1,String invoiceDocNo,String invoiceId1,String rangeFrom,String rangeTo, String invUniqueNo) throws IOException {
					StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("set_invoice_tag_to_one_for_range");
					long reqDocNo=Long.parseLong(reqDocNo1);
					long invoiceId=Long.parseLong(invoiceId1);
					
					procedureQuery.registerStoredProcedureParameter("reqno", String.class, ParameterMode.IN);
					procedureQuery.registerStoredProcedureParameter("instruname", String.class, ParameterMode.IN);
					procedureQuery.registerStoredProcedureParameter("reqdocid", Long.class, ParameterMode.IN);
					procedureQuery.registerStoredProcedureParameter("invdocno", String.class, ParameterMode.IN);
					procedureQuery.registerStoredProcedureParameter("invoiceid", Long.class, ParameterMode.IN);
					procedureQuery.registerStoredProcedureParameter("rangefrom", String.class, ParameterMode.IN);
					procedureQuery.registerStoredProcedureParameter("rangeto", String.class, ParameterMode.IN);
					procedureQuery.registerStoredProcedureParameter("invuniqueno", String.class, ParameterMode.IN);
					
					procedureQuery.setParameter("reqno", (String) reqNo);
					procedureQuery.setParameter("instruname", (String) instruName);
					procedureQuery.setParameter("reqdocid", (Long) reqDocNo);
					procedureQuery.setParameter("invdocno", (String) invoiceDocNo);
					procedureQuery.setParameter("invoiceid", (Long) invoiceId);
					procedureQuery.setParameter("rangefrom", (String) rangeFrom);
					procedureQuery.setParameter("rangeto", (String) rangeTo);
					  procedureQuery.setParameter("invuniqueno", (String) invUniqueNo);
					
					procedureQuery.execute();

					String response = "{\"message\":\"Post With ngResource: tag set successfully: " + reqNo+instruName+reqDocNo+invoiceDocNo+invoiceId+rangeFrom+rangeTo
							+ "\"}";

					logger.info("/trialsetInvoiceTagToOneForRange - set invoice tag to 1 for range " + reqNo+" "+instruName+" "+reqDocNo+" "+invoiceDocNo+" "+invoiceId+" "+rangeFrom+" "+rangeTo+" "+invUniqueNo);

					return response;

					
				}
				
				///////// set invoice tag to Zero for range
				public String trialSetInvoiceTagToZeroForRange(@Valid @RequestBody String instruName,String invoiceDocNo,String reqDocId1,String rangeFrom,String rangeTo, String invUniqueNo) throws IOException {
					StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("set_invoice_tag_to_zero_for_range");
					//long reqDocNo=Long.parseLong(reqDocNo1);
					long reqDocId=Long.parseLong(reqDocId1);
					
					//procedureQuery.registerStoredProcedureParameter("reqno", String.class, ParameterMode.IN);
					procedureQuery.registerStoredProcedureParameter("instruname", String.class, ParameterMode.IN);
					procedureQuery.registerStoredProcedureParameter("reqdocid", Long.class, ParameterMode.IN);
				//	procedureQuery.registerStoredProcedureParameter("invdocno", String.class, ParameterMode.IN);
					//procedureQuery.registerStoredProcedureParameter("invoiceid", Long.class, ParameterMode.IN);
					procedureQuery.registerStoredProcedureParameter("rangefrom", String.class, ParameterMode.IN);
					procedureQuery.registerStoredProcedureParameter("rangeto", String.class, ParameterMode.IN);
					procedureQuery.registerStoredProcedureParameter("invuniqueno", String.class, ParameterMode.IN);

					//procedureQuery.setParameter("reqno", (String) reqNo);
					procedureQuery.setParameter("instruname", (String) instruName);
					procedureQuery.setParameter("reqdocid", (Long) reqDocId);
					//procedureQuery.setParameter("invdocno", (String) invoiceDocNo);
					//procedureQuery.setParameter("invoiceid", (Long) invoiceId);
					procedureQuery.setParameter("rangefrom", (String) rangeFrom);
					procedureQuery.setParameter("rangeto", (String) rangeTo);
					  procedureQuery.setParameter("invuniqueno", (String) invUniqueNo);
					
					procedureQuery.execute();

					String response = "{\"message\":\"Post With ngResource: tag set successfully: " + instruName+invoiceDocNo+reqDocId+rangeFrom+rangeTo
							+ "\"}";

					logger.info("/trialsetInvoiceTagToZeroForRange - set invoice tag to 0 for range " + instruName+" "+invoiceDocNo+" "+reqDocId+" "+rangeFrom+" "+rangeTo+" "+invUniqueNo);

					return response;

					
				}
				
				public String setInvoiceTagToZeroForRequestNoAndInvNo(String data, String data1) throws IOException {
				//	String response = "";

					StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("set_invoice_tag_to_zero_for_invdoc_and_reqdoc");
					
					procedureQuery.registerStoredProcedureParameter("reqdocno", String.class, ParameterMode.IN);
					procedureQuery.registerStoredProcedureParameter("invdocno", String.class, ParameterMode.IN);
					
					procedureQuery.setParameter("reqdocno", (String) data1);
					procedureQuery.setParameter("invdocno", (String) data);
					procedureQuery.execute();
					
					logger.info("/setInvTagToZeroForReqNoAndInvNo - set_invoice_tag_to_zero_for_invdoc_and_reqdoc " + data+" "+data1);
					
					String response = "{\"message\":\"Post With ngResource: tag set successfully: " + data+" "+data1
							+ "\"}";

					
					return response;
				}

}
