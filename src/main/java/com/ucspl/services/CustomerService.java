
package com.ucspl.services;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ucspl.controller.UserController;
import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.Customer;
import com.ucspl.model.CustomerTaxId;
import com.ucspl.model.CustomerbyName;
import com.ucspl.model.User;
import com.ucspl.repository.CustomerRepository;

@Service
public class CustomerService {

	private static final Logger logger = Logger.getLogger(CustomerService.class);
	
	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	private CustomerRepository customerRepository;
	
	private static CustomerbyName searchCustomer = new CustomerbyName();
	
	
	public List<Customer> getAllChildCustomers() {

		logger.info("/getChildCust - Get All Child Customer!");	
		int param=0;
		return customerRepository.findChildCustomers(param);
	}
	
	
	public List<Customer> getAllCustomers() {

		logger.info("/customers - Get All Customers!");
		return customerRepository.findAll();
	}
	
	
	
	public ResponseEntity<Customer> getCustomerById(Long customerId)
			throws ResourceNotFoundException {
		Customer customer = customerRepository.findById(customerId)
				.orElseThrow(() -> new ResourceNotFoundException("customer not found for this id :: " + customerId));
		
		logger.info("/customers/{id} - Get Customer By Id!"+ customerId);
		
		return ResponseEntity.ok().body(customer);
	}
	
	
	public Customer createCustomer(Customer customer) {
		
		logger.info("/customers - Save Customer details!");
		return customerRepository.save(customer);
	}
	
	
	public ResponseEntity<Customer> updateCustomer(Long customerId, @Valid @RequestBody Customer customerDetails)
			throws ResourceNotFoundException {
		Customer customer = customerRepository.findById(customerId)
				.orElseThrow(() -> new ResourceNotFoundException("Customer not found for this id :: " + customerId));

		customer.setIsParent(customerDetails.getIsParent());
		customer.setParentReference(customerDetails.getParentReference());
		customer.setName(customerDetails.getName());
		customer.setDepartment(customerDetails.getDepartment());
		customer.setPanNo(customerDetails.getPanNo());
		customer.setGstNo(customerDetails.getGstNo());
		customer.setCountryPrefixForMobile1(customerDetails.getCountryPrefixForMobile1());
		customer.setCountryPrefixForMobile2(customerDetails.getCountryPrefixForMobile2());
		customer.setMobileNumber1(customerDetails.getMobileNumber1());
		customer.setMobileNumber2(customerDetails.getMobileNumber2());
		customer.setLandlineNumber(customerDetails.getLandlineNumber());
		customer.setEmail1(customerDetails.getEmail1());
		customer.setEmail2(customerDetails.getEmail2());
		customer.setAddressLine1(customerDetails.getAddressLine1());
		customer.setAddressLine2(customerDetails.getAddressLine2());
		customer.setCountry(customerDetails.getCountry());
		customer.setState(customerDetails.getState());
		customer.setCity(customerDetails.getCity());
		customer.setPin(customerDetails.getPin());
		customer.setRegionAreaCode(customerDetails.getRegionAreaCode());
		customer.setBillingCreditPeriod(customerDetails.getBillingCreditPeriod());
		customer.setStatus(customerDetails.getStatus());
		customer.setVendorCode(customerDetails.getVendorCode());
		customer.setPassFailRemark(customerDetails.getPassFailRemark());
		customer.setTaxes(customerDetails.getTaxes());
		customer.setApplicableTaxes(customerDetails.getApplicableTaxes());
		customer.setTaxpayerType(customerDetails.getTaxpayerType());
		customer.setCgst(customerDetails.getCgst());
		customer.setIgst(customerDetails.getIgst());
		customer.setSgst(customerDetails.getSgst());
		customer.setSezNote(customerDetails.getSezNote());
		customer.setCalibratedProcedure(customerDetails.getCalibratedProcedure());
		customer.setReferencedUsed(customerDetails.getReferencedUsed());
		customer.setExpandedUncertainty(customerDetails.getExpandedUncertainty());
		customer.setNote(customerDetails.getNote());
		customer.setLut(customerDetails.getLut());
		customer.setSez(customerDetails.getSez());
		customer.setMasterlhs(customerDetails.getMasterlhs());
		customer.setUuclhs(customerDetails.getUuclhs());
		customer.setParentCustomer(customerDetails.getParentCustomer());
		customer.setCustomFields(customerDetails.getCustomFields());
		customer.setCheckedBy(customerDetails.getCheckedBy());
		customer.setCreatedBy(customerDetails.getCreatedBy());
		customer.setDateCreated(customerDetails.getDateCreated());

		


		
		final Customer updatedCustomer = customerRepository.save(customer);

		logger.info("/customers/{id} - Update Customer Details for id - " + customerId);

		return ResponseEntity.ok(updatedCustomer);
	}
	

	public String searchAndModifyCustomer(CustomerbyName customer) {
	
		searchCustomer = customer;

		String response = "{\"message\":\"Post With ngResource: The customer firstname: " + searchCustomer.getName() + "\"}";	
		logger.info("/search_cust -"  + " Search customer by name or department or address " + searchCustomer);
		
		return response;
	}
	
	
	
	
	public List<Object[]> getAllCustsByNameOrDeptOrAddress() {

			
		List<Object[]> resultValue;
			
		String name = searchCustomer.getName();
		String nameAnd = searchCustomer.getNameAnd(); 
		int status = searchCustomer.getStatus();
		String department = searchCustomer.getDepartment();
		String address = searchCustomer.getAddress();

		System.out.println("name :"+ name);
		logger.info("trial data check -"  + " customer by name or department or address " + searchCustomer);

		if (department.trim().length() == 0 && address.trim().length() == 0 && nameAnd.trim().length() != 0
				&& name.trim().length() != 0) {


			StoredProcedureQuery procedureQuery = em
						.createStoredProcedureQuery("search_and_modify_customer_by_name_and");
			procedureQuery.registerStoredProcedureParameter("name", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("name_and", String.class, ParameterMode.IN); 
			procedureQuery.registerStoredProcedureParameter("status", Integer.class, ParameterMode.IN);

			procedureQuery.setParameter("name", (String) name);
			procedureQuery.setParameter("name_and", (String) nameAnd); 
			procedureQuery.setParameter("status", (int) status);
			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			logger.info("/search_cust -"  + " Get Result For Search Customer by name and nameAnd- " + searchCustomer);



		}

		else if (name.trim().length() != 0 && department.trim().length() != 0 && nameAnd.trim().length() != 0
				&& address.trim().length() == 0) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_customer_by_name_and_department_or_nameand");
			procedureQuery.registerStoredProcedureParameter("name", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("name_and", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("department", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("status", Integer.class, ParameterMode.IN);

			procedureQuery.setParameter("name", (String) name);
			procedureQuery.setParameter("name_and", (String) nameAnd);
			procedureQuery.setParameter("department", (String) department);
			procedureQuery.setParameter("status", (int) status);
			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			logger.info("/search_cust -" + " Get Result For Search Customer by name,department and nameAnd- "
					+ searchCustomer);

		}

		else if (name.trim().length() != 0 && address.trim().length() != 0 && nameAnd.trim().length() != 0
				&& department.trim().length() == 0) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_customer_by_name_and_address_or_nameand");
			procedureQuery.registerStoredProcedureParameter("name", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("name_and", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("address", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("status", Integer.class, ParameterMode.IN);

			procedureQuery.setParameter("name", (String) name);
			procedureQuery.setParameter("name_and", (String) nameAnd);
			procedureQuery.setParameter("address", (String) address);
			procedureQuery.setParameter("status", (int) status);
			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			logger.info("/search_cust -" + " Get Result For Search Customer by name,address and nameAnd- "
					+ searchCustomer);

		}

		else if (name.trim().length() != 0 && department.trim().length() != 0 && nameAnd.trim().length() != 0
				&& address.trim().length() != 0) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_customer_by_name_department_and_address_or_nameand");
			procedureQuery.registerStoredProcedureParameter("name", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("name_and", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("department", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("address", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("status", Integer.class, ParameterMode.IN);

			procedureQuery.setParameter("name", (String) name);
			procedureQuery.setParameter("name_and", (String) nameAnd);
			procedureQuery.setParameter("department", (String) department);
			procedureQuery.setParameter("address", (String) address);
			procedureQuery.setParameter("status", (int) status);
			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			logger.info("/search_cust -" + " Get Result For Search Customer by all fields - " + searchCustomer);

		} else if (department.trim().length() == 0 && address.trim().length() == 0 && nameAnd.trim().length() == 0
				&& name.trim().length() != 0) {

			StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("search_and_modify_customer_by_name");
			procedureQuery.registerStoredProcedureParameter("name", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("status", Integer.class, ParameterMode.IN);

			procedureQuery.setParameter("name", (String) name);
			procedureQuery.setParameter("status", (int) status);
			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			logger.info("/search_cust -" + " Get Result For Search Customer by name- " + searchCustomer);

		}

		else if (name.trim().length() == 0 && address.trim().length() == 0 && nameAnd.trim().length() == 0
				&& department.trim().length() != 0) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_customer_by_department");
			procedureQuery.registerStoredProcedureParameter("department", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("status", Integer.class, ParameterMode.IN);

			procedureQuery.setParameter("department", (String) department);
			procedureQuery.setParameter("status", (int) status);
			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			logger.info("/search_cust -" + " Get Result For Search Customer by department- " + searchCustomer);

		}

		else if (name.trim().length() == 0 && department.trim().length() == 0 && nameAnd.trim().length() == 0
				&& address.trim().length() != 0) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_customer_by_address");
			procedureQuery.registerStoredProcedureParameter("address", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("status", Integer.class, ParameterMode.IN);

			procedureQuery.setParameter("address", (String) address);
			procedureQuery.setParameter("status", (int) status);
			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			logger.info("/search_cust -" + " Get Result For Search Customer by address- " + searchCustomer);

		}

		else if (name.trim().length() != 0 && department.trim().length() != 0 && nameAnd.trim().length() == 0
				&& address.trim().length() == 0) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_customer_by_name_and_department");
			procedureQuery.registerStoredProcedureParameter("name", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("department", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("status", Integer.class, ParameterMode.IN);

			procedureQuery.setParameter("name", (String) name);
			procedureQuery.setParameter("department", (String) department);
			procedureQuery.setParameter("status", (int) status);
			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			logger.info("/search_cust -" + " Get Result For Search Customer by name and department- " + searchCustomer);

		}

		else if (name.trim().length() != 0 && address.trim().length() != 0 && nameAnd.trim().length() == 0
				&& department.trim().length() == 0) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_customer_by_name_and_address");
			procedureQuery.registerStoredProcedureParameter("name", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("address", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("status", Integer.class, ParameterMode.IN);

			procedureQuery.setParameter("name", (String) name);
			procedureQuery.setParameter("address", (String) address);
			procedureQuery.setParameter("status", (int) status);
			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			logger.info("/search_cust -" + " Get Result For Search Customer by name and address - " + searchCustomer);

		}

		else if (address.trim().length() != 0 && department.trim().length() != 0 && nameAnd.trim().length() == 0
				&& name.trim().length() == 0) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_customer_by_department_and_address");
			procedureQuery.registerStoredProcedureParameter("department", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("address", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("status", Integer.class, ParameterMode.IN);

			procedureQuery.setParameter("department", (String) department);
			procedureQuery.setParameter("address", (String) address);
			procedureQuery.setParameter("status", (int) status);
			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			logger.info("/search_cust -" + " Get Result For Search Customer department and address- " + searchCustomer);

		}

		else if (name.trim().length() != 0 && department.trim().length() != 0 && address.trim().length() != 0
				&& nameAnd.trim().length() == 0) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_customer_by_name_department_and_address");
			procedureQuery.registerStoredProcedureParameter("name", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("department", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("address", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("status", Integer.class, ParameterMode.IN);

			procedureQuery.setParameter("name", (String) name);
			procedureQuery.setParameter("department", (String) department);
			procedureQuery.setParameter("address", (String) address);
			procedureQuery.setParameter("status", (int) status);
			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();

			logger.info("/search_cust -" + " Get Result For Search Customer by name,department and address- "
					+ searchCustomer);
		}



		else {
			resultValue = null;
		}

		return resultValue;

	}

	
	public Customer customerForGettingCustomerDetails(long id) throws ResourceNotFoundException {
		
		
		Customer customer = customerRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Customer not found for this id :: " + id));

		logger.info("/CCustDetails -" + id + " id For Getting Customer Details");


		return customer;
	}

	
	public String addCustomerAndTerm(List<CustomerTaxId> customerTaxId) throws IOException {

		List<CustomerTaxId> results2 = new ArrayList<>();
		for (CustomerTaxId a : customerTaxId) {
			System.out.println(a.getTaxId());
			System.out.println(a.getCustomerId());
		}

		for (CustomerTaxId a : customerTaxId) {
			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("add_customer_and_tax_id_in_customer_tax_junction_table");
			procedureQuery.registerStoredProcedureParameter("customerid", Long.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("taxid", Long.class, ParameterMode.IN);

			procedureQuery.setParameter("customerid", (long) a.getCustomerId());
			procedureQuery.setParameter("taxid", (long) a.getTaxId());
			procedureQuery.execute();
		}
		String response = "{\"message\":\"Post With ngResource: The customerid is added sucessfully: " + "\"}";

		logger.info("/AddCustTermsId - Store Customer id and Tax id in database table for customer");

		return response;
	}

	
	public List<BigInteger> getCustomerAndTerm( long id) {
		

		long custId = id;

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("tax_id_for_customerid_of_createcustomer_table");
		procedureQuery.registerStoredProcedureParameter("cust_id", Long.class, ParameterMode.IN);

		procedureQuery.setParameter("cust_id", (long) custId);
		procedureQuery.execute();

		@SuppressWarnings("unchecked")
		List<BigInteger> resultValueTax = procedureQuery.getResultList();

		
		logger.info("/getCustTaxId - Get tax Details of customer for id "+ custId);
		
		
		return resultValueTax;

	}
}
