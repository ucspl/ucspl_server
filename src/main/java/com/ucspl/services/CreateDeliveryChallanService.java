package com.ucspl.services;

import java.io.IOException;

import javax.persistence.StoredProcedureQuery;
import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateChallanCumInvoiceDocument;
import com.ucspl.model.CreateDeliveryChallan;
import com.ucspl.model.SearchChallanCumInvoiceDocument;
import com.ucspl.repository.CreateChallanCumInvoiceDocumentRepository;
import com.ucspl.repository.CreateDeliveryChallanRepository;

@Service
public class CreateDeliveryChallanService {
	
	private static final Logger logger = Logger.getLogger(CreateDeliveryChallanService.class);
	@Autowired
	private CreateDeliveryChallanRepository createDeliveryChallanRepositoryObject;

	@PersistenceContext
	private EntityManager em;

	private String docNoVariable;
	
	
	//////////////////////// doc no for invoice ///////////////////////////

	public String getDocumentNumber() throws IOException {

		Object documentNumberId;

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("document_number_for_delivery_challan_document");
		procedureQuery.execute();

		documentNumberId = procedureQuery.getSingleResult();
		String stringToConvert = String.valueOf(documentNumberId);
		String response = "{\"number\":\"" + stringToConvert + "\"}";

		logger.info("/DocNoForDeliChallan -" + docNoVariable
				+ " Document No For Delivery Challan");
		return response;
	}
	
	// Get Delivery Challan Header Information 
		
			public List< CreateDeliveryChallan> getAllCreatedDeliveryChallan() {
				
				logger.info("/createDeliChallan - Get All Delivery Challan Header Information!");
				
				return createDeliveryChallanRepositoryObject.findAll();
				
		}

		
		//Get Delivery Challan Header Information By Id
			public ResponseEntity<CreateDeliveryChallan> getAllCreatedDeliveryChallanById(
					@PathVariable(value = "id") Long createDeliveryChallanId) throws ResourceNotFoundException {
				CreateDeliveryChallan createDeliveryChallan = createDeliveryChallanRepositoryObject
					.findById(createDeliveryChallanId).orElseThrow(() -> new ResourceNotFoundException(
							"Document not found for this id :: " + createDeliveryChallanId));

			logger.info("/createDeliChallan/{id} - Get Delivery Challan Header Information By Id"
					+ createDeliveryChallanId);
			return ResponseEntity.ok().body(createDeliveryChallan);	
		
		}

		// Save Delivery Challan
		       public  CreateDeliveryChallan saveDeliveryChallan(
					@Valid @RequestBody CreateDeliveryChallan createDeliveryChallan) {
				logger.info("/createDeliChallan - Save Header Details of elivery Challan!");

				return createDeliveryChallanRepositoryObject.save(createDeliveryChallan);

			}
		

		// Update Delivery Challan for id
		       public ResponseEntity<CreateDeliveryChallan> updateDeliveryChallanById(
						@PathVariable(value = "id") Long createDeliveryChallanId,
						@Valid @RequestBody  CreateDeliveryChallan createDeliveryChallanDetails)
						throws ResourceNotFoundException {
		    	   CreateDeliveryChallan createDeliveryChallanObj = createDeliveryChallanRepositoryObject
					.findById(createDeliveryChallanId).orElseThrow(() -> new ResourceNotFoundException(
							"Document not found for this id :: " + createDeliveryChallanId));

		    createDeliveryChallanObj.setDocumentTypeId(createDeliveryChallanDetails.getDocumentTypeId());
		    createDeliveryChallanObj.setDcDocumentNumber(createDeliveryChallanDetails.getDcDocumentNumber());
		    createDeliveryChallanObj.setBranchId(createDeliveryChallanDetails.getBranchId());
		    createDeliveryChallanObj.setInvoiceTypeId(createDeliveryChallanDetails.getInvoiceTypeId());
		    createDeliveryChallanObj.setCustShippedTo(createDeliveryChallanDetails.getCustShippedTo());
		   // createDeliveryChallanObj.setCustBilledTo(createDeliveryChallanDetails.getCustBilledTo());
		    createDeliveryChallanObj.setInvoiceDate(createDeliveryChallanDetails.getInvoiceDate());
		    createDeliveryChallanObj.setPoDate(createDeliveryChallanDetails.getPoDate());
		    createDeliveryChallanObj.setDcDate(createDeliveryChallanDetails.getDcDate());
		    createDeliveryChallanObj.setPoNo(createDeliveryChallanDetails.getPoNo());
		    createDeliveryChallanObj.setDcNo(createDeliveryChallanDetails.getDcNo());
		    createDeliveryChallanObj.setRequestNumber(createDeliveryChallanDetails.getRequestNumber());
		    createDeliveryChallanObj.setVendorCodeNumber(createDeliveryChallanDetails.getVendorCodeNumber());
		    createDeliveryChallanObj.setArchieveDate(createDeliveryChallanDetails.getArchieveDate());
		    createDeliveryChallanObj.setCreatedBy(createDeliveryChallanDetails.getCreatedBy());
			createDeliveryChallanObj.setDraft(createDeliveryChallanDetails.getDraft());
			createDeliveryChallanObj.setApproved(createDeliveryChallanDetails.getApproved());
			createDeliveryChallanObj.setArchieved(createDeliveryChallanDetails.getArchieved());
			createDeliveryChallanObj.setSubmitted(createDeliveryChallanDetails.getSubmitted());
			createDeliveryChallanObj.setRejected(createDeliveryChallanDetails.getRejected());
			createDeliveryChallanObj.setRemark(createDeliveryChallanDetails.getRemark());
			createDeliveryChallanObj.setModeOfSubmission(createDeliveryChallanDetails.getModeOfSubmission());
			createDeliveryChallanObj.setDispatchMode(createDeliveryChallanDetails.getDispatchMode());
			createDeliveryChallanObj.setTotalQuantity(createDeliveryChallanDetails.getTotalQuantity());

			final CreateDeliveryChallan updatedDeliveryChallanDocument = createDeliveryChallanRepositoryObject
					.save(createDeliveryChallanObj);
			logger.info("/createInv/{id} - Update Delivery Challan Header Details for id "
					+ createDeliveryChallanId);

			return ResponseEntity.ok(updatedDeliveryChallanDocument);
		}
		
		

}
