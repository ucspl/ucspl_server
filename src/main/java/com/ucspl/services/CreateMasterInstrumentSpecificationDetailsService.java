
package com.ucspl.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateMasterInstrumentSpecificationDetails;
import com.ucspl.model.SearchMasterByMasterName;
import com.ucspl.model.SearchMasterInstruByMIdPNmAndStatus;
import com.ucspl.repository.CreateMasterInstrumentSpecificationDetailsRepository;

@Service
public class CreateMasterInstrumentSpecificationDetailsService {

	private static final Logger logger = LogManager.getLogger(CreateMasterInstrumentSpecificationDetailsService.class);

	SearchMasterByMasterName searchMasterByMasterName = new SearchMasterByMasterName();

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateMasterInstrumentSpecificationDetailsRepository createMasterInstrumentSpecificationDetailsRepository;

	public CreateMasterInstrumentSpecificationDetailsService() {

	}

	public List<CreateMasterInstrumentSpecificationDetails> getAllCreatedMasterSpecificationDetails() {

		logger.info("/crtMSpeD - Get All Created Master specification!");
		return createMasterInstrumentSpecificationDetailsRepository.findAll();
	}

	public ResponseEntity<CreateMasterInstrumentSpecificationDetails> getMasterSpecificationDetailsById(
			Long createMasterInstrumentSpecificationDetailsId) throws ResourceNotFoundException {
		CreateMasterInstrumentSpecificationDetails createMasterSpecificationDetails = createMasterInstrumentSpecificationDetailsRepository
				.findById(createMasterInstrumentSpecificationDetailsId)
				.orElseThrow(() -> new ResourceNotFoundException(" master specification not found for this id :: "
						+ createMasterInstrumentSpecificationDetailsId));

		logger.info("/crtMSpeD/{id} - Get master specification By Id!" + createMasterInstrumentSpecificationDetailsId);
		return ResponseEntity.ok().body(createMasterSpecificationDetails);
	}

	public ResponseEntity<CreateMasterInstrumentSpecificationDetails> updateMasterInstrumentSpecificationDetailsById(
			Long createMasterInstrumentSpecificationDetailId,
			CreateMasterInstrumentSpecificationDetails createMasterInstrumentSpecificationDetails)
			throws ResourceNotFoundException {
		CreateMasterInstrumentSpecificationDetails createMasterInstrumentSpecification = createMasterInstrumentSpecificationDetailsRepository
				.findById(createMasterInstrumentSpecificationDetailId).orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + createMasterInstrumentSpecificationDetailId));

		createMasterInstrumentSpecification
				.setCreateMasterInstruId(createMasterInstrumentSpecificationDetails.getCreateMasterInstruId());
		createMasterInstrumentSpecification.setParameterId(createMasterInstrumentSpecificationDetails.getParameterId());
		createMasterInstrumentSpecification.setRangeFrom(createMasterInstrumentSpecificationDetails.getRangeFrom());
		createMasterInstrumentSpecification.setRangeTo(createMasterInstrumentSpecificationDetails.getRangeTo());
		createMasterInstrumentSpecification.setRangeUom(createMasterInstrumentSpecificationDetails.getRangeUom());
		createMasterInstrumentSpecification.setFreqFrom(createMasterInstrumentSpecificationDetails.getFreqFrom());
		createMasterInstrumentSpecification.setFreqFromUom(createMasterInstrumentSpecificationDetails.getFreqFromUom());
		createMasterInstrumentSpecification.setFreqTo(createMasterInstrumentSpecificationDetails.getFreqTo());
		createMasterInstrumentSpecification.setFreqToUom(createMasterInstrumentSpecificationDetails.getFreqToUom());
		createMasterInstrumentSpecification.setLeastCount(createMasterInstrumentSpecificationDetails.getLeastCount());
		createMasterInstrumentSpecification
				.setLeastCountUom(createMasterInstrumentSpecificationDetails.getLeastCountUom());
		createMasterInstrumentSpecification.setAccuracy(createMasterInstrumentSpecificationDetails.getAccuracy());
		createMasterInstrumentSpecification
				.setFormulaAccuracy(createMasterInstrumentSpecificationDetails.getFormulaAccuracy());
		createMasterInstrumentSpecification.setUomAccuracy(createMasterInstrumentSpecificationDetails.getUomAccuracy());
		createMasterInstrumentSpecification.setAccuracy1(createMasterInstrumentSpecificationDetails.getAccuracy1());
		createMasterInstrumentSpecification
				.setFormulaAccuracy1(createMasterInstrumentSpecificationDetails.getFormulaAccuracy1());
		createMasterInstrumentSpecification
				.setUomAccuracy1(createMasterInstrumentSpecificationDetails.getUomAccuracy1());
		createMasterInstrumentSpecification
				.setTempChartType(createMasterInstrumentSpecificationDetails.getTempChartType());
		createMasterInstrumentSpecification.setModeType(createMasterInstrumentSpecificationDetails.getModeType());
		createMasterInstrumentSpecification.setDraft(createMasterInstrumentSpecificationDetails.getDraft());
		createMasterInstrumentSpecification.setApproved(createMasterInstrumentSpecificationDetails.getApproved());
		createMasterInstrumentSpecification.setArchieved(createMasterInstrumentSpecificationDetails.getArchieved());
		createMasterInstrumentSpecification.setSubmitted(createMasterInstrumentSpecificationDetails.getSubmitted());
		createMasterInstrumentSpecification.setRejected(createMasterInstrumentSpecificationDetails.getRejected());

		final CreateMasterInstrumentSpecificationDetails updatedMasterInstrumentSpecificationDetail = createMasterInstrumentSpecificationDetailsRepository
				.save(createMasterInstrumentSpecification);
		logger.info("/crtMSpeD/{id} - Update MasterInstrumentSpecificationDetail for id "
				+ createMasterInstrumentSpecificationDetailId);

		return ResponseEntity.ok(updatedMasterInstrumentSpecificationDetail);
	}

	
	
	
	
	public ResponseEntity<CreateMasterInstrumentSpecificationDetails> updateMasterInstrumentSpecificationDetailsByIdForApprove(
			Long createMasterInstrumentSpecificationDetailId,
			CreateMasterInstrumentSpecificationDetails createMasterInstrumentSpecificationDetails)
			throws ResourceNotFoundException {
		CreateMasterInstrumentSpecificationDetails createMasterInstrumentSpecification = createMasterInstrumentSpecificationDetailsRepository
				.findById(createMasterInstrumentSpecificationDetailId).orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + createMasterInstrumentSpecificationDetailId));
	
		createMasterInstrumentSpecification.setMasterIdParaName(createMasterInstrumentSpecificationDetails.getMasterIdParaName());
	//	createMasterInstrumentSpecification.setParameterNumber(createMasterInstrumentSpecificationDetails.getParameterNumber());   //comment this line 
		createMasterInstrumentSpecification.setDraft(createMasterInstrumentSpecificationDetails.getDraft());
		createMasterInstrumentSpecification.setApproved(createMasterInstrumentSpecificationDetails.getApproved());
		createMasterInstrumentSpecification.setSubmitted(createMasterInstrumentSpecificationDetails.getSubmitted());

		final CreateMasterInstrumentSpecificationDetails updatedMasterInstrumentSpecificationDetail = createMasterInstrumentSpecificationDetailsRepository
				.save(createMasterInstrumentSpecification);
		logger.info("/crtMSpeD/{id} - Update MasterInstrumentSpecificationDetail for id to approve the specification"
				+ createMasterInstrumentSpecificationDetailId);

		return ResponseEntity.ok(updatedMasterInstrumentSpecificationDetail);
	}
	
	
	public ResponseEntity<CreateMasterInstrumentSpecificationDetails> updateMasterInstrumentSpecificationDetailsByIdForParaNumber(
			Long createMasterInstrumentSpecificationDetailId,
			CreateMasterInstrumentSpecificationDetails createMasterInstrumentSpecificationDetails)
			throws ResourceNotFoundException {
		CreateMasterInstrumentSpecificationDetails createMasterInstrumentSpecification = createMasterInstrumentSpecificationDetailsRepository
				.findById(createMasterInstrumentSpecificationDetailId).orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + createMasterInstrumentSpecificationDetailId));
	
		
		
		createMasterInstrumentSpecification.setParameterNumber(createMasterInstrumentSpecificationDetails.getParameterNumber());

		final CreateMasterInstrumentSpecificationDetails updatedMasterInstrumentSpecificationDetail = createMasterInstrumentSpecificationDetailsRepository
				.save(createMasterInstrumentSpecification);
		logger.info("/crtMSpeDFrParaNo/{id} - Update MasterInstrumentSpecificationDetail parameter number of specification"
				+ createMasterInstrumentSpecificationDetailId);

		return ResponseEntity.ok(updatedMasterInstrumentSpecificationDetail);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public CreateMasterInstrumentSpecificationDetails createMasterInstrumentSpecificationDetail(
			CreateMasterInstrumentSpecificationDetails createMasterInstrumentSpecification) {

		logger.info("/crtMSpeD - Save the master specification!");
		return createMasterInstrumentSpecificationDetailsRepository.save(createMasterInstrumentSpecification);
	}

	// Delete User Details for id
	public Map<String, Boolean> deleteMasterInstrumentSpecificationDetails(Long masterInstrumentDetailId)
			throws ResourceNotFoundException {

		Map<String, Boolean> response = new HashMap<>();
		createMasterInstrumentSpecificationDetailsRepository.deleteById(masterInstrumentDetailId);
		response.put("deleted", Boolean.TRUE);
		logger.info("/crtMSpeD/{id} - Delete Master Instrument Detail of id - " + masterInstrumentDetailId);

		return response;
	}

////////////////////////////sp for getting sr no for master instruments ////////////////////////////////  

	public List<Object[]> srNoDetailsOfMasterSpecification(long masterId) {

		System.out.println("master id" + masterId);
		logger.info("/srnoMSDet -" + " Search master specification Details For master id" + masterId);

		List<Object[]> resultValue;

		StoredProcedureQuery procedureQuery = em
				.createStoredProcedureQuery("srno_master_instru_specification_detail_list");
		procedureQuery.registerStoredProcedureParameter("id_no", Long.class, ParameterMode.IN);

		procedureQuery.setParameter("id_no", (long) masterId);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

		return resultValue;
	}

////////////////////////////sp for getting sr no for master instruments by masteridparaname ////////////////////////////////  

	public List<Object[]> srNoDetailsOfMasterSpecificationByMasterIdParaName(
			SearchMasterInstruByMIdPNmAndStatus masterIdParaNameObj) {

		System.out.println("masterid paraname" + masterIdParaNameObj.getMsIdParaName());
		logger.info("/srnoMSDetByMipn -" + " Search master specification Details For masterIdParaName"
				+ masterIdParaNameObj.getMsIdParaName());

		List<Object[]> resultValue = null;

		if (masterIdParaNameObj.getMsIdParaName() != null && masterIdParaNameObj.getDraft() == 1
				|| masterIdParaNameObj.getApproved() == 1 || masterIdParaNameObj.getSubmitted() == 1
				|| masterIdParaNameObj.getRejected() == 1 || masterIdParaNameObj.getArchieved() == 1) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("srno_master_instru_specification_detail_list_by_masteridparaname");

			procedureQuery.registerStoredProcedureParameter("mastidparanm", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("draft", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("approved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("submitted", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("rejected", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("archieved", Integer.class, ParameterMode.IN);

			procedureQuery.setParameter("mastidparanm", (String) masterIdParaNameObj.getMsIdParaName());
			procedureQuery.setParameter("draft", (int) masterIdParaNameObj.getDraft());
			procedureQuery.setParameter("approved", (int) masterIdParaNameObj.getApproved());
			procedureQuery.setParameter("submitted", (int) masterIdParaNameObj.getSubmitted());
			procedureQuery.setParameter("rejected", (int) masterIdParaNameObj.getRejected());
			procedureQuery.setParameter("archieved", (int) masterIdParaNameObj.getArchieved());

			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();

		}

		return resultValue;
	}

}

 

