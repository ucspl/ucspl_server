

package com.ucspl.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ucspl.controller.CreateMasterInstrumentDetailsController;
import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateMasterInstrumentDetails;
import com.ucspl.model.SearchMasterByMasterName;
import com.ucspl.repository.CreateMasterInstrumentDetailsRepository;

@Service
public class CreateMasterInstrumentDetailsService {

	private static final Logger logger = Logger.getLogger(CreateMasterInstrumentDetailsService.class);

	@Autowired
	private CreateMasterInstrumentDetailsRepository createMasterInstrumentDetailsRepository;

	@PersistenceContext
	private EntityManager em;
	
	public CreateMasterInstrumentDetailsService() {

	} 

	public List<CreateMasterInstrumentDetails> getAllCreateMasterInstrumentDetails() {
		logger.info("/crtMInstruDet - Get all Created Master Instrument Details!");

		return createMasterInstrumentDetailsRepository.findAll();
	}
	
	public ResponseEntity<CreateMasterInstrumentDetails> getCreateMasterInstrumentDetailsById(
			Long createMasterInstrumentDetailsId) throws ResourceNotFoundException {
		CreateMasterInstrumentDetails createMasterInstrumentDetail = createMasterInstrumentDetailsRepository
				.findById(createMasterInstrumentDetailsId).orElseThrow(() -> new ResourceNotFoundException(
						" Request and inward not found for this id :: " + createMasterInstrumentDetailsId));

		logger.info("/crtMInstruDet/{id} - Get create Master Instrument Details By Id!" + createMasterInstrumentDetailsId);

		return ResponseEntity.ok().body(createMasterInstrumentDetail);
	}


	public ResponseEntity<CreateMasterInstrumentDetails> updateMasterInstrumentDetailsById(
			 Long createMasterInstrumentDetailsId,
			 CreateMasterInstrumentDetails createMasterInstrumentDetails) throws ResourceNotFoundException {
		CreateMasterInstrumentDetails createMasterInstrument = createMasterInstrumentDetailsRepository
				.findById(createMasterInstrumentDetailsId).orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + createMasterInstrumentDetailsId));

		createMasterInstrument.setCalibrationLabId(createMasterInstrumentDetails.getCalibrationLabId());
		createMasterInstrument.setIdNo(createMasterInstrumentDetails.getIdNo());
		createMasterInstrument.setPersonHolding(createMasterInstrumentDetails.getPersonHolding());
		createMasterInstrument.setMasterName(createMasterInstrumentDetails.getMasterName());
		createMasterInstrument.setInstrumentTypeId(createMasterInstrumentDetails.getInstrumentTypeId());
		createMasterInstrument.setCalibrationFrequency(createMasterInstrumentDetails.getCalibrationFrequency());
		createMasterInstrument.setSrNo(createMasterInstrumentDetails.getSrNo());
		createMasterInstrument.setRangeTypeId(createMasterInstrumentDetails.getRangeTypeId());
		createMasterInstrument.setPurchasedFrom(createMasterInstrumentDetails.getPurchasedFrom());
		createMasterInstrument.setMake(createMasterInstrumentDetails.getMake());
		createMasterInstrument.setMasterTypeId(createMasterInstrumentDetails.getMasterTypeId());
		createMasterInstrument.setDateOfPurchase(createMasterInstrumentDetails.getDateOfPurchase());
		createMasterInstrument.setModel(createMasterInstrumentDetails.getModel());
		createMasterInstrument.setReadingOverloading(createMasterInstrumentDetails.getReadingOverloading());
		createMasterInstrument.setDateOfInstallation(createMasterInstrumentDetails.getDateOfInstallation());
		createMasterInstrument.setLcUomChart(createMasterInstrumentDetails.getLcUomChart());
		createMasterInstrument.setStatus(createMasterInstrumentDetails.getStatus());
		createMasterInstrument.setBranchId(createMasterInstrumentDetails.getBranchId());
		createMasterInstrument.setDraft(createMasterInstrumentDetails.getDraft());
		createMasterInstrument.setApproved(createMasterInstrumentDetails.getApproved());
		createMasterInstrument.setArchieved(createMasterInstrumentDetails.getArchieved());
		createMasterInstrument.setSubmitted(createMasterInstrumentDetails.getSubmitted());
		createMasterInstrument.setRejected(createMasterInstrumentDetails.getRejected());

		final CreateMasterInstrumentDetails updatedMasterInstrumentDetails = createMasterInstrumentDetailsRepository
				.save(createMasterInstrument);
		logger.info("/crtMInstruDet/{id} - Update Master Instrument Details for id " + createMasterInstrumentDetailsId);

		return ResponseEntity.ok(updatedMasterInstrumentDetails);
	}
	
	public CreateMasterInstrumentDetails createMasterInstrumentDetail(
			CreateMasterInstrumentDetails createMasterInstrumentDetails) {
			
		logger.info("/crtMInstruDet - Save Master Instrument Details!");
		
		return createMasterInstrumentDetailsRepository.save(createMasterInstrumentDetails);

	}
	
// code for search and modify master instrument
	public List<Object[]> searchAndModifyMasterInstrument(SearchMasterByMasterName searchMasterInstruObject) {
	
		logger.info("/searchMI - Search master instrument For " + searchMasterInstruObject.getSearchDocument());
		System.out.println(searchMasterInstruObject);
			
		List<Object[]> resultValue;

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("search_and_modify_master_instrument");
		procedureQuery.registerStoredProcedureParameter("search_document", String.class, ParameterMode.IN);
		procedureQuery.setParameter("search_document", (String)searchMasterInstruObject.getSearchDocument());

		System.out.println("object is "+searchMasterInstruObject.getSearchDocument());
		procedureQuery.execute();
		resultValue = procedureQuery.getResultList();
		logger.info("/searchMI -" + " Get Result For Search master instrument scope- "+ searchMasterInstruObject.getSearchDocument());

		return resultValue;
				
	}

}