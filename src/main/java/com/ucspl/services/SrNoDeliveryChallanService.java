package com.ucspl.services;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.ChallanCumInvoiceDocument;
import com.ucspl.model.SrNoDeliveryChallan;
import com.ucspl.repository.SrNoDeliveryChallanRepository;

@Service
public class SrNoDeliveryChallanService {

	private static final Logger logger = Logger.getLogger(SrNoDeliveryChallanService.class);

	@Autowired
	SrNoDeliveryChallanRepository srNoDeliveryChallanRepositoryObject;
	
	@PersistenceContext
	private EntityManager em;


	// Get All Sr No Information for Delivery Challan
	public List<SrNoDeliveryChallan> getAllSrNoInformationForDeliveryChallan() {

		logger.info("/createSrNoDeliChallan -  Get All Sr No Information for Delivery Challan");

		return srNoDeliveryChallanRepositoryObject.findAll();
	}

	// Get Sr No Information for Delivery Challan By Id
	public ResponseEntity<SrNoDeliveryChallan> getAllSrNoInformationForDeliveryChallanById(
			@PathVariable(value = "id") Long srNoDeliveryChallanId) throws ResourceNotFoundException {
		SrNoDeliveryChallan srNoDeliveryChallan = srNoDeliveryChallanRepositoryObject.findById(srNoDeliveryChallanId)
				.orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + srNoDeliveryChallanId));

		logger.info("/createSrNoDeliChallan/{id} - Get Sr No Information for Delivery Challan By Id!"
				+ srNoDeliveryChallanId);
		return ResponseEntity.ok().body(srNoDeliveryChallan);
	}

	// Save Sr No Information for Delivery Challan
	public SrNoDeliveryChallan saveSrNoInformationForDeliveryChallan(@Valid @RequestBody SrNoDeliveryChallan srNoData) {

		logger.info("/createSrNoDeliChallan - Save Sr No Information for Delivery Challan !");
		return srNoDeliveryChallanRepositoryObject.save(srNoData);

	}

	// Update Sr No Information for Delivery Challan for id
	public ResponseEntity<SrNoDeliveryChallan> updateSrNoInformationForDeliveryChallanById(
			@PathVariable(value = "id") Long srNoTableId, @Valid @RequestBody SrNoDeliveryChallan srNoDetails)
			throws ResourceNotFoundException {

		SrNoDeliveryChallan srNoDeliveryChallanObject = srNoDeliveryChallanRepositoryObject.findById(srNoTableId)
				.orElseThrow(() -> new ResourceNotFoundException("Document not found for this id :: " + srNoDetails));

		srNoDeliveryChallanObject.setDeliChallanDocumentNumber(srNoDetails.getDeliChallanDocumentNumber());
		srNoDeliveryChallanObject.setDeliChallanNumber(srNoDetails.getDeliChallanNumber());
		srNoDeliveryChallanObject.setSrNo(srNoDetails.getSrNo());
		// srNoDeliveryChallanObject.setPoSrNo(srNoDetails.getPoSrNo());
		srNoDeliveryChallanObject.setItemOrPartCode(srNoDetails.getItemOrPartCode());
		srNoDeliveryChallanObject.setInstruId(srNoDetails.getInstruId());
		srNoDeliveryChallanObject.setDescription(srNoDetails.getDescription());
		srNoDeliveryChallanObject.setRangeFrom(srNoDetails.getRangeFrom());
		srNoDeliveryChallanObject.setRangeTo(srNoDetails.getRangeTo());
		srNoDeliveryChallanObject.setSacCode(srNoDetails.getSacCode());
		srNoDeliveryChallanObject.setHsnNo(srNoDetails.getHsnNo());
		srNoDeliveryChallanObject.setQuantity(srNoDetails.getQuantity());
		// srNoDeliveryChallanObject.setUnitPrice(srNoDetails.getUnitPrice());
		// srNoDeliveryChallanObject.setAmount(srNoDetails.getAmount());

		final SrNoDeliveryChallan updatedSrNoDeliveryChallan = srNoDeliveryChallanRepositoryObject
				.save(srNoDeliveryChallanObject);
		logger.info(
				"/createSrNoDeliChallan/{id} -  Update Sr No Information for Delivery Challan for id " + srNoTableId);

		return ResponseEntity.ok(updatedSrNoDeliveryChallan);
	}

	// Update unique Doc no in Sr No Delivery Challan table
	@PutMapping("/updateUniqueInvNo/{id}")
	public ResponseEntity<SrNoDeliveryChallan> updateSrNoInformationForDeliveryChallanByUniqueDocNo(
			@PathVariable(value = "id") Long srNoTableId, @Valid @RequestBody SrNoDeliveryChallan uniqueNo)
			throws ResourceNotFoundException {

		SrNoDeliveryChallan SrNoDeliveryChallanObj1 = srNoDeliveryChallanRepositoryObject.findById(srNoTableId)
				.orElseThrow(() -> new ResourceNotFoundException("Document not found for this id :: " + srNoTableId));

		SrNoDeliveryChallanObj1.setUniqueNo(uniqueNo.getUniqueNo());

		final SrNoDeliveryChallan updatedUniqueNumber = srNoDeliveryChallanRepositoryObject
				.save(SrNoDeliveryChallanObj1);
		logger.info("/updateUniqueInvNo/{id} -  Update unique Doc no in Sr No  Delivery Challan table " + srNoTableId);

		return ResponseEntity.ok(updatedUniqueNumber);
	}

////////////////////store procedure to get request Info For Calibration Tag,dc tag and Invoice Tag for cust Id/////////

	public List<Object[]> getRequestDetailByCalibrationDcAndInvoiceTagForCustId(Integer number) throws IOException {

		List<Object[]> resultValue;

		StoredProcedureQuery procedureQuery = em
				.createStoredProcedureQuery("get_req_data_with_calibration_dc_and_invoice_tag_through_custid");
		procedureQuery.registerStoredProcedureParameter("cust_id", Integer.class, ParameterMode.IN);

		procedureQuery.setParameter("cust_id", (Integer) number);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

		logger.info(
				"/reqInfoForCaliDeliAndInvoiceTagForCustId - get request and inward data with calibration tag,dc tag and invoice tag through customer id "
						+ number);

		return resultValue;
	}

      ////////////////////store procedure to get request Info For Calibration Tag,dc tag and Invoice Tag for Request document Number/////////

	public List<Object[]> trialgetRequestDetailByCalibrationDcAndInvoiceTagForReqDocNo(String docNumber)
			throws IOException {

		List<Object[]> resultValue;

		StoredProcedureQuery procedureQuery = em
				.createStoredProcedureQuery("get_req_data_with_calibration_dc_and_invoice_tag_through_reqdocno");
		procedureQuery.registerStoredProcedureParameter("doc_no", String.class, ParameterMode.IN);

		procedureQuery.setParameter("doc_no", (String) docNumber);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

		logger.info(
				"/trialreqInfoForCaliDeliAndInvoiceTagForReqDocNo - get request Info For Calibration Tag,dc tag and Invoice Tag for Request document Number "
						+ docNumber);

		return resultValue;
	}

//store procedure to get delivery challan data with dc tag for dc document number
	public List<Object> getAllDeliveryChallanDataForDcDocNo(@Valid @RequestBody String number) throws IOException {

		List<Object> resultValue;

		StoredProcedureQuery procedureQuery = em
				.createStoredProcedureQuery("get_all_dc_data_with_dc_tag_for_delivery_challan_doc_no");
		procedureQuery.registerStoredProcedureParameter("doc_no", String.class, ParameterMode.IN);

		procedureQuery.setParameter("doc_no", (String) number);
		// procedureQuery.setParameter("cust_id", (String) number);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

		logger.info(
				"/getAllDcDataWithDcTagForDcDocNo -get delivery challan data with dc tag for dc document number  " + number);

		return resultValue;
	}
	
	
	// store procedure to get sr no deli challan data deli challan document document no
	
		public List<Object> getSrNoDataForDeliChallanDocumentNo(@Valid @RequestBody String number) throws IOException {

			List<Object> resultValue;

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("get_sr_no_data_for_deli_challan_document_number");
			procedureQuery.registerStoredProcedureParameter("doc_no", String.class, ParameterMode.IN);

			procedureQuery.setParameter("doc_no", (String) number);
			// procedureQuery.setParameter("cust_id", (String) number);
			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();

			logger.info(
					"/srNoDataForDeliChallanDoc -get sr no deli challan data deli challan document document no  " + number);

			return resultValue;
		}
		
		
		//////////////////////////////////////////////////////////set dc tag//////////////////////////////////////////////////////
		
		// set invoice tag and unique no to zero for invoice doc no
		public String setInvAndUniNoToZeroForInvDocNo(@Valid @RequestBody String documentNO) throws IOException {
			StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("set_invoicetag_and_uniqueno_to_zero");
			procedureQuery.registerStoredProcedureParameter("invdocno", String.class, ParameterMode.IN);

			procedureQuery.setParameter("invdocno", (String) documentNO);
			procedureQuery.execute();

			String response = "{\"message\":\"Post With ngResource: data deleted sucessfully: " + documentNO
					+ "\"}";

			logger.info("/removeAllSrNoForInvDocNo - set invoice tag and unique no to zero for invoice doc no " + documentNO);

			return response;

			
		}
		
		
		// set invoice tag to one for instru through reqDocId                   requestNo,instruName,reqDocId,invoiceDocNo,invoiceId,invUniqueNo
		public String trialsetDcTagToOneForInstru(@Valid @RequestBody String reqNo,String instruName,String reqDocNo1,String invoiceDocNo,String invoiceId1, String invUniqueNo) throws IOException {
			StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("set_invoice_tag_to_one_for_instru_name");
			long reqDocNo=Long.parseLong(reqDocNo1);
			long invoiceId=Long.parseLong(invoiceId1);
			
			procedureQuery.registerStoredProcedureParameter("reqno", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("instruname", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("reqdocid", Long.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("invdocno", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("invoiceid", Long.class, ParameterMode.IN);
			
			procedureQuery.registerStoredProcedureParameter("invuniqueno", String.class, ParameterMode.IN);
			

			procedureQuery.setParameter("reqno", (String) reqNo);
			procedureQuery.setParameter("instruname", (String) instruName);
			procedureQuery.setParameter("reqdocid", (Long) reqDocNo);
			procedureQuery.setParameter("invdocno", (String) invoiceDocNo);
			procedureQuery.setParameter("invoiceid", (Long) invoiceId);
			procedureQuery.setParameter("invuniqueno", (String) invUniqueNo);
			procedureQuery.execute();

			String response = "{\"message\":\"Post With ngResource: tag set successfully: " + reqNo+instruName+reqDocNo+invoiceDocNo+invoiceId
					+ "\"}";

			logger.info("/trialsetInvoiceTagToOneForInstru - set invoice tag to 1 for instrument " + reqNo+" "+instruName+" "+reqDocNo+" "+invoiceDocNo+" "+invoiceId);

			return response;

			
		}
		
		// set invoice tag to one for instru through InvId
		public String trialSetInvoiceTagToOneForInstruThroughInvId(@Valid @RequestBody String reqNo,String instruName,String reqDocNo1,String invoiceDocNo,String invoiceId1) throws IOException {
			StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("set_invoice_tag_to_one_for_instru_name_through_inv_id");
			long reqDocNo=Long.parseLong(reqDocNo1);
			long invoiceId=Long.parseLong(invoiceId1);
			
			procedureQuery.registerStoredProcedureParameter("reqno", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("instruname", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("reqdocid", Long.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("invdocno", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("invoiceid", Long.class, ParameterMode.IN);

			procedureQuery.setParameter("reqno", (String) reqNo);
			procedureQuery.setParameter("instruname", (String) instruName);
			procedureQuery.setParameter("reqdocid", (Long) reqDocNo);
			procedureQuery.setParameter("invdocno", (String) invoiceDocNo);
			procedureQuery.setParameter("invoiceid", (Long) invoiceId);
			procedureQuery.execute();

			String response = "{\"message\":\"Post With ngResource: tag set successfully: " + reqNo+instruName+reqDocNo+invoiceDocNo+invoiceId
					+ "\"}";

			logger.info("/trialsetInvoiceTagToOneForInstruThroughInvId - set invoice tag to 1 for instrument " + reqNo+" "+instruName+" "+reqDocNo+" "+invoiceDocNo+" "+invoiceId);

			return response;

			
		}
		
		
		// set invoice tag to Zero for instru   instruName,reqDocId,invoiceDocNo,invoiceId,invUniqueNo
		public String trialsetDcTagToZeroForInstru(@Valid @RequestBody String instruName,String reqDocId1,String invoiceDocNo,String invoiceId1, String invUniqueNo) throws IOException {
			StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("set_invoice_tag_to_zero_for_instru_name");
			long reqDocNo=Long.parseLong(reqDocId1);
			long invoiceId=Long.parseLong(invoiceId1);
			
			//procedureQuery.registerStoredProcedureParameter("reqno", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("instruname", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("reqdocid", Long.class, ParameterMode.IN);
	        //procedureQuery.registerStoredProcedureParameter("invdocno", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("invoiceid", Long.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("invuniqueno", String.class, ParameterMode.IN);
			
			//procedureQuery.setParameter("reqno", (String) reqNo);
			procedureQuery.setParameter("instruname", (String) instruName);
	        procedureQuery.setParameter("reqdocid", (Long) reqDocNo);
			//procedureQuery.setParameter("invdocno", (String) invoiceDocNo);
	         procedureQuery.setParameter("invoiceid", (Long) invoiceId);
	         procedureQuery.setParameter("invuniqueno", (String) invUniqueNo);
			procedureQuery.execute();

			String response = "{\"message\":\"Post With ngResource: tag set successfully: " + instruName+invoiceDocNo+invoiceId
					+ "\"}";

			logger.info("/trialsetInvoiceTagToZeroForInstru - set invoice tag to 0 for instrument " + instruName+" "+invoiceDocNo+" "+invoiceId);

			return response;

			
		}
		
		///// set invoice tag to one for range
		public String trialsetDcTagToOneForRange(@Valid @RequestBody String reqNo,String instruName,String reqDocNo1,String invoiceDocNo,String invoiceId1,String rangeFrom,String rangeTo, String invUniqueNo) throws IOException {
			StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("set_invoice_tag_to_one_for_range");
			long reqDocNo=Long.parseLong(reqDocNo1);
			long invoiceId=Long.parseLong(invoiceId1);
			
			procedureQuery.registerStoredProcedureParameter("reqno", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("instruname", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("reqdocid", Long.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("invdocno", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("invoiceid", Long.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("rangefrom", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("rangeto", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("invuniqueno", String.class, ParameterMode.IN);
			
			procedureQuery.setParameter("reqno", (String) reqNo);
			procedureQuery.setParameter("instruname", (String) instruName);
			procedureQuery.setParameter("reqdocid", (Long) reqDocNo);
			procedureQuery.setParameter("invdocno", (String) invoiceDocNo);
			procedureQuery.setParameter("invoiceid", (Long) invoiceId);
			procedureQuery.setParameter("rangefrom", (String) rangeFrom);
			procedureQuery.setParameter("rangeto", (String) rangeTo);
			  procedureQuery.setParameter("invuniqueno", (String) invUniqueNo);
			
			procedureQuery.execute();

			String response = "{\"message\":\"Post With ngResource: tag set successfully: " + reqNo+instruName+reqDocNo+invoiceDocNo+invoiceId+rangeFrom+rangeTo
					+ "\"}";

			logger.info("/trialsetInvoiceTagToOneForRange - set invoice tag to 1 for range " + reqNo+" "+instruName+" "+reqDocNo+" "+invoiceDocNo+" "+invoiceId+" "+rangeFrom+" "+rangeTo+" "+invUniqueNo);

			return response;

			
		}
		
		///////// set invoice tag to Zero for range
		public String trialsetDcTagToZeroForRange(@Valid @RequestBody String instruName,String invoiceDocNo,String reqDocId1,String rangeFrom,String rangeTo, String invUniqueNo) throws IOException {
			StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("set_invoice_tag_to_zero_for_range");
			//long reqDocNo=Long.parseLong(reqDocNo1);
			long reqDocId=Long.parseLong(reqDocId1);
			
			//procedureQuery.registerStoredProcedureParameter("reqno", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("instruname", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("reqdocid", Long.class, ParameterMode.IN);
		//	procedureQuery.registerStoredProcedureParameter("invdocno", String.class, ParameterMode.IN);
			//procedureQuery.registerStoredProcedureParameter("invoiceid", Long.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("rangefrom", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("rangeto", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("invuniqueno", String.class, ParameterMode.IN);

			//procedureQuery.setParameter("reqno", (String) reqNo);
			procedureQuery.setParameter("instruname", (String) instruName);
			procedureQuery.setParameter("reqdocid", (Long) reqDocId);
			//procedureQuery.setParameter("invdocno", (String) invoiceDocNo);
			//procedureQuery.setParameter("invoiceid", (Long) invoiceId);
			procedureQuery.setParameter("rangefrom", (String) rangeFrom);
			procedureQuery.setParameter("rangeto", (String) rangeTo);
			  procedureQuery.setParameter("invuniqueno", (String) invUniqueNo);
			
			procedureQuery.execute();

			String response = "{\"message\":\"Post With ngResource: tag set successfully: " + instruName+invoiceDocNo+reqDocId+rangeFrom+rangeTo
					+ "\"}";

			logger.info("/trialsetInvoiceTagToZeroForRange - set invoice tag to 0 for range " + instruName+" "+invoiceDocNo+" "+reqDocId+" "+rangeFrom+" "+rangeTo+" "+invUniqueNo);

			return response;

			
		}
		
		public String setInvoiceTagToZeroForRequestNoAndInvNo(String data, String data1) throws IOException {
		//	String response = "";

			StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("set_invoice_tag_to_zero_for_invdoc_and_reqdoc");
			
			procedureQuery.registerStoredProcedureParameter("reqdocno", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("invdocno", String.class, ParameterMode.IN);
			
			procedureQuery.setParameter("reqdocno", (String) data1);
			procedureQuery.setParameter("invdocno", (String) data);
			procedureQuery.execute();
			
			logger.info("/setInvTagToZeroForReqNoAndInvNo - set_invoice_tag_to_zero_for_invdoc_and_reqdoc " + data+" "+data1);
			
			String response = "{\"message\":\"Post With ngResource: tag set successfully: " + data+" "+data1
					+ "\"}";

			
			return response;
		}


}
