package com.ucspl.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ucspl.controller.CustomerInstrumentIdentificationController;
import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CustomerInstrumentIdentification;
import com.ucspl.repository.CustomerInstrumentIdentificationRepository;

@Service
public class CustomerInstrumentIdentificationService {

	
	private static final Logger logger = LogManager.getLogger(CustomerInstrumentIdentificationController.class);

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CustomerInstrumentIdentificationRepository customerInstrumentIdentificationRepository;


	public CustomerInstrumentIdentificationService() {
		// TODO Auto-generated constructor stub
	}


	public List<CustomerInstrumentIdentification> getAllCustomerInstrumentIdentification() {

		logger.info("/cusInsruIdenty - Get All Created Customer Instrument Identification!");
		return customerInstrumentIdentificationRepository.findAll();
	}


	public ResponseEntity<CustomerInstrumentIdentification> getCustomerInstrumentIdentificationByCustInstruId(
			Long CustomerInstrumentIdentificationId) throws ResourceNotFoundException {
		CustomerInstrumentIdentification customerInstrumentIdentification = customerInstrumentIdentificationRepository
				.findByreqDocId(CustomerInstrumentIdentificationId);

		logger.info("/cusInsruIdenty/{id} - Get Customer Instrument Identification By Req Iwd Id!" + CustomerInstrumentIdentificationId);

		return ResponseEntity.ok().body(customerInstrumentIdentification);	
	}
	

	public CustomerInstrumentIdentification createCustomerInstrumentIdentification(
			CustomerInstrumentIdentification customerInstrumentIdentification) {

		logger.info("/cusInsruIdenty - Save the customer Instrument Identification!");

		return customerInstrumentIdentificationRepository.save(customerInstrumentIdentification);
	}
	

	public ResponseEntity<CustomerInstrumentIdentification> updateCustomerInstrumentIdentification(
			Long customerInstrumentIdentificationId,
			CustomerInstrumentIdentification customerInstrumentIdentificationDetails)
			throws ResourceNotFoundException {
		CustomerInstrumentIdentification customerInstrumentIdentification = customerInstrumentIdentificationRepository
				.findById(customerInstrumentIdentificationId).orElseThrow(() -> new ResourceNotFoundException(
						"Customer Instrument Identification not found for this id :: " + customerInstrumentIdentificationId));

		customerInstrumentIdentification.setReqDocId(customerInstrumentIdentificationDetails.getReqDocId());
		customerInstrumentIdentification.setCustId(customerInstrumentIdentificationDetails.getCustId());
		customerInstrumentIdentification.setNomenclatureId(customerInstrumentIdentificationDetails.getNomenclatureId());
		customerInstrumentIdentification.setIdNo(customerInstrumentIdentificationDetails.getIdNo());
		customerInstrumentIdentification.setSrNo(customerInstrumentIdentificationDetails.getSrNo());
		
		final CustomerInstrumentIdentification updatedCustomerInstrumentIdentification = customerInstrumentIdentificationRepository
				.save(customerInstrumentIdentification);

		logger.info("/cusInsruIdenty/{id} - Update Customer Instrument Identification Details for id " + customerInstrumentIdentificationId);

		return ResponseEntity.ok(updatedCustomerInstrumentIdentification);
	}

	
	
}
