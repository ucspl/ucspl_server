package com.ucspl.services;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateChallanCumInvoiceDocument;
import com.ucspl.model.SearchChallanCumInvoiceDocument;
import com.ucspl.repository.CreateChallanCumInvoiceDocumentRepository;

@Service
public class CreateChallanCumInvoiceDocumentService {

	private static final Logger logger = Logger.getLogger(CreateChallanCumInvoiceDocumentService.class);
	@Autowired
	private CreateChallanCumInvoiceDocumentRepository createChallanCumInvoiceDocumentRepository;

	@PersistenceContext
	private EntityManager em;

	private String docNoVariable;

	private SearchChallanCumInvoiceDocument searchChallanCumInvoiceDocument = new SearchChallanCumInvoiceDocument();

	// Get Challan Cum Invoice Document Header Information By Id
	public List<CreateChallanCumInvoiceDocument> getAllCreateChallanCumInvoiceDocument() {

		logger.info("/createInv - Get All Challan Cum Invoice Document Header Information!");
		return createChallanCumInvoiceDocumentRepository.findAll();
	}

//  Get Challan Cum Invoice Document Header Information By Id
	public ResponseEntity<CreateChallanCumInvoiceDocument> getCreateChallanCumInvoiceDocumentById(
			@PathVariable(value = "id") Long createChallanCumInvoiceDocumentId) throws ResourceNotFoundException {
		CreateChallanCumInvoiceDocument createChallanCumInvoiceDocument = createChallanCumInvoiceDocumentRepository
				.findById(createChallanCumInvoiceDocumentId).orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + createChallanCumInvoiceDocumentId));

		logger.info("/createInv/{id} - Get Challan Cum Invoice Document Header Information By Id!"
				+ createChallanCumInvoiceDocumentId);
		return ResponseEntity.ok().body(createChallanCumInvoiceDocument);
	}

	// Save Header Details of Challan Cum Invoice Document
	public CreateChallanCumInvoiceDocument createChallanCumInvoiceDocument(
			@Valid @RequestBody CreateChallanCumInvoiceDocument createChallanCumInvoiceDocument) {

		logger.info("/createInv - Save Header Details of Challan Cum Invoice Document!");
		return createChallanCumInvoiceDocumentRepository.save(createChallanCumInvoiceDocument);

	}

	// Update Challan Cum Invoice Document Header Details for id
	public ResponseEntity<CreateChallanCumInvoiceDocument> updateCreateChallanCumInvoiceDocumentById(
			@PathVariable(value = "id") Long createChallanCumInvoiceDocumentId,
			@Valid @RequestBody CreateChallanCumInvoiceDocument createChallanCumInvoiceDocumentDetails)
			throws ResourceNotFoundException {
		CreateChallanCumInvoiceDocument createChallanCumInvoiceDocument = createChallanCumInvoiceDocumentRepository
				.findById(createChallanCumInvoiceDocumentId).orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + createChallanCumInvoiceDocumentId));

		createChallanCumInvoiceDocument.setDocumentTypeId(createChallanCumInvoiceDocumentDetails.getDocumentTypeId());
		createChallanCumInvoiceDocument.setInvDocumentNumber(createChallanCumInvoiceDocumentDetails.getInvDocumentNumber());
		createChallanCumInvoiceDocument.setBranchId(createChallanCumInvoiceDocumentDetails.getBranchId());
		createChallanCumInvoiceDocument.setInvoiceTypeId(createChallanCumInvoiceDocumentDetails.getInvoiceTypeId());
		createChallanCumInvoiceDocument.setCustShippedTo(createChallanCumInvoiceDocumentDetails.getCustShippedTo());
		createChallanCumInvoiceDocument.setCustBilledTo(createChallanCumInvoiceDocumentDetails.getCustBilledTo());
		createChallanCumInvoiceDocument.setInvoiceDate(createChallanCumInvoiceDocumentDetails.getInvoiceDate());
		createChallanCumInvoiceDocument.setPoDate(createChallanCumInvoiceDocumentDetails.getPoDate());
		createChallanCumInvoiceDocument.setDcDate(createChallanCumInvoiceDocumentDetails.getDcDate());
		createChallanCumInvoiceDocument.setPoNo(createChallanCumInvoiceDocumentDetails.getPoNo());
		createChallanCumInvoiceDocument.setDcNo(createChallanCumInvoiceDocumentDetails.getDcNo());
		createChallanCumInvoiceDocument.setRequestNumber(createChallanCumInvoiceDocumentDetails.getRequestNumber());
		createChallanCumInvoiceDocument.setVendorCodeNumber(createChallanCumInvoiceDocumentDetails.getVendorCodeNumber());
		createChallanCumInvoiceDocument.setArchieveDate(createChallanCumInvoiceDocumentDetails.getArchieveDate());
		createChallanCumInvoiceDocument.setCreatedBy(createChallanCumInvoiceDocumentDetails.getCreatedBy());
		createChallanCumInvoiceDocument.setDraft(createChallanCumInvoiceDocumentDetails.getDraft());
		createChallanCumInvoiceDocument.setApproved(createChallanCumInvoiceDocumentDetails.getApproved());
		createChallanCumInvoiceDocument.setArchieved(createChallanCumInvoiceDocumentDetails.getArchieved());
		createChallanCumInvoiceDocument.setSubmitted(createChallanCumInvoiceDocumentDetails.getSubmitted());
		createChallanCumInvoiceDocument.setRejected(createChallanCumInvoiceDocumentDetails.getRejected());
		createChallanCumInvoiceDocument.setRemark(createChallanCumInvoiceDocumentDetails.getRemark());

		final CreateChallanCumInvoiceDocument updatedCreateChallanCumInvoiceDocument = createChallanCumInvoiceDocumentRepository
				.save(createChallanCumInvoiceDocument);
		logger.info("/createInv/{id} - Update Challan Cum Invoice Document Header Details for id "
				+ createChallanCumInvoiceDocumentId);

		return ResponseEntity.ok(updatedCreateChallanCumInvoiceDocument);
	}

	//////////////////////// doc no for invoice ///////////////////////////

	public String getDocumentNumber() throws IOException {

		Object documentNumberId;

		StoredProcedureQuery procedureQuery = em
				.createStoredProcedureQuery("document_number_for_challan_cum_invoice_document");
		procedureQuery.execute();

		documentNumberId = procedureQuery.getSingleResult();
		String stringToConvert = String.valueOf(documentNumberId);
		String response = "{\"number\":\"" + stringToConvert + "\"}";

		logger.info("/DocNoForInv -" + docNoVariable
				+ " Document No For Getting Challan Cum Invoice Document Header Details");
		return response;
	}

	// code for search and modify Invoice

	@RequestMapping(value = "/searchInvs", method = RequestMethod.POST, produces = "application/json")
	public String searchAndModifyChallanCumInvoiceDocument(
			@Valid @RequestBody SearchChallanCumInvoiceDocument searchChallanCumInvoiceDoc) {

		searchChallanCumInvoiceDocument = searchChallanCumInvoiceDoc;
		searchChallanCumInvoiceDocument = searchChallanCumInvoiceDoc;

		String response = "{\"message\":\"Post With ngResource: The user firstName: "
				+ searchChallanCumInvoiceDocument.getSearchDocument() + "\"}";

		logger.info("/searchInvs -" + " Search Challan Cum Invoice Documents For " + searchChallanCumInvoiceDocument);

		return response;
	}

	///// Result For Search Invoice Document
	public List<Object[]> getAllChallanCumInvoiceDocument() {

		List<Object[]> resultValue;

		// search by document number
		if (searchChallanCumInvoiceDocument.getSearchDocument() != null
				&& searchChallanCumInvoiceDocument.getDraft() == 0 && searchChallanCumInvoiceDocument.getApproved() == 0
				&& searchChallanCumInvoiceDocument.getSubmitted() == 0
				&& searchChallanCumInvoiceDocument.getRejected() == 0
				&& searchChallanCumInvoiceDocument.getArchieved() == 0) {
			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_invoice_document_by_only_docname");
			procedureQuery.registerStoredProcedureParameter("searchdocument", String.class, ParameterMode.IN);

			procedureQuery.setParameter("searchdocument", (String) searchChallanCumInvoiceDocument.getSearchDocument());

			procedureQuery.execute();
			
			resultValue = procedureQuery.getResultList();

			// System.out.println("get doc by only document name");
			logger.info("/searchInvs -" + " Get Result For Search Invoice Document by docname only - "
					+ searchChallanCumInvoiceDocument.getSearchDocument());

			return resultValue;
		}

		// search by document number and status
		else if (searchChallanCumInvoiceDocument.getSearchDocument() != null
				&& searchChallanCumInvoiceDocument.getDraft() == 1 || searchChallanCumInvoiceDocument.getApproved() == 1
				|| searchChallanCumInvoiceDocument.getSubmitted() == 1
				|| searchChallanCumInvoiceDocument.getRejected() == 1
				|| searchChallanCumInvoiceDocument.getArchieved() == 1
						&& searchChallanCumInvoiceDocument.getFromDate() == null
						&& searchChallanCumInvoiceDocument.getToDate() == null) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_invoice_document_by_docname_and_status");
			procedureQuery.registerStoredProcedureParameter("searchdocument", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("draft", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("approved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("submitted", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("rejected", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("archieved", Integer.class, ParameterMode.IN);

			procedureQuery.setParameter("searchdocument", (String) searchChallanCumInvoiceDocument.getSearchDocument());
			procedureQuery.setParameter("draft", (int) searchChallanCumInvoiceDocument.getDraft());
			procedureQuery.setParameter("approved", (int) searchChallanCumInvoiceDocument.getApproved());
			procedureQuery.setParameter("submitted", (int) searchChallanCumInvoiceDocument.getSubmitted());
			procedureQuery.setParameter("rejected", (int) searchChallanCumInvoiceDocument.getRejected());
			procedureQuery.setParameter("archieved", (int) searchChallanCumInvoiceDocument.getArchieved());

			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();

			logger.info("/searchInvs -" + " Get Result For Search Invoice Document by docname and status- ");
			return resultValue;

		}
		// search by status and date
		else if (searchChallanCumInvoiceDocument.getSearchDocument() == null
				&& searchChallanCumInvoiceDocument.getFromDate() != null
				&& searchChallanCumInvoiceDocument.getToDate() != null
				&& searchChallanCumInvoiceDocument.getDraft() == 1 || searchChallanCumInvoiceDocument.getApproved() == 1
				|| searchChallanCumInvoiceDocument.getSubmitted() == 1
				|| searchChallanCumInvoiceDocument.getRejected() == 1
				|| searchChallanCumInvoiceDocument.getArchieved() == 1) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_invoice_document_by_status_and_date");

			procedureQuery.registerStoredProcedureParameter("draft", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("approved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("submitted", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("rejected", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("archieved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("fromdate", java.util.Date.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("todate", java.util.Date.class, ParameterMode.IN);

			procedureQuery.setParameter("draft", (int) searchChallanCumInvoiceDocument.getDraft());
			procedureQuery.setParameter("approved", (int) searchChallanCumInvoiceDocument.getApproved());
			procedureQuery.setParameter("submitted", (int) searchChallanCumInvoiceDocument.getSubmitted());
			procedureQuery.setParameter("rejected", (int) searchChallanCumInvoiceDocument.getRejected());
			procedureQuery.setParameter("archieved", (int) searchChallanCumInvoiceDocument.getArchieved());
			procedureQuery.setParameter("fromdate", (java.util.Date) searchChallanCumInvoiceDocument.getFromDate());
			procedureQuery.setParameter("todate", (java.util.Date) searchChallanCumInvoiceDocument.getToDate());

			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
	
			logger.info("/searchSales -" + " Get Result For Search Invoice Document by status and date - ");
			return resultValue;

		}
		// search by from date and to date
		else if (searchChallanCumInvoiceDocument.getSearchDocument() == null
				&& searchChallanCumInvoiceDocument.getFromDate() != null
				&& searchChallanCumInvoiceDocument.getToDate() != null
				&& searchChallanCumInvoiceDocument.getDraft() == 0 || searchChallanCumInvoiceDocument.getApproved() == 0
				|| searchChallanCumInvoiceDocument.getSubmitted() == 0
				|| searchChallanCumInvoiceDocument.getRejected() == 0
				|| searchChallanCumInvoiceDocument.getArchieved() == 0) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_invoice_document_by_from_date_and_to_date");

			procedureQuery.registerStoredProcedureParameter("fromdate", java.util.Date.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("todate", java.util.Date.class, ParameterMode.IN);

			procedureQuery.setParameter("fromdate", (java.util.Date) searchChallanCumInvoiceDocument.getFromDate());
			procedureQuery.setParameter("todate", (java.util.Date) searchChallanCumInvoiceDocument.getToDate());

			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			logger.info("/searchSales -" + " Get Result For Search Invoice Document by from date and to date- ");

			return resultValue;

		}

		// search by document number,status,from date and to date
		else if (searchChallanCumInvoiceDocument.getSearchDocument() != null
				&& searchChallanCumInvoiceDocument.getFromDate() != null
				&& searchChallanCumInvoiceDocument.getToDate() != null
				&& searchChallanCumInvoiceDocument.getDraft() == 1 || searchChallanCumInvoiceDocument.getApproved() == 1
				|| searchChallanCumInvoiceDocument.getSubmitted() == 1
				|| searchChallanCumInvoiceDocument.getRejected() == 1
				|| searchChallanCumInvoiceDocument.getArchieved() == 1) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_invoice_document_by_status_and_date_and_name");
			procedureQuery.registerStoredProcedureParameter("searchdocument", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("draft", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("approved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("submitted", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("rejected", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("archieved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("fromdate", java.util.Date.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("todate", java.util.Date.class, ParameterMode.IN);

			procedureQuery.setParameter("searchdocument", (String) searchChallanCumInvoiceDocument.getSearchDocument());
			procedureQuery.setParameter("draft", (int) searchChallanCumInvoiceDocument.getDraft());
			procedureQuery.setParameter("approved", (int) searchChallanCumInvoiceDocument.getApproved());
			procedureQuery.setParameter("submitted", (int) searchChallanCumInvoiceDocument.getSubmitted());
			procedureQuery.setParameter("rejected", (int) searchChallanCumInvoiceDocument.getRejected());
			procedureQuery.setParameter("archieved", (int) searchChallanCumInvoiceDocument.getArchieved());
			procedureQuery.setParameter("fromdate", (java.util.Date) searchChallanCumInvoiceDocument.getFromDate());
			procedureQuery.setParameter("todate", (java.util.Date) searchChallanCumInvoiceDocument.getToDate());

			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			logger.info("/searchSales -" + " Get Result For Search Invoice Document by status and date - ");

			return resultValue;
		}

		else {
			return null;
		}
	}

//////////////////////////////STORE PROCEDURE FOR GETTING DETAILS OF CREATE Invoice  FOR SEARCH Invoice /////////////////////////////////////////////  

	@RequestMapping(value = "/CInvsDet", method = RequestMethod.POST, produces = "application/json")
	public String docNoForGettingCreateChallanCumInvoiceDocumentDetails(@Valid @RequestBody String docNo) {

		docNoVariable = docNo;
		String response = "{\"message\":\" " + docNoVariable + "\"}";
		logger.info("/CInvsDet -" + " Search Challan Cum Invoice Document Details For Document Number" + docNoVariable);
		return response;
	}

	@GetMapping(value = "/CInvsDet", produces = "application/json")
	public List<Object[]> gettingCreateChallanCumInvoiceDocumentDetails() {

		List<Object[]> resultValue;
		String docNo = docNoVariable;

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("create_invoice_details_for_search_invs");
		procedureQuery.registerStoredProcedureParameter("doc_no", String.class, ParameterMode.IN);

		procedureQuery.setParameter("doc_no", (String) docNo);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

		logger.info("/CInvsDet -" + "Get Header Details For Search Challan Cum Invoice Document - " + docNo);
		return resultValue;
	}
}
