package com.ucspl.services;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateSalesQuotation;
import com.ucspl.model.CreateSalesQuotationRepository;
import com.ucspl.model.DocumentNumberForSalesAndQuotation;
import com.ucspl.model.SearchSalesAndQuotation;

@Service
public class CreateSalesQuotationService {

	private static final Logger logger = Logger.getLogger(CreateSalesQuotationService.class);

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateSalesQuotationRepository createSalesQuotationRepository;

	private String docNoVariable;

	private SearchSalesAndQuotation searchSalesAndQuotation = new SearchSalesAndQuotation();

	// Get all sales quotation document information
	public List<CreateSalesQuotation> getAllCreatedSalesQuotation() {

		logger.info("/createSales - Get All Created Sales Quotation Document Information!");
		return createSalesQuotationRepository.findAll();
	}

	// Get create Sales Detail for id
	public ResponseEntity<CreateSalesQuotation> getSalesQuotationById(
			@PathVariable(value = "id") Long createSalesQuotationId) throws ResourceNotFoundException {

		CreateSalesQuotation createSalesQuotation = createSalesQuotationRepository.findById(createSalesQuotationId)
				.orElseThrow(() -> new ResourceNotFoundException(
						"Sales Quotation not found for this id :: " + createSalesQuotationId));

		logger.info("/createSales/{id} - Get Create Sales Quotation Document Screen Information By Id!"
				+ createSalesQuotationId);
		return ResponseEntity.ok().body(createSalesQuotation);
	}

	// save create sales details
	public CreateSalesQuotation createSalesQuotation(@Valid @RequestBody CreateSalesQuotation createsalesquotation) {

		logger.info("/createSales - Save Details of Create Sales Quotation Screen!");

		return createSalesQuotationRepository.save(createsalesquotation);
	}

	// Update Create Sales Quotation Document Screen Details for id
	public ResponseEntity<CreateSalesQuotation> updateSalesQuotation(@PathVariable(value = "id") Long salesId,
			@Valid @RequestBody CreateSalesQuotation createSalesQuotationDetails) throws ResourceNotFoundException {

		CreateSalesQuotation createSalesQuotation = createSalesQuotationRepository.findById(salesId).orElseThrow(
				() -> new ResourceNotFoundException("Sales Quotation not found for this id :: " + salesId));

		createSalesQuotation.setTypeOfQuotation(createSalesQuotationDetails.getTypeOfQuotation());
		createSalesQuotation.setDocumentNumber(createSalesQuotationDetails.getDocumentNumber());
		createSalesQuotation.setDateOfQuotation(createSalesQuotationDetails.getDateOfQuotation());
		createSalesQuotation.setQuotationNumber(createSalesQuotationDetails.getQuotationNumber());
		createSalesQuotation.setCustomerId(createSalesQuotationDetails.getCustomerId());
		createSalesQuotation.setBranchId(createSalesQuotationDetails.getBranchId());
		createSalesQuotation.setKindAttachment(createSalesQuotationDetails.getKindAttachment());
		createSalesQuotation.setCustRefNo(createSalesQuotationDetails.getCustRefNo());
		createSalesQuotation.setRefDate(createSalesQuotationDetails.getRefDate());
		createSalesQuotation.setArchieveDate(createSalesQuotationDetails.getArchieveDate());
		createSalesQuotation.setCountryPrefixForMobile(createSalesQuotationDetails.getCountryPrefixForMobile());
		createSalesQuotation.setContactNo(createSalesQuotationDetails.getContactNo());
		createSalesQuotation.setEmail(createSalesQuotationDetails.getEmail());
		createSalesQuotation.setQuotationItemFile(createSalesQuotationDetails.getQuotationItemFile());
		createSalesQuotation.setTerms(createSalesQuotationDetails.getTerms());
		createSalesQuotation.setEnquiryAttachment(createSalesQuotationDetails.getEnquiryAttachment());
		createSalesQuotation.setTermsAndCondition(createSalesQuotationDetails.getTermsAndCondition());
		createSalesQuotation.setDraft(createSalesQuotationDetails.getDraft());
		createSalesQuotation.setSubmitted(createSalesQuotationDetails.getSubmitted());
		createSalesQuotation.setRejected(createSalesQuotationDetails.getRejected());
		createSalesQuotation.setArchieved(createSalesQuotationDetails.getArchieved());
		createSalesQuotation.setApproved(createSalesQuotationDetails.getApproved());
		createSalesQuotation.setRemark(createSalesQuotationDetails.getRemark());
		createSalesQuotation.setCreatedBy(createSalesQuotationDetails.getCreatedBy());

		final CreateSalesQuotation updatedCreateSalesQuotation = createSalesQuotationRepository
				.save(createSalesQuotation);

		logger.info("/createSales/{id} - Update Create Sales Quotation Document Screen Details for id " + salesId);

		return ResponseEntity.ok(updatedCreateSalesQuotation);
	}

	// code for customer_and_terms_junction
	public String addCustomerAndTerm(@Valid @RequestBody DocumentNumberForSalesAndQuotation documentCustomerAndTermsId)
			throws IOException {
		String response;
		String remark = documentCustomerAndTermsId.getRemark();

		if (remark.trim().length() != 0) {
			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("add_customer_and_term_id_in_customer_and_terms_junction_table");
			procedureQuery.registerStoredProcedureParameter("documentid", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("customerid", Long.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("termsid", Long.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("box1", Long.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("box2", Long.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("remark", String.class, ParameterMode.IN);

			procedureQuery.setParameter("documentid", (String) documentCustomerAndTermsId.getDocumentNumber());
			procedureQuery.setParameter("customerid", (long) documentCustomerAndTermsId.getCustomerId());
			procedureQuery.setParameter("termsid", (long) documentCustomerAndTermsId.getTermsId());
			procedureQuery.setParameter("box1", (long) documentCustomerAndTermsId.getBox1());
			procedureQuery.setParameter("box2", (long) documentCustomerAndTermsId.getBox2());
			procedureQuery.setParameter("remark", (String) documentCustomerAndTermsId.getRemark());
			procedureQuery.execute();

			response = "{\"message\":\"Post With ngResource: The customerid is added sucessfully: "
					+ documentCustomerAndTermsId.getCustomerId() + "\"}";

			logger.info("/AddCustTermsId - Store Customer id and Terms id in database table for document "
					+ documentCustomerAndTermsId.getDocumentNumber());
		}

		else if (remark.trim().length() == 0) {
			StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery(
					"add_customer_and_term_id_without_remark_in_customer_and_terms_junction_table");
			procedureQuery.registerStoredProcedureParameter("documentid", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("customerid", Long.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("termsid", Long.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("box1", Long.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("box2", Long.class, ParameterMode.IN);

			procedureQuery.setParameter("documentid", (String) documentCustomerAndTermsId.getDocumentNumber());
			procedureQuery.setParameter("customerid", (long) documentCustomerAndTermsId.getCustomerId());
			procedureQuery.setParameter("termsid", (long) documentCustomerAndTermsId.getTermsId());
			procedureQuery.setParameter("box1", (long) documentCustomerAndTermsId.getBox1());
			procedureQuery.setParameter("box2", (long) documentCustomerAndTermsId.getBox2());

			procedureQuery.execute();

			response = "{\"message\":\"Post With ngResource: The customerid is added sucessfully: "
					+ documentCustomerAndTermsId.getCustomerId() + "\"}";

			logger.info("/AddCustTermsId - Store Customer id and Terms id in database table for document "
					+ documentCustomerAndTermsId.getDocumentNumber());

		} else {

			response = null;
		}
		return response;
	}

	// code to delete single term
	public String removeCustomerAndTerm(
			@Valid @RequestBody DocumentNumberForSalesAndQuotation documentCustomerAndTermsId) throws IOException {

		StoredProcedureQuery procedureQuery = em
				.createStoredProcedureQuery("remove_customer_and_term_id_from_customer_and_terms_junction_table");
		procedureQuery.registerStoredProcedureParameter("documentid", String.class, ParameterMode.IN);
		procedureQuery.registerStoredProcedureParameter("customerid", Integer.class, ParameterMode.IN);
		procedureQuery.registerStoredProcedureParameter("termsid", Integer.class, ParameterMode.IN);

		procedureQuery.setParameter("documentid", (String) documentCustomerAndTermsId.getDocumentNumber());
		procedureQuery.setParameter("customerid", (int) documentCustomerAndTermsId.getCustomerId());
		procedureQuery.setParameter("termsid", (int) documentCustomerAndTermsId.getTermsId());
		procedureQuery.execute();

		String response = "{\"message\":\"Post With ngResource: The customerid is added sucessfully: "
				+ documentCustomerAndTermsId.getCustomerId() + "\"}";

		logger.info("/DelCustTermsId - Remove Customer id and Terms id in database table for document "
				+ documentCustomerAndTermsId.getDocumentNumber());

		return response;

	}

	// remove all terms
	public String removeAllCustomerAndTerm(@Valid @RequestBody String documentId) throws IOException {

		StoredProcedureQuery procedureQuery = em
				.createStoredProcedureQuery("remove_all_customer_and_term_id_from_customer_and_terms_junction_table");
		procedureQuery.registerStoredProcedureParameter("documentid", String.class, ParameterMode.IN);

		procedureQuery.setParameter("documentid", (String) documentId);
		procedureQuery.execute();

		String response = "{\"message\":\"Post With ngResource: The customerid is added sucessfully: " + documentId
				+ "\"}";

		logger.info(
				"/DelCustTermsId - Remove all Customer id and Terms id in database table for document " + documentId);

		return response;

	}

	// Get last saved document number
	public String getDocumentNumber() throws IOException {

		Object documentNumberId;

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("document_number_for_sales_and_quotation");
		procedureQuery.execute();

		documentNumberId = procedureQuery.getSingleResult();
		String stringToConvert = String.valueOf(documentNumberId);
		String response = "{\"number\":\"" + stringToConvert + "\"}";

		return response;
	}

	// code for search and modify sales and quotation
	public String searchAndModifySalesAndQuotation(
			@Valid @RequestBody SearchSalesAndQuotation searchSalesAndQuotation1) {

		searchSalesAndQuotation = searchSalesAndQuotation1;

		String response = "{\"message\":\"Post With ngResource: The user firstname: "
				+ searchSalesAndQuotation1.getSearchDocument() + "\"}";

		logger.info("/searchSales - Search Sales quotation Documents For " + searchSalesAndQuotation);

		return response;
	}

	// Result For Search Sales quotation Document
	public List<Object[]> getAllSalesAndQuotation() {

		List<Object[]> resultValue;

		// search by document number
		if (searchSalesAndQuotation.getSearchDocument() != null && searchSalesAndQuotation.getDraft() == 0
				&& searchSalesAndQuotation.getApproved() == 0 && searchSalesAndQuotation.getSubmitted() == 0
				&& searchSalesAndQuotation.getRejected() == 0 && searchSalesAndQuotation.getArchieved() == 0) {
			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_sales_and_quotation_by_only_docname");
			procedureQuery.registerStoredProcedureParameter("searchdocument", String.class, ParameterMode.IN);

			procedureQuery.setParameter("searchdocument", (String) searchSalesAndQuotation.getSearchDocument());

			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();

			logger.info("/searchSales -" + " Get Result For Search Sales quotation Document by docname only - "
					+ searchSalesAndQuotation.getSearchDocument());

			return resultValue;
		}

		// search by document number and status
		else if (searchSalesAndQuotation.getSearchDocument() != null && searchSalesAndQuotation.getDraft() == 1
				|| searchSalesAndQuotation.getApproved() == 1 || searchSalesAndQuotation.getSubmitted() == 1
				|| searchSalesAndQuotation.getRejected() == 1
				|| searchSalesAndQuotation.getArchieved() == 1 && searchSalesAndQuotation.getFromDate() == null
						&& searchSalesAndQuotation.getToDate() == null) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_sales_and_quotation_by_docname_and_status");
			procedureQuery.registerStoredProcedureParameter("searchdocument", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("draft", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("approved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("submitted", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("rejected", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("archieved", Integer.class, ParameterMode.IN);

			procedureQuery.setParameter("searchdocument", (String) searchSalesAndQuotation.getSearchDocument());
			procedureQuery.setParameter("draft", (int) searchSalesAndQuotation.getDraft());
			procedureQuery.setParameter("approved", (int) searchSalesAndQuotation.getApproved());
			procedureQuery.setParameter("submitted", (int) searchSalesAndQuotation.getSubmitted());
			procedureQuery.setParameter("rejected", (int) searchSalesAndQuotation.getRejected());
			procedureQuery.setParameter("archieved", (int) searchSalesAndQuotation.getArchieved());

			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			logger.info("/searchSales -" + " Get Result For Search Sales quotation Document by docname and status- "
					+ searchSalesAndQuotation.getSearchDocument());

			return resultValue;

		}
		// search by status and date
		else if (searchSalesAndQuotation.getSearchDocument() == null && searchSalesAndQuotation.getFromDate() != null
				&& searchSalesAndQuotation.getToDate() != null && searchSalesAndQuotation.getDraft() == 1
				|| searchSalesAndQuotation.getApproved() == 1 || searchSalesAndQuotation.getSubmitted() == 1
				|| searchSalesAndQuotation.getRejected() == 1 || searchSalesAndQuotation.getArchieved() == 1) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_sales_and_quotation_by_status_and_date");

			procedureQuery.registerStoredProcedureParameter("draft", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("approved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("submitted", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("rejected", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("archieved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("fromdate", java.util.Date.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("todate", java.util.Date.class, ParameterMode.IN);

			procedureQuery.setParameter("draft", (int) searchSalesAndQuotation.getDraft());
			procedureQuery.setParameter("approved", (int) searchSalesAndQuotation.getApproved());
			procedureQuery.setParameter("submitted", (int) searchSalesAndQuotation.getSubmitted());
			procedureQuery.setParameter("rejected", (int) searchSalesAndQuotation.getRejected());
			procedureQuery.setParameter("archieved", (int) searchSalesAndQuotation.getArchieved());
			procedureQuery.setParameter("fromdate", (java.util.Date) searchSalesAndQuotation.getFromDate());
			procedureQuery.setParameter("todate", (java.util.Date) searchSalesAndQuotation.getToDate());

			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			logger.info("/searchSales -" + " Get Result For Search Sales quotation Document by status and date - ");

			return resultValue;
		}
		// search by from date and to date
		else if (searchSalesAndQuotation.getSearchDocument() == null && searchSalesAndQuotation.getFromDate() != null
				&& searchSalesAndQuotation.getToDate() != null && searchSalesAndQuotation.getDraft() == 0
				|| searchSalesAndQuotation.getApproved() == 0 || searchSalesAndQuotation.getSubmitted() == 0
				|| searchSalesAndQuotation.getRejected() == 0 || searchSalesAndQuotation.getArchieved() == 0) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_sales_and_quotation_by_from_date_and_to_date");

			procedureQuery.registerStoredProcedureParameter("fromdate", java.util.Date.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("todate", java.util.Date.class, ParameterMode.IN);

			procedureQuery.setParameter("fromdate", (java.util.Date) searchSalesAndQuotation.getFromDate());
			procedureQuery.setParameter("todate", (java.util.Date) searchSalesAndQuotation.getToDate());

			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			logger.info(
					"/searchSales -" + " Get Result For Search Sales quotation Document by from date and to date- ");

			return resultValue;

		}
		// search by document number,status,from date and to date
		else if (searchSalesAndQuotation.getSearchDocument() != null && searchSalesAndQuotation.getFromDate() != null
				&& searchSalesAndQuotation.getToDate() != null && searchSalesAndQuotation.getDraft() == 1
				|| searchSalesAndQuotation.getApproved() == 1 || searchSalesAndQuotation.getSubmitted() == 1
				|| searchSalesAndQuotation.getRejected() == 1 || searchSalesAndQuotation.getArchieved() == 1) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_sales_and_quotation_by_status_and_date_and_name");
			procedureQuery.registerStoredProcedureParameter("searchdocument", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("draft", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("approved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("submitted", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("rejected", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("archieved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("fromdate", java.util.Date.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("todate", java.util.Date.class, ParameterMode.IN);

			procedureQuery.setParameter("searchdocument", (String) searchSalesAndQuotation.getSearchDocument());
			procedureQuery.setParameter("draft", (int) searchSalesAndQuotation.getDraft());
			procedureQuery.setParameter("approved", (int) searchSalesAndQuotation.getApproved());
			procedureQuery.setParameter("submitted", (int) searchSalesAndQuotation.getSubmitted());
			procedureQuery.setParameter("rejected", (int) searchSalesAndQuotation.getRejected());
			procedureQuery.setParameter("archieved", (int) searchSalesAndQuotation.getArchieved());
			procedureQuery.setParameter("fromdate", (java.util.Date) searchSalesAndQuotation.getFromDate());
			procedureQuery.setParameter("todate", (java.util.Date) searchSalesAndQuotation.getToDate());

			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			logger.info("/searchSales -" + " Get Result For Search Sales quotation Document by status and date - ");

			return resultValue;

		}

		else {
			return null;
		}
	}

    // STORE PROCEDURE FOR GETTING DETAILS OF CREATE SALES FOR SEARCH SALES
	public String docNoForGettingCreateSalesDetails(@Valid @RequestBody String docNo) {

		docNoVariable = docNo;
		System.out.println("doc no" + docNoVariable);

		String response = "{\"message\":\" " + docNoVariable + "\"}";
		logger.info("/CSalesDet -" + " Search Sales quotation document Details For Document Number" + docNoVariable);

		return response;
	}

	public List<Object[]> getCreateSalesDetailsForSearchSales() {

		List<Object[]> resultValueSet;
		String docNo = docNoVariable;
		System.out.println("document number" + docNo);

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("create_sales_details_for_search_sales");
		procedureQuery.registerStoredProcedureParameter("doc_no", String.class, ParameterMode.IN);

		procedureQuery.setParameter("doc_no", (String) docNo);

		System.out.println("document number" + docNo);
		procedureQuery.execute();

		resultValueSet = procedureQuery.getResultList();

		logger.info(
				"/CSalesDet -" + "Get Details of Create Sales quotation screen For Search Sales document - " + docNo);

		return resultValueSet;
	}

}
