package com.ucspl.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CustomerContactList;
import com.ucspl.repository.CustomerContactListRepository;

@Service
public class CustomerContactListService {

	private static final Logger logger = Logger.getLogger(CustomerContactListService.class);

	@Autowired
	private CustomerContactListRepository customerContactListRepository;

	@PersistenceContext
	private EntityManager em;

	public List<CustomerContactList> getAllCustomerContactList() {

		logger.info("/custConList - Get All Created Customer Contact List!");
		return customerContactListRepository.findAll();
	}

	public ResponseEntity<CustomerContactList> getCustomerContactById(Long customerContactId)
			throws ResourceNotFoundException {
		CustomerContactList customerContactList = customerContactListRepository.findById(customerContactId).orElseThrow(
				() -> new ResourceNotFoundException(" customer Contact not found for this id :: " + customerContactId));

		logger.info("/custConList/{id} - Get customer Contact By Id!" + customerContactId);

		return ResponseEntity.ok().body(customerContactList);
	}

	public CustomerContactList createCustomerContact(CustomerContactList customerContactList) {

		logger.info("/custConList - Save the Customer Contact!");

		return customerContactListRepository.save(customerContactList);

	}
	

	public ResponseEntity<CustomerContactList> updateCustomerContactById(Long customerContactId,
			CustomerContactList customerContactDetails) throws ResourceNotFoundException {
		CustomerContactList customerContactList = customerContactListRepository.findById(customerContactId).orElseThrow(
				() -> new ResourceNotFoundException("Customer Contact not found for this id :: " + customerContactId));

		customerContactList.setCustomerId(customerContactDetails.getCustomerId());
		customerContactList.setPersonName(customerContactDetails.getPersonName());
		customerContactList.setDepartment(customerContactDetails.getDepartment());
		customerContactList.setJobTitle(customerContactDetails.getJobTitle());
		customerContactList.setMobileNumber1(customerContactDetails.getMobileNumber1());
		customerContactList.setMobileNumber2(customerContactDetails.getMobileNumber2());
		customerContactList.setPhoneNumber(customerContactDetails.getPhoneNumber());
		customerContactList.setEmail(customerContactDetails.getEmail());
		customerContactList.setStatus(customerContactDetails.getStatus());

		final CustomerContactList updatedCustomerContact = customerContactListRepository.save(customerContactList);

		logger.info("/custConList/{id} - Update Customer Contact Details for id " + customerContactId);
		return ResponseEntity.ok(updatedCustomerContact);
	}

	// Delete contact list Details for id
	public Map<String, Boolean> deleteCustomerContactList(Long customerContactId) throws ResourceNotFoundException {
		CustomerContactList customerContactList = customerContactListRepository.findById(customerContactId).orElseThrow(
				() -> new ResourceNotFoundException("Customer Contact not found for this id :: " + customerContactId));

		customerContactListRepository.delete(customerContactList);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);

		logger.info("/custConList/{id} - Delete Customer Contact Details of id - " + customerContactId);

		return response;

	}

	// get customer contact data for cust id ///////////////////////////////////////
	public List<CustomerContactList> docNoForGettingSrNo(long custId) {

		logger.info("/conListForCustId -" + custId + " Customer id For Getting Customer Contact Details");

		List<CustomerContactList> resultValue;

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("cust_id_for_getting_cust_contact_details");
		procedureQuery.registerStoredProcedureParameter("cust_id", Long.class, ParameterMode.IN);

		procedureQuery.setParameter("cust_id", (long) custId);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

		return resultValue;

	}

}
