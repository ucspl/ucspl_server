package com.ucspl.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ucspl.controller.UserController;
import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.User;
import com.ucspl.model.UserByName;
import com.ucspl.repository.UserRepository;
import com.ucspl.util.EmailUtil;

@Service
public class UserService {

	private static final Logger logger = Logger.getLogger(UserService.class);

	@Autowired
	private UserRepository userRepository;

	@PersistenceContext
	private EntityManager em;

	private static UserByName searchUser = new UserByName();

	public List<User> getAllUsers() {
		try {
			logger.info("/users - Get all Users information!");
			return userRepository.findAll();
		} catch (Exception e) {
			logger.error("/users - Error while getting Users information!");
			return null;
		}
	}

	public ResponseEntity<User> getUserById(Long userId) throws ResourceNotFoundException {
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User not found for this id :: " + userId));

		logger.info("/users/{id} - Get User Information By Id - " + userId);

		return ResponseEntity.ok().body(user);
	}

	
	public User createUser(User user) {

		logger.info("/users - Save User Details!"+" " +"User Name:"+user.getCreatedBy());
		

		return userRepository.save(user);
	}

	public ResponseEntity<User> updateUser(Long userId, @Valid @RequestBody User userDetails)
			throws ResourceNotFoundException {
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User not found for this id :: " + userId));

		user.setEmail(userDetails.getEmail());
		user.setLastName(userDetails.getLastName());
		user.setFirstName(userDetails.getFirstName());
		user.setCustomer(userDetails.getCustomer());
		user.setMiddleName(userDetails.getMiddleName());
		user.setDepartment(userDetails.getDepartment());
		user.setGender(userDetails.getGender());
		user.setId(userDetails.getId());
		user.setPassword(userDetails.getPassword());
		user.setCountryPrefixForMobile(userDetails.getCountryPrefixForMobile());
		user.setPhoneNo(userDetails.getPhoneNo());
		user.setRole(userDetails.getRole());
		user.setSecurityAnswer(userDetails.getSecurityAnswer());
		user.setSecurityQuestionId(userDetails.getSecurityQuestionId());
		user.setSelectBranch(userDetails.getSelectBranch());
		user.setStatus(userDetails.getStatus());
		user.setUserName(userDetails.getUserName());
		user.setUserNumber(userDetails.getUserNumber());
		user.setModifiedBy(userDetails.getModifiedBy());

		final User updatedUser = userRepository.save(user);

		logger.info("/users/{id} - Update User Details for id - " + userId);

		return ResponseEntity.ok(updatedUser);
	}

	public Map<String, Boolean> deleteUser(Long userId) throws ResourceNotFoundException {
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User not found for this id :: " + userId));

		userRepository.delete(user);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);

		logger.info("/users/{id} - Delete User Details of id - " + userId);

		return response;
	}

	public String searchAndModifyUser(UserByName user) {

		searchUser = user;

		String response = "{\"message\":\"Post With ngResource: The user firstname: " + searchUser.getName() + "\"}";

		logger.info("/search_user -" + " Search User by name or department " + searchUser);

		return response;
	}

	public List<Object[]> getAllUsersByNameOrDepartment() {

		List<Object[]> resultValue = null;
		String name = searchUser.getName();
		int status = searchUser.getStatus();
		String department = searchUser.getDepartment();

		try {
			if (department.trim().length() == 0 && name.trim().length() != 0) {

				StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("search_and_modify_user_by_name");
				procedureQuery.registerStoredProcedureParameter("name", String.class, ParameterMode.IN);
				procedureQuery.registerStoredProcedureParameter("status", Integer.class, ParameterMode.IN);

				procedureQuery.setParameter("name", (String) name);
				procedureQuery.setParameter("status", (int) status);
				procedureQuery.execute();

				resultValue = procedureQuery.getResultList();
				logger.info("/search_user -" + " Get Result For Search User by name - " + searchUser);

			}

			else if (name.trim().length() == 0 && department.trim().length() != 0) {

				StoredProcedureQuery procedureQuery = em
						.createStoredProcedureQuery("search_and_modify_user_by_department");
				procedureQuery.registerStoredProcedureParameter("department", String.class, ParameterMode.IN);
				procedureQuery.registerStoredProcedureParameter("status", Integer.class, ParameterMode.IN);

				procedureQuery.setParameter("department", (String) department);
				procedureQuery.setParameter("status", (int) status);
				procedureQuery.execute();

				resultValue = procedureQuery.getResultList();

				logger.info("/search_user -" + " Get Result For Search User by department - " + searchUser);

			}

			else if (name.trim().length() != 0 && department.trim().length() != 0) {

				StoredProcedureQuery procedureQuery = em
						.createStoredProcedureQuery("search_and_modify_user_by_name_and_department");
				procedureQuery.registerStoredProcedureParameter("name", String.class, ParameterMode.IN);
				procedureQuery.registerStoredProcedureParameter("department", String.class, ParameterMode.IN);
				procedureQuery.registerStoredProcedureParameter("status", Integer.class, ParameterMode.IN);

				procedureQuery.setParameter("name", (String) name);
				procedureQuery.setParameter("department", (String) department);
				procedureQuery.setParameter("status", (int) status);
				procedureQuery.execute();

				resultValue = procedureQuery.getResultList();
				logger.info("/search_user -" + " Get Result For Search User by name and department- " + searchUser);

			}

			/*else if (name.trim().length() == 0 && department.trim().length() == 0) {

				StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("search_and_modify_user");

				procedureQuery.registerStoredProcedureParameter("status", Integer.class, ParameterMode.IN);

				procedureQuery.setParameter("status", (int) status);
				procedureQuery.execute();

				resultValue = procedureQuery.getResultList();
				logger.info("/search_user -" + " Get Result For Search User by name and department- " + searchUser);

			}*/
			 else {
				throw new Exception("error");
			}
			return resultValue;

		}

		catch (Exception e) {

			logger.error("/search_user -" + " Error while Getting Result For Search User - " + searchUser);
			return null;

		}

	}

	public User userNoForGettingUserDetails(long id) throws ResourceNotFoundException {
		
		
		User user = userRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("User not found for this id :: " + id));

		logger.info("/CUserDetails -" + id + " id For Getting User Details");


		return user;
	}

	// for forgot password
	public void forgotPassword(@Valid @RequestBody User user) {
		EmailUtil emailUtil = new EmailUtil();

		// String email = user.getEmail();
		String userName = user.getUserName();
		String userPassword = user.getPassword();

		String host = "smtp.gmail.com";
		final String from = "swatee.universal@gmail.com";
		final String password = "Swatee@123";
		String to = "swateepachaghare.9158@gmail.com";

		emailUtil.sendEmail(host, from, to, password, userPassword, userName);

	}

}
