
package com.ucspl.services;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CustomerAttachments;
import com.ucspl.model.EnquiryAttachmentForSalesAndQuotation;
import com.ucspl.model.ImageModel;
import com.ucspl.model.PurchaseOrderAttachment;
import com.ucspl.model.SupplierAttachments;
import com.ucspl.repository.CustomerAttachmentsRepository;
import com.ucspl.repository.EnquiryAttachmentForSalesAndQuotationRepository;
import com.ucspl.repository.PurchaseOrderAttachmentRepository;

@Service
public class PurchaseOrderAttachmentService {

	private static final Logger logger = Logger.getLogger(PurchaseOrderAttachmentService.class);

	PurchaseOrderAttachment imgData= new PurchaseOrderAttachment();
	
	@Autowired
	private PurchaseOrderAttachmentRepository PurchaseOrderAttachmentRepositoryObject;
	
	@PersistenceContext
	private EntityManager em;

	public ResponseEntity<PurchaseOrderAttachment> uplaodImage(MultipartFile file) throws IOException {

		String path="C:\\Users\\Admin\\Documents\\Workspace\\Upload\\PoAttachment";
		  		      
		String fileName = file.getOriginalFilename();

		String docLocation =path+"\\"+fileName;
		File file1 = new File(docLocation);
		FileCopyUtils.copy(file.getBytes(), file1);
	    
		imgData.setAttachedFileName(fileName);
		imgData.setType(file.getContentType());
		imgData.setFileLocation(docLocation);
		
		logger.info("/uploadPoAttach - uploaded purchase order data successfully!");
		
		return ResponseEntity.ok().body(imgData);
	}
	

	public PurchaseOrderAttachment createPoAttachment(PurchaseOrderAttachment purchaseOrderAttachment) {
		
		purchaseOrderAttachment.setAttachedFileName(imgData.getAttachedFileName());
		purchaseOrderAttachment.setType(imgData.getType());
		purchaseOrderAttachment.setFileLocation(imgData.getFileLocation());
		//customerAttachments.setPicbyte(imgData.getPicbyte());
		
		logger.info("/PoAttach - Save the Po Attachments data!");
		
		return PurchaseOrderAttachmentRepositoryObject.save(purchaseOrderAttachment);

	}
	
	public ImageModel getSelectedPoFileInByteFormat(PurchaseOrderAttachment attachFile) throws IOException {

		File file = new File(attachFile.getFileLocation());
		
		
		Path path = Paths.get(attachFile.getFileLocation());
		String name = attachFile.getAttachedFileName();
		String originalFileName = attachFile.getOriginalFileName();
		String contentType = attachFile.getType();
		byte[] content = null;
		try {
		    content = Files.readAllBytes(path);
		} catch (final IOException e) {
		}
		MultipartFile result = new MockMultipartFile(name,
		                     originalFileName, contentType, content);
		 
		String fileName = result.getOriginalFilename();

		ImageModel img = new ImageModel(result.getName(), result.getContentType(),
				(result.getBytes()));
	
		return img;
	}
	
	public ResponseEntity<PurchaseOrderAttachment> getPoAttachmentById(Long poAttachmentsId) throws ResourceNotFoundException {
		PurchaseOrderAttachment poAttachments = PurchaseOrderAttachmentRepositoryObject
				.findById(poAttachmentsId).orElseThrow(() -> new ResourceNotFoundException(
						" po attachment not found for this id :: " + poAttachmentsId));
		
		logger.info("/poAttach/{id} - Get po attachment By Id!"+ poAttachmentsId);
		
		return ResponseEntity.ok().body(poAttachments);
	}
	

	
	public List<PurchaseOrderAttachment> getAllPoAttachments() {

		logger.info("/getPoAttach - Get All CreatedPo Attachments!");
		return PurchaseOrderAttachmentRepositoryObject.findAll();
	}
	
	
	public List<PurchaseOrderAttachment> getImage(String suppId) throws IOException {

		final List<PurchaseOrderAttachment> retrievedImage = PurchaseOrderAttachmentRepositoryObject.findPOAttachmentsByDocNo(suppId);
		List<PurchaseOrderAttachment> resultValue;
		
		return retrievedImage;
	}
	
	// code to delete Po File
	public String removePoFile(@Valid @RequestBody long indexToDelete) throws IOException {

			StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("remove_attached_po_file");
			procedureQuery.registerStoredProcedureParameter("id", Long.class, ParameterMode.IN);

			procedureQuery.setParameter("id", (long) indexToDelete);
			procedureQuery.execute();

			String response = "{\"message\":\"Post With ngResource: The po file is deleted sucessfully: " + indexToDelete
					+ "\"}";

			logger.info(
					"/deleteAttachedPoFile - Remove po File " + indexToDelete);

			return response;

		}
		
	
	
	
}