package com.ucspl.services;

import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.SalesQuotationDocument;
import com.ucspl.repository.SalesQuotationDocumentRepository;

@Service
public class SalesQuotationDocumentService {

	private static final Logger logger = Logger.getLogger(SalesQuotationDocumentService.class);

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private SalesQuotationDocumentRepository salesQuotationDocumentRepository;

	private static SalesQuotationDocument salesQuotationDocument = new SalesQuotationDocument();

	List<Object> resultValueforTerms;
	private String docNoVariable;

//  Get All Sales Quotation Document Item Information
	public List<SalesQuotationDocument> getAllCreateSalesQuotationDocument() {

		logger.info("/SalesQuotDoc - Get All Sales Quotation Document Item Information!");

		return salesQuotationDocumentRepository.findAll();
	}

	// Get Sales quotation Document Item Information By Id
	public ResponseEntity<SalesQuotationDocument> getCreateSalesQuotationDocumentById(
			@PathVariable(value = "id") Long createSalesQuotationDocumentId) throws ResourceNotFoundException {

		SalesQuotationDocument createSalesQuotationDocument = salesQuotationDocumentRepository
				.findById(createSalesQuotationDocumentId).orElseThrow(() -> new ResourceNotFoundException(
						"Employee not found for this id :: " + createSalesQuotationDocumentId));

		logger.info("/SalesQuotDoc/{id} - Get Sales quotation Document Item Information By Id!"
				+ createSalesQuotationDocumentId);

		return ResponseEntity.ok().body(createSalesQuotationDocument);
	}

	// Save Item Details of Sales Quotation Document
	public SalesQuotationDocument createSalesQuotationDocument(
			@Valid @RequestBody SalesQuotationDocument salesQuotationDocument) {

		System.out.println("data saved");
		logger.info("/SalesQuotDoc - Save Item Details of Sales Quotation Document !");

		return salesQuotationDocumentRepository.save(salesQuotationDocument);

	}

	// Update Sales Quotation Document Item Details for id
	public ResponseEntity<SalesQuotationDocument> updateSalesQuotationDocumentById(
			@PathVariable(value = "id") Long createSalesQuotationDocumentId,
			@Valid @RequestBody SalesQuotationDocument salesQuotationDocumentDetails) throws ResourceNotFoundException {

		SalesQuotationDocument salesQuotationDocument = salesQuotationDocumentRepository
				.findById(createSalesQuotationDocumentId).orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + createSalesQuotationDocumentId));

		salesQuotationDocument.setDocumentNumber(salesQuotationDocumentDetails.getDocumentNumber());
		salesQuotationDocument.setSrNo(salesQuotationDocumentDetails.getSrNo());
		salesQuotationDocument.setInstruName(salesQuotationDocumentDetails.getInstruName());
		salesQuotationDocument.setDescription(salesQuotationDocumentDetails.getDescription());
		salesQuotationDocument.setIdNo(salesQuotationDocumentDetails.getIdNo());
		salesQuotationDocument.setAccrediation(salesQuotationDocumentDetails.getAccrediation());
		salesQuotationDocument.setRangeAccuracy(salesQuotationDocumentDetails.getRangeAccuracy());
		salesQuotationDocument.setSacCode(salesQuotationDocumentDetails.getSacCode());
		salesQuotationDocument.setQuantity(salesQuotationDocumentDetails.getQuantity());
		salesQuotationDocument.setRate(salesQuotationDocumentDetails.getRate());
		salesQuotationDocument.setDiscountOnItem(salesQuotationDocumentDetails.getDiscountOnItem());
		salesQuotationDocument.setDiscountOnTotal(salesQuotationDocumentDetails.getDiscountOnTotal());
		salesQuotationDocument.setAmount(salesQuotationDocumentDetails.getAmount());
		salesQuotationDocument.setSubTotal(salesQuotationDocumentDetails.getSubTotal());
		salesQuotationDocument.setTotal(salesQuotationDocumentDetails.getTotal());
		salesQuotationDocument.setCgst(salesQuotationDocumentDetails.getCgst());
		salesQuotationDocument.setIgst(salesQuotationDocumentDetails.getIgst());
		salesQuotationDocument.setSgst(salesQuotationDocumentDetails.getSgst());
		salesQuotationDocument.setNet(salesQuotationDocumentDetails.getNet());

		final SalesQuotationDocument updatedSalesQuotationDocument = salesQuotationDocumentRepository
				.save(salesQuotationDocument);

		logger.info("/SalesQuotDoc/{id} - Update Sales Quotation Document Item Details for id "
				+ createSalesQuotationDocumentId);
		return ResponseEntity.ok(updatedSalesQuotationDocument);
	}

	// Store Procedure for getting Sr no for document number

	public String docNoForGettingSrNo(@Valid @RequestBody String docNo) {

		docNoVariable = docNo;

		String response = "{\"message\":\" " + docNoVariable + "\"}";

		logger.info("/srnoSQDoc -" + docNoVariable + " Document No For Getting Sales Quotation  Document Item Details");

		return response;
	}

	// Get Items Details of Sales quotation Document for Document No

	public List<Object[]> getSrNoForDocNo() {

		List<Object[]> resultValue;
		String docNo = docNoVariable;

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("sr_no_for_sales_quotation_document");
		procedureQuery.registerStoredProcedureParameter("doc_no", String.class, ParameterMode.IN);

		procedureQuery.setParameter("doc_no", (String) docNo);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

		logger.info("/srnoSQDoc - Get Items Details of Sales quotation Document for Document No " + docNoVariable);

		return resultValue;

	}
	// Store Procedure for getting terms id

	public String docNoForGettingTermsId(@Valid @RequestBody String docNo) {

		docNoVariable = docNo;

		String response = "{\"message\":\" " + docNoVariable + "\"}";

		logger.info(
				"/CSalesTerms -" + docNoVariable + " Document No For Getting Sales Quotation Document Terms Details");

		return response;
	}

	// Get Terms Details of Sales quotation Document for Document No
	public List<BigInteger> getTermsIdForDocNo() {

		String docNo = docNoVariable;

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("terms_id_for_sales_quotation_document");
		procedureQuery.registerStoredProcedureParameter("doc_no", String.class, ParameterMode.IN);

		procedureQuery.setParameter("doc_no", (String) docNo);
		procedureQuery.execute();

		@SuppressWarnings("unchecked")
		List<BigInteger> resultValueforTerms = procedureQuery.getResultList();

		logger.info("/CSalesTerms - Get Terms Details of Sales quotation Document for Document No " + docNoVariable);

		return resultValueforTerms;

	}
	
	 // code to delete sales Row
	public String removeSalesRow(@Valid @RequestBody long indexToDelete) throws IOException {

		logger.info("/deleteSalesRow - delete row Item Details of sales Document !");
	
		salesQuotationDocumentRepository.deleteById(indexToDelete);
		 
		 String response = "{\"message\":\" " + indexToDelete + "row deleted successfully" + "\"}";
		
	     return response;

	}
	

}
