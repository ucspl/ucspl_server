package com.ucspl.services;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.Supplier;
import com.ucspl.model.SupplierByNameOrAddress;
import com.ucspl.repository.SupplierRepository;

@Service
public class SupplierService {

	private static final Logger logger = Logger.getLogger(SupplierService.class);

	@Autowired
	private SupplierRepository supplierRepository;

	SupplierByNameOrAddress searchSupplier = new SupplierByNameOrAddress();

	@PersistenceContext
	private EntityManager em;

	public List<Supplier> getAllSuppliers() {
		
		logger.info("/supplier - Get all suppliers!");

		return supplierRepository.findAll();
	}

	public ResponseEntity<Supplier> getSupplierById(Long supplierId) throws ResourceNotFoundException {
		Supplier supplier = supplierRepository.findById(supplierId)
				.orElseThrow(() -> new ResourceNotFoundException(" Supplier not found for this id :: " + supplierId));

		logger.info("/supplier/{id} - Get supplier By Id!" + supplierId);

		return ResponseEntity.ok().body(supplier);
	}

	public Supplier createSupplier(Supplier supplier) {

		logger.info("/supplier - Save the Supplier Contact!");

		return supplierRepository.save(supplier);
	}

	public ResponseEntity<Supplier> updateSupplierById(Long supplierId, Supplier supplierDetails)
			throws ResourceNotFoundException {
		Supplier supplier = supplierRepository.findById(supplierId)
				.orElseThrow(() -> new ResourceNotFoundException("Supplier not found for this id :: " + supplierId));

		supplier.setName(supplierDetails.getName());
		supplier.setPrimaryContact(supplierDetails.getPrimaryContact());
		supplier.setSecondaryContact(supplierDetails.getSecondaryContact());
		supplier.setCountryPrefixForPhoneNo(supplierDetails.getCountryPrefixForPhoneNo());
		supplier.setCountryPrefixForPrimaryMobile(supplierDetails.getCountryPrefixForPrimaryMobile());
		supplier.setCountryPrefixForSecondaryMobile(supplierDetails.getCountryPrefixForSecondaryMobile());
		supplier.setPrimaryPhoneNo(supplierDetails.getPrimaryPhoneNo());
		supplier.setSecondaryPhoneNo(supplierDetails.getSecondaryPhoneNo());
		supplier.setPhoneNo(supplierDetails.getPhoneNo());
		supplier.setSupplierType(supplierDetails.getSupplierType());
		supplier.setEmail(supplierDetails.getEmail());
		supplier.setPanNo(supplierDetails.getPanNo());
		supplier.setGstNo(supplierDetails.getGstNo());
		supplier.setApprovalStatus(supplierDetails.getApprovalStatus());
		supplier.setStatus(supplierDetails.getStatus());
		supplier.setAddressLine1(supplierDetails.getAddressLine1());
		supplier.setAddressLine2(supplierDetails.getAddressLine2());
		supplier.setCity(supplierDetails.getCity());
		supplier.setCode(supplierDetails.getCode());
		supplier.setState(supplierDetails.getState());
		supplier.setCountry(supplierDetails.getCountry());

		final Supplier updatedSupplier = supplierRepository.save(supplier);

		logger.info("/supplier/{id} - Update Supplier Details for id " + supplierId);
		return ResponseEntity.ok(updatedSupplier);
	}

	public Supplier getSupplierDetailsById(long id) throws ResourceNotFoundException {

		Supplier supplier = supplierRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Supplier not found for this id :: " + id));

		logger.info("/CSuppDetails -" + id + " id For Getting Supplier Details");

		return supplier;
	}

	public String searchAndModifySupplier(SupplierByNameOrAddress supplier) {

		searchSupplier = supplier;

		String response = "{\"message\":\"Post With ngResource: The firstname: " + searchSupplier.getName() + "\"}";
		logger.info("/searchSupplier -" + " Search customer by name or department or address " + searchSupplier);

		return response;
	}
	
	public List<Object[]> getAllSuppByNameOrDeptOrAddress() {

		
		List<Object[]> resultValue;
			
		String name = searchSupplier.getName();
		String nameAnd = searchSupplier.getNameAnd(); 
		int status = searchSupplier.getStatus();
		String address = searchSupplier.getAddress();
		
		if (address.trim().length() == 0 && name.trim().length() != 0) {

			StoredProcedureQuery procedureQuery = em
						.createStoredProcedureQuery("search_and_modify_supplier_by_name");
			procedureQuery.registerStoredProcedureParameter("name", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("status", Integer.class, ParameterMode.IN);

			procedureQuery.setParameter("name", (String) name);
			procedureQuery.setParameter("status", (int) status);
			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			logger.info("/searchSupplier -"  + " Get Result For Search Supplier by name- " + searchSupplier.getName());

		}

		else if (name.trim().length() == 0 && address.trim().length() != 0) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_supplier_by_address");
			procedureQuery.registerStoredProcedureParameter("address", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("status", Integer.class, ParameterMode.IN);

			procedureQuery.setParameter("address", (String) address);
			procedureQuery.setParameter("status", (int) status);
			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			logger.info("/searchSupplier -" + " Get Result For Search Supplier by address -"
					+ searchSupplier.getAddress());

		}

		else if (name.trim().length() != 0 && address.trim().length() != 0) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_customer_by_name_and_address");
			procedureQuery.registerStoredProcedureParameter("name", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("address", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("status", Integer.class, ParameterMode.IN);

			procedureQuery.setParameter("name", (String) name);
			procedureQuery.setParameter("address", (String) address);
			procedureQuery.setParameter("status", (int) status);
			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			logger.info("/searchSupplier -" + " Get Result For Search Supplier by all fields - " + searchSupplier);
		} 

		else {
			resultValue = null;
		}

		return resultValue;                                               
	}

}



