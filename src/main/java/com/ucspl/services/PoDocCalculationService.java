package com.ucspl.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.PoDocCalculation;
import com.ucspl.model.SalesQuotationDocumentCalculation;
import com.ucspl.repository.PoDocCalculationRepository;
import com.ucspl.repository.SalesQuotationDocumentCalculationRepository;

@Service
public class PoDocCalculationService {

	private static final Logger logger = Logger.getLogger(PoDocCalculationService.class);

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private PoDocCalculationRepository poDocCalculationRepositoryObject;

	//private static SalesQuotationDocumentCalculation salesQuotationDocumentCalculation = new SalesQuotationDocumentCalculation();
	private String docNoVariable;
	
	// get All Create Po Document
	public List<PoDocCalculation> getAllCreatePoDocument() {

		logger.info("/PoDocCal - Get all Po Document calculations!");
		return poDocCalculationRepositoryObject.findAll();
	}
	
	
		
	//  Get Po Document Calculations By Id
		public ResponseEntity<PoDocCalculation> getPoDocumentCalculationById(
				@PathVariable(value = "id") Long createPoDocumentCalculationId)
				throws ResourceNotFoundException {

			PoDocCalculation createPoDocumentCalculation = poDocCalculationRepositoryObject
					.findById(createPoDocumentCalculationId).orElseThrow(() -> new ResourceNotFoundException(
							"Document not found for this id :: " + createPoDocumentCalculationId));

			logger.info("/PoDocCal/{id} - Get Po Document Calculations By Id!"
					+ createPoDocumentCalculationId);

			return ResponseEntity.ok().body(createPoDocumentCalculation);
		}
		
		

		// Save Calculation of Po Document
		public PoDocCalculation createPoDocCalculation(
				@Valid @RequestBody PoDocCalculation poDocCalculation) {

			logger.info("/PoDocCal - Save Calculation of Po Document!");

			return poDocCalculationRepositoryObject.save(poDocCalculation);

		}


		
		// Update Po Document calculation for id
		public ResponseEntity<PoDocCalculation> updatePoDocCalculationById(
				@PathVariable(value = "id") Long createPoDocCalculationId,
				@Valid @RequestBody PoDocCalculation poDocumentCalculationDetails)
				throws ResourceNotFoundException {

			PoDocCalculation poDocCalculation = poDocCalculationRepositoryObject
					.findById(createPoDocCalculationId)
					.orElseThrow(() -> new ResourceNotFoundException(
							"po Document calculation not found for this id :: "
									+ createPoDocCalculationId));

			poDocCalculation.setDocumentNumber(poDocumentCalculationDetails.getDocumentNumber());
			poDocCalculation.setSubTotal(poDocumentCalculationDetails.getSubTotal());
			poDocCalculation.setDiscountOnSubtotal(poDocumentCalculationDetails.getDiscountOnSubtotal());
			poDocCalculation.setTotal(poDocumentCalculationDetails.getTotal());
			poDocCalculation.setCgst(poDocumentCalculationDetails.getCgst());
			poDocCalculation.setIgst(poDocumentCalculationDetails.getIgst());
			poDocCalculation.setSgst(poDocumentCalculationDetails.getSgst());
			poDocCalculation.setNet(poDocumentCalculationDetails.getNet());

			final PoDocCalculation updatedPoDocCalculation = poDocCalculationRepositoryObject
					.save(poDocCalculation);

			logger.info("/PoDocCal/{id} - Update Po Document calculation for id "
					+ createPoDocCalculationId);

			return ResponseEntity.ok(updatedPoDocCalculation);
		}

	
		// store procedure for doc calculation table detail

		public String DocNoForGettingDocDetails(@Valid @RequestBody String docNo) {

			docNoVariable = docNo;

			String response = "{\"message\":\" " + docNoVariable + "\"}";

			logger.info("/PoDocCalDet -" + docNoVariable
					+ " Document No for Getting Po Document calculation details");

			return response;
		}

		// Get details of po Document calculation for Document No
		@SuppressWarnings("unchecked")
		public List<Object[]> getDocDetailsForDocNo() {

			List<Object[]> resultValue;
			String docNo = docNoVariable;

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("doc_details_for_po_document_calculation");
			procedureQuery.registerStoredProcedureParameter("doc_no", String.class, ParameterMode.IN);

			procedureQuery.setParameter("doc_no", (String) docNo);
			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();

			logger.info(
					"/PoDocCalDet - Get details of PO Document calculation for Document No " + docNoVariable);

			return resultValue;

		}
}
