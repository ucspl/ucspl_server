package com.ucspl.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateNablSpecification;
import com.ucspl.model.SearchNabl;
import com.ucspl.model.SearchNablSpecificationByNablIdPNmAndStatus;
import com.ucspl.repository.CreateNablScopeMasterRepository;
import com.ucspl.repository.CreateNablSpecificationRepository;

@Service
public class CreateNablSpecificationService {

	private static final Logger logger = Logger.getLogger(CreateNablSpecificationService.class);

	String certiNoVariable;
	String nablScopeMasterIdVariable;

	@Autowired
	private CreateNablSpecificationRepository createNablSpecificationRepository;

	@PersistenceContext
	private EntityManager em;

	// Get All Created Nabl Specification
	public List<CreateNablSpecification> getAllCreatedNablSpecificationDetails() {

		logger.info("/crtNablSpeDet - Get All Created Nabl Specification!");
		return createNablSpecificationRepository.findAll();
	}

	// Get nabl specification By Id
	public ResponseEntity<CreateNablSpecification> getNablSpecificationDetailsById(
			@PathVariable(value = "id") Long createNablSpecificationDetailsId) throws ResourceNotFoundException {
		CreateNablSpecification createNablSpecificationDetails = createNablSpecificationRepository
				.findById(createNablSpecificationDetailsId).orElseThrow(() -> new ResourceNotFoundException(
						" master specification not found for this id :: " + createNablSpecificationDetailsId));

		logger.info("/crtNablSpeDet/{id} - Get nabl specification By Id!" + createNablSpecificationDetailsId);
		return ResponseEntity.ok().body(createNablSpecificationDetails);
	}

	// Save the Nabl specification
	public CreateNablSpecification createNablSpecification(
			@Valid @RequestBody CreateNablSpecification createNablSpecification) {

		logger.info("/crtNablSpeDet - Save the Nabl specification!");
		return createNablSpecificationRepository.save(createNablSpecification);
	}

//  Update Nabl Specification Details for id
	@PutMapping("/crtNablSpeDet/{id}")
	public ResponseEntity<CreateNablSpecification> updateNablSpecificationDetailsById(
			@PathVariable(value = "id") Long createNablSpecificationDetailsId,
			@Valid @RequestBody CreateNablSpecification createNablSpecificationDetails)
			throws ResourceNotFoundException {
		CreateNablSpecification createNablSpecification = createNablSpecificationRepository
				.findById(createNablSpecificationDetailsId).orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + createNablSpecificationDetailsId));

		createNablSpecification.setCreateNablId(createNablSpecificationDetails.getCreateNablId());
		createNablSpecification.setCalibrationLabId(createNablSpecificationDetails.getCalibrationLabId());
		createNablSpecification.setGroupId(createNablSpecificationDetails.getGroupId());
		createNablSpecification.setParameterId(createNablSpecificationDetails.getParameterId());
		createNablSpecification.setParameterNumber(createNablSpecificationDetails.getParameterNumber());
		createNablSpecification.setRangeFrom(createNablSpecificationDetails.getRangeFrom());
		createNablSpecification.setRangeTo(createNablSpecificationDetails.getRangeTo());
		createNablSpecification.setRangeUom(createNablSpecificationDetails.getRangeUom());
		createNablSpecification.setFreqFrom(createNablSpecificationDetails.getFreqFrom());
		createNablSpecification.setFreqFromUom(createNablSpecificationDetails.getFreqFromUom());
		createNablSpecification.setFreqTo(createNablSpecificationDetails.getFreqTo());
		createNablSpecification.setFreqToUom(createNablSpecificationDetails.getFreqToUom());
		createNablSpecification.setLeastCount(createNablSpecificationDetails.getLeastCount());
		createNablSpecification.setLeastCountUom(createNablSpecificationDetails.getLeastCountUom());
		createNablSpecification.setUncValue(createNablSpecificationDetails.getUncValue());
		createNablSpecification.setUncFormulaId(createNablSpecificationDetails.getUncFormulaId());
		createNablSpecification.setUncUom(createNablSpecificationDetails.getUncUom());
		createNablSpecification.setTempChartType(createNablSpecificationDetails.getTempChartType());
		createNablSpecification.setModeType(createNablSpecificationDetails.getModeType());
		createNablSpecification.setAmendmentDate(createNablSpecificationDetails.getAmendmentDate());
		createNablSpecification.setInhouse(createNablSpecificationDetails.getInhouse());
		createNablSpecification.setOnsite(createNablSpecificationDetails.getOnsite());
		createNablSpecification.setDraft(createNablSpecificationDetails.getDraft());
		createNablSpecification.setApproved(createNablSpecificationDetails.getApproved());
		createNablSpecification.setArchieved(createNablSpecificationDetails.getArchieved());
		createNablSpecification.setSubmitted(createNablSpecificationDetails.getSubmitted());
		createNablSpecification.setRejected(createNablSpecificationDetails.getRejected());

		final CreateNablSpecification updatedNablSpecificationDetail = createNablSpecificationRepository
				.save(createNablSpecification);
		logger.info(
				"/crtNablSpeDet/{id} - Update Nabl Specification Details for id " + createNablSpecificationDetailsId);

		return ResponseEntity.ok(updatedNablSpecificationDetail);
	}

	// Delete Nabl Instrument Detail of id
	public Map<String, Boolean> deleteNablSpecificationDetails(@PathVariable(value = "id") Long nablDetailId)
			throws ResourceNotFoundException {

		Map<String, Boolean> response = new HashMap<>();
		createNablSpecificationRepository.deleteById(nablDetailId);
		response.put("deleted", Boolean.TRUE);
		logger.info("/crtNablSpeDet/{id} - Delete Nabl Instrument Detail of id - " + nablDetailId);

		return response;
	}

//////////////////////////////sp for getting sr no for Nabl instruments ////////////////////////////////  

	public List<Object[]> srNoDetailsOfNablSpecification(@Valid @RequestBody long nablId) {

		System.out.println("master id" + nablId);

		logger.info("/srNoNablDet -" + " Search nabl specification Details For nabl id" + nablId);

		List<Object[]> resultValue;

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("srno_nabl_specification_detail_list");
		procedureQuery.registerStoredProcedureParameter("id_no", Long.class, ParameterMode.IN);

		procedureQuery.setParameter("id_no", (long) nablId);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

		return resultValue;
	}

////////////////////////////sp for getting sr no for nabl specification by nablidparaname ////////////////////////////////  

	public List<Object[]> srNoDetailsOfNablSpecificationByNablIdParaName(
			@Valid @RequestBody SearchNablSpecificationByNablIdPNmAndStatus nablIdParaNameObj) {

		System.out.println("nablid paraname" + nablIdParaNameObj.getNablIdParaName());
		logger.info("/srnoNablSpecDetByNablIdPn -" + " Search nabl specification Details For nablIdParaName"
				+ nablIdParaNameObj.getNablIdParaName());

		List<Object[]> resultValue = null;

		if (nablIdParaNameObj.getNablIdParaName() != null && nablIdParaNameObj.getDraft() == 1
				|| nablIdParaNameObj.getApproved() == 1 || nablIdParaNameObj.getSubmitted() == 1
				|| nablIdParaNameObj.getRejected() == 1 || nablIdParaNameObj.getArchieved() == 1) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("srno_nabl_specification_detail_list_by_nablidparaname");

			procedureQuery.registerStoredProcedureParameter("nablidparanm", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("draft", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("approved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("submitted", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("rejected", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("archieved", Integer.class, ParameterMode.IN);

			procedureQuery.setParameter("nablidparanm", (String) nablIdParaNameObj.getNablIdParaName());
			procedureQuery.setParameter("draft", (int) nablIdParaNameObj.getDraft());
			procedureQuery.setParameter("approved", (int) nablIdParaNameObj.getApproved());
			procedureQuery.setParameter("submitted", (int) nablIdParaNameObj.getSubmitted());
			procedureQuery.setParameter("rejected", (int) nablIdParaNameObj.getRejected());
			procedureQuery.setParameter("archieved", (int) nablIdParaNameObj.getArchieved());

			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();

		}

		return resultValue;
	}

}
