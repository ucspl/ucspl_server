package com.ucspl.services;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ucspl.controller.RequestAndInwardDocumentController;
import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CustomerInstrumentIdentification;
import com.ucspl.model.RequestAndInwardDocument;
import com.ucspl.repository.RequestAndInwardDocumentRepository;

@Service
public class RequestAndInwardDocumentService {

	private static final Logger logger = LogManager.getLogger(RequestAndInwardDocumentService.class);

	private  String docNoVariable;
	private long docNoVariable1;
	
	
	private  CustomerInstrumentIdentification customerInstrumentIdentification;
	
	@PersistenceContext
	private EntityManager em;

	@Autowired
	private RequestAndInwardDocumentRepository requestAndInwardDocumentRepository;

	public RequestAndInwardDocumentService() {
		// TODO Auto-generated constructor stub
	}

	public List<RequestAndInwardDocument> getAllRequestAndInwardDocument() {

		logger.info("/reqIwdDoc - Get All Created Request Inward Document!");
		return requestAndInwardDocumentRepository.findAll();
		
	}

	public ResponseEntity<RequestAndInwardDocument> getRequestAndInwardDocumentById(
			 Long requestAndInwardDocumentId) throws ResourceNotFoundException {
		RequestAndInwardDocument requestAndInwardDocument = requestAndInwardDocumentRepository
				.findById(requestAndInwardDocumentId).orElseThrow(() -> new ResourceNotFoundException(
						" Request inward not found for this id :: " + requestAndInwardDocumentId));

		logger.info("/reqIwdDoc/{id} - Get Request Inward Document By Id!" + requestAndInwardDocumentId);

		return ResponseEntity.ok().body(requestAndInwardDocument);
	}

	
	public RequestAndInwardDocument createRequestAndInwardDocument(
			 RequestAndInwardDocument requestAndInwardDocument) {

		logger.info("/reqIwdDoc - Save the Request Inward Document!");
			
		return requestAndInwardDocumentRepository.save(requestAndInwardDocument);
	}

	
	public ResponseEntity<RequestAndInwardDocument> updateRequestAndInwardDocument(
			Long requestAndInwardDocumentId,
			RequestAndInwardDocument requestAndInwardDocumentDetails)
			throws ResourceNotFoundException {
		RequestAndInwardDocument requestAndInwardDocument = requestAndInwardDocumentRepository
				.findById(requestAndInwardDocumentId).orElseThrow(() -> new ResourceNotFoundException(
						"Req and Iwd not found for this id :: " + requestAndInwardDocumentId));

		requestAndInwardDocument.setCustInstruIdentificationId(requestAndInwardDocumentDetails.getCustInstruIdentificationId());
		requestAndInwardDocument.setCreateReqAndIwdTableId(requestAndInwardDocumentDetails.getCreateReqAndIwdTableId());
		requestAndInwardDocument.setSrNo(requestAndInwardDocumentDetails.getSrNo());
		requestAndInwardDocument.setNomenclature(requestAndInwardDocumentDetails.getNomenclature());
		requestAndInwardDocument.setParameter(requestAndInwardDocumentDetails.getParameter());
		requestAndInwardDocument.setRangeSize(requestAndInwardDocumentDetails.getRangeSize());
		requestAndInwardDocument.setMake(requestAndInwardDocumentDetails.getMake());
		requestAndInwardDocument.setIdNo(requestAndInwardDocumentDetails.getIdNo());
		requestAndInwardDocument.setFrequencyCondition(requestAndInwardDocumentDetails.getFrequencyCondition());
		requestAndInwardDocument.setInstrumentType(requestAndInwardDocumentDetails.getInstrumentType());
		requestAndInwardDocument.setRangeFrom(requestAndInwardDocumentDetails.getRangeFrom());
		requestAndInwardDocument.setRangeTo(requestAndInwardDocumentDetails.getRangeTo());
		requestAndInwardDocument.setRuom(requestAndInwardDocumentDetails.getRuom());
		requestAndInwardDocument.setLeastCount(requestAndInwardDocumentDetails.getLeastCount());
		requestAndInwardDocument.setLcuom1(requestAndInwardDocumentDetails.getLcuom1());
		requestAndInwardDocument.setAccuracy(requestAndInwardDocumentDetails.getAccuracy());
		requestAndInwardDocument.setFormulaAccuracy(requestAndInwardDocumentDetails.getFormulaAccuracy());
		requestAndInwardDocument.setUomAccuracy(requestAndInwardDocumentDetails.getUomAccuracy());
		requestAndInwardDocument.setAccuracy1(requestAndInwardDocumentDetails.getAccuracy1());
		requestAndInwardDocument.setFormulaAccuracy1(requestAndInwardDocumentDetails.getFormulaAccuracy1());
		requestAndInwardDocument.setUomAccuracy1(requestAndInwardDocumentDetails.getUomAccuracy1());
		requestAndInwardDocument.setLocation(requestAndInwardDocumentDetails.getLocation());
		requestAndInwardDocument.setCalibrationFrequency(requestAndInwardDocumentDetails.getCalibrationFrequency());
		requestAndInwardDocument.setLabType(requestAndInwardDocumentDetails.getLabType());


		final RequestAndInwardDocument updatedCreateSalesQuotation = requestAndInwardDocumentRepository
				.save(requestAndInwardDocument);

		logger.info(
				"/reqIwdDoc/{id} - Update Request and Inward document Details for id " + requestAndInwardDocumentId);

		return ResponseEntity.ok(updatedCreateSalesQuotation);
	}

	
	public ResponseEntity<RequestAndInwardDocument> updateReqInwardDocumentForCreateCertificate(
			Long requestAndInwardDocumentId,
			RequestAndInwardDocument requestAndInwardDocumentDetails)
			throws ResourceNotFoundException {
		RequestAndInwardDocument requestAndInwardDocument = requestAndInwardDocumentRepository
				.findById(requestAndInwardDocumentId).orElseThrow(() -> new ResourceNotFoundException(
						"Req and Iwd not found for this id :: " + requestAndInwardDocumentId));


	
		requestAndInwardDocument.setSrNo(requestAndInwardDocumentDetails.getSrNo());
		requestAndInwardDocument.setRangeSize(requestAndInwardDocumentDetails.getRangeSize());
		requestAndInwardDocument.setMake(requestAndInwardDocumentDetails.getMake());
		requestAndInwardDocument.setIdNo(requestAndInwardDocumentDetails.getIdNo());
		requestAndInwardDocument.setFrequencyCondition(requestAndInwardDocumentDetails.getFrequencyCondition());
		requestAndInwardDocument.setInstrumentType(requestAndInwardDocumentDetails.getInstrumentType());
		requestAndInwardDocument.setRangeFrom(requestAndInwardDocumentDetails.getRangeFrom());
		requestAndInwardDocument.setRangeTo(requestAndInwardDocumentDetails.getRangeTo());
		requestAndInwardDocument.setLeastCount(requestAndInwardDocumentDetails.getLeastCount());
		requestAndInwardDocument.setAccuracy(requestAndInwardDocumentDetails.getAccuracy());
		requestAndInwardDocument.setAccuracy1(requestAndInwardDocumentDetails.getAccuracy1());
		requestAndInwardDocument.setLocation(requestAndInwardDocumentDetails.getLocation());
		requestAndInwardDocument.setDateOfCalibration(requestAndInwardDocumentDetails.getDateOfCalibration());
		requestAndInwardDocument.setDateOfIssueCertificate(requestAndInwardDocumentDetails.getDateOfIssueCertificate());
		requestAndInwardDocument.setReceivedDate(requestAndInwardDocumentDetails.getReceivedDate());
		requestAndInwardDocument.setOrderNo(requestAndInwardDocumentDetails.getOrderNo());
		requestAndInwardDocument.setCertificateNo(requestAndInwardDocumentDetails.getCertificateNo());
		requestAndInwardDocument.setCertificatePrintMode(requestAndInwardDocumentDetails.getCertificatePrintMode());
		requestAndInwardDocument.setRemark(requestAndInwardDocumentDetails.getRemark());
		requestAndInwardDocument.setCalibrationDueOn(requestAndInwardDocumentDetails.getCalibrationDueOn());
		requestAndInwardDocument.setCalibratedBy(requestAndInwardDocumentDetails.getCalibratedBy());
		requestAndInwardDocument.setAuthorizedBy(requestAndInwardDocumentDetails.getAuthorizedBy());
		requestAndInwardDocument.setReminderDate(requestAndInwardDocumentDetails.getReminderDate());
		requestAndInwardDocument.setInstruCondition(requestAndInwardDocumentDetails.getInstruCondition());
		requestAndInwardDocument.setAssignedTo(requestAndInwardDocumentDetails.getAssignedTo());
		requestAndInwardDocument.setStatusOfOrder(requestAndInwardDocumentDetails.getStatusOfOrder());

		final RequestAndInwardDocument updatedCreateSalesQuotation = requestAndInwardDocumentRepository
				.save(requestAndInwardDocument);

		logger.info(
				"/uReqIwdDocCer/{id} - Update Request and Inward document create certificate Details for id " + requestAndInwardDocumentId);

		return ResponseEntity.ok(updatedCreateSalesQuotation);
	}

	
	
	
	public ResponseEntity<RequestAndInwardDocument> updateRequestAndInwardDocumentForCalibrationTag(
			Long requestAndInwardDocumentId,
			RequestAndInwardDocument requestAndInwardDocumentDetails)
			throws ResourceNotFoundException {
		RequestAndInwardDocument requestAndInwardDocument = requestAndInwardDocumentRepository
				.findById(requestAndInwardDocumentId).orElseThrow(() -> new ResourceNotFoundException(
						"Req and Iwd not found for this id :: " + requestAndInwardDocumentId));
	
		final RequestAndInwardDocument updatedCreateSalesQuotation = requestAndInwardDocumentRepository
				.save(requestAndInwardDocument);

		logger.info(
				"/reqIwdDocForCaliTag/{id} - Update Request and Inward document Details for id - set calibration tag " + requestAndInwardDocumentId);

		return ResponseEntity.ok(updatedCreateSalesQuotation);
	}

	
////////////////////////////////////Store Procedure for getting Sr no//////////////////////////////////
	public String docNoForGettingSrNo(long docNo) {

		docNoVariable1 = docNo;

		String response = "{\"message\":\" " + docNoVariable1 + "\"}";

		logger.info(
				"/srnoRIDoc -" + docNoVariable1 + " Document No For Request and Inward Document Item Details");

		return response;
	}

	public List<Object[]> getSrNoForDocNoReq() {
		
		List<Object[]> resultValue;
		long docNo = docNoVariable1;
		
		System.out.println("getSrNoForDocNoReq()"+docNoVariable);

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("srno_instru_detail_list_for_req_iwd");
		procedureQuery.registerStoredProcedureParameter("doc_no", Long.class, ParameterMode.IN);

		System.out.println("getSrNoForDocNoReq()..."+docNoVariable);
		
		procedureQuery.setParameter("doc_no", (long) docNo);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

		logger.info("/srnoRIDoc - Get Items Details of Request Inward Document for cust instru identification id No " + docNoVariable);

		return resultValue;
		
	}
	
	public String objForGettingUniqueSrNo(CustomerInstrumentIdentification customerInstrumentIden) {

		customerInstrumentIdentification = customerInstrumentIden;
		String response = "{\"message\":\" " + "sp data" + "\"}";
		logger.info(
				"/srnoIdentiRIDoc -" + " Document No For Request and Inward Document Item Details");

		return response;
	}
	
	public List<Object[]> getIdentySrNoForDocNoReq() {
		
		List<Object[]> resultValue;
		CustomerInstrumentIdentification customerInstrumentIden = customerInstrumentIdentification;
		
		System.out.println("fun");
		
		long custId= customerInstrumentIden.getCustId();
		long instruId= customerInstrumentIden.getNomenclatureId();
		String idNo=  customerInstrumentIden.getIdNo();
		String srNo= customerInstrumentIden.getSrNo();

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("srno_identify_instru_detail_list_for_req_iwd");
		procedureQuery.registerStoredProcedureParameter("cust_id", Long.class, ParameterMode.IN);
		procedureQuery.registerStoredProcedureParameter("instru_id", Long.class, ParameterMode.IN);
		procedureQuery.registerStoredProcedureParameter("id_no", String.class, ParameterMode.IN);
		procedureQuery.registerStoredProcedureParameter("sr_no", String.class, ParameterMode.IN);

		System.out.println("getSrNoForDocNoReq()..."+docNoVariable);
		
		procedureQuery.setParameter("cust_id", (long) custId);
		procedureQuery.setParameter("instru_id", (long) instruId);
		procedureQuery.setParameter("id_no", (String) idNo);
		procedureQuery.setParameter("sr_no", (String) srNo);
		
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

		logger.info("/srnoRIDoc - trial Get Items Details of Request Inward Document for Document No " + docNoVariable);

		return resultValue;

	}
	

	public List<Object[]> getSrNoForDocNoWithNamesReq() {
		

		List<Object[]> resultValue;
		long docNo = docNoVariable1;
		System.out.println("getSrNoForDocNoReq()"+docNoVariable);

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("srno_instru_detail_list_with_names_for_req_iwd");
		procedureQuery.registerStoredProcedureParameter("doc_no", Long.class, ParameterMode.IN);

		System.out.println("getSrNoForDocNoReq()..."+docNoVariable);
		
		procedureQuery.setParameter("doc_no", (long) docNo);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

		logger.info("/srnoRIDoc - Get Items Details with names of Request Inward Document for Document No " + docNoVariable);

		return resultValue;

	}
	

	public List<Object[]> getSrNoForDocNoWithNamesReqByInstruNo() {
		
		List<Object[]> resultValue;
		String docNo = docNoVariable;
		
		System.out.println("getSrNoForDocNoReq()"+docNoVariable);

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("srno_instru_detail_list_with_names_for_req_iwd_by_iwdinstruno");
		procedureQuery.registerStoredProcedureParameter("doc_no", String.class, ParameterMode.IN);

		System.out.println("getSrNoForDocNoReq()..."+docNoVariable);
		
		procedureQuery.setParameter("doc_no", (String) docNo);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

		logger.info("/srnoRIDoc - Get Items Details with names of Request Inward Document for Document No " + docNoVariable);

		return resultValue;

	}
	
	
//////////////////////////////////  store procedure to get cust instru id through reqDocNo ////////////////////////
	
	public List<Object[]> getCustInstruIdForReqDocNo(String number) throws IOException {

		List<Object[]> resultValue;
	
		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("get_custinstruid_through_reqdocno");
		procedureQuery.registerStoredProcedureParameter("doc_no", String.class, ParameterMode.IN);

		procedureQuery.setParameter("doc_no", (String) number);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

		logger.info("/ciIdForReqNo - Get cust instru id through reqDocNo " + number);

		return resultValue;
	}
	
	
///////////////////////////////////////////// Multi para list //////////////////////////////////	
	
	public List<Object[]> getDocumentNumber( String number) throws IOException {

		List<Object[]> resultValue;
	
		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("multipara_details_for_request_and_inward");
		procedureQuery.registerStoredProcedureParameter("number", String.class, ParameterMode.IN);

		procedureQuery.setParameter("number", (String) number);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

		logger.info("/mulParaSN - Get multi para details for inward instrument number " + number);

		return resultValue;
	}
		
	
	
}
