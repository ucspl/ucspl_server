package com.ucspl.services;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateRequestAndInward;
import com.ucspl.model.SearchRequestAndInward;
import com.ucspl.repository.CreateRequestAndInwardRepository;


@Service
public class CreateRequestAndInwardService {
	
	
	private static final Logger logger = LogManager.getLogger(CreateRequestAndInwardService.class);

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateRequestAndInwardRepository createRequestAndInwardRepository;

	private SearchRequestAndInward searchReqAndIwd = new SearchRequestAndInward();
	private String docNoVariable;

	public CreateRequestAndInwardService() {
		// TODO Auto-generated constructor stub
	}

	public List<CreateRequestAndInward> getAllCreatedRequestAndInward() {

		
		logger.info("/createreq - Get All Created Request and inward!");
		return createRequestAndInwardRepository.findAll();
	}


	public ResponseEntity<CreateRequestAndInward> getRequestAndInwardById(
			 Long createRequestAndInwardId) throws ResourceNotFoundException {
		CreateRequestAndInward createRequestAndInward = createRequestAndInwardRepository
				.findById(createRequestAndInwardId).orElseThrow(() -> new ResourceNotFoundException(
						" Request and inward not found for this id :: " + createRequestAndInwardId));

		logger.info("/createreq/{id} - Get Request and inward By Id!" + createRequestAndInwardId);

		return ResponseEntity.ok().body(createRequestAndInward);
	}


	public ResponseEntity<CreateRequestAndInward> updateRequestAndInwardById(
			 Long createRequestAndInwardId,
			 CreateRequestAndInward createRequestAndInwardDetails) throws ResourceNotFoundException {
		CreateRequestAndInward createRequestAndInward = createRequestAndInwardRepository
				.findById(createRequestAndInwardId).orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + createRequestAndInwardId));

		createRequestAndInward.setInwardType(createRequestAndInwardDetails.getInwardType());
		createRequestAndInward.setInwardDate(createRequestAndInwardDetails.getInwardDate());
		createRequestAndInward.setReqInwDocumentNo(createRequestAndInwardDetails.getReqInwDocumentNo());
		createRequestAndInward.setInwardReqNo(createRequestAndInwardDetails.getInwardReqNo());
		createRequestAndInward.setCustomerName(createRequestAndInwardDetails.getCustomerName());
		createRequestAndInward.setEndDate(createRequestAndInwardDetails.getEndDate());
		createRequestAndInward.setReportInNameOf(createRequestAndInwardDetails.getReportInNameOf());
		createRequestAndInward.setCustomerDCNo(createRequestAndInwardDetails.getCustomerDCNo());
		createRequestAndInward.setDcDate(createRequestAndInwardDetails.getDcDate());
		createRequestAndInward.setExpectedDeliveryDate(createRequestAndInwardDetails.getExpectedDeliveryDate());
		createRequestAndInward.setLabLocation(createRequestAndInwardDetails.getLabLocation());
		createRequestAndInward.setNoOfInstruments(createRequestAndInwardDetails.getNoOfInstruments());
		createRequestAndInward.setReqContactName(createRequestAndInwardDetails.getReqContactName());
		createRequestAndInward.setCountryPrefixForMobile1(createRequestAndInwardDetails.getCountryPrefixForMobile1());
		createRequestAndInward.setReqContactPhone(createRequestAndInwardDetails.getReqContactPhone());
		createRequestAndInward.setReqType(createRequestAndInwardDetails.getReqType());
		createRequestAndInward.setReqEmail(createRequestAndInwardDetails.getReqEmail());
		createRequestAndInward.setAssignedTo(createRequestAndInwardDetails.getAssignedTo());
		createRequestAndInward.setDraft(createRequestAndInwardDetails.getDraft());
		createRequestAndInward.setApproved(createRequestAndInwardDetails.getApproved());
		createRequestAndInward.setArchieved(createRequestAndInwardDetails.getArchieved());
		createRequestAndInward.setSubmitted(createRequestAndInwardDetails.getSubmitted());
		createRequestAndInward.setRejected(createRequestAndInwardDetails.getRejected());
		createRequestAndInward.setTypeOfDocument(createRequestAndInwardDetails.getTypeOfDocument());
		createRequestAndInward.setDateModified(createRequestAndInwardDetails.getDateModified());

		final CreateRequestAndInward updatedCreateRequestAndInward = createRequestAndInwardRepository
				.save(createRequestAndInward);
		logger.info("/createreq/{id} - Update request and inward for id " + createRequestAndInwardId);

		return ResponseEntity.ok(updatedCreateRequestAndInward);
	}


	public CreateRequestAndInward createRequestAndInward(CreateRequestAndInward createRequestAndInward) {
			
		logger.info("/createreq - Save the Request and inward!");
		
		return createRequestAndInwardRepository.save(createRequestAndInward);

	}

	///////////// get document number for req and inw /////////////////////////
	public String getDocumentNumber(int type) throws IOException {

		Object documentNumberId;
		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("document_number_for_request_and_inward");
		procedureQuery.registerStoredProcedureParameter("type", Integer.class, ParameterMode.IN);

		procedureQuery.setParameter("type", (int) type);
		procedureQuery.execute();

		documentNumberId = procedureQuery.getSingleResult();
		String stringToConvert = String.valueOf(documentNumberId);
		String response = "{\"number\":\"" + stringToConvert + "\"}";

		return response;
	}

	public String getCounterDocumentNumber(CreateRequestAndInward obj) throws IOException {

		Object documentNumberId;
		String response;
		StoredProcedureQuery procedureQuery = em
				.createStoredProcedureQuery("document_number_counter_for_request_and_inward");
		procedureQuery.registerStoredProcedureParameter("date", String.class, ParameterMode.IN);
		procedureQuery.registerStoredProcedureParameter("type", Integer.class, ParameterMode.IN);

		procedureQuery.setParameter("date", (String) obj.getInwardDate());
		procedureQuery.setParameter("type", (int) obj.getInwardType());

		System.out.print("function call before execution");
		procedureQuery.execute();
		System.out.print("function call after execution");

		documentNumberId = procedureQuery.setMaxResults(1).getResultList().stream().findFirst().orElse(null);
		if (documentNumberId == null) {
			System.out.print("function call null");
			response = "{\"number\":\"" + "nothing" + "\"}";
		} else {
			documentNumberId = procedureQuery.getSingleResult();
			String stringToConvert = String.valueOf(documentNumberId);
			System.out.print("function call not null");
			response = "{\"number\":\"" + stringToConvert + "\"}";
		}

		
		return response;
	}

	// code for search and modify req and iwd
	public String searchAndModifyRequestAndInward(SearchRequestAndInward searchReqAndIwd1) {

		searchReqAndIwd = searchReqAndIwd1;

		String response = "{\"message\":\"Post With ngResource: The response is: "
				+ searchReqAndIwd1.getSearchDocument() + "\"}";

		logger.info("/searchSales - Search Request and Inward Documents For " + searchReqAndIwd);

		return response;
	}

	// Result For search and modify req and iwd
	public List<Object[]> getAllRequestAndInward() {

		List<Object[]> resultValue;

		//search_and_modify_request_and_inward_by_only_docname
		if (searchReqAndIwd.getSearchDocument() != null && searchReqAndIwd.getDraft() == 0
				&& searchReqAndIwd.getApproved() == 0 && searchReqAndIwd.getSubmitted() == 0
				&& searchReqAndIwd.getRejected() == 0 && searchReqAndIwd.getArchieved() == 0) {
			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_request_and_inward_by_only_docname");
			procedureQuery.registerStoredProcedureParameter("searchdocument", String.class, ParameterMode.IN);

			procedureQuery.setParameter("searchdocument", (String) searchReqAndIwd.getSearchDocument());

			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();

		
			logger.info("/searchReq -" + " Get Result For Search Request and Inward Document by docname only - "
					+ searchReqAndIwd.getSearchDocument());

			return resultValue;
		} 
		
		//search_and_modify_request_and_inward_by_docname_and_status
		else if (searchReqAndIwd.getSearchDocument() != null && searchReqAndIwd.getDraft() == 1
				|| searchReqAndIwd.getApproved() == 1 || searchReqAndIwd.getSubmitted() == 1
				|| searchReqAndIwd.getRejected() == 1 || searchReqAndIwd.getArchieved() == 1
				&& searchReqAndIwd.getFromDate()==null && searchReqAndIwd.getToDate()==null) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_request_and_inward_by_docname_and_status");
			procedureQuery.registerStoredProcedureParameter("searchdocument", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("draft", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("approved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("submitted", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("rejected", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("archieved", Integer.class, ParameterMode.IN);

			procedureQuery.setParameter("searchdocument", (String) searchReqAndIwd.getSearchDocument());
			procedureQuery.setParameter("draft", (int) searchReqAndIwd.getDraft());
			procedureQuery.setParameter("approved", (int) searchReqAndIwd.getApproved());
			procedureQuery.setParameter("submitted", (int) searchReqAndIwd.getSubmitted());
			procedureQuery.setParameter("rejected", (int) searchReqAndIwd.getRejected());
			procedureQuery.setParameter("archieved", (int) searchReqAndIwd.getArchieved());

			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			logger.info("/searchReq -" + " Get Result For Search Request and Inward Document by docname and status- "
					+ searchReqAndIwd.getSearchDocument());

			return resultValue;

		} 
		
		//search_and_modify_request_and_inward_by_status_and_date
		else if (searchReqAndIwd.getSearchDocument() == null && searchReqAndIwd.getFromDate() != null
				&& searchReqAndIwd.getToDate() != null && searchReqAndIwd.getDraft() == 1
				|| searchReqAndIwd.getApproved() == 1 || searchReqAndIwd.getSubmitted() == 1
				|| searchReqAndIwd.getRejected() == 1 || searchReqAndIwd.getArchieved() == 1) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_request_and_inward_by_status_and_date");
			procedureQuery.registerStoredProcedureParameter("draft", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("approved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("submitted", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("rejected", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("archieved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("fromdate", java.util.Date.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("todate", java.util.Date.class, ParameterMode.IN);
			procedureQuery.setParameter("draft", (int) searchReqAndIwd.getDraft());
			procedureQuery.setParameter("approved", (int) searchReqAndIwd.getApproved());
			procedureQuery.setParameter("submitted", (int) searchReqAndIwd.getSubmitted());
			procedureQuery.setParameter("rejected", (int) searchReqAndIwd.getRejected());
			procedureQuery.setParameter("archieved", (int) searchReqAndIwd.getArchieved());
			procedureQuery.setParameter("fromdate", (java.util.Date) searchReqAndIwd.getFromDate());
			procedureQuery.setParameter("todate", (java.util.Date) searchReqAndIwd.getToDate());

			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			logger.info("/searchReq -" + " Get Result For Search Request and Inward Document by status and date - ");

			return resultValue;

		} 
		
		//search_and_modify_request_and_inward_by_from_date_and_to_date
		else if (searchReqAndIwd.getSearchDocument() == null && searchReqAndIwd.getFromDate() != null
				&& searchReqAndIwd.getToDate() != null && searchReqAndIwd.getDraft() == 0
				|| searchReqAndIwd.getApproved() == 0 || searchReqAndIwd.getSubmitted() == 0
				|| searchReqAndIwd.getRejected() == 0 || searchReqAndIwd.getArchieved() == 0) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_request_and_inward_by_from_date_and_to_date");

			procedureQuery.registerStoredProcedureParameter("fromdate", java.util.Date.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("todate", java.util.Date.class, ParameterMode.IN);

			procedureQuery.setParameter("fromdate", (java.util.Date) searchReqAndIwd.getFromDate());
			procedureQuery.setParameter("todate", (java.util.Date) searchReqAndIwd.getToDate());

			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			logger.info("/searchReq -" + " Get Result For Search Request and Inward Document by from date and to date- ");

			return resultValue;

		} 
		
		//search_and_modify_request_and_inward_by_status_and_date_and_name
		else if (searchReqAndIwd.getSearchDocument() != null && searchReqAndIwd.getFromDate() != null
				&& searchReqAndIwd.getToDate() != null && searchReqAndIwd.getDraft() == 1
				|| searchReqAndIwd.getApproved() == 1 || searchReqAndIwd.getSubmitted() == 1
				|| searchReqAndIwd.getRejected() == 1 || searchReqAndIwd.getArchieved() == 1) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_request_and_inward_by_status_and_date_and_name");
			procedureQuery.registerStoredProcedureParameter("searchdocument", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("draft", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("approved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("submitted", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("rejected", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("archieved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("fromdate", java.util.Date.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("todate", java.util.Date.class, ParameterMode.IN);

			procedureQuery.setParameter("searchdocument", (String) searchReqAndIwd.getSearchDocument());
			procedureQuery.setParameter("draft", (int) searchReqAndIwd.getDraft());
			procedureQuery.setParameter("approved", (int) searchReqAndIwd.getApproved());
			procedureQuery.setParameter("submitted", (int) searchReqAndIwd.getSubmitted());
			procedureQuery.setParameter("rejected", (int) searchReqAndIwd.getRejected());
			procedureQuery.setParameter("archieved", (int) searchReqAndIwd.getArchieved());
			procedureQuery.setParameter("fromdate", (java.util.Date) searchReqAndIwd.getFromDate());
			procedureQuery.setParameter("todate", (java.util.Date) searchReqAndIwd.getToDate());

			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			logger.info("/searchReq -" + " Get Result For Search Request and Inward Document by status and date - ");

			return resultValue;

		}

		else {
			return null;
		}
	}

////// Store procedure for getting search result for doc no ////////
	public String docNoForGettingCreateReqDetails(String docNo) {

		docNoVariable = docNo;
		System.out.println("doc no" + docNoVariable);
		String response = "{\"message\":\" " + docNoVariable + "\"}";
		logger.info("/CSalesDet -" + " Search Request and Inward document Details For Document Number" + docNoVariable);

		return response;
	}


	public List<Object[]> getCreateReqDetailsForSearchReq() {

		List<Object[]> resultValueSet;
		String docNo = docNoVariable;
		System.out.println("document number" + docNo);

		StoredProcedureQuery procedureQuery = em
				.createStoredProcedureQuery("create_request_details_for_search_request");
		procedureQuery.registerStoredProcedureParameter("doc_no", String.class, ParameterMode.IN);

		procedureQuery.setParameter("doc_no", (String) docNo);

		System.out.println("document number" + docNo);
		procedureQuery.execute();

		resultValueSet = procedureQuery.getResultList();

		logger.info("/CSalesDet -"
				+ "Get Details of Create Request and Inward screen For Search Request Inward document - " + docNo);

		return resultValueSet;

	}

}
