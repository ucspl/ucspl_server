package com.ucspl.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.ucspl.controller.RequestAndInwardDocumentController;
import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.ReqDocnoAndCustInstruRelationship;
import com.ucspl.repository.ReqDocnoAndCustInstruRelationshipRepository;

@Service
public class ReqDocnoAndCustInstruRelationshipService {


	private static final Logger logger = LogManager.getLogger(ReqDocnoAndCustInstruRelationshipService.class);

	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	private ReqDocnoAndCustInstruRelationshipRepository reqDocnoAndCustInstruRelationshipRepository;
	
	
	public ReqDocnoAndCustInstruRelationshipService() {
		// TODO Auto-generated constructor stub
	}

	public List<ReqDocnoAndCustInstruRelationship> getAllRequestAndInwardDocument() {

		logger.info("/reqDocCusInsIden - Get All Created Request Inward Document!");
		return reqDocnoAndCustInstruRelationshipRepository.findAll();
	}

	public ResponseEntity<ReqDocnoAndCustInstruRelationship> getRequestAndInwardDocumentById(
			Long reqDocnoAndCustInstruRelationshipId) throws ResourceNotFoundException {
		ReqDocnoAndCustInstruRelationship reqDocnoAndCustInstruRelationship = reqDocnoAndCustInstruRelationshipRepository
				.findById(reqDocnoAndCustInstruRelationshipId).orElseThrow(() -> new ResourceNotFoundException(
						" reqDocCusInsIden not found for this id :: " + reqDocnoAndCustInstruRelationshipId));

		logger.info("/reqDocCusInsIden/{id} - Get Request Inward Document By Id!" + reqDocnoAndCustInstruRelationshipId);

		return ResponseEntity.ok().body(reqDocnoAndCustInstruRelationship);
	}
	

	public ReqDocnoAndCustInstruRelationship createRequestAndInwardDocument(
			ReqDocnoAndCustInstruRelationship reqDocnoAndCustInstruRelationship) {

		logger.info("/reqDocCusInsIden - Save the Request Inward Document relationship!");
			
		return reqDocnoAndCustInstruRelationshipRepository.save(reqDocnoAndCustInstruRelationship);
	}
	
	public ResponseEntity<ReqDocnoAndCustInstruRelationship> updateReqDocnoAndCustInstruRelationshipByIdForCalibrationTag(
			Long createReqDocnoAndCustInstruRelationshipId,
			ReqDocnoAndCustInstruRelationship createReqDocnoAndCustInstruRelationshipDetails) throws ResourceNotFoundException {
		ReqDocnoAndCustInstruRelationship reqDocnoAndCustInstruRelationship = reqDocnoAndCustInstruRelationshipRepository
				.findById(createReqDocnoAndCustInstruRelationshipId).orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + createReqDocnoAndCustInstruRelationshipId));

		System.out.println("tag : "+createReqDocnoAndCustInstruRelationshipDetails.getCalibrationTag());
		reqDocnoAndCustInstruRelationship.setCalibrationTag(createReqDocnoAndCustInstruRelationshipDetails.getCalibrationTag());

		final ReqDocnoAndCustInstruRelationship updatedReqDocnoAndCustInstruRelationshipDetail = reqDocnoAndCustInstruRelationshipRepository
				.save(reqDocnoAndCustInstruRelationship);
		logger.info("/reqDocCusInsIden/{id} - Update Req Docno And CustInstru Relationship for id " + createReqDocnoAndCustInstruRelationshipId);

		return ResponseEntity.ok(updatedReqDocnoAndCustInstruRelationshipDetail);
	}
	
///////////////////////////store procedure to get Req Docno And CustInstru Relationship by custInstruIdentificationId /////////////////////////////
	
	public List<Object[]> srNoDetailsOfReqDocCustIdMasterInstru(@Valid @RequestBody long custInstruIdentificationId) {

		System.out.println("master id" + custInstruIdentificationId);

		logger.info("/srnoMSDet -" + " Search Req Docno And CustInstru Relationship Details For master id" + custInstruIdentificationId);
		
		List<Object[]> resultValue;

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("srno_of_reqdocno_and_custinstrurelationship_by_custinstruidentificationid");
		procedureQuery.registerStoredProcedureParameter("id_no", Long.class, ParameterMode.IN);

		procedureQuery.setParameter("id_no", (long) custInstruIdentificationId);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

	    return resultValue;
	}
	
	
///////////////////////////store procedure to get Req Docno And CustInstru Relationship by custInstruIdentificationId /////////////////////////////

	public List<Object[]> srNoDetailsOfReqDocCustIdMasterInstruByCreateReqInwId(@Valid @RequestBody long createReqInwId) {

		System.out.println("master id" + createReqInwId);

		logger.info("/srnoMSDet -" + " Search Req Docno And CustInstru Relationship Details For createReqInwId"
				+ createReqInwId);

		List<Object[]> resultValue;

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery(
				"srno_of_reqdocno_and_custinstrurelationship_by_createreqinwtableid");
		procedureQuery.registerStoredProcedureParameter("id_no", Long.class, ParameterMode.IN);

		procedureQuery.setParameter("id_no", (long) createReqInwId);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

		return resultValue;
	}	
	

}
