package com.ucspl.services;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.SalesQuotationDocumentCalculation;
import com.ucspl.repository.SalesQuotationDocumentCalculationRepository;

@Service
public class SalesQuotationDocumentCalculationService {

	private static final Logger logger = Logger.getLogger(SalesQuotationDocumentCalculationService.class);

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private SalesQuotationDocumentCalculationRepository salesQuotationDocumentCalculationRepository;

	private static SalesQuotationDocumentCalculation salesQuotationDocumentCalculation = new SalesQuotationDocumentCalculation();
	private String docNoVariable;

	// get All Create Sales Quotation Document
	public List<SalesQuotationDocumentCalculation> getAllCreateSalesQuotationDocument() {

		logger.info("/SalesQuotDocCal - Get all Sales Quotation Document calculations!");
		return salesQuotationDocumentCalculationRepository.findAll();
	}

//  Get Sales Quotation Document Calculations By Id
	public ResponseEntity<SalesQuotationDocumentCalculation> getSalesQuotationDocumentCalculationById(
			@PathVariable(value = "id") Long createSalesQuotationDocumentCalculationId)
			throws ResourceNotFoundException {

		SalesQuotationDocumentCalculation createSalesQuotationDocumentCalculation = salesQuotationDocumentCalculationRepository
				.findById(createSalesQuotationDocumentCalculationId).orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + createSalesQuotationDocumentCalculationId));

		logger.info("/SalesQuotDocCal/{id} - Get Sales Quotation Document Calculations By Id!"
				+ createSalesQuotationDocumentCalculationId);

		return ResponseEntity.ok().body(createSalesQuotationDocumentCalculation);
	}

	// Save Calculation of Sales Quotation Document
	public SalesQuotationDocumentCalculation createSalesQuotationDocumentCalculation(
			@Valid @RequestBody SalesQuotationDocumentCalculation salesQuotationDocumentCalculation) {

		logger.info("/SalesQuotDocCal - Save Calculation of Sales Quotation Document!");

		return salesQuotationDocumentCalculationRepository.save(salesQuotationDocumentCalculation);

	}

	// Update Sales Quotation Document calculation for id
	public ResponseEntity<SalesQuotationDocumentCalculation> updateSalesQuotationDocumentCalculationById(
			@PathVariable(value = "id") Long createSalesQuotationDocumentCalculationId,
			@Valid @RequestBody SalesQuotationDocumentCalculation salesQuotationDocumentCalculationDetails)
			throws ResourceNotFoundException {

		SalesQuotationDocumentCalculation salesQuotationDocumentCalculation = salesQuotationDocumentCalculationRepository
				.findById(createSalesQuotationDocumentCalculationId)
				.orElseThrow(() -> new ResourceNotFoundException(
						"Sales Quotation Document calculation not found for this id :: "
								+ createSalesQuotationDocumentCalculationId));

		salesQuotationDocumentCalculation
				.setDocumentNumber(salesQuotationDocumentCalculationDetails.getDocumentNumber());
		salesQuotationDocumentCalculation.setSubTotal(salesQuotationDocumentCalculationDetails.getSubTotal());
		salesQuotationDocumentCalculation
				.setDiscountOnSubtotal(salesQuotationDocumentCalculationDetails.getDiscountOnSubtotal());
		salesQuotationDocumentCalculation.setTotal(salesQuotationDocumentCalculationDetails.getTotal());
		salesQuotationDocumentCalculation.setCgst(salesQuotationDocumentCalculationDetails.getCgst());
		salesQuotationDocumentCalculation.setIgst(salesQuotationDocumentCalculationDetails.getIgst());
		salesQuotationDocumentCalculation.setSgst(salesQuotationDocumentCalculationDetails.getSgst());
		salesQuotationDocumentCalculation.setNet(salesQuotationDocumentCalculationDetails.getNet());

		final SalesQuotationDocumentCalculation updatedSalesQuotationDocumentCalculation = salesQuotationDocumentCalculationRepository
				.save(salesQuotationDocumentCalculation);

		logger.info("/SalesQuotDocCal/{id} - Update Sales Quotation Document calculation for id "
				+ createSalesQuotationDocumentCalculationId);

		return ResponseEntity.ok(updatedSalesQuotationDocumentCalculation);
	}

	// store procedure for doc calculation table detail

	public String DocNoForGettingDocDetails(@Valid @RequestBody String docNo) {

		docNoVariable = docNo;

		String response = "{\"message\":\" " + docNoVariable + "\"}";

		logger.info("/SQDocCalDet -" + docNoVariable
				+ " Document No for Getting Sales Quotation Document calculation details");

		return response;
	}

	// Get details of Sales Quotation Document calculation for Document No
	@SuppressWarnings("unchecked")
	public List<Object[]> getDocDetailsForDocNo() {

		List<Object[]> resultValue;
		String docNo = docNoVariable;

		StoredProcedureQuery procedureQuery = em
				.createStoredProcedureQuery("doc_details_for_sales_quotation_document_calculation");
		procedureQuery.registerStoredProcedureParameter("doc_no", String.class, ParameterMode.IN);

		procedureQuery.setParameter("doc_no", (String) docNo);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

		logger.info(
				"/SQDocCalDet - Get details of Sales Quotation Document calculation for Document No " + docNoVariable);

		return resultValue;

	}
	
	
		

}
