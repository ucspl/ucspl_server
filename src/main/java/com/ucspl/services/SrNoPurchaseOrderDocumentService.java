
package com.ucspl.services;

import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.SalesQuotationDocument;
import com.ucspl.model.SrNoPurchaseOrderDocument;
import com.ucspl.repository.SalesQuotationDocumentRepository;
import com.ucspl.repository.SrNoPurchaseOrderDocumentRepository;

@Service
public class SrNoPurchaseOrderDocumentService {

	private static final Logger logger = Logger.getLogger(SrNoPurchaseOrderDocumentService.class);

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private SrNoPurchaseOrderDocumentRepository srNoPurchaseOrderDocumentRepository;

	private static SalesQuotationDocument salesQuotationDocument = new SalesQuotationDocument();


	private String docNoVariable;

	
	// Get All purchase Order Document Item Information
		public List<SrNoPurchaseOrderDocument> getAllCreatePoDocument() {
			
			logger.info("/poDoc - Get All Sales Quotation Document Item Information!");

			return srNoPurchaseOrderDocumentRepository.findAll();
		}
		
	
	// Get PO Document Item Information By Id
		public ResponseEntity<SrNoPurchaseOrderDocument> getCreatePoDocumentById(
				@PathVariable(value = "id") Long createPoDocumentId) throws ResourceNotFoundException {

			SrNoPurchaseOrderDocument poDocument = srNoPurchaseOrderDocumentRepository
					.findById(createPoDocumentId).orElseThrow(() -> new ResourceNotFoundException(
							"Employee not found for this id :: " + createPoDocumentId));

			logger.info("/poDoc/{id} - Get PO Document Item Information By Id!"
					+ createPoDocumentId);

			return ResponseEntity.ok().body(poDocument);
			
		}


	// Save Item Details of Po Document
	public SrNoPurchaseOrderDocument createPoDocument(
			@Valid @RequestBody SrNoPurchaseOrderDocument srNoPurchaseOrderDocument) {

		logger.info("/poDoc - Save Item Details of po Document !");

		return srNoPurchaseOrderDocumentRepository.save(srNoPurchaseOrderDocument);
	}
	


	// Update Po Document Item Details for id
		public ResponseEntity<SrNoPurchaseOrderDocument> updatePoDocumentById(
				@PathVariable(value = "id") Long createPoDocumentId,
				@Valid @RequestBody SrNoPurchaseOrderDocument poDocumentDetails) throws ResourceNotFoundException {

			SrNoPurchaseOrderDocument srNoPurchaseOrderDocument = srNoPurchaseOrderDocumentRepository
				.findById(createPoDocumentId).orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + createPoDocumentId));

		srNoPurchaseOrderDocument.setDocumentNumber(poDocumentDetails.getDocumentNumber());
		srNoPurchaseOrderDocument.setSrNo(poDocumentDetails.getSrNo());
		srNoPurchaseOrderDocument.setInstruName(poDocumentDetails.getInstruName());
		srNoPurchaseOrderDocument.setDescription(poDocumentDetails.getDescription());
		srNoPurchaseOrderDocument.setIdNo(poDocumentDetails.getIdNo());
		srNoPurchaseOrderDocument.setQuantity(poDocumentDetails.getQuantity());
		srNoPurchaseOrderDocument.setRate(poDocumentDetails.getRate());
		srNoPurchaseOrderDocument.setDiscountOnItem(poDocumentDetails.getDiscountOnItem());
		srNoPurchaseOrderDocument.setAmount(poDocumentDetails.getAmount());
		srNoPurchaseOrderDocument.setCustomerPartNo(poDocumentDetails.getCustomerPartNo());
		srNoPurchaseOrderDocument.setHsnNo(poDocumentDetails.getHsnNo());
		srNoPurchaseOrderDocument.setRemainingQuantity(poDocumentDetails.getRemainingQuantity());
		srNoPurchaseOrderDocument.setRangeFrom(poDocumentDetails.getRangeFrom());
		srNoPurchaseOrderDocument.setRangeTo(poDocumentDetails.getRangeTo());
	
		

		final SrNoPurchaseOrderDocument updatedPoDocument = srNoPurchaseOrderDocumentRepository
				.save(srNoPurchaseOrderDocument);

		logger.info("/poDoc/{id} - Update PO Document Item Details for id "
				+ createPoDocumentId);
		return ResponseEntity.ok(updatedPoDocument);
	}

	// Store Procedure for getting Sr no for document number

	public String docNoForGettingSrNo(@Valid @RequestBody String docNo) {

		docNoVariable = docNo;

		String response = "{\"message\":\" " + docNoVariable + "\"}";

		logger.info("/srnopoDoc -" + docNoVariable + " Document No For Getting po  Document Item Details");

		return response;
	}
	
	

	// Get Items Details of Sales quotation Document for Document No

	public List<Object[]> getSrNoForDocNo() {

		List<Object[]> resultValue;
		String documentNo = docNoVariable;

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("sr_no_for_po_document");
		procedureQuery.registerStoredProcedureParameter("doc_no", String.class, ParameterMode.IN);

		procedureQuery.setParameter("doc_no", (String) documentNo);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

		logger.info("/srnopoDoc - Get Items Details of Po Document for Document No " + docNoVariable);

		return resultValue;

	}
	
	 // code to delete Po Row
	public String removePoRow(@Valid @RequestBody long indexToDelete) throws IOException {

		logger.info("/deletePoRow - delete row Item Details of po Document !");
	
		 srNoPurchaseOrderDocumentRepository.deleteById(indexToDelete);
		 
		 String response = "{\"message\":\" " + indexToDelete + "row deleted successfully" + "\"}";
		
	     return response;

	}

	
	
	
}
