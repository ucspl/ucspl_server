
 
package com.ucspl.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.BillingCreditPeriod;
import com.ucspl.model.Branch;
import com.ucspl.model.CalibrationAgencyOfMasterInstrument;
import com.ucspl.model.CalibrationLabTypeForMasterInstrument;
import com.ucspl.model.Country;
import com.ucspl.model.Department;
import com.ucspl.model.DocumentTypeForInvoice;
import com.ucspl.model.FinancialYear;
import com.ucspl.model.FormulaAccuracyReqAndIwd;
import com.ucspl.model.FrquencyUomOfMasterInstruments;
import com.ucspl.model.GroupForNablScopeMaster;
import com.ucspl.model.InstrumentList;
import com.ucspl.model.InstrumentTypeForMasterInstrument;
import com.ucspl.model.InvoiceTypeForInvoice;
import com.ucspl.model.InwardTypeOfRequestAndInward;
import com.ucspl.model.MasterTypeForMasterInstrument;
import com.ucspl.model.ModeTypeForMasterInstrument;
import com.ucspl.model.NablScopeName;
import com.ucspl.model.ParameterForReqAndIwd;
import com.ucspl.model.RangeTypeForMasterInstrument;
import com.ucspl.model.ReferenceForSalesQuotation;
import com.ucspl.model.RegionAreaCode;
import com.ucspl.model.ReqTypeOfRequestAndInward;
import com.ucspl.model.Role;
import com.ucspl.model.SearchDefinition;
import com.ucspl.model.SecurityQuestion;
import com.ucspl.model.SupplierType;
import com.ucspl.model.TaxpayerTypeForCustomer;
import com.ucspl.model.TempChartTypeForMasterInstrument;
import com.ucspl.model.Terms;
import com.ucspl.model.TypeOfQuotationForSalesAndQuotation;
import com.ucspl.model.UncDegreeOfFreedom;
import com.ucspl.model.UncDividingFactor;
import com.ucspl.model.UncFormulaForNabl;
import com.ucspl.model.UncMasterName;
import com.ucspl.model.UncMasterNumber;
import com.ucspl.model.UncMasterType;
import com.ucspl.model.UncProbabilityDistribution;
import com.ucspl.model.UncSourceOfUncertainty;
import com.ucspl.model.UncType;
import com.ucspl.model.UomListForReqAndIwd;
import com.ucspl.repository.BillingCreditPeriodRepository;
import com.ucspl.repository.BranchRepository;
import com.ucspl.repository.CalibrationAgencyOfMasterInstrumentRepository;
import com.ucspl.repository.CalibrationLabTypeForMasterInstrumentRepository;
import com.ucspl.repository.CountryRepository;
import com.ucspl.repository.DepartmentRepository;
import com.ucspl.repository.DocumentTypeForInvoiceRepository;
import com.ucspl.repository.FinancialYearRepository;
import com.ucspl.repository.FormulaAccuracyReqAndIwdRepository;
import com.ucspl.repository.FrquencyUomOfMasterInstrumentsRepository;
import com.ucspl.repository.GroupForNablScopeMasterRepository;
import com.ucspl.repository.InstrumentListRepository;
import com.ucspl.repository.InstrumentTypeForMasterInstrumentRepository;
import com.ucspl.repository.InvoiceTypeForInvoiceRepository;
import com.ucspl.repository.InwardTypeOfRequestAndInwardRepository;
import com.ucspl.repository.MasterTypeForMasterInstrumentRepository;
import com.ucspl.repository.ModeTypeForMasterInstrumentRepository;
import com.ucspl.repository.NablScopeNameRepository;
import com.ucspl.repository.ParameterForReqAndIwdRepository;
import com.ucspl.repository.RangeTypeForMasterInstrumentRepository;
import com.ucspl.repository.ReferenceForSalesQuotationRepository;
import com.ucspl.repository.RegionAreaCodeRepository;
import com.ucspl.repository.ReqTypeOfRequestAndInwardRepository;
import com.ucspl.repository.RoleRepository;
import com.ucspl.repository.SecurityQuestionRepository;
import com.ucspl.repository.SupplierTypeRepository;
import com.ucspl.repository.TaxpayerTypeForCustomerRepository;
import com.ucspl.repository.TempChartTypeForMasterInstrumentRepository;
import com.ucspl.repository.TermsRepository;
import com.ucspl.repository.TypeOfQuotationForSalesAndQuotationRepository;
import com.ucspl.repository.UncDegreeOfFreedomRepository;
import com.ucspl.repository.UncDividingFactorRepository;
import com.ucspl.repository.UncFormulaForNablRepository;
import com.ucspl.repository.UncMasterNameRepository;
import com.ucspl.repository.UncMasterNumberRepository;
import com.ucspl.repository.UncMasterTypeRepository;
import com.ucspl.repository.UncProbabilityDistributionRepository;
import com.ucspl.repository.UncSourceOfUncertaintyRepository;
import com.ucspl.repository.UncTypeRepository;
import com.ucspl.repository.UomListForReqAndIwdRepository;

@Service
public class CreateDefinitionService {

	private static final Logger logger = Logger.getLogger(CreateDefinitionService.class);

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private UomListForReqAndIwdRepository uomListForReqAndIwdRepository;

	@Autowired
	private ParameterForReqAndIwdRepository parameterForReqAndIwdRepository;

	@Autowired
	private BranchRepository createBranchRepositoryObject;

	@Autowired
	private RoleRepository createRoleRepositoryObject;

	@Autowired
	private DepartmentRepository createDepartmentRepositoryObject;

	@Autowired
	private SecurityQuestionRepository createSecurityQuestionRepositoryObject; 
	
	@Autowired
	private CountryRepository createCountryRepositoryObject;

	@Autowired
	private RegionAreaCodeRepository regionAreaCodeRepositoryObject;
	
	@Autowired
	private BillingCreditPeriodRepository billingCreditPeriodRepository;

	@Autowired
	private TaxpayerTypeForCustomerRepository taxpayerTypeForCustomerRepository;

	@Autowired
	private TypeOfQuotationForSalesAndQuotationRepository typeOfQuotationForSalesAndQuotationRepository;

	@Autowired
	private ReferenceForSalesQuotationRepository referenceForSalesQuotationRepository;
	
//	@Autowired
//	private FrquencyUomOfMasterInstrumentsRepository frquencyUomOfMasterInstrumentsRepository;
	
	
	@Autowired
	private InstrumentListRepository instrumentListRepository;
	@Autowired
	private TermsRepository termsRepository;
	@Autowired
	private SupplierTypeRepository supplierTypeRepository;
	@Autowired
	private InwardTypeOfRequestAndInwardRepository inwardTypeOfRequestAndInwardRepository;
	@Autowired
	private ReqTypeOfRequestAndInwardRepository reqTypeOfRequestAndInwardRepository;
	@Autowired
	private DocumentTypeForInvoiceRepository documentTypeForInvoiceRepository;

	@Autowired
	private InvoiceTypeForInvoiceRepository invoiceTypeForInvoiceRepository;

	@Autowired
	private FinancialYearRepository financialYearRepositoryObject;

	@Autowired
	private NablScopeNameRepository nablScopeNameRepository;

	@Autowired
	private GroupForNablScopeMasterRepository groupForNablScopeMasterRepository;

	@Autowired
	private FormulaAccuracyReqAndIwdRepository formulaAccuracyReqAndIwdRepository;

	@Autowired
	private CalibrationLabTypeForMasterInstrumentRepository calibrationLabTypeForMasterInstrumentRepository;
	
	@Autowired
	private MasterTypeForMasterInstrumentRepository masterTypeForMasterInstrumentRepository;
	
	@Autowired
	private InstrumentTypeForMasterInstrumentRepository instrumentTypeForMasterInstrumentRepository;
	
	@Autowired
	private RangeTypeForMasterInstrumentRepository rangeTypeForMasterInstrumentRepository;
	
	@Autowired
	private ModeTypeForMasterInstrumentRepository modeTypeForMasterInstrumentRepository;
	
	@Autowired
	private TempChartTypeForMasterInstrumentRepository tempChartTypeForMasterInstrumentRepository;
	
	@Autowired
	private CalibrationAgencyOfMasterInstrumentRepository calibrationAgencyOfMasterInstrumentRepository;
	
	@Autowired
	private FrquencyUomOfMasterInstrumentsRepository frquencyUomOfMasterInstrumentsRepository;
	
	@Autowired
	private UncFormulaForNablRepository uncFormulaForNablRepository;
	
	
	@Autowired
	private UncMasterTypeRepository uncMasterTypeRepository;
	
	@Autowired
	private UncMasterNumberRepository uncMasterNumberRepository;
	
	@Autowired
	private UncMasterNameRepository uncMasterNameRepository;
	

	@Autowired
	private UncProbabilityDistributionRepository uncProbabilityDistributionRepository;
	
	@Autowired
	private UncSourceOfUncertaintyRepository uncSourceOfUncertaintyRepository;
	
	@Autowired
	private UncTypeRepository uncTypeRepository;
	
	@Autowired
	private UncDividingFactorRepository uncDividingFactorRepository;
	
	@Autowired
	private UncDegreeOfFreedomRepository uncDegreeOfFreedomRepository;
	


	


	private SearchDefinition searchDefinitionObject = new SearchDefinition(); 
	
	
	// get invoice type list for invoice
		public List<InvoiceTypeForInvoice> getAllInvoiceTypeForInvoice() {

			logger.info("/InvoiceTypeOfInv - Get all invoice type for invoice!");
			return invoiceTypeForInvoiceRepository.findAll();
		}
		
		
		// get country list
		public List<Country> getAllCountries() {

			logger.info("/country -Get All Countries!");
			return createCountryRepositoryObject.findAll();
		}
		
		// get region area code list
		public List<RegionAreaCode> getAllRegionAreaCode() {

			logger.info("/regionareacode -Get All region area code!");
			return regionAreaCodeRepositoryObject.findAll();

		}

		// get Billing Credit Period list
		public List<BillingCreditPeriod> getAllBillingCreditPeriod() {
			logger.info("/billcredperiod -Get All Billing Credit Period!");

			return billingCreditPeriodRepository.findAll();

		}

		// get taxpayer type list
		public List<TaxpayerTypeForCustomer> getAllTaxpayerType() {

			logger.info("/taxptype -Get All Taxpayer Type!");

			return taxpayerTypeForCustomerRepository.findAll();

		}

		// get quotation type list
		public List<TypeOfQuotationForSalesAndQuotation> getAllQuotationType() {
			logger.info("/quotationtype -Get All Quotation Type!");

			return typeOfQuotationForSalesAndQuotationRepository.findAll();

		}

		// get reference list for sales and quotation
		public List<ReferenceForSalesQuotation> getAllreferences() {

			logger.info("/referenceforsales -reference list for sales and quotation!");

			return referenceForSalesQuotationRepository.findAll();

		}
		
		// get instrument list
		public List<InstrumentList> getAllInstruments() {
			logger.info("/instruList-get instrument list");

			return instrumentListRepository.findAll();
		}

		// get all terms list
		public List<Terms> getAllTerms() {

			logger.info("/terms-get terms list");
			return termsRepository.findAll();

		}

		// get all supplier type list)
		public List<SupplierType> getAllSupplierTypes() {
			logger.info("/supplierType-get all supplier type list");

			return supplierTypeRepository.findAll();

		}

		// Get all inward type for Request And Inward Type
		public List<InwardTypeOfRequestAndInward> getAllInwardTypes() {
			logger.info("/reqInwType - Get all inward type for Request And Inward Type!");
			return inwardTypeOfRequestAndInwardRepository.findAll();
		}

		// get All request Type Of Request And Inwards
		public List<ReqTypeOfRequestAndInward> getAllreqTypeOfRequestAndInwards() {
			logger.info("/reqType - get All request Type Of Request And Inwards!");
			return reqTypeOfRequestAndInwardRepository.findAll();
		}

		// get document type list for invoice
		public List<DocumentTypeForInvoice> getAllDocumentTypes() {

			logger.info("/DocTypeOfInv - get All Document Type Of Invoice!");
			return documentTypeForInvoiceRepository.findAll();

		}

		// get financial year list
		public List<FinancialYear> getAllFinancialYears() {

			logger.info("/financialYear - get All Financial year date!");
			return financialYearRepositoryObject.findAll();

		}

		// get nabl Scope Name list
		public List<NablScopeName> getAllNablScopeNames() {
			logger.info("/nablScopeName - Get all nabl Scope Name!");
			return nablScopeNameRepository.findAll();

		}

		// get nabl group list
		public List<GroupForNablScopeMaster> getAllGroups() {

			logger.info("/groupForNabl - Get all nabl group Name!");
			System.out.println("group here");
			return groupForNablScopeMasterRepository.findAll();
			

		}
	
	// Get all unc formula Name
	public List<UncFormulaForNabl> getAllUncFormula() {

	logger.info("/uncFormula - Get all unc formula Name!");
	return uncFormulaForNablRepository.findAll();
	}
	// get frequency uom list
	public List<FrquencyUomOfMasterInstruments> getAllFrequencyUoms() {

	    logger.info("/frequencyUom - Get all frequency uom list!");
	    return frquencyUomOfMasterInstrumentsRepository.findAll();

	}
	
	//get calibration agency Type For MasterInstrument list
	public List<CalibrationAgencyOfMasterInstrument> getAllCalibrationAgency() {
		
		logger.info("/calAgency - Get all mode Type For Master Instrument List!");
		return calibrationAgencyOfMasterInstrumentRepository.findAll();
	}
	
	//get mode Type For MasterInstrument list
	public List<TempChartTypeForMasterInstrument> getAllTempChartTypeForMasterInstruments() {
		
		logger.info("/temChart - Get all mode Type For Master Instrument List!");
		return tempChartTypeForMasterInstrumentRepository.findAll();
	}
	
	//get mode Type For MasterInstrument list
	public List<ModeTypeForMasterInstrument> getAllModeTypesOfMasterInstrument() {
		
		logger.info("/mModeType - Get all mode Type For Master Instrument List!");
		return modeTypeForMasterInstrumentRepository.findAll();
	}
	
	//get range Type For MasterInstrument list
	public List<RangeTypeForMasterInstrument> getAllRangeTypeForMasterInstrument() {
		
		logger.info("/mInsType - Get all range Type For Master Instrument List!");
		return rangeTypeForMasterInstrumentRepository.findAll();
	}
	
	//get instrument Type For MasterInstrument list
	public List<InstrumentTypeForMasterInstrument> getAllInstrumentTypeForMasterInstrument() {
		
		logger.info("/mInsType - Get all Instrument Type For Master Instrument List!");
		return instrumentTypeForMasterInstrumentRepository.findAll();
	}
	
	
	//get master Type For MasterInstrument list
	public List<MasterTypeForMasterInstrument> getAllMasterTypeForMasterInstrument() {
		
		logger.info("/mMasType - Get all Master Type For Master Instrument List!");
		return masterTypeForMasterInstrumentRepository.findAll();
	}
	
	//get Calibration Lab Type For MasterInstrument list
	public List<CalibrationLabTypeForMasterInstrument> getAllCalibrationLabTypeForMasterInstrument() {
		
		logger.info("/mCLType - Get all CalibrationLabTypeForMasterInstrument List!");
		return calibrationLabTypeForMasterInstrumentRepository.findAll();
	}

	// get uom list
	public List<UomListForReqAndIwd> getAllUomList() {
		
		logger.info("/uomList - Get all Uom List!");
		return uomListForReqAndIwdRepository.findAll();
	}

	// get parameter list
	public List<ParameterForReqAndIwd> getAllParameterList() {
		
		logger.info("/parameterList - Get all Parameter List!");
		return parameterForReqAndIwdRepository.findAll();
	}

	// get formula accuracy list
	public List<FormulaAccuracyReqAndIwd> getAllFormulaAccuracy() {
		
		logger.info("/formulaAccuList - Get all formula accuracy List!");
		return formulaAccuracyReqAndIwdRepository.findAll();
	}

	// get branch list
	public List<Branch> getAllBranches() {
	    
		logger.info("/branch - Get all Branches!");
		return createBranchRepositoryObject.findAll();
	}

	// get security question list
	public List<SecurityQuestion> SecurityQuestion() {

		logger.info("/securityQuestion - Get all Security Question!");
		return createSecurityQuestionRepositoryObject.findAll();
	}

	// get role details
	public List<Role> getAllRoles() {

		logger.info("/role - Get all roles!");
		return createRoleRepositoryObject.findAll();
	}

	// get department list
	public List<Department> getAllDepartment() {

		logger.info("Get All Departments!");
		return createDepartmentRepositoryObject.findAll();
	}

	// get uncMasterType list
	public List<UncMasterType> getAllUncMasterTypeList() {

		logger.info("Get All UncMasterType!");
		return uncMasterTypeRepository.findAll();
	}
	
	
	
	// get uncMasterType list
	public List<UncMasterNumber> getAllUncMasterNumber() {

		logger.info("Get All crted UncMaster Number!");
		return uncMasterNumberRepository.findAll();
	}
	
	// get uncMasterName list
	public List<UncMasterName> getAllUncMasterNames() {

		logger.info("Get All crted UncMaster Name!");
		return uncMasterNameRepository.findAll();
	}
	
	
	// get Probability Distribution list
	public List<UncProbabilityDistribution> getAllUncProbabilityDistribution() {

		logger.info("Get All crted Probability Distribution!");
		return uncProbabilityDistributionRepository.findAll();
	}
	
	
	// get  Source Of Uncertainty list
	public List<UncSourceOfUncertainty> getAllUncSourceOfUncertainty() {

		logger.info("Get All crted Source Of Uncertainty!");
		return uncSourceOfUncertaintyRepository.findAll();
	}
	
	
	// get uncMaster Type list
	public List<UncType> getAllUncType() {

		logger.info("Get All crted Unc Type!");
		return uncTypeRepository.findAll();
	}
	
	
	// get Dividing Factor list
	public List<UncDividingFactor> getAllUncDividingFactor() {

		logger.info("Get All crted Dividing Factor!");
		return uncDividingFactorRepository.findAll();
	}
	
	
	// get Degree Of Freedom list
	public List<UncDegreeOfFreedom> getAllUncDegreeOfFreedom() {

		logger.info("Get All crted Unc Degree Of Freedom!");
		return uncDegreeOfFreedomRepository.findAll();
	}
	
	
	
//////////////////////////////////////create functionalities/////////////////////////// 
	
	
	
	// Save create uncMasterType Details
	public UncMasterType createUncMasterType(@Valid @RequestBody UncMasterType uncMasterType) {

		logger.info("/uncMasType -Save create uncMasType Details!" + " " + "User Name:" + uncMasterType.getCreatedBy());

		return uncMasterTypeRepository.save(uncMasterType);
	}
	
	
	// Save create uncMasterNumber Details
	public UncMasterNumber createUncMasterNumber(@Valid @RequestBody UncMasterNumber uncMasterNumber) {

		logger.info("/crtUncMastNo -Save create uncMasNumber Details!" + " " + "User Name:" + uncMasterNumber.getCreatedBy());

		return uncMasterNumberRepository.save(uncMasterNumber);
	}
	
	
	// Save create uncMasterName Details
	public UncMasterName createUncMasterName(@Valid @RequestBody UncMasterName uncMasterName) {

		logger.info("/crtUncMastName -Save create uncMasName Details!" + " " + "User Name:" + uncMasterName.getCreatedBy());

		return uncMasterNameRepository.save(uncMasterName);
	}
	
	
	// Save create uncMasterType Details
	public UncProbabilityDistribution creatUncProbabilityDistribution(@Valid @RequestBody UncProbabilityDistribution uncProbabilityDistribution) {

		logger.info("/crtUncProbDistribu -Save create UncProbabilityDistribution Details!" + " " + "User Name:" + uncProbabilityDistribution.getCreatedBy());

		return uncProbabilityDistributionRepository.save(uncProbabilityDistribution);
	}
	
	
	// Save create uncMasterNumber Details
	public UncSourceOfUncertainty createUncSourceOfUncertainty(@Valid @RequestBody UncSourceOfUncertainty uncSourceOfUncertainty) {

		logger.info("/crtUncSouOfUncer -Save create UncSourceOfUncertainty Details!" + " " + "User Name:" + uncSourceOfUncertainty.getCreatedBy());

		return uncSourceOfUncertaintyRepository.save(uncSourceOfUncertainty);
	}
	
	// Save create crtUncType Details
	public UncType createUncType(@Valid @RequestBody UncType uncType) {

		logger.info("/crtUncType -Save create crt Unc Type Details!" + " " + "User Name:" + uncType.getCreatedBy());

		return uncTypeRepository.save(uncType);
	}
	
	
	// Save create uncMasterNumber Details
	public UncDividingFactor createUncDividingFactor(@Valid @RequestBody UncDividingFactor uncDividingFactor) {

		logger.info("/crtUncDivFactor -Save create UncDividingFactor Details!" + " " + "User Name:" + uncDividingFactor.getCreatedBy());

		return uncDividingFactorRepository.save(uncDividingFactor);
	}
	
	// Save create uncMasterType Details
	public UncDegreeOfFreedom createUncDegreeOfFreedom(@Valid @RequestBody UncDegreeOfFreedom uncDegreeOfFreedom) {

		logger.info("/crtUncDeFredom -Save create uncMasType Details!" + " " + "User Name:" + uncDegreeOfFreedom.getCreatedBy());

		return uncDegreeOfFreedomRepository.save(uncDegreeOfFreedom);
	}
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// Save create frequency uom Details
	public FrquencyUomOfMasterInstruments createFrequencyUom(@Valid @RequestBody FrquencyUomOfMasterInstruments frquencyUomOfMasterInstrumentsObject) {

		logger.info("/createFrequencyUom -Save create frequency uom Details!" + " " + "User Name:" + frquencyUomOfMasterInstrumentsObject.getCreatedBy());

		return frquencyUomOfMasterInstrumentsRepository.save(frquencyUomOfMasterInstrumentsObject);
	}

	// Save create nabl Scope Name
		public NablScopeName createNablScopeName(NablScopeName nablScopeNameObject) {

			logger.info("/createNablScopeName - Save create nabl Scope Name Details!" + " " + "User Name:"
					+ nablScopeNameObject.getCreatedBy());

			return nablScopeNameRepository.save(nablScopeNameObject);

		}

		// Save create nabl group
		public GroupForNablScopeMaster createNablGroup(GroupForNablScopeMaster groupForNablScopeMasterObject) {

			logger.info("/createNablGroup - Save create nabl Scope Name Details!" + " " + "User Name:"
					+ groupForNablScopeMasterObject.getCreatedBy());

			return groupForNablScopeMasterRepository.save(groupForNablScopeMasterObject);
		}

		// save create country

		public Country createCountry(@Valid @RequestBody Country countryObject) {

			logger.info(
					"/createCountry - Save Create Country Details!" + " " + "User Name:" + countryObject.getCreatedBy());
			return createCountryRepositoryObject.save(countryObject);

		}
		
		// Save create region area code

		public RegionAreaCode createRegionAreaCode(@Valid @RequestBody RegionAreaCode regionAreaCodeObject) {

			logger.info("/createRegionAreaCode - Save Create Region Area Code Details!" + " " + "User Name:"
					+ regionAreaCodeObject.getCreatedBy());
			return regionAreaCodeRepositoryObject.save(regionAreaCodeObject);

		}

		// Save create Billing Credit Period
		public BillingCreditPeriod createBillingCreditPeriod(
				@Valid @RequestBody BillingCreditPeriod billingCreditPeriodObject) {

			logger.info("/createBillingCreditPeriod - Save Create Billing Credit Period Details!" + " " + "User Name:"
					+ billingCreditPeriodObject.getCreatedBy());
			return billingCreditPeriodRepository.save(billingCreditPeriodObject);

		}

		// Save create Taxpayer Type
		public TaxpayerTypeForCustomer createTaxpayerTypeForCustomer(
				@Valid @RequestBody TaxpayerTypeForCustomer taxpayerTypeForCustomerObject) {

			logger.info("/createTaxpayerType - Save Create taxpayer type Details!" + " " + "User Name:"
					+ taxpayerTypeForCustomerObject.getCreatedBy());
			return taxpayerTypeForCustomerRepository.save(taxpayerTypeForCustomerObject);
		}

		// Save create quotation type Details
		public TypeOfQuotationForSalesAndQuotation createTypeOfQuotationForSalesAndQuotation(
				@Valid @RequestBody TypeOfQuotationForSalesAndQuotation typeOfQuotationForSalesAndQuotationObject) {

			logger.info("/createTypeOfQuotation - Save Create Type of Quotation Details!" + " " + "User Name:"
					+ typeOfQuotationForSalesAndQuotationObject.getCreatedBy());
			return typeOfQuotationForSalesAndQuotationRepository.save(typeOfQuotationForSalesAndQuotationObject);

		}

		// Save create reference for sales and quotation
		public ReferenceForSalesQuotation createReferenceForSalesQuotation(
				@Valid @RequestBody ReferenceForSalesQuotation referenceForSalesQuotationObject) {

			logger.info("/createRefNoForQuotation - Save Create reference for sales and quotation Details!" + " "
					+ "User Name:" + referenceForSalesQuotationObject.getCreatedBy());
			return referenceForSalesQuotationRepository.save(referenceForSalesQuotationObject);
		}

		// save instrument list
		public InstrumentList createInstrument(@Valid @RequestBody InstrumentList instrumentListObject) {
			logger.info(
					"/createInstrument-save instrument details" + " " + "UserName:" + instrumentListObject.getCreatedBy());

			return instrumentListRepository.save(instrumentListObject);
		}

		// save create terms
		public Terms createTerms(@Valid @RequestBody Terms termObject) {

			logger.info("/createTerms-save terms detail" + " " + termObject.getCreatedBy());

			return termsRepository.save(termObject);

		}

		// save create supplier type
		public SupplierType createSupplierType(@Valid @RequestBody SupplierType supplierTypeObject) {

			logger.info("/createSupplierType-save create supplier type" + " " + supplierTypeObject.getCreatedBy());

			return supplierTypeRepository.save(supplierTypeObject);
		}
	
	// Save create unc formula Details
	public UncFormulaForNabl createUncFormula(@Valid @RequestBody UncFormulaForNabl uncFormulaForNablObject) {

	logger.info("/createUncFormula - Save create unc formula Details!" + " " + "User Name:" + uncFormulaForNablObject.getCreatedBy());
	return uncFormulaForNablRepository.save(uncFormulaForNablObject);
	}
	

	// code for create calibration agency type
	public CalibrationAgencyOfMasterInstrument createCalibrationAgency(
			CalibrationAgencyOfMasterInstrument calibrationAgencyOfMasterInstrumentData) {

		logger.info("/calAgency - Save temp chart Type For Master Instrument Details!" + " " + "User Name:"
				+ calibrationAgencyOfMasterInstrumentData.getCreatedBy());
		return calibrationAgencyOfMasterInstrumentRepository.save(calibrationAgencyOfMasterInstrumentData);
	}
	
	// code for create parameter type
	public ParameterForReqAndIwd createParameter(
			ParameterForReqAndIwd paraData) {

		logger.info("/parameterList - Save temp chart Type For Master Instrument Details!" + " " + "User Name:"
				+ paraData.getCreatedBy());
		return parameterForReqAndIwdRepository.save(paraData);
	}
	
	// code for create uom type
	public UomListForReqAndIwd createUom(
			UomListForReqAndIwd uomData) {

		logger.info("/uomList - Save uom  Details!" + " " + "User Name:"
				+ uomData.getCreatedBy());
		return uomListForReqAndIwdRepository.save(uomData);
	}
	
	
	// code for create temp chart type of master instrument
	public TempChartTypeForMasterInstrument createTempChartTypeForMasterInstrument(
			TempChartTypeForMasterInstrument tempChartTypeForMasterInstrument) {

		logger.info("/temChart - Save temp chart Type For Master Instrument Details!" + " " + "User Name:"
				+ tempChartTypeForMasterInstrument.getCreatedBy());
		return tempChartTypeForMasterInstrumentRepository.save(tempChartTypeForMasterInstrument);
	}
	
	
	
	// code for create mode type of master instrument
	public ModeTypeForMasterInstrument createModeTypesOfMasterInstrument(
			ModeTypeForMasterInstrument modeTypeForMasterInstrumentObject) {

		logger.info("/mModeType - Save mode Type For Master Instrument Details!" + " " + "User Name:"
				+ modeTypeForMasterInstrumentObject.getCreatedBy());
		return modeTypeForMasterInstrumentRepository.save(modeTypeForMasterInstrumentObject);
	}
	
	
	
	// code for create range type of master instrument
	public RangeTypeForMasterInstrument createRangeTypeForMasterInstrument(
			RangeTypeForMasterInstrument rangeTypeForMasterInstrumentObject) {

		logger.info("/mRangeType - Save range Type For Master Instrument Details!" + " " + "User Name:"
				+ rangeTypeForMasterInstrumentObject.getCreatedBy());
		return rangeTypeForMasterInstrumentRepository.save(rangeTypeForMasterInstrumentObject);
	}
	
	// code for create instrument type of master instrument
	public InstrumentTypeForMasterInstrument createInstrumentTypeForMasterInstrument(
			InstrumentTypeForMasterInstrument instrumentTypeForMasterInstrumentObject) {

		logger.info("/mInsType - Save create instrument Type For Master Instrument Details!" + " " + "User Name:"
				+ instrumentTypeForMasterInstrumentObject.getCreatedBy());
		return instrumentTypeForMasterInstrumentRepository.save(instrumentTypeForMasterInstrumentObject);
	}
	
	
	// code for create master type of master instrument
	public MasterTypeForMasterInstrument createMasterTypeForMasterInstrument(
			MasterTypeForMasterInstrument masterTypeForMasterInstrumentObject) {

		logger.info("/mMasType - Save create Master Type For Master Instrument Details!" + " " + "User Name:"
				+ masterTypeForMasterInstrumentObject.getCreatedBy());
		return masterTypeForMasterInstrumentRepository.save(masterTypeForMasterInstrumentObject);
	}
	
	// code for create master calibration lab
	public CalibrationLabTypeForMasterInstrument createCalibrationLabTypeForMasterInstrument(
			CalibrationLabTypeForMasterInstrument calibrationLabTypeForMasterInstrumentObject) {

		logger.info("/mCLType - Save calibration type of master instrument Details!" + " " + "User Name:"
				+ calibrationLabTypeForMasterInstrumentObject.getCreatedBy());
		return calibrationLabTypeForMasterInstrumentRepository.save(calibrationLabTypeForMasterInstrumentObject);
	}

	// code for create branch
	public Branch createBranch(Branch branchObject) {

		logger.info("/createbranch - Save Create branch Details!" + " " + "User Name:" + branchObject.getCreatedBy());
		return createBranchRepositoryObject.save(branchObject);
	}

	// code for create role
	public Role createRole(Role roleObject) {

		logger.info("/createrole - Save Create role Details!" + " " + "User Name:" + roleObject.getCreatedBy());
		return createRoleRepositoryObject.save(roleObject);
	}

	// code for create department
	public Department createDepartment(Department departmentObject) {

		logger.info("/createdepartment - Save Create department Details!" + " " + "User Name:"
				+ departmentObject.getCreatedBy());
		return createDepartmentRepositoryObject.save(departmentObject);
	}

	// Save Security Question Details
	public SecurityQuestion createSecurityQuestion(@Valid @RequestBody SecurityQuestion securityQuestionObject) {

		logger.info("/createSecurityQuestion - Save Create security question Details!" + " " + "User Name:"
				+ securityQuestionObject.getCreatedBy());
		return createSecurityQuestionRepositoryObject.save(securityQuestionObject);
	}
	
	// save inward type for Request And Inward
		public InwardTypeOfRequestAndInward createInwardTypeOfRequestAndInward(
				@Valid @RequestBody InwardTypeOfRequestAndInward inwardTypeOfRequestAndInwardObject) {

			logger.info("/createReqInwType-save inward type for Request And Inward " + " "
					+ inwardTypeOfRequestAndInwardObject.getCreatedBy());

			return inwardTypeOfRequestAndInwardRepository.save(inwardTypeOfRequestAndInwardObject);

		}

		// Save create request Type Of Request And Inwards
		public ReqTypeOfRequestAndInward createReqTypeOfRequestAndInward(
				@Valid @RequestBody ReqTypeOfRequestAndInward reqTypeOfRequestAndInwardObject) {

			logger.info("/createRequestType-Save create request Type Of Request And Inwards" + " "
					+ reqTypeOfRequestAndInwardObject.getCreatedBy());

			return reqTypeOfRequestAndInwardRepository.save(reqTypeOfRequestAndInwardObject);
		}

		// Save create financial year
		public FinancialYear createFinancialYear(@Valid @RequestBody FinancialYear financialYearObject) {

			logger.info("/createFinancialYear-Save financial year date" + " " + financialYearObject.getCreatedBy());

			return financialYearRepositoryObject.save(financialYearObject);
		}
		
		// Update Financial Year for id
		public ResponseEntity<FinancialYear> updateFinancialYear(Long financialYearId,
				@Valid @RequestBody FinancialYear financialYearDetails) throws ResourceNotFoundException {

			FinancialYear financialYear = financialYearRepositoryObject.findById(financialYearId)
					.orElseThrow(() -> new ResourceNotFoundException("date not found for this id :: " + financialYearId));

			financialYear.setStatus(financialYearDetails.getStatus());

			final FinancialYear updatedFinancialYear = financialYearRepositoryObject.save(financialYear);

			logger.info("/createFinancialYear/{id} - Update Financial Year Details for id - " + financialYearId);

			return ResponseEntity.ok(updatedFinancialYear);

		}

//	// code for search and modify sales and quotation
//
//	public String searchAndModifyDefinition(@Valid @RequestBody SearchDefinition searchDefinitionObject1) {
//
//		searchDefinitionObject = searchDefinitionObject1;
//		String response = "{\"message\":\"Post With ngResource: The Search value for definition: "
//				+ searchDefinitionObject.getSearchDefinitionValue() + "\"}";
//		logger.info("/searchdefinition - Search definition For " + searchDefinitionObject1);
//		return response;
//	}
//
//	// Result For Search Definition
//	public List<Object[]> getAllDefinition() {
//
//		List<Object[]> resultValue;
//
//		StoredProcedureQuery procedureQuery = em
//				.createStoredProcedureQuery("search_and_modify_definition_by_definitionvalue_and_status");
//
//		procedureQuery.registerStoredProcedureParameter("searchdefinition", String.class, ParameterMode.IN);
//		procedureQuery.registerStoredProcedureParameter("status", Integer.class, ParameterMode.IN);
//
//		procedureQuery.setParameter("searchdefinition", (String) searchDefinitionObject.getSearchDefinitionValue());
//		procedureQuery.setParameter("status", (int) searchDefinitionObject.getStatus());
//
//		procedureQuery.execute();
//
//		resultValue = procedureQuery.getResultList();
//
//		logger.info("/searchdefinition -" + " Get Result For Search definition by definitional Value and status - "
//				+ searchDefinitionObject.getSearchDefinitionValue());
//		return resultValue;
//	}

}

