
package com.ucspl.services;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateProformaDocument;
import com.ucspl.model.SearchProformaInvoice;
import com.ucspl.repository.CreateProformaDocumentRepository;

@Service
public class CreateProformaDocumentService {

	private static final Logger logger = Logger.getLogger(CreateProformaDocumentService.class);
	@Autowired
	private CreateProformaDocumentRepository createProformaDocumentRepository;

	@PersistenceContext
	private EntityManager em;

	private String docNoVariable;

	private SearchProformaInvoice searchProformaInvoiceObj = new SearchProformaInvoice();

	// Get Get All proforma Document Header Information By Id
	public List<CreateProformaDocument> getAllCreateProformaDocument() {

		logger.info("/createproforma - Get All proforma Document Header Information!");
		return createProformaDocumentRepository.findAll();
	}

	// Get Proforma Document Header Information By Id
	public ResponseEntity<CreateProformaDocument> getCreateProformaDocumentById(
			@PathVariable(value = "id") Long createProformaDocumentId) throws ResourceNotFoundException {
		CreateProformaDocument createProformaDocument = createProformaDocumentRepository
				.findById(createProformaDocumentId).orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + createProformaDocumentId));

		logger.info(
				"/createproforma/{id} -  Get Proforma Document Header Information By Id!" + createProformaDocumentId);

		return ResponseEntity.ok().body(createProformaDocument);
	}

	// Save Header Details of proforma Document
	public CreateProformaDocument createProformaDocument(
			@Valid @RequestBody CreateProformaDocument createProformaDocument) {

		logger.info("/createInv - Save Header Details of create Proforma Document!");

		return createProformaDocumentRepository.save(createProformaDocument);
	}

	// Update Proforma Document Header Details for id
	public ResponseEntity<CreateProformaDocument> updateCreateProformaDocumentById(
			@PathVariable(value = "id") Long createProformaDocumentId,
			@Valid @RequestBody CreateProformaDocument createProformaDocumentDetails) throws ResourceNotFoundException {
		CreateProformaDocument createProformaDocument = createProformaDocumentRepository
				.findById(createProformaDocumentId).orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + createProformaDocumentId));

		createProformaDocument.setDocumentTypeId(createProformaDocumentDetails.getDocumentTypeId());
		createProformaDocument.setProformaDocumentNumber(createProformaDocumentDetails.getProformaDocumentNumber());
		createProformaDocument.setBranchId(createProformaDocumentDetails.getBranchId());
		createProformaDocument.setInvoiceTypeId(createProformaDocumentDetails.getInvoiceTypeId());
		createProformaDocument.setCustShippedTo(createProformaDocumentDetails.getCustShippedTo());
		createProformaDocument.setCustBilledTo(createProformaDocumentDetails.getCustBilledTo());
		createProformaDocument.setInvoiceDate(createProformaDocumentDetails.getInvoiceDate());
		createProformaDocument.setPoDate(createProformaDocumentDetails.getPoDate());
		createProformaDocument.setDcDate(createProformaDocumentDetails.getDcDate());
		createProformaDocument.setPoNo(createProformaDocumentDetails.getPoNo());
		createProformaDocument.setDcNo(createProformaDocumentDetails.getDcNo());
		createProformaDocument.setRequestNumber(createProformaDocumentDetails.getRequestNumber());
		createProformaDocument.setVendorCodeNumber(createProformaDocumentDetails.getVendorCodeNumber());
		createProformaDocument.setArchieveDate(createProformaDocumentDetails.getArchieveDate());
		createProformaDocument.setCreatedBy(createProformaDocumentDetails.getCreatedBy());
		createProformaDocument.setDraft(createProformaDocumentDetails.getDraft());
		createProformaDocument.setApproved(createProformaDocumentDetails.getApproved());
		createProformaDocument.setArchieved(createProformaDocumentDetails.getArchieved());
		createProformaDocument.setSubmitted(createProformaDocumentDetails.getSubmitted());
		createProformaDocument.setRejected(createProformaDocumentDetails.getRejected());
		createProformaDocument.setRemark(createProformaDocumentDetails.getRemark());

		final CreateProformaDocument updatedProformaDocument = createProformaDocumentRepository.save(createProformaDocument);
		logger.info("/createproforma/{id} - Update proforma Document Header Details for id " + createProformaDocumentId);

		return ResponseEntity.ok(updatedProformaDocument);
	}

	////////////////////// doc no for invoice ///////////////////////////

	public String getDocumentNumber() throws IOException {

		Object documentNumberId;
		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("document_number_for_proforma_document");
		procedureQuery.execute();

		documentNumberId = procedureQuery.getSingleResult();
		String stringToConvert = String.valueOf(documentNumberId);
		String response = "{\"number\":\"" + stringToConvert + "\"}";
		logger.info("/DocNoForProforma -" + docNoVariable + " Document No For Getting proforma Document Header Details");

		return response;
	}

	// code for search and modify Invoice
	public String searchAndModifyProformaDocument(@Valid @RequestBody SearchProformaInvoice searchProformaDocObject) {

		searchProformaInvoiceObj = searchProformaDocObject;

		String response = "{\"message\":\"Post With ngResource: The user firstName: "
				+ searchProformaInvoiceObj.getSearchDocument() + "\"}";

		logger.info("/searchProforma -" + " Search proforma Documents For " + searchProformaInvoiceObj);

		return response;
	}

	// Result For Search proforma Document
	public List<Object[]> getAllProformaDocument() {

		List<Object[]> resultValue;

		// search by document number
		if (searchProformaInvoiceObj.getSearchDocument() != null && searchProformaInvoiceObj.getDraft() == 0
				&& searchProformaInvoiceObj.getApproved() == 0 && searchProformaInvoiceObj.getSubmitted() == 0
				&& searchProformaInvoiceObj.getRejected() == 0 && searchProformaInvoiceObj.getArchieved() == 0) {
			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_proforma_document_by_only_docname");
			procedureQuery.registerStoredProcedureParameter("searchdocument", String.class, ParameterMode.IN);

			procedureQuery.setParameter("searchdocument", (String) searchProformaInvoiceObj.getSearchDocument());

			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();

			logger.info("/searchProforma-" + " Get Result For Search proforma Document by docname only - "
					+ searchProformaInvoiceObj.getSearchDocument());

			return resultValue;
		}

		// search by document number and status
		else if (searchProformaInvoiceObj.getSearchDocument() != null && searchProformaInvoiceObj.getDraft() == 1
				|| searchProformaInvoiceObj.getApproved() == 1 || searchProformaInvoiceObj.getSubmitted() == 1
				|| searchProformaInvoiceObj.getRejected() == 1
				|| searchProformaInvoiceObj.getArchieved() == 1 && searchProformaInvoiceObj.getFromDate() == null
						&& searchProformaInvoiceObj.getToDate() == null) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_proforma_document_by_docname_and_status");
			procedureQuery.registerStoredProcedureParameter("searchdocument", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("draft", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("approved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("submitted", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("rejected", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("archieved", Integer.class, ParameterMode.IN);

			procedureQuery.setParameter("searchdocument", (String) searchProformaInvoiceObj.getSearchDocument());
			procedureQuery.setParameter("draft", (int) searchProformaInvoiceObj.getDraft());
			procedureQuery.setParameter("approved", (int) searchProformaInvoiceObj.getApproved());
			procedureQuery.setParameter("submitted", (int) searchProformaInvoiceObj.getSubmitted());
			procedureQuery.setParameter("rejected", (int) searchProformaInvoiceObj.getRejected());
			procedureQuery.setParameter("archieved", (int) searchProformaInvoiceObj.getArchieved());

			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			logger.info("/searchProforma -" + " Get Result For Search proforma Document by docname and status- ");

			return resultValue;

		}

		// search by status and date
		else if (searchProformaInvoiceObj.getSearchDocument() == null && searchProformaInvoiceObj.getFromDate() != null
				&& searchProformaInvoiceObj.getToDate() != null && searchProformaInvoiceObj.getDraft() == 1
				|| searchProformaInvoiceObj.getApproved() == 1 || searchProformaInvoiceObj.getSubmitted() == 1
				|| searchProformaInvoiceObj.getRejected() == 1 || searchProformaInvoiceObj.getArchieved() == 1) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_proforma_document_by_status_and_date");

			procedureQuery.registerStoredProcedureParameter("draft", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("approved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("submitted", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("rejected", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("archieved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("fromdate", java.util.Date.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("todate", java.util.Date.class, ParameterMode.IN);

			procedureQuery.setParameter("draft", (int) searchProformaInvoiceObj.getDraft());
			procedureQuery.setParameter("approved", (int) searchProformaInvoiceObj.getApproved());
			procedureQuery.setParameter("submitted", (int) searchProformaInvoiceObj.getSubmitted());
			procedureQuery.setParameter("rejected", (int) searchProformaInvoiceObj.getRejected());
			procedureQuery.setParameter("archieved", (int) searchProformaInvoiceObj.getArchieved());
			procedureQuery.setParameter("fromdate", (java.util.Date) searchProformaInvoiceObj.getFromDate());
			procedureQuery.setParameter("todate", (java.util.Date) searchProformaInvoiceObj.getToDate());

			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();

			logger.info("/searchProforma -" + " Get Result For Search proforma Document by status and date - ");
			return resultValue;
		}

		// search by from date and to date
		else if (searchProformaInvoiceObj.getSearchDocument() == null && searchProformaInvoiceObj.getFromDate() != null
				&& searchProformaInvoiceObj.getToDate() != null && searchProformaInvoiceObj.getDraft() == 0
				|| searchProformaInvoiceObj.getApproved() == 0 || searchProformaInvoiceObj.getSubmitted() == 0
				|| searchProformaInvoiceObj.getRejected() == 0 || searchProformaInvoiceObj.getArchieved() == 0) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_proforma_document_by_from_date_and_to_date");

			procedureQuery.registerStoredProcedureParameter("fromdate", java.util.Date.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("todate", java.util.Date.class, ParameterMode.IN);

			procedureQuery.setParameter("fromdate", (java.util.Date) searchProformaInvoiceObj.getFromDate());
			procedureQuery.setParameter("todate", (java.util.Date) searchProformaInvoiceObj.getToDate());

			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			
			logger.info("/searchProforma -" + " Get Result For Search proforma Document by from date and to date");

			return resultValue;

		}

		// search by document number,status,from date and to date
		else if (searchProformaInvoiceObj.getSearchDocument() != null && searchProformaInvoiceObj.getFromDate() != null
				&& searchProformaInvoiceObj.getToDate() != null && searchProformaInvoiceObj.getDraft() == 1
				|| searchProformaInvoiceObj.getApproved() == 1 || searchProformaInvoiceObj.getSubmitted() == 1
				|| searchProformaInvoiceObj.getRejected() == 1 || searchProformaInvoiceObj.getArchieved() == 1) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_proforma_document_by_status_and_date_and_name");
			procedureQuery.registerStoredProcedureParameter("searchdocument", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("draft", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("approved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("submitted", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("rejected", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("archieved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("fromdate", java.util.Date.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("todate", java.util.Date.class, ParameterMode.IN);

			procedureQuery.setParameter("searchdocument", (String) searchProformaInvoiceObj.getSearchDocument());
			procedureQuery.setParameter("draft", (int) searchProformaInvoiceObj.getDraft());
			procedureQuery.setParameter("approved", (int) searchProformaInvoiceObj.getApproved());
			procedureQuery.setParameter("submitted", (int) searchProformaInvoiceObj.getSubmitted());
			procedureQuery.setParameter("rejected", (int) searchProformaInvoiceObj.getRejected());
			procedureQuery.setParameter("archieved", (int) searchProformaInvoiceObj.getArchieved());
			procedureQuery.setParameter("fromdate", (java.util.Date) searchProformaInvoiceObj.getFromDate());
			procedureQuery.setParameter("todate", (java.util.Date) searchProformaInvoiceObj.getToDate());

			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			logger.info("/searchProforma -" + " Get Result For Search proforma Document by status and date");
			return resultValue;
		}

		else {
			return null;
		}
	}

////////////////////////////////STORE PROCEDURE FOR GETTING DETAILS OF CREATE Invoice  FOR SEARCH Invoice /////////////////////////////////////////////  

	public String docNoForGettingCreateProformaDocumentDetails(@Valid @RequestBody String docNo) {

		docNoVariable = docNo;

		String response = "{\"message\":\" " + docNoVariable + "\"}";

		logger.info("/CProformaInvsDet -" + " Search proforma Invoice Document Details For Document Number"
				+ docNoVariable);

		return response;
	}

	public List<Object[]> gettingCreateProformaDocumentDetails() {

		List<Object[]> resultValue;
		String docNo = docNoVariable;

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("create_proforma_details_for_search_invs");
		procedureQuery.registerStoredProcedureParameter("doc_no", String.class, ParameterMode.IN);

		procedureQuery.setParameter("doc_no", (String) docNo);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

		logger.info("/CProformaInvsDet -" + "Get Header Details For Search proforma  Document - " + docNo);

		return resultValue;

	}
}
