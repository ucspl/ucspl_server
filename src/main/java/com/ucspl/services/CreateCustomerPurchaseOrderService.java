package com.ucspl.services;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateCustomerPurchaseOrder;
import com.ucspl.model.CreateSalesQuotation;
import com.ucspl.model.CreateSalesQuotationRepository;
import com.ucspl.model.SearchPo;
import com.ucspl.model.SearchSalesAndQuotation;
import com.ucspl.model.SrNoPurchaseOrderDocument;
import com.ucspl.repository.CreateCustomerPurchaseOrderRepository;
import com.ucspl.repository.SrNoPurchaseOrderDocumentRepository;

@Service
public class CreateCustomerPurchaseOrderService {

	private static final Logger logger = Logger.getLogger(CreateCustomerPurchaseOrderService.class);

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private CreateCustomerPurchaseOrderRepository createCustomerPurchaseOrderRepository;
	
	@Autowired
	private SrNoPurchaseOrderDocumentRepository srNoPurchaseOrderDocumentRepository;

	private SearchPo searchPoObject = new SearchPo();

	private String docNoVariable;

	// Get All Created Customer Purchase Orders
	public List<CreateCustomerPurchaseOrder> getAllCreateCustomerPurchaseOrders() {

		logger.info("/createcustpo - Get All Created Customer Purchase Orders!");
		return createCustomerPurchaseOrderRepository.findAll();
	}

	// Get create Po Details for id
	public ResponseEntity<CreateCustomerPurchaseOrder> getCustomerPurchaseOrderById(
			@PathVariable(value = "id") Long createCustomerPurchaseOrderId) throws ResourceNotFoundException {

		CreateCustomerPurchaseOrder createCustomerPurchaseOrder = createCustomerPurchaseOrderRepository
				.findById(createCustomerPurchaseOrderId).orElseThrow(() -> new ResourceNotFoundException(
						" Purchase Order not found for this id :: " + createCustomerPurchaseOrderId));

		logger.info("/createcustpo/{id} - Get Customer Purchase Order By Id!" + createCustomerPurchaseOrderId);
		return ResponseEntity.ok().body(createCustomerPurchaseOrder);
	}

	// Save the Customer Purchase Order
	public CreateCustomerPurchaseOrder createCustomerPurchaseOrder(
			@Valid @RequestBody CreateCustomerPurchaseOrder createCustomerPurchaseOrder) {

		logger.info("/createcustpo - Save the Customer Purchase Order!");
		return createCustomerPurchaseOrderRepository.save(createCustomerPurchaseOrder);
	}

	// Update po Details for id
	public ResponseEntity<CreateCustomerPurchaseOrder> updatePo(@PathVariable(value = "id") Long poId,
			@Valid @RequestBody CreateCustomerPurchaseOrder createCustomerPurchaseOrderDetails)
			throws ResourceNotFoundException {

		CreateCustomerPurchaseOrder createCustomerPurchaseOrder = createCustomerPurchaseOrderRepository.findById(poId)
				.orElseThrow(() -> new ResourceNotFoundException("po not found for this id :: " + poId));

		createCustomerPurchaseOrder.setDocumentType(createCustomerPurchaseOrderDetails.getDocumentType());
		createCustomerPurchaseOrder.setDocumentNumber(createCustomerPurchaseOrderDetails.getDocumentNumber());
		createCustomerPurchaseOrder.setCustomerId(createCustomerPurchaseOrderDetails.getCustomerId());
		createCustomerPurchaseOrder.setCustPoNo(createCustomerPurchaseOrderDetails.getCustPoNo());
		createCustomerPurchaseOrder.setValidToDate(createCustomerPurchaseOrderDetails.getValidToDate());
		createCustomerPurchaseOrder.setPoDate(createCustomerPurchaseOrderDetails.getPoDate());
		createCustomerPurchaseOrder.setDraft(createCustomerPurchaseOrderDetails.getDraft());
		createCustomerPurchaseOrder.setSubmitted(createCustomerPurchaseOrderDetails.getSubmitted());
		createCustomerPurchaseOrder.setRejected(createCustomerPurchaseOrderDetails.getRejected());
		createCustomerPurchaseOrder.setArchieved(createCustomerPurchaseOrderDetails.getArchieved());
		createCustomerPurchaseOrder.setApproved(createCustomerPurchaseOrderDetails.getApproved());
		createCustomerPurchaseOrder.setCreatedBy(createCustomerPurchaseOrderDetails.getCreatedBy());
		createCustomerPurchaseOrder.setApprovedBy(createCustomerPurchaseOrderDetails.getApprovedBy());
		//createCustomerPurchaseOrder.setValidToDate(createCustomerPurchaseOrderDetails.getValidToDate());
		createCustomerPurchaseOrder.setPoAttachedFileId(createCustomerPurchaseOrderDetails.getPoAttachedFileId());
		createCustomerPurchaseOrder.setUpdateHistory(createCustomerPurchaseOrderDetails.getUpdateHistory());
		createCustomerPurchaseOrder.setPoValue(createCustomerPurchaseOrderDetails.getPoValue());
		createCustomerPurchaseOrder.setRemainingPoValue(createCustomerPurchaseOrderDetails.getRemainingPoValue());

		final CreateCustomerPurchaseOrder updatedPo = createCustomerPurchaseOrderRepository
				.save(createCustomerPurchaseOrder);

		logger.info("/createcustpo/{id} - Update Create po Details for id " + poId);
		return ResponseEntity.ok(updatedPo);
	}

	// Get last saved document number
	@GetMapping(path = { "/getPODocumentNo" })
	public String getDocumentNumber() throws IOException {

		Object documentNumberId;
		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("document_number_for_purchase_order");

		procedureQuery.execute();

		documentNumberId = procedureQuery.getSingleResult();

		String stringToConvert = String.valueOf(documentNumberId);

		String response = "{\"number\":\"" + stringToConvert + "\"}";
		return response;
	}

	////////////////////////////// Search Po///////////////////////////////

	// code for search po
	public String searchAndModifyPo(@Valid @RequestBody SearchPo searchPoObject1) {

		searchPoObject = searchPoObject1;

		String response = "{\"message\":\"Post With ngResource: The user firstname: "
				+ searchPoObject.getSearchDocument() + "\"}";

		logger.info("/searchSales - Search Po Documents For " + searchPoObject1);

		return response;
	}

	// Result For Search Sales quotation Document
	public List<Object[]> getAllPo() {

		List<Object[]> resultValue;

		// search by document number
		if (searchPoObject.getSearchDocument() != null && searchPoObject.getDraft() == 0
				&& searchPoObject.getApproved() == 0 && searchPoObject.getSubmitted() == 0
				&& searchPoObject.getRejected() == 0 && searchPoObject.getArchieved() == 0) {
			
			StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("search_and_modify_po_by_only_docname");
			procedureQuery.registerStoredProcedureParameter("searchdocument", String.class, ParameterMode.IN);
			procedureQuery.setParameter("searchdocument", (String) searchPoObject.getSearchDocument());
			procedureQuery.execute();
			resultValue = procedureQuery.getResultList();

			logger.info("/searchPo  -" + " Get Result For Search po Document by docname only - "
					+ searchPoObject.getSearchDocument());

			return resultValue;
		}

		// search by document number and status
		else if (searchPoObject.getSearchDocument() != null && searchPoObject.getDraft() == 1
				|| searchPoObject.getApproved() == 1 || searchPoObject.getSubmitted() == 1
				|| searchPoObject.getRejected() == 1 || searchPoObject.getArchieved() == 1
						&& searchPoObject.getFromDate() == null && searchPoObject.getToDate() == null) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_po_by_docname_and_status");
			procedureQuery.registerStoredProcedureParameter("searchdocument", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("draft", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("approved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("submitted", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("rejected", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("archieved", Integer.class, ParameterMode.IN);

			procedureQuery.setParameter("searchdocument", (String) searchPoObject.getSearchDocument());
			procedureQuery.setParameter("draft", (int) searchPoObject.getDraft());
			procedureQuery.setParameter("approved", (int) searchPoObject.getApproved());
			procedureQuery.setParameter("submitted", (int) searchPoObject.getSubmitted());
			procedureQuery.setParameter("rejected", (int) searchPoObject.getRejected());
			procedureQuery.setParameter("archieved", (int) searchPoObject.getArchieved());

			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			// System.out.println("get doc by all parameters");
			logger.info("/searchPo  -" + " Get Result For Search po Document by docname and status- "
					+ searchPoObject.getSearchDocument());

			return resultValue;

		}
		// search by status and date
		else if (searchPoObject.getSearchDocument() == null && searchPoObject.getFromDate() != null
				&& searchPoObject.getToDate() != null && searchPoObject.getDraft() == 1
				|| searchPoObject.getApproved() == 1 || searchPoObject.getSubmitted() == 1
				|| searchPoObject.getRejected() == 1 || searchPoObject.getArchieved() == 1) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_po_by_status_and_date");
			
			procedureQuery.registerStoredProcedureParameter("draft", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("approved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("submitted", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("rejected", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("archieved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("fromdate", java.util.Date.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("todate", java.util.Date.class, ParameterMode.IN);

			procedureQuery.setParameter("draft", (int) searchPoObject.getDraft());
			procedureQuery.setParameter("approved", (int) searchPoObject.getApproved());
			procedureQuery.setParameter("submitted", (int) searchPoObject.getSubmitted());
			procedureQuery.setParameter("rejected", (int) searchPoObject.getRejected());
			procedureQuery.setParameter("archieved", (int) searchPoObject.getArchieved());
			procedureQuery.setParameter("fromdate", (java.util.Date) searchPoObject.getFromDate());
			procedureQuery.setParameter("todate", (java.util.Date) searchPoObject.getToDate());

			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			logger.info("/searchPo  -" + " Get Result For Search po Document by status and date - ");

			return resultValue;

		}
		// search by from date and to date
		else if (searchPoObject.getSearchDocument() == null && searchPoObject.getFromDate() != null
				&& searchPoObject.getToDate() != null && searchPoObject.getDraft() == 0
				|| searchPoObject.getApproved() == 0 || searchPoObject.getSubmitted() == 0
				|| searchPoObject.getRejected() == 0 || searchPoObject.getArchieved() == 0) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_po_by_from_date_and_to_date");

			procedureQuery.registerStoredProcedureParameter("fromdate", java.util.Date.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("todate", java.util.Date.class, ParameterMode.IN);

			procedureQuery.setParameter("fromdate", (java.util.Date) searchPoObject.getFromDate());
			procedureQuery.setParameter("todate", (java.util.Date) searchPoObject.getToDate());

			procedureQuery.execute();
			resultValue = procedureQuery.getResultList();
			
			logger.info(
					"/searchSales -" + " Get Result For Search Sales quotation Document by from date and to date- ");

			return resultValue;

		}
		// search by document number,status,from date and to date
		else if (searchPoObject.getSearchDocument() != null && searchPoObject.getFromDate() != null
				&& searchPoObject.getToDate() != null && searchPoObject.getDraft() == 1
				|| searchPoObject.getApproved() == 1 || searchPoObject.getSubmitted() == 1
				|| searchPoObject.getRejected() == 1 || searchPoObject.getArchieved() == 1) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("search_and_modify_po_by_status_and_date_and_name");
			procedureQuery.registerStoredProcedureParameter("searchdocument", String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("draft", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("approved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("submitted", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("rejected", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("archieved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("fromdate", java.util.Date.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("todate", java.util.Date.class, ParameterMode.IN);

			procedureQuery.setParameter("searchdocument", (String) searchPoObject.getSearchDocument());
			procedureQuery.setParameter("draft", (int) searchPoObject.getDraft());
			procedureQuery.setParameter("approved", (int) searchPoObject.getApproved());
			procedureQuery.setParameter("submitted", (int) searchPoObject.getSubmitted());
			procedureQuery.setParameter("rejected", (int) searchPoObject.getRejected());
			procedureQuery.setParameter("archieved", (int) searchPoObject.getArchieved());
			procedureQuery.setParameter("fromdate", (java.util.Date) searchPoObject.getFromDate());
			procedureQuery.setParameter("todate", (java.util.Date) searchPoObject.getToDate());

			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();
			logger.info("/searchPo -" + " Get Result For Search po Document by status and date - ");

			return resultValue;

		}

		else {
			return null;
		}
	}

	// STORE PROCEDURE FOR GETTING DETAILS OF CREATE PO FOR SEARCH PO
	public String docNoForGettingCreatePoDetails(@Valid @RequestBody String docNo) {

		docNoVariable = docNo;
		System.out.println("doc no" + docNoVariable);

		String response = "{\"message\":\" " + docNoVariable + "\"}";
		logger.info("/createPoDetails -" + " Search Po document Details For Document Number" + docNoVariable);

		return response;
	}

	public List<Object[]> getCreatePODetailsForSearchPo() {

		List<Object[]> resultValueSet;
		String docNo = docNoVariable;
		System.out.println("document number" + docNo);

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("create_po_details_for_search_po");
		procedureQuery.registerStoredProcedureParameter("doc_no", String.class, ParameterMode.IN);

		procedureQuery.setParameter("doc_no", (String) docNo);

		System.out.println("document number" + docNo);
		procedureQuery.execute();

		resultValueSet = procedureQuery.getResultList();

		logger.info("/createPoDetails -" + "Get Details of Createpo screen For Search Sales document - " + docNo);
		return resultValueSet;
	}
	
	// Update reamaining quantity in sr no table
		public ResponseEntity<SrNoPurchaseOrderDocument> updateRemainingQuantity(@PathVariable(value = "id") Long srNoId,
				@Valid @RequestBody SrNoPurchaseOrderDocument remQuantityDetails)
				throws ResourceNotFoundException {

			SrNoPurchaseOrderDocument srNoPurchaseOrderDocumentObj = srNoPurchaseOrderDocumentRepository.findById(srNoId)
					.orElseThrow(() -> new ResourceNotFoundException("po not found for this id :: " + srNoId));

			srNoPurchaseOrderDocumentObj.setRemainingQuantity(remQuantityDetails.getRemainingQuantity());
			

			final SrNoPurchaseOrderDocument updatedRemQuantity = srNoPurchaseOrderDocumentRepository
					.save(srNoPurchaseOrderDocumentObj);

			logger.info("/updateRemQuantity/{id} - Update remaining quantity in sr no po table for id " + srNoId);
			return ResponseEntity.ok(updatedRemQuantity);
		}
		
		
		// Update po value in create po table Details 
				public ResponseEntity<CreateCustomerPurchaseOrder> updateRemainingPoValue(@PathVariable(value = "id") Long createPoId,
						@Valid @RequestBody CreateCustomerPurchaseOrder poValueDetails)
						throws ResourceNotFoundException {

					CreateCustomerPurchaseOrder CreateCustomerPurchaseObj = createCustomerPurchaseOrderRepository.findById(createPoId)
							.orElseThrow(() -> new ResourceNotFoundException("po not found for this id :: " + createPoId));

					CreateCustomerPurchaseObj.setRemainingPoValue(poValueDetails.getRemainingPoValue());
					

					final CreateCustomerPurchaseOrder updatedRemPoValue = createCustomerPurchaseOrderRepository
							.save(CreateCustomerPurchaseObj);

					logger.info("/updateRemPoValue/{id} - Update remaining po value for id " + createPoId);
					return ResponseEntity.ok(updatedRemPoValue);
				}


}
