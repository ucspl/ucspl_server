package com.ucspl.services;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CustomerAttachments;
import com.ucspl.model.ImageModel;
import com.ucspl.repository.CustomerAttachmentsRepository;

@Service
public class CustomerAttachmentsService {

	private static final Logger logger = Logger.getLogger(CustomerAttachmentsService.class);

	CustomerAttachments imgData= new CustomerAttachments();
	
	@Autowired
	private CustomerAttachmentsRepository customerAttachmentsRepository;
	
	@PersistenceContext
	private EntityManager em;

	public ResponseEntity<CustomerAttachments> uplaodImage(MultipartFile file) throws IOException {

		String path="C:\\Users\\Admin\\Documents\\Workspace\\upload\\customer";
		  		      
		String fileName = file.getOriginalFilename();

		String docLocation =path+"\\"+fileName;
		File file1 = new File(docLocation);
		FileCopyUtils.copy(file.getBytes(), file1);
	    
		imgData.setAttachedFileName(fileName);
		imgData.setType(file.getContentType());
		imgData.setFileLocation(docLocation);
		
		return ResponseEntity.ok().body(imgData);
	}
	

	public List<CustomerAttachments> getImage(long custId) throws IOException {

		final List<CustomerAttachments> retrievedImage = customerAttachmentsRepository.findCustAttachmentsByCustId(custId);
		List<CustomerAttachments> resultValue;
		
		return retrievedImage;
	}
	

	public List<CustomerAttachments> getAllCustomerAttachments() {

		logger.info("/getCustAttach - Get All Created Customer Attachments!");
		return customerAttachmentsRepository.findAll();
	}


	public ResponseEntity<CustomerAttachments> getCustomerAttachmentById(Long customerAttachmentsId) throws ResourceNotFoundException {
		CustomerAttachments customerAttachments = customerAttachmentsRepository
				.findById(customerAttachmentsId).orElseThrow(() -> new ResourceNotFoundException(
						" customer attachment not found for this id :: " + customerAttachmentsId));
		
		logger.info("/custAttach/{id} - Get customer attachment By Id!"+ customerAttachmentsId);
		
		return ResponseEntity.ok().body(customerAttachments);
	}
	

	public CustomerAttachments createCustomerAttachment(CustomerAttachments customerAttachments) {
		
		customerAttachments.setAttachedFileName(imgData.getAttachedFileName());
		customerAttachments.setType(imgData.getType());
		customerAttachments.setFileLocation(imgData.getFileLocation());
		
		logger.info("/custAttach - Save the Customer Attachments!");
		
		return customerAttachmentsRepository.save(customerAttachments);

	}
	

	public ImageModel getSelectedCustFileInByteFormat(CustomerAttachments attachFile) throws IOException {

		File file = new File(attachFile.getFileLocation());
		
		
		Path path = Paths.get(attachFile.getFileLocation());
		String name = attachFile.getAttachedFileName();
		String originalFileName = attachFile.getOriginalFileName();
		String contentType = attachFile.getType();
		byte[] content = null;
		try {
		    content = Files.readAllBytes(path);
		} catch (final IOException e) {
		}
		MultipartFile result = new MockMultipartFile(name, originalFileName, contentType, content);
		

 		      
		String fileName = result.getOriginalFilename();

		
		ImageModel img = new ImageModel(result.getName(), result.getContentType(),
				(result.getBytes()));

		
		return img;
	}
	
}
