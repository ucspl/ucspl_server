package com.ucspl.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.ChallanCumInvoiceDocumentCalculation;
import com.ucspl.model.SalesQuotationDocumentCalculation;
import com.ucspl.repository.ChallanCumInvoiceDocumentCalculationRepository;
import com.ucspl.repository.SalesQuotationDocumentCalculationRepository;

@Service
public class ChallanCumInvoiceDocumentCalculationService {

	
	private static final Logger logger = Logger.getLogger(ChallanCumInvoiceDocumentCalculationService.class);

	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	private ChallanCumInvoiceDocumentCalculationRepository challanCumInvoiceDocumentCalculationRepositoryObject;
	
	
	
	
	private String docNoVariable;
	
	// Get All Challan Cum Invoice Document Calculations
		public List<ChallanCumInvoiceDocumentCalculation> getChallanCumInvoiceDocumentCalculation() {
			
			logger.info("/invDocCal - Get All Challan Cum Invoice Document Calculations!");
			return challanCumInvoiceDocumentCalculationRepositoryObject.findAll();
		}
		
		
		// Get All Challan Cum Invoice Document by id
		public ResponseEntity<ChallanCumInvoiceDocumentCalculation> getChallanCumInvoiceDocumentCalculationById(
				@PathVariable(value = "id") Long challanCumInvoiceDocumentCalculationId) throws ResourceNotFoundException {

			ChallanCumInvoiceDocumentCalculation challanCumInvoiceDocumentCalculation = challanCumInvoiceDocumentCalculationRepositoryObject.
					findById(challanCumInvoiceDocumentCalculationId).orElseThrow(() -> new ResourceNotFoundException(
							"Document not found for this id :: " + challanCumInvoiceDocumentCalculationId));

			logger.info("/invDocCal/{id} - Get Challan Cum Invoice Document Calculations By Id!" + challanCumInvoiceDocumentCalculationId);
			return ResponseEntity.ok().body(challanCumInvoiceDocumentCalculation);
		}
		
		
		// Save Calculation of Challan Cum Invoice Document
		public ChallanCumInvoiceDocumentCalculation createChallanCumInvoiceDocumentCalculation(
				@Valid @RequestBody ChallanCumInvoiceDocumentCalculation challanCumInvoiceDocumentCalculation) {
	

			logger.info("/invDocCal - Save Calculation of Challan Cum Invoice Document!");
			return challanCumInvoiceDocumentCalculationRepositoryObject.save(challanCumInvoiceDocumentCalculation);
		}
		
		

		// Update Challan Cum Invoice Document Calculation for id
		public ResponseEntity<ChallanCumInvoiceDocumentCalculation> updateChallanCumInvoiceDocumentCalculationById(
				@PathVariable(value = "id") Long challanCumInvoiceDocumentCalculationId,
				@Valid @RequestBody ChallanCumInvoiceDocumentCalculation challanCumInvoiceDocumentCalculationDetails)
				throws ResourceNotFoundException {
			ChallanCumInvoiceDocumentCalculation challanCumInvoiceDocumentCalculation = challanCumInvoiceDocumentCalculationRepositoryObject
					.findById(challanCumInvoiceDocumentCalculationId).orElseThrow(() -> new ResourceNotFoundException(
							"Document not found for this id :: " + challanCumInvoiceDocumentCalculationId));

			challanCumInvoiceDocumentCalculation.setInvDocumentNumber(challanCumInvoiceDocumentCalculationDetails.getInvDocumentNumber());
			challanCumInvoiceDocumentCalculation.setSubTotal(challanCumInvoiceDocumentCalculationDetails.getSubTotal());
			challanCumInvoiceDocumentCalculation.setDiscountOnSubtotal(challanCumInvoiceDocumentCalculationDetails.getDiscountOnSubtotal());
			challanCumInvoiceDocumentCalculation.setTotal(challanCumInvoiceDocumentCalculationDetails.getTotal());
			challanCumInvoiceDocumentCalculation.setCgst(challanCumInvoiceDocumentCalculationDetails.getCgst());
			challanCumInvoiceDocumentCalculation.setSgst(challanCumInvoiceDocumentCalculationDetails.getSgst());
			challanCumInvoiceDocumentCalculation.setIgst(challanCumInvoiceDocumentCalculationDetails.getIgst());
			challanCumInvoiceDocumentCalculation.setNet(challanCumInvoiceDocumentCalculationDetails.getNet());

			final ChallanCumInvoiceDocumentCalculation updatedChallanCumInvoiceDocumentCalculation = challanCumInvoiceDocumentCalculationRepositoryObject
					.save(challanCumInvoiceDocumentCalculation);

			logger.info("/invDocCal/{id} - Update Challan Cum Invoice Document Calculation for id "
					+ challanCumInvoiceDocumentCalculationId);
			return ResponseEntity.ok(updatedChallanCumInvoiceDocumentCalculation);
		}
		
		
    // store procedure for doc calculation table detail
		public String docNoForGettingDocDetails(@Valid @RequestBody String docNo) {

			docNoVariable = docNo;
			String response = "{\"message\":\" " + docNoVariable + "\"}";

			logger.info("/IncDocCal -" + docNoVariable + " Document No For Getting Challan Cum Invoice Document Calculation Details");
			return response;
		}
		
		
		// Get details of Sales Quotation Document calculation for Document No
		public List<Object[]> getDocDetailsForDocNo() {

			List<Object[]> resultValue;
			String documentNo = docNoVariable;

			StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("doc_details_for_challan_cum_invoice_document_calculation");
			procedureQuery.registerStoredProcedureParameter("doc_no", String.class, ParameterMode.IN);
			procedureQuery.setParameter("doc_no", (String) documentNo);
			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();

			logger.info("/IncDocCal - Get Details of Challan Cum Invoice Document Calculation for Document No "
					+ docNoVariable);

			return resultValue;

		}	
		
}
