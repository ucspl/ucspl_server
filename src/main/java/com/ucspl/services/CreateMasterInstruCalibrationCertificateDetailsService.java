package com.ucspl.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateMasterInstruCalibrationCertificateDetails;
import com.ucspl.model.SearchMasterInstruByMastIdAndParameterId;
import com.ucspl.model.SearchMasterInstruByMastIdAndStatus;
import com.ucspl.repository.CreateMasterInstruCalibrationCertificateDetailsRepository;

@Service
public class CreateMasterInstruCalibrationCertificateDetailsService {

	private static final Logger logger = Logger.getLogger(CreateMasterInstruCalibrationCertificateDetailsService.class);

	@Autowired
	private CreateMasterInstruCalibrationCertificateDetailsRepository createMasterInstruCalibrationCertificateDetailsRepository;

	@PersistenceContext
	private EntityManager em;
	
	public CreateMasterInstruCalibrationCertificateDetailsService() {

	} 

	public List<CreateMasterInstruCalibrationCertificateDetails> getAllCreateMasterInstrumentCalCertificateDetails() {
		logger.info("/crtMCCerD - Get all Created Master Instrument cal certificate Details!");

		return createMasterInstruCalibrationCertificateDetailsRepository.findAll();
	}
	
	public ResponseEntity<CreateMasterInstruCalibrationCertificateDetails> getCreateMasterInstrumentCalCertificateDetailsById(
			Long createMasterInstrumentDetailsId) throws ResourceNotFoundException {
		CreateMasterInstruCalibrationCertificateDetails createMasterInstrumentDetail = createMasterInstruCalibrationCertificateDetailsRepository
				.findById(createMasterInstrumentDetailsId).orElseThrow(() -> new ResourceNotFoundException(
						" Request and inward not found for this id :: " + createMasterInstrumentDetailsId));

		logger.info("/crtMCCerD/{id} - Get create Master Instrument calibration certificate Details By Id!" + createMasterInstrumentDetailsId);

		return ResponseEntity.ok().body(createMasterInstrumentDetail);
	}

	public ResponseEntity<CreateMasterInstruCalibrationCertificateDetails> updateCreateMasterInstrumentCalCertificateDetailsById(
			Long createMasterInstruCalCertificateDetailsId,
			CreateMasterInstruCalibrationCertificateDetails createMasterInstruCalCertificateDetails) throws ResourceNotFoundException {
		CreateMasterInstruCalibrationCertificateDetails createMasterInstruCalCertificate = createMasterInstruCalibrationCertificateDetailsRepository
				.findById(createMasterInstruCalCertificateDetailsId).orElseThrow(() -> new ResourceNotFoundException(
						"Document not found for this id :: " + createMasterInstruCalCertificateDetailsId));

		createMasterInstruCalCertificate.setCalCertificate(createMasterInstruCalCertificateDetails.getCalCertificate());
		createMasterInstruCalCertificate.setCalCertificateAgencyId(createMasterInstruCalCertificateDetails.getCalCertificateAgencyId());
		createMasterInstruCalCertificate.setCalibrationDate(createMasterInstruCalCertificateDetails.getCalibrationDate());
		createMasterInstruCalCertificate.setCalibrationDue(createMasterInstruCalCertificateDetails.getCalibrationDue());
		createMasterInstruCalCertificate.setDateOfOutward(createMasterInstruCalCertificateDetails.getDateOfOutward());
		createMasterInstruCalCertificate.setDateOfInward(createMasterInstruCalCertificateDetails.getDateOfInward());
		createMasterInstruCalCertificate.setCalibrationCharges(createMasterInstruCalCertificateDetails.getCalibrationCharges());
		createMasterInstruCalCertificate.setCalibrationData(createMasterInstruCalCertificateDetails.getCalibrationData());
		createMasterInstruCalCertificate.setDraft(createMasterInstruCalCertificateDetails.getDraft());
		createMasterInstruCalCertificate.setApproved(createMasterInstruCalCertificateDetails.getApproved());
		createMasterInstruCalCertificate.setArchieved(createMasterInstruCalCertificateDetails.getArchieved());
		createMasterInstruCalCertificate.setSubmitted(createMasterInstruCalCertificateDetails.getSubmitted());
		createMasterInstruCalCertificate.setRejected(createMasterInstruCalCertificateDetails.getRejected());
	
		final CreateMasterInstruCalibrationCertificateDetails updatedMasterInstruCalCertificateDetails = createMasterInstruCalibrationCertificateDetailsRepository
				.save(createMasterInstruCalCertificate);
		logger.info("/crtMCCerD/{id} - Update Master Instrument calibration certificate Details for id " + createMasterInstruCalCertificateDetailsId);

		return ResponseEntity.ok(updatedMasterInstruCalCertificateDetails);
	}

	public CreateMasterInstruCalibrationCertificateDetails createCreateMasterInstrumentCalCertificateDetails(
			CreateMasterInstruCalibrationCertificateDetails createMasterInstruCalCertificateDetails) {
			
		logger.info("/crtMCCerD - Save Master Instrument calibration certificate Details!");
		return createMasterInstruCalibrationCertificateDetailsRepository.save(createMasterInstruCalCertificateDetails);
	}
	
	
	// Delete User Details for id
	public Map<String, Boolean> deleteMasterInstrumentCalCertificateDetails(Long masterInstrumentDetailId) throws ResourceNotFoundException {

		Map<String, Boolean> response = new HashMap<>();
		createMasterInstruCalibrationCertificateDetailsRepository.deleteById(masterInstrumentDetailId);
		response.put("deleted", Boolean.TRUE);
		logger.info("/crtMSpeD/{id} - Delete Master Instrument Detail of id - " + masterInstrumentDetailId);
			
		return response;
	}

	
////////////////////////////sp for getting sr no for master instruments ////////////////////////////////  

	public List<Object[]> srNoDetailsOfMasterInstruCalCertificate(long masterId) {

		System.out.println("master id" + masterId);
		logger.info("/srnoMSDet -" + " Search master instru cal certificate Details For master id" + masterId);

		List<Object[]> resultValue;

		StoredProcedureQuery procedureQuery = em
				.createStoredProcedureQuery("srno_master_instru_cal_certificate_detail_list");
		procedureQuery.registerStoredProcedureParameter("id_no", Long.class, ParameterMode.IN);

		procedureQuery.setParameter("id_no", (long) masterId);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

		return resultValue;
	}

//////////////////////////// sp for getting sr no for master instruments ////////////////////////////////  

	public List<Object[]> srNoDetailsOfMasterInstruCalCertificateByStatus(SearchMasterInstruByMastIdAndStatus status) {

		System.out.println("master id" + status);
		logger.info("/srnoMSDet -" + " Search master instru cal certificate Details For Status" + status);

		List<Object[]> resultValue = null;
			
		if (status.getDraft() == 1
				|| status.getApproved() == 1 || status.getSubmitted() == 1
				|| status.getRejected() == 1 || status.getArchieved() == 1) {

			StoredProcedureQuery procedureQuery = em
					.createStoredProcedureQuery("srno_master_instru_cal_certificate_detail_list_by_status_and_mastid");
			
			procedureQuery.registerStoredProcedureParameter("mastid", Long.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("draft", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("approved", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("submitted", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("rejected", Integer.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter("archieved", Integer.class, ParameterMode.IN);

			procedureQuery.setParameter("mastid", (long) status.getMastId());
			procedureQuery.setParameter("draft", (int) status.getDraft());
			procedureQuery.setParameter("approved", (int) status.getApproved());
			procedureQuery.setParameter("submitted", (int) status.getSubmitted());
			procedureQuery.setParameter("rejected", (int) status.getRejected());
			procedureQuery.setParameter("archieved", (int) status.getArchieved());

			procedureQuery.execute();
			resultValue = procedureQuery.getResultList();
		}

		return resultValue;
	}
	
	
////////////////////////////sp for getting sr no for master instruments ////////////////////////////////  

	public List<Object[]> srNoDetailsOfCalibraSpecificationResultForSelectedParameter(long paraId) {

		System.out.println("parameter id" + paraId);
		logger.info("/srnoCRDForPara -" + " Search  cal specification Details For parameter id" + paraId);

		List<Object[]> resultValue;

		StoredProcedureQuery procedureQuery = em
				.createStoredProcedureQuery("srno_master_instru_callibration_specification_detail_by_paraid");
		procedureQuery.registerStoredProcedureParameter("para_id", Long.class, ParameterMode.IN);

		procedureQuery.setParameter("para_id", (long) paraId);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

		return resultValue;
	}
	
	
////////////////////////////sp for getting sr no for master instruments ////////////////////////

	public List<Object[]> srNoDetailsOfCalibraSpecificationResultForCreateMastInstruId(SearchMasterInstruByMastIdAndParameterId createMastInstruIdParaId) {

		System.out.println("parameter id" + createMastInstruIdParaId.getParameterId());

		logger.info("/srCRDForCreMIIdParaId -" + " Search  cal specification Details For parameter id" + createMastInstruIdParaId.getParameterId());

		List<Object[]> resultValue;

		StoredProcedureQuery procedureQuery = em
				.createStoredProcedureQuery("srno_master_instru_callibration_specification_detail_by_createmastinstruid_and_paraid");
		procedureQuery.registerStoredProcedureParameter("createmastinstruid", Long.class, ParameterMode.IN);
		procedureQuery.registerStoredProcedureParameter("para_id", Long.class, ParameterMode.IN);
		
		procedureQuery.setParameter("createmastinstruid", (long) createMastInstruIdParaId.getCreateMasterInstruId());
		procedureQuery.setParameter("para_id", (long) createMastInstruIdParaId.getParameterId());
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

		return resultValue;
	}	
	
	
}
