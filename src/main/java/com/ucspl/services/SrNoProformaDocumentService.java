
package com.ucspl.services;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.ChallanCumInvoiceDocument;
import com.ucspl.model.SalesQuotationDocument;
import com.ucspl.model.SrNoProformaDocument;
import com.ucspl.repository.ChallanCumInvoiceDocumentRepository;
import com.ucspl.repository.SrNoProformaDocumentRepository;

@Service
public class SrNoProformaDocumentService {
	
	private static final Logger logger = Logger.getLogger(SrNoProformaDocumentService.class);
	
	@Autowired
	private SrNoProformaDocumentRepository srNoProformaDocumentRepository;
	
	@PersistenceContext
	private EntityManager em;
	
	private String docNoVariable;
	
	// Get All Proforma Document Item Information
	public List<SrNoProformaDocument> getAllProformaDocument() {
			
		
			logger.info("/proformaDoc - Get All Proforma Document Item Information!");
			return srNoProformaDocumentRepository.findAll();
		}
		

		
		// Get proforma Document Item Information By Id
	public ResponseEntity<SrNoProformaDocument> getProformaDocumentById(
			@PathVariable(value = "id") Long proformaDocumentId) throws ResourceNotFoundException {
		SrNoProformaDocument srNoProformaDocument = srNoProformaDocumentRepository
					.findById(proformaDocumentId).orElseThrow(() -> new ResourceNotFoundException(
							"Document not found for this id :: " + proformaDocumentId));
			logger.info("/proformaDoc/{id} - Get proforma Document Item Information By Id!"+ proformaDocumentId);
			
			return ResponseEntity.ok().body(srNoProformaDocument);
		}
		

	// Save Item Details of Proforma Document
	public SrNoProformaDocument createProformaDocument(
			@Valid @RequestBody SrNoProformaDocument proformaDocument) {
			
			 logger.info("/proformaDoc - Save Item Details of Proforma Document !");
			 
			return srNoProformaDocumentRepository.save(proformaDocument);

		}
		
	
		
	//Update Proforma Document Item Details for id
	public ResponseEntity<SrNoProformaDocument> updateProformaDocumentById(
			@PathVariable(value = "id") Long proformaDocumentId,
			@Valid @RequestBody SrNoProformaDocument proformaDocumentDetails)
					throws ResourceNotFoundException {
		SrNoProformaDocument srNoProformaDocument = srNoProformaDocumentRepository
					.findById(proformaDocumentId).orElseThrow(() -> new ResourceNotFoundException(
							"Document not found for this id :: " + proformaDocumentId));

		srNoProformaDocument.setProformaDocumentNumber(proformaDocumentDetails.getProformaDocumentNumber());
		srNoProformaDocument.setInvoiceNumber(proformaDocumentDetails.getInvoiceNumber());
		srNoProformaDocument.setSrNo(proformaDocumentDetails.getSrNo());
		srNoProformaDocument.setPoSrNo(proformaDocumentDetails.getPoSrNo());
		srNoProformaDocument.setItemOrPartCode(proformaDocumentDetails.getItemOrPartCode());
		srNoProformaDocument.setInstruId(proformaDocumentDetails.getInstruId());
		srNoProformaDocument.setDescription(proformaDocumentDetails.getDescription());
		srNoProformaDocument.setRange(proformaDocumentDetails.getRange());
		srNoProformaDocument.setSacCode(proformaDocumentDetails.getSacCode());
		srNoProformaDocument.setHsnNo(proformaDocumentDetails.getHsnNo());
		srNoProformaDocument.setQuantity(proformaDocumentDetails.getQuantity());
		srNoProformaDocument.setUnitPrice(proformaDocumentDetails.getUnitPrice());
		srNoProformaDocument.setAmount(proformaDocumentDetails.getAmount());

			final SrNoProformaDocument updatedProformaDocument = srNoProformaDocumentRepository
					.save(srNoProformaDocument);
			
			logger.info("/proformaDoc/{id} - Update Proforma Document Item Details for id "+ proformaDocumentId);
			
			
			return ResponseEntity.ok(updatedProformaDocument);
		}
		
		
		
		//get sr no store procedure for doc no 
		public String docNoForGettingSrNo(@Valid @RequestBody String docNo) {
		
			docNoVariable = docNo;

			String response = "{\"message\":\" " + docNoVariable + "\"}";

			logger.info("/srProformaDoc -" + docNoVariable+ " Document No For Getting Proforma Document Item Details");
			
			
			return response;
		}
		
		
		// sr no detail for doc no
		 public List<Object[]> getSrNoForDocNo() {

			List<Object[]> resultValue;
			String documentNo = docNoVariable;

			StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("sr_no_for_proforma_document");
			procedureQuery.registerStoredProcedureParameter("doc_no", String.class, ParameterMode.IN);

			procedureQuery.setParameter("doc_no", (String) documentNo);
			procedureQuery.execute();

			resultValue = procedureQuery.getResultList();

			logger.info("/srProformaDoc - Get Items Details of Proforma Document for Document No "+ docNoVariable);
			
			return resultValue;

		}
		
		
		 // code to delete proforma Row
		public String removeProformaRow(@Valid @RequestBody long indexToDelete) throws IOException {

			logger.info("/deleteProformaRow - delete row Item Details of proforma Document !");
		
			srNoProformaDocumentRepository.deleteById(indexToDelete);
			 
			 String response = "{\"message\":\" " + indexToDelete + "row deleted successfully" + "\"}";
			
		     return response;

		}
		
}
