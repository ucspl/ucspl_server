package com.ucspl.services;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.ImageModel;
import com.ucspl.model.SupplierAttachments;
import com.ucspl.repository.SupplierAttachmentsRepository;

@Service
public class SupplierAttachmentsService {

	private static final Logger logger = Logger.getLogger(SupplierAttachmentsService.class);

	SupplierAttachments imgData= new SupplierAttachments();
	
	@Autowired
	private SupplierAttachmentsRepository supplierAttachmentsRepository;
	
	@PersistenceContext
	private EntityManager em;

	public ResponseEntity<SupplierAttachments> uplaodImage(MultipartFile file) throws IOException {

	

		String path="C:\\Users\\Admin\\Documents\\Workspace\\upload\\supplier";
		  		      
		String fileName = file.getOriginalFilename();


		String docLocation =path+"\\"+fileName;
		File file1 = new File(docLocation);
		FileCopyUtils.copy(file.getBytes(), file1);
	    
		imgData.setAttachedFileName(fileName);
		imgData.setType(file.getContentType());
		imgData.setFileLocation(docLocation);

		return ResponseEntity.ok().body(imgData);
	}
	

	public List<SupplierAttachments> getImage(long suppId) throws IOException {

		final List<SupplierAttachments> retrievedImage = supplierAttachmentsRepository.findSuppAttachmentsBySuppId(suppId);
		List<SupplierAttachments> resultValue;
		
		return retrievedImage;
	}
	

	public List<SupplierAttachments> getAllSupplierAttachments() {

		logger.info("/getSuppAttach - Get All Created Supplier Attachments!");
		return supplierAttachmentsRepository.findAll();
	}


	public ResponseEntity<SupplierAttachments> getSupplierAttachmentById(Long supplierAttachmentsId) throws ResourceNotFoundException {
		SupplierAttachments supplierAttachments = supplierAttachmentsRepository
				.findById(supplierAttachmentsId).orElseThrow(() -> new ResourceNotFoundException(
						" supplier attachment not found for this id :: " + supplierAttachmentsId));
		
		logger.info("/suppAttach/{id} - Get supplier attachment By Id!"+ supplierAttachmentsId);
		
		return ResponseEntity.ok().body(supplierAttachments);
	}
	

	public SupplierAttachments createSupplierAttachment(SupplierAttachments supplierAttachments) {
		
		supplierAttachments.setAttachedFileName(imgData.getAttachedFileName());
		supplierAttachments.setType(imgData.getType());
		supplierAttachments.setFileLocation(imgData.getFileLocation());
		
		logger.info("/suppAttach - Save the Supplier Attachments!");
		
		return supplierAttachmentsRepository.save(supplierAttachments);

	}
	

	public ImageModel getSelectedSuppFileInByteFormat(SupplierAttachments attachFile) throws IOException {

		File file = new File(attachFile.getFileLocation());
		
		
		Path path = Paths.get(attachFile.getFileLocation());
		String name = attachFile.getAttachedFileName();
		String originalFileName = attachFile.getOriginalFileName();
		String contentType = attachFile.getType();
		byte[] content = null;
		try {
		    content = Files.readAllBytes(path);
		} catch (final IOException e) {
		}
		MultipartFile result = new MockMultipartFile(name,
		                     originalFileName, contentType, content);
		 
		String fileName = result.getOriginalFilename();

		ImageModel img = new ImageModel(result.getName(), result.getContentType(),
				(result.getBytes()));
	
		return img;
	}
	
}
