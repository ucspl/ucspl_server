package com.ucspl.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CreateNablScopeMaster;
import com.ucspl.model.SearchNabl;
import com.ucspl.repository.CreateNablScopeMasterRepository;

@Service
public class CreateNablScopeMasterService {

	private static final Logger logger = Logger.getLogger(CreateNablScopeMasterService.class);

	private SearchNabl searchNablObj = new SearchNabl();
	String certiNoVariable;
	String nablScopeMasterIdVariable;

	@Autowired
	private CreateNablScopeMasterRepository createNablScopeMasterRepository;

	@PersistenceContext
	private EntityManager em;

	// Get all nabl scope master information
	public List<CreateNablScopeMaster> getAllNablScopeMasters() {

		logger.info("/crtNablScopeMaster - all nabl scope master information!");
		return createNablScopeMasterRepository.findAll();

	}

	// Get nabl scope master information Information By Id
	public ResponseEntity<CreateNablScopeMaster> getNablScopeMasterById(
			@PathVariable(value = "id") Long nableScopeMasterId) throws ResourceNotFoundException {
		CreateNablScopeMaster createNablScopeMaster = createNablScopeMasterRepository.findById(nableScopeMasterId)
				.orElseThrow(() -> new ResourceNotFoundException(
						"nabl scope master not found for this id :: " + nableScopeMasterId));

		logger.info("/crtNablScopeMaster/{id} - Get nabl scope master Information By Id - " + nableScopeMasterId);

		return ResponseEntity.ok().body(createNablScopeMaster);
	}

	// Save nabl scope master information Details
	public CreateNablScopeMaster createNablScopeMaster(CreateNablScopeMaster createNablScopeMaster) {

		logger.info("/crtNablScopeMaster - Save nabl scope master Details!" + " " + "User Name:"
				+ createNablScopeMaster.getCreatedBy());

		return createNablScopeMasterRepository.save(createNablScopeMaster);
	}

	// Update nabl scope master information Details for id
	@PutMapping("/crtNablScopeMaster/{id}")
	public ResponseEntity<CreateNablScopeMaster> updateNablScopeMaster(Long nableScopeMasterId,
			@Valid @RequestBody CreateNablScopeMaster nableScopeMasterDetails) throws ResourceNotFoundException {

		CreateNablScopeMaster createNablScopeMaster = createNablScopeMasterRepository.findById(nableScopeMasterId)
				.orElseThrow(
						() -> new ResourceNotFoundException("User not found for this id :: " + nableScopeMasterId));

		createNablScopeMaster.setNablCertificateNo(nableScopeMasterDetails.getNablCertificateNo());
		createNablScopeMaster.setLabLocation(nableScopeMasterDetails.getLabLocation());
		createNablScopeMaster.setDateOfIssue(nableScopeMasterDetails.getDateOfIssue());
		createNablScopeMaster.setValidUpTo(nableScopeMasterDetails.getValidUpTo());
		createNablScopeMaster.setDraft(nableScopeMasterDetails.getDraft());
		createNablScopeMaster.setApproved(nableScopeMasterDetails.getApproved());
		createNablScopeMaster.setArchieved(nableScopeMasterDetails.getArchieved());
		createNablScopeMaster.setRejected(nableScopeMasterDetails.getRejected());
		createNablScopeMaster.setSubmitted(nableScopeMasterDetails.getSubmitted());
		createNablScopeMaster.setCreatedBy(nableScopeMasterDetails.getCreatedBy());
		createNablScopeMaster.setSystemCertificateNo(nableScopeMasterDetails.getSystemCertificateNo());

		System.out.println("nableScopeMasterId" + nableScopeMasterId);
		final CreateNablScopeMaster updateNablScopeMaster = createNablScopeMasterRepository.save(createNablScopeMaster);

		logger.info("/crtNablScopeMaster/{id} - Update Nabl Scope Master Details for id - " + nableScopeMasterId);

		return ResponseEntity.ok(updateNablScopeMaster);

	}

	// code for search and modify nabl
	public String searchAndModifyNablScope(@Valid @RequestBody SearchNabl searchNablObject) {

		searchNablObj = searchNablObject;
		String response = "{\"message\":\"Post With ngResource: The user firstname: "
				+ searchNablObj.getSearchNablScope() + "\"}";
		logger.info("/searchNabl - Search Nabl Documents For " + searchNablObj);
		System.out.println(searchNablObj);
		return response;

	}

	// Result For Search Sales quotation Document
	public List<Object[]> getAllNablScope() {

		List<Object[]> resultValue;

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("search_and_modify_nabl_scope");
		procedureQuery.registerStoredProcedureParameter("searchNablObj", String.class, ParameterMode.IN);

		procedureQuery.setParameter("searchNablObj", (String) searchNablObj.getSearchNablScope());

		System.out.println("object is" + searchNablObj);
		procedureQuery.execute();

		resultValue = procedureQuery.getResultList();

		logger.info("/searchSales -" + " Get Result For Search nabl scope- " + searchNablObj.getSearchNablScope());

		return resultValue;
	}

	// STORE PROCEDURE FOR GETTING DETAILS OF CREATE NABL FOR SEARCH NABL
	public String certiNoForGettingCreateNablDetails(@Valid @RequestBody String certiNo) {

		certiNoVariable = certiNo;
		System.out.println("doc no" + certiNoVariable);
		
		String response = "{\"message\":\" " + certiNoVariable + "\"}";
		logger.info("/createNablDet -" + " Document Number for Update Nabl" + certiNoVariable);

		return response;
	}

	public List<Object[]> getCreateNablDetailsForSearchNabl() {

		List<Object[]> resultValueSet;
		String certiNo = certiNoVariable;
		System.out.println("document number" + certiNo);

		StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("create_nabl_details_for_search_nabl");
		procedureQuery.registerStoredProcedureParameter("certi_no", String.class, ParameterMode.IN);

		procedureQuery.setParameter("certi_no", (String) certiNo);

		System.out.println("certificate number" + certiNo);
		procedureQuery.execute();
		resultValueSet = procedureQuery.getResultList();

		logger.info("/createNablDet -" + "Get Details of Create nabl screen For Search nabl - " + certiNo);

		return resultValueSet;
	}

	// store procedure for getting nabl specification details for update nabl specification screen
	public String nablScopeMasterIdForGettingSpecificationDetails(@Valid @RequestBody String nablScopeMasterId) {

		nablScopeMasterIdVariable = nablScopeMasterId;
		System.out.println("doc no" + nablScopeMasterIdVariable);

		String response = "{\"message\":\" " + nablScopeMasterIdVariable + "\"}";

		logger.info(
				"/createNablSpecDet-" + " Document Number for Update Nabl specification" + nablScopeMasterIdVariable);

		return response;
	}

	public List<Object[]> getNablSpecificationDetailsForSearchNabl() {

		List<Object[]> resultValueSet;
		String nablScopeMasterId = nablScopeMasterIdVariable;
		System.out.println("document number " + nablScopeMasterId);

		StoredProcedureQuery procedureQuery = em
				.createStoredProcedureQuery("create_nabl_specification_details_for_search_nabl");
		procedureQuery.registerStoredProcedureParameter("nabl_scope_master_id", String.class, ParameterMode.IN);

		procedureQuery.setParameter("nabl_scope_master_id", (String) nablScopeMasterId);

		System.out.println("nabl scope master id " + nablScopeMasterId);
		procedureQuery.execute();

		resultValueSet = procedureQuery.getResultList();

		logger.info(
				"/createNablSpecDet -" + "Get Details of Create nabl screen For Search nabl - " + nablScopeMasterId);

		return resultValueSet;
	}

}
