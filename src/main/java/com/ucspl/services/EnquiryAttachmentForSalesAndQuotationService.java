
package com.ucspl.services;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import com.ucspl.exception.ResourceNotFoundException;
import com.ucspl.model.CustomerAttachments;
import com.ucspl.model.EnquiryAttachmentForSalesAndQuotation;
import com.ucspl.model.ImageModel;
import com.ucspl.repository.CustomerAttachmentsRepository;
import com.ucspl.repository.EnquiryAttachmentForSalesAndQuotationRepository;

@Service
public class EnquiryAttachmentForSalesAndQuotationService {

	private static final Logger logger = Logger.getLogger(EnquiryAttachmentForSalesAndQuotationService.class);

	EnquiryAttachmentForSalesAndQuotation imgData = new EnquiryAttachmentForSalesAndQuotation();

	@Autowired
	private EnquiryAttachmentForSalesAndQuotationRepository enquiryAttachmentForSalesAndQuotationRepository;

	@PersistenceContext
	private EntityManager em;

	public ResponseEntity<EnquiryAttachmentForSalesAndQuotation> uplaodImage(MultipartFile file) throws IOException {

		String path = "C:\\Users\\Admin\\Documents\\Workspace\\Upload\\EnquiryAttachmentForSalesAndQuotation";

		String fileName = file.getOriginalFilename();

		String docLocation = path + "\\" + fileName;
		File file1 = new File(docLocation);
		FileCopyUtils.copy(file.getBytes(), file1);

		imgData.setAttachedFileName(fileName);
		imgData.setType(file.getContentType());
		imgData.setFileLocation(docLocation);

		return ResponseEntity.ok().body(imgData);
	}

	public EnquiryAttachmentForSalesAndQuotation createEnquiryAttachment(
			EnquiryAttachmentForSalesAndQuotation enquiryAttachmentForSalesAndQuotation) {

		enquiryAttachmentForSalesAndQuotation.setAttachedFileName(imgData.getAttachedFileName());
		enquiryAttachmentForSalesAndQuotation.setType(imgData.getType());
		enquiryAttachmentForSalesAndQuotation.setFileLocation(imgData.getFileLocation());

		logger.info("/custAttach - Save the Customer Attachments!");

		return enquiryAttachmentForSalesAndQuotationRepository.save(enquiryAttachmentForSalesAndQuotation);

	}

}