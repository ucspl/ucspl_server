package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.UncMasterType;
import com.ucspl.model.UncSourceOfUncertainty;


@Repository
public interface UncSourceOfUncertaintyRepository extends JpaRepository< UncSourceOfUncertainty, Long> {

}