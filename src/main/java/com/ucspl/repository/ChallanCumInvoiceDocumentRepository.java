package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.ChallanCumInvoiceDocument;

@Repository
public interface ChallanCumInvoiceDocumentRepository extends JpaRepository<ChallanCumInvoiceDocument, Long> {

}
