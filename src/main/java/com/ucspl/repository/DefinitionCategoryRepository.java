package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.DefinitionCategory;

@Repository
public interface DefinitionCategoryRepository extends JpaRepository<DefinitionCategory, Long>
 {

}
