package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.CreateUomCalculator;

@Repository
public interface CreateUomCalculatorRepository extends JpaRepository< CreateUomCalculator, Long> {

}