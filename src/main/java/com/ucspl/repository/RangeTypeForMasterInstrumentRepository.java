
package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.RangeTypeForMasterInstrument;

@Repository
public interface RangeTypeForMasterInstrumentRepository extends JpaRepository<RangeTypeForMasterInstrument, Long> {

}

