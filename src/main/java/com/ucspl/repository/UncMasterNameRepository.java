
package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.ucspl.model.UncMasterName;

@Repository
public interface UncMasterNameRepository extends JpaRepository<UncMasterName, Long> {

}
