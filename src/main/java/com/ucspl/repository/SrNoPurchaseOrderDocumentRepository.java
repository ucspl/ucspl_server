
package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.SalesQuotationDocument;
import com.ucspl.model.SrNoPurchaseOrderDocument;


@Repository
public interface SrNoPurchaseOrderDocumentRepository extends JpaRepository<SrNoPurchaseOrderDocument, Long>{

}

