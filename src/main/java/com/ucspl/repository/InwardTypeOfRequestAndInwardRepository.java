package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.InwardTypeOfRequestAndInward;

@Repository
public interface InwardTypeOfRequestAndInwardRepository extends JpaRepository<InwardTypeOfRequestAndInward, Long> {

	
}
