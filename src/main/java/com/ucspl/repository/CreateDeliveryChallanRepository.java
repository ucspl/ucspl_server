package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.CreateChallanCumInvoiceDocument;
import com.ucspl.model.CreateDeliveryChallan;



@Repository
public interface CreateDeliveryChallanRepository extends JpaRepository<CreateDeliveryChallan, Long> {

}
