package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.JunctionOfUomAndCreateUUCParaSpecification;


@Repository
public interface JunctionOfUomAndCreateUUCParaSpecificationRepository extends JpaRepository<JunctionOfUomAndCreateUUCParaSpecification, Long> {

}