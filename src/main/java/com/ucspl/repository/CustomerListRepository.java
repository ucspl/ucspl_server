
package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.CustomerList;

@Repository
public interface CustomerListRepository extends JpaRepository<CustomerList, Long>{

}
