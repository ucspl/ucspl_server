package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.FormulaAccuracyReqAndIwd;


@Repository
public interface FormulaAccuracyReqAndIwdRepository extends JpaRepository<FormulaAccuracyReqAndIwd, Long>{

}

