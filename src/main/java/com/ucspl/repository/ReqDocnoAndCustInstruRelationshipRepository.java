
package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.ReqDocnoAndCustInstruRelationship;




@Repository
public interface ReqDocnoAndCustInstruRelationshipRepository extends JpaRepository<ReqDocnoAndCustInstruRelationship, Long> {

}
