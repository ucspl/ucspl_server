package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.Taxes;


@Repository
public interface TaxesRepository extends JpaRepository<Taxes, Long> {

}
