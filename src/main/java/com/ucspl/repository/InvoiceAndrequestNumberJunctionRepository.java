
package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.InvoiceAndrequestNumberJunction;

@Repository
public interface InvoiceAndrequestNumberJunctionRepository extends JpaRepository<InvoiceAndrequestNumberJunction, Long> {

}

