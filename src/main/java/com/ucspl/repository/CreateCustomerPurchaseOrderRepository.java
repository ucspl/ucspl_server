package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.CreateCustomerPurchaseOrder;


@Repository
public interface CreateCustomerPurchaseOrderRepository extends JpaRepository<CreateCustomerPurchaseOrder, Long>{
	
}

