package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.UncMasterType;


@Repository
public interface UncMasterTypeRepository extends JpaRepository< UncMasterType, Long> {

}
