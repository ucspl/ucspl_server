package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.UncMasterNumber;


@Repository
public interface UncMasterNumberRepository extends JpaRepository< UncMasterNumber, Long> {

}
