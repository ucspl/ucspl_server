package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.UncMasterType;
import com.ucspl.model.UncProbabilityDistribution;

@Repository
public interface UncProbabilityDistributionRepository extends JpaRepository< UncProbabilityDistribution, Long> {

}