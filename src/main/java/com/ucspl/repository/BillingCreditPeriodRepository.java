package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.BillingCreditPeriod;


@Repository
public interface BillingCreditPeriodRepository extends JpaRepository<BillingCreditPeriod, Long> {

}