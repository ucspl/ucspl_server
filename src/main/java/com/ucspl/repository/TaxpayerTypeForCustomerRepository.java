package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.TaxpayerTypeForCustomer;


@Repository
public interface TaxpayerTypeForCustomerRepository extends JpaRepository<TaxpayerTypeForCustomer, Long> {

}
