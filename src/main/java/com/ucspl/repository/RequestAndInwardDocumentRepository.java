
package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.RequestAndInwardDocument;


@Repository
public interface RequestAndInwardDocumentRepository extends JpaRepository<RequestAndInwardDocument, Long> {

}
