package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.CreateUucNameMaster;


@Repository
public interface CreateUucNameMasterRepository extends JpaRepository<CreateUucNameMaster, Long>{
	

 }


