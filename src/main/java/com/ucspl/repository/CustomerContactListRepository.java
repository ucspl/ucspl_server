
package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.CustomerContactList;

@Repository
public interface CustomerContactListRepository extends JpaRepository<CustomerContactList, Long> {

}
