
package com.ucspl.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ucspl.model.MasterInstruCalCertAttachments;


@Repository
public interface MasterInstruCalCertAttachmentsRepository extends JpaRepository<MasterInstruCalCertAttachments, Long> {

	@Query("SELECT u FROM master_instru_cal_certificate_attachments_table u WHERE u.mastId = :param")
	List<MasterInstruCalCertAttachments> findMasterInstruCalCertAttachmentsByMastId(@Param("param") long param);
}
