
package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.CreateProformaDocument;

@Repository
public interface CreateProformaDocumentRepository extends JpaRepository<CreateProformaDocument, Long> {

}
