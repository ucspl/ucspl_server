package com.ucspl.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.ucspl.model.CreateCalibrationResultDataForMasterInstrument;


@Repository
public interface CreateCalibrationResultDataForMasterInstrumentRepository extends JpaRepository<CreateCalibrationResultDataForMasterInstrument, Long> {

}
