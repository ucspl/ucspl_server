package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.ucspl.model.GroupForNablScopeMaster;


@Repository
public interface GroupForNablScopeMasterRepository extends JpaRepository<GroupForNablScopeMaster, Long> {

}
