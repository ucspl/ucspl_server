
package com.ucspl.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ucspl.model.SupplierAttachments;


@Repository
public interface SupplierAttachmentsRepository extends JpaRepository<SupplierAttachments, Long> {
	
	@Query("SELECT u FROM supplier_attachments_table u WHERE u.suppId = :param")
	List<SupplierAttachments> findSuppAttachmentsBySuppId(@Param("param") long param);
}
