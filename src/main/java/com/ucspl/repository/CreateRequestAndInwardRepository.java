
package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.CreateRequestAndInward;

@Repository
public interface CreateRequestAndInwardRepository extends JpaRepository<CreateRequestAndInward, Long>{

}
