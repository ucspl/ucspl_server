package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.CustomerInstrumentIdentification;


@Repository
public interface  CustomerInstrumentIdentificationRepository extends JpaRepository<CustomerInstrumentIdentification, Long>{

	
	CustomerInstrumentIdentification findByreqDocId(long reqDocId);
	CustomerInstrumentIdentification save(long custInstruIdentificationId);
		
}
