package com.ucspl.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.ucspl.model.CreateNablSpecification;

@Repository
public interface CreateNablSpecificationRepository extends JpaRepository<CreateNablSpecification, Long> {

}
