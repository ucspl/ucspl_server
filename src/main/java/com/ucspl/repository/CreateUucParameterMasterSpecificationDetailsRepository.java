package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.ucspl.model.CreateUucParameterMasterSpecificationDetails;


@Repository
public interface CreateUucParameterMasterSpecificationDetailsRepository extends JpaRepository<CreateUucParameterMasterSpecificationDetails, Long>{
	

 }

