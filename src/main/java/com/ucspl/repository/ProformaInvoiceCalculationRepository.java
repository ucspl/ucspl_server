
package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.ChallanCumInvoiceDocumentCalculation;
import com.ucspl.model.ProformaInvoiceCalculation;

@Repository
public interface ProformaInvoiceCalculationRepository extends JpaRepository<ProformaInvoiceCalculation, Long> {

}

