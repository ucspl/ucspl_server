package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.ucspl.model.CreateUncNameMaster;



@Repository
public interface CreateUncNameMasterRepository extends JpaRepository<CreateUncNameMaster, Long>{
	

 }
