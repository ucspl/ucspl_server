package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.ucspl.model.CalibrationAgencyOfMasterInstrument;

@Repository
public interface CalibrationAgencyOfMasterInstrumentRepository extends JpaRepository<CalibrationAgencyOfMasterInstrument, Long> {

}
