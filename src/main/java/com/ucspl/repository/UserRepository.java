package com.ucspl.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ucspl.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{
	
	@Procedure(name = "search_and_modify_user_by_name")
	List<Object[]> inAndOutTest(@Param("name") String name, @Param("status") int status);
	
	
 }