
package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.ucspl.model.SupplierType;


@Repository
public interface SupplierTypeRepository extends JpaRepository<SupplierType, Long> {

}
