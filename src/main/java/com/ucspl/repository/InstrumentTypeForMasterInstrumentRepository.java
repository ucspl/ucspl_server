
package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.InstrumentTypeForMasterInstrument;

@Repository
public interface InstrumentTypeForMasterInstrumentRepository extends JpaRepository<InstrumentTypeForMasterInstrument, Long> {

}
