
package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.SrNoProformaDocument;

@Repository
public interface SrNoProformaDocumentRepository extends JpaRepository<SrNoProformaDocument, Long> {

}
