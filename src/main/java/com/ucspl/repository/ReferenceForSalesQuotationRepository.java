
package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.ReferenceForSalesQuotation;

@Repository
public interface ReferenceForSalesQuotationRepository extends JpaRepository<ReferenceForSalesQuotation, Long> {

}
