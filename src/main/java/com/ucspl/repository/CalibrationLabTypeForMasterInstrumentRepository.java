package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.CalibrationLabTypeForMasterInstrument;

@Repository
public interface CalibrationLabTypeForMasterInstrumentRepository extends JpaRepository<CalibrationLabTypeForMasterInstrument, Long> {

}
