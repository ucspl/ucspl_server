
package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.ucspl.model.Terms;

@Repository
public interface TermsRepository  extends JpaRepository<Terms, Long> {

}
