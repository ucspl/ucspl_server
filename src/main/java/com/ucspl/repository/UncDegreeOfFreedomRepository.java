package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.UncDegreeOfFreedom;
import com.ucspl.model.UncDividingFactor;

@Repository
public interface UncDegreeOfFreedomRepository extends JpaRepository< UncDegreeOfFreedom, Long> {

}