
package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.SalesQuotationDocumentCalculation;



@Repository
public interface SalesQuotationDocumentCalculationRepository extends JpaRepository<SalesQuotationDocumentCalculation, Long>{
	
}

