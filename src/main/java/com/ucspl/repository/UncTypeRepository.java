package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.UncSourceOfUncertainty;
import com.ucspl.model.UncType;

@Repository
public interface UncTypeRepository extends JpaRepository< UncType, Long> {

}