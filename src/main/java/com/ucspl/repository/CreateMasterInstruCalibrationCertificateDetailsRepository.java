package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.CreateMasterInstruCalibrationCertificateDetails;



@Repository
public interface CreateMasterInstruCalibrationCertificateDetailsRepository extends JpaRepository<CreateMasterInstruCalibrationCertificateDetails, Long> {

}
