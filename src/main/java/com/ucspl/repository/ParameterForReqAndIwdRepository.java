
package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.ParameterForReqAndIwd;


@Repository
public interface ParameterForReqAndIwdRepository extends JpaRepository<ParameterForReqAndIwd, Long> {

}
