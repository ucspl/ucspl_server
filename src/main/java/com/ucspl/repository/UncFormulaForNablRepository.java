
package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.Branch;
import com.ucspl.model.UncFormulaForNabl;

@Repository
public interface UncFormulaForNablRepository extends JpaRepository<UncFormulaForNabl, Long> {

}
