package com.ucspl.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ucspl.model.CustomerAttachments;


@Repository
public interface CustomerAttachmentsRepository extends JpaRepository<CustomerAttachments, Long> {

	
	@Query("SELECT u FROM customer_attachments_table u WHERE u.custId = :param")
	List<CustomerAttachments> findCustAttachmentsByCustId(@Param("param") long param);
}
