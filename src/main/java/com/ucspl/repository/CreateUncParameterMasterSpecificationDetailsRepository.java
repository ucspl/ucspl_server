package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.CreateUncParameterMasterSpecificationDetails;


@Repository
public interface CreateUncParameterMasterSpecificationDetailsRepository extends JpaRepository<CreateUncParameterMasterSpecificationDetails, Long>{
	

 }

