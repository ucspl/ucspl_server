package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.CreateUomCalculatorDetails;


@Repository
public interface CreateUomCalculatorDetailsRepository extends JpaRepository<CreateUomCalculatorDetails, Long> {

}
