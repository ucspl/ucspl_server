package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.CreateEnvMaster;

@Repository
public interface CreateEnvMasterRepository extends JpaRepository<CreateEnvMaster, Long> {

}
