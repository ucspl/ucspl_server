
package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.States;



@Repository
public interface StatesRepository extends JpaRepository<States, Long> {

}
