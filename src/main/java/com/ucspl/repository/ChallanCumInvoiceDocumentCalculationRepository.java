
package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.ChallanCumInvoiceDocumentCalculation;

@Repository
public interface ChallanCumInvoiceDocumentCalculationRepository
		extends JpaRepository<ChallanCumInvoiceDocumentCalculation, Long> {

}
