
package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.ReqTypeOfRequestAndInward;

@Repository
public interface ReqTypeOfRequestAndInwardRepository extends JpaRepository<ReqTypeOfRequestAndInward, Long> {

}
