package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.DocumentTypeForInvoice;


@Repository
public interface DocumentTypeForInvoiceRepository extends JpaRepository<DocumentTypeForInvoice, Long> {

}
