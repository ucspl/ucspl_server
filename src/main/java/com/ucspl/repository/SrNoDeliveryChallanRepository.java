package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.CreateDeliveryChallan;
import com.ucspl.model.SrNoDeliveryChallan;

@Repository
public interface SrNoDeliveryChallanRepository extends JpaRepository<SrNoDeliveryChallan, Long> {

}
