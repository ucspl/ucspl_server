package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.ucspl.model.CreateMasterInstrumentSpecificationDetails;

@Repository
public interface CreateMasterInstrumentSpecificationDetailsRepository extends JpaRepository<CreateMasterInstrumentSpecificationDetails, Long> {

}
