package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.CreateNablScopeMaster;
import com.ucspl.model.TypeOfQuotationForSalesAndQuotation;


@Repository
public interface CreateNablScopeMasterRepository extends JpaRepository<CreateNablScopeMaster, Long>{

}
