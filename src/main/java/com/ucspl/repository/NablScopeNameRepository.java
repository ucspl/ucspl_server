package com.ucspl.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ucspl.model.Branch;
import com.ucspl.model.NablScopeName;

@Repository
public interface NablScopeNameRepository extends JpaRepository<NablScopeName, Long> {

}
