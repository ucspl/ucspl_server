package com.javainuse;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import com.ucspl.UcsplProjectApplication;

@SpringBootTest(classes=UcsplProjectApplication.class)
class UcsplProjectApplicationTests {

	@Test
	void contextLoads() {
	}

}
